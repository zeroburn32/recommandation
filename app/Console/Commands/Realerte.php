<?php

namespace App\Console\Commands;

use App\Http\Controllers\MethodesStaticController;
use App\Models\Acteur;
use App\Models\Alertes;
use App\Models\EnvoiAlertes;
use App\Models\Recommandations;
use App\Models\Session;
use App\Models\Templatemsgs;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Realerte extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Realert:send {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Cette commande permet de renvoyer les alertes";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $id = $this->argument('id');

        $alertes = EnvoiAlertes::where('envoialertes.ale_id',$id)
            ->where('envoialertes.ena_isenvoi',0)->get();

        foreach ($alertes as $alerte) {
            $messageAlert = new EnvoiAlertes();
            $messageAlert->ena_msg = $alerte->ena_msg;
            $messageAlert->ena_ses_description = $alerte->ena_ses_description;
            $messageAlert->ena_ses_annee = $alerte->ena_ses_annee;
            $messageAlert->ena_ses_debut = $alerte->ena_ses_debut;
            $messageAlert->ena_ses_fin = $alerte->ena_ses_fin;
            $messageAlert->ena_ses_lieu = $alerte->ena_ses_lieu;
            $messageAlert->user_nom = $alerte->user_nom;
            $messageAlert->user_prenom = $alerte->user_prenom;
            $messageAlert->user_email = $alerte->user_email;
            $messageAlert->ale_mail_subject = $alerte->ale_mail_subject;
            MethodesStaticController::sendEmail($messageAlert, $alerte->ena_id);
        }

    }
}