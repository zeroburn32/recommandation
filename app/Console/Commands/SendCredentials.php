<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendCredentials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */ 
    protected $signature = 'credentials:send {user} {motDePasse} {view} {objet}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoi des informations de connexion lors de la création ou de la réinitialisation du compte';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $userId = $this->argument('user');
        $newPassword = $this->argument('motDePasse');
        $view = $this->argument('view');
        $user = User::find($userId);
        $contenu = "Votre nouveau mot de passe est : " . $newPassword;
        $objet = $this->argument('objet');
        $email = $user->email;
        $data = ['nom' => $user->nom, 'prenom' => $user->prenom, "email" => $user->email, "contenu" => $contenu];
        try {
            Mail::send($view, $data, function ($message) use ($objet, $email) {
                $message->to($email)->subject($objet);
            });
            $this->info('Mail envoyé avec succès à '.$user->email);
        } catch (\Swift_TransportException $e) {
            $this->error("Erreur lors de l'envoi du mail " . $user->email);
        }
    }
}
