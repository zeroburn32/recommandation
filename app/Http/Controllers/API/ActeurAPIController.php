', 'Sólan', 'Stardal', 'Stein', 'Steinbekk', 'Steinberg', 'Storm', 'Straumberg', 'Svanhild', 'Svarfdal', 'Sædal', 'Val', 'Valagils', 'Vald', 'Varmdal', 'Vatnsfjörð', 'Vattar', 'Vattnes', 'Viðfjörð', 'Vídalín', 'Víking', 'Vopnfjörð', 'Yngling', 'Þor', 'Önfjörð', 'Örbekk', 'Öxdal', 'Öxndal'
    );

    /**
     * Randomly return a icelandic middle name.
     *
     * @return string
     */
    public static function middleName()
    {
        return static::randomElement(static::$middleName);
    }

    /**
     * Generate prepared last name for further processing
     *
     * @return string
     */
    public function lastName()
    {
        $name = static::firstNameMale();

        if (substr($name, -2) === 'ur') {
            $name = substr($name, 0, strlen($name) - 2);
        }

        if (substr($name, -1) !== 's') {
            $name .= 's';
        }

        return $name;
    }

    /**
     * Randomly return a icelandic last name for woman.
     *
     * @return string
     */
    public function lastNameMale()
    {
        return $this->lastName().'son';
    }

    /**
     * Randomly return a icelandic last name for man.
     *
     * @return string
     */
    public function lastNameFemale()
    {
        return $this->lastName().'dóttir';
    }

    /**
     * Randomly return a icelandic Kennitala (Social Security number) format.
     *
     * @link http://en.wikipedia.org/wiki/Kennitala
     *
     * @return string
     */
    public static function ssn()
    {
        // random birth date
        $birthdate = new \DateTime('@' . mt_rand(0, time()));

        // last four buffer
        $lastFour = null;

        // security variable reference
        $ref = '32765432';

        // valid flag
        $valid = false;

        while (! $valid) {
            // make two random numbers
            $rand = static::randomDigit().static::randomDigit();

            // 8 char string with birth date and two random numbers
            $tmp = $birthdate->format('dmy').$rand;

            // loop through temp string
            for ($i = 7, $sum = 0; $i >= 0; $i--) {
                // calculate security variable
                $sum += ($tmp[$i] * $ref[$i]);
            }

            // subtract 11 if not 11
            $chk = ($sum % 11 === 0) ? 0 : (11 - ($sum % 11));

            if ($chk < 10) {
                $lastFour = $rand.$chk.substr($birthdate->format('Y'), 1, 1);

                $valid = true;
            }
        }

        return sprintf('%s-%s', $birthdate->format('dmy'), $lastFour);
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       