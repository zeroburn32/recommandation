<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateActivitesAPIRequest;
use App\Http\Requests\API\UpdateActivitesAPIRequest;
use App\Models\Activites;
use App\Repositories\ActivitesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ActivitesController
 * @package App\Http\Controllers\API
 */

class ActivitesAPIController extends AppBaseController
{
    /** @var  ActivitesRepository */
    private $activitesRepository;

    public function __construct(ActivitesRepository $activitesRepo)
    {
        $this->activitesRepository = $activitesRepo;
    }

    /**
     * Display a listing of the Activites.
     * GET|HEAD /activites
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $activites = $this->activitesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($activites->toArray(), 'Activites retrieved successfully');
    }

    /**
     * Store a newly created Activites in storage.
     * POST /activites
     *
     * @param CreateActivitesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateActivitesAPIRequest $request)
    {
        $input = $request->all();

        $activites = $this->activitesRepository->create($input);

        return $this->sendResponse($activites->toArray(), 'Activites saved successfully');
    }

    /**
     * Display the specified Activites.
     * GET|HEAD /activites/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Activites $activites */
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            return $this->sendError('Activites not found');
        }

        return $this->sendResponse($activites->toArray(), 'Activites retrieved successfully');
    }

    /**
     * Update the specified Activites in storage.
     * PUT/PATCH /activites/{id}
     *
     * @param int $id
     * @param UpdateActivitesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivitesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Activites $activites */
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            return $this->sendError('Activites not found');
        }

        $activites = $this->activitesRepository->update($input, $id);

        return $this->sendResponse($activites->toArray(), 'Activites updated successfully');
    }

    /**
     * Remove the specified Activites from storage.
     * DELETE /activites/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Activites $activites */
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            return $this->sendError('Activites not found');
        }

        $activites->delete();

        return $this->sendSuccess('Activites deleted successfully');
    }
}
