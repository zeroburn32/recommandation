<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAlertesAPIRequest;
use App\Http\Requests\API\UpdateAlertesAPIRequest;
use App\Models\Alertes;
use App\Repositories\AlertesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AlertesController
 * @package App\Http\Controllers\API
 */

class AlertesAPIController extends AppBaseController
{
    /** @var  AlertesRepository */
    private $alertesRepository;

    public function __construct(AlertesRepository $alertesRepo)
    {
        $this->alertesRepository = $alertesRepo;
    }

    /**
     * Display a listing of the Alertes.
     * GET|HEAD /alertes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $alertes = $this->alertesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($alertes->toArray(), 'Alertes retrieved successfully');
    }

    /**
     * Store a newly created Alertes in storage.
     * POST /alertes
     *
     * @param CreateAlertesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAlertesAPIRequest $request)
    {
        $input = $request->all();

        $alertes = $this->alertesRepository->create($input);

        return $this->sendResponse($alertes->toArray(), 'Alertes saved successfully');
    }

    /**
     * Display the specified Alertes.
     * GET|HEAD /alertes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Alertes $alertes */
        $alertes = $this->alertesRepository->find($id);

        if (empty($alertes)) {
            return $this->sendError('Alertes not found');
        }

        return $this->sendResponse($alertes->toArray(), 'Alertes retrieved successfully');
    }

    /**
     * Update the specified Alertes in storage.
     * PUT/PATCH /alertes/{id}
     *
     * @param int $id
     * @param UpdateAlertesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlertesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Alertes $alertes */
        $alertes = $this->alertesRepository->find($id);

        if (empty($alertes)) {
            return $this->sendError('Alertes not found');
        }

        $alertes = $this->alertesRepository->update($input, $id);

        return $this->sendResponse($alertes->toArray(), 'Alertes updated successfully');
    }

    /**
     * Remove the specified Alertes from storage.
     * DELETE /alertes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Alertes $alertes */
        $alertes = $this->alertesRepository->find($id);

        if (empty($alertes)) {
            return $this->sendError('Alertes not found');
        }

        $alertes->delete();

        return $this->sendSuccess('Alertes deleted successfully');
    }
}
