<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEnvoiAlertesAPIRequest;
use App\Http\Requests\API\UpdateEnvoiAlertesAPIRequest;
use App\Models\EnvoiAlertes;
use App\Repositories\EnvoiAlertesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EnvoiAlertesController
 * @package App\Http\Controllers\API
 */

class EnvoiAlertesAPIController extends AppBaseController
{
    /** @var  EnvoiAlertesRepository */
    private $envoiAlertesRepository;

    public function __construct(EnvoiAlertesRepository $envoiAlertesRepo)
    {
        $this->envoiAlertesRepository = $envoiAlertesRepo;
    }

    /**
     * Display a listing of the EnvoiAlertes.
     * GET|HEAD /envoiAlertes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $envoiAlertes = $this->envoiAlertesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($envoiAlertes->toArray(), 'Envoi Alertes retrieved successfully');
    }

    /**
     * Store a newly created EnvoiAlertes in storage.
     * POST /envoiAlertes
     *
     * @param CreateEnvoiAlertesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEnvoiAlertesAPIRequest $request)
    {
        $input = $request->all();

        $envoiAlertes = $this->envoiAlertesRepository->create($input);

        return $this->sendResponse($envoiAlertes->toArray(), 'Envoi Alertes saved successfully');
    }

    /**
     * Display the specified EnvoiAlertes.
     * GET|HEAD /envoiAlertes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EnvoiAlertes $envoiAlertes */
        $envoiAlertes = $this->envoiAlertesRepository->find($id);

        if (empty($envoiAlertes)) {
            return $this->sendError('Envoi Alertes not found');
        }

        return $this->sendResponse($envoiAlertes->toArray(), 'Envoi Alertes retrieved successfully');
    }

    /**
     * Update the specified EnvoiAlertes in storage.
     * PUT/PATCH /envoiAlertes/{id}
     *
     * @param int $id
     * @param UpdateEnvoiAlertesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnvoiAlertesAPIRequest $request)
    {
        $input = $request->all();

        /** @var EnvoiAlertes $envoiAlertes */
        $envoiAlertes = $this->envoiAlertesRepository->find($id);

        if (empty($envoiAlertes)) {
            return $this->sendError('Envoi Alertes not found');
        }

        $envoiAlertes = $this->envoiAlertesRepository->update($input, $id);

        return $this->sendResponse($envoiAlertes->toArray(), 'EnvoiAlertes updated successfully');
    }

    /**
     * Remove the specified EnvoiAlertes from storage.
     * DELETE /envoiAlertes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EnvoiAlertes $envoiAlertes */
        $envoiAlertes = $this->envoiAlertesRepository->find($id);

        if (empty($envoiAlertes)) {
            return $this->sendError('Envoi Alertes not found');
        }

        $envoiAlertes->delete();

        return $this->sendSuccess('Envoi Alertes deleted successfully');
    }
}
