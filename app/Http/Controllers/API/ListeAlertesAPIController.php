<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateListeAlertesAPIRequest;
use App\Http\Requests\API\UpdateListeAlertesAPIRequest;
use App\Models\ListeAlertes;
use App\Repositories\ListeAlertesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ListeAlertesController
 * @package App\Http\Controllers\API
 */

class ListeAlertesAPIController extends AppBaseController
{
    /** @var  ListeAlertesRepository */
    private $listeAlertesRepository;

    public function __construct(ListeAlertesRepository $listeAlertesRepo)
    {
        $this->listeAlertesRepository = $listeAlertesRepo;
    }

    /**
     * Display a listing of the ListeAlertes.
     * GET|HEAD /listeAlertes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $listeAlertes = $this->listeAlertesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($listeAlertes->toArray(), 'Liste Alertes retrieved successfully');
    }

    /**
     * Store a newly created ListeAlertes in storage.
     * POST /listeAlertes
     *
     * @param CreateListeAlertesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateListeAlertesAPIRequest $request)
    {
        $input = $request->all();

        $listeAlertes = $this->listeAlertesRepository->create($input);

        return $this->sendResponse($listeAlertes->toArray(), 'Liste Alertes saved successfully');
    }

    /**
     * Display the specified ListeAlertes.
     * GET|HEAD /listeAlertes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ListeAlertes $listeAlertes */
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            return $this->sendError('Liste Alertes not found');
        }

        return $this->sendResponse($listeAlertes->toArray(), 'Liste Alertes retrieved successfully');
    }

    /**
     * Update the specified ListeAlertes in storage.
     * PUT/PATCH /listeAlertes/{id}
     *
     * @param int $id
     * @param UpdateListeAlertesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateListeAlertesAPIRequest $request)
    {
        $input = $request->all();

        /** @var ListeAlertes $listeAlertes */
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            return $this->sendError('Liste Alertes not found');
        }

        $listeAlertes = $this->listeAlertesRepository->update($input, $id);

        return $this->sendResponse($listeAlertes->toArray(), 'ListeAlertes updated successfully');
    }

    /**
     * Remove the specified ListeAlertes from storage.
     * DELETE /listeAlertes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ListeAlertes $listeAlertes */
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            return $this->sendError('Liste Alertes not found');
        }

        $listeAlertes->delete();

        return $this->sendSuccess('Liste Alertes deleted successfully');
    }
}
