<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartenairesAPIRequest;
use App\Http\Requests\API\UpdatePartenairesAPIRequest;
use App\Models\Partenaires;
use App\Repositories\PartenairesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PartenairesController
 * @package App\Http\Controllers\API
 */

class PartenairesAPIController extends AppBaseController
{
    /** @var  PartenairesRepository */
    private $partenairesRepository;

    public function __construct(PartenairesRepository $partenairesRepo)
    {
        $this->partenairesRepository = $partenairesRepo;
    }

    /**
     * Display a listing of the Partenaires.
     * GET|HEAD /partenaires
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $partenaires = $this->partenairesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($partenaires->toArray(), 'Partenaires retrieved successfully');
    }

    /**
     * Store a newly created Partenaires in storage.
     * POST /partenaires
     *
     * @param CreatePartenairesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartenairesAPIRequest $request)
    {
        $input = $request->all();

        $partenaires = $this->partenairesRepository->create($input);

        return $this->sendResponse($partenaires->toArray(), 'Partenaires saved successfully');
    }

    /**
     * Display the specified Partenaires.
     * GET|HEAD /partenaires/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Partenaires $partenaires */
        $partenaires = $this->partenairesRepository->find($id);

        if (empty($partenaires)) {
            return $this->sendError('Partenaires not found');
        }

        return $this->sendResponse($partenaires->toArray(), 'Partenaires retrieved successfully');
    }

    /**
     * Update the specified Partenaires in storage.
     * PUT/PATCH /partenaires/{id}
     *
     * @param int $id
     * @param UpdatePartenairesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartenairesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Partenaires $partenaires */
        $partenaires = $this->partenairesRepository->find($id);

        if (empty($partenaires)) {
            return $this->sendError('Partenaires not found');
        }

        $partenaires = $this->partenairesRepository->update($input, $id);

        return $this->sendResponse($partenaires->toArray(), 'Partenaires updated successfully');
    }

    /**
     * Remove the specified Partenaires from storage.
     * DELETE /partenaires/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Partenaires $partenaires */
        $partenaires = $this->partenairesRepository->find($id);

        if (empty($partenaires)) {
            return $this->sendError('Partenaires not found');
        }

        $partenaires->delete();

        return $this->sendSuccess('Partenaires deleted successfully');
    }
}
