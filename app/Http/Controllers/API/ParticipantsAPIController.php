<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateParticipantsAPIRequest;
use App\Http\Requests\API\UpdateParticipantsAPIRequest;
use App\Models\Participants;
use App\Repositories\ParticipantsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ParticipantsController
 * @package App\Http\Controllers\API
 */

class ParticipantsAPIController extends AppBaseController
{
    /** @var  ParticipantsRepository */
    private $participantsRepository;

    public function __construct(ParticipantsRepository $participantsRepo)
    {
        $this->participantsRepository = $participantsRepo;
    }

    /**
     * Display a listing of the Participants.
     * GET|HEAD /participants
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $participants = $this->participantsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($participants->toArray(), 'Participants retrieved successfully');
    }

    /**
     * Store a newly created Participants in storage.
     * POST /participants
     *
     * @param CreateParticipantsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateParticipantsAPIRequest $request)
    {
        $input = $request->all();

        $participants = $this->participantsRepository->create($input);

        return $this->sendResponse($participants->toArray(), 'Participants saved successfully');
    }

    /**
     * Display the specified Participants.
     * GET|HEAD /participants/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Participants $participants */
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            return $this->sendError('Participants not found');
        }

        return $this->sendResponse($participants->toArray(), 'Participants retrieved successfully');
    }

    /**
     * Update the specified Participants in storage.
     * PUT/PATCH /participants/{id}
     *
     * @param int $id
     * @param UpdateParticipantsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParticipantsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Participants $participants */
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            return $this->sendError('Participants not found');
        }

        $participants = $this->participantsRepository->update($input, $id);

        return $this->sendResponse($participants->toArray(), 'Participants updated successfully');
    }

    /**
     * Remove the specified Participants from storage.
     * DELETE /participants/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Participants $participants */
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            return $this->sendError('Participants not found');
        }

        $participants->delete();

        return $this->sendSuccess('Participants deleted successfully');
    }
}
