<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProgrammeBudgetairesAPIRequest;
use App\Http\Requests\API\UpdateProgrammeBudgetairesAPIRequest;
use App\Models\ProgrammeBudgetaires;
use App\Repositories\ProgrammeBudgetairesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProgrammeBudgetairesController
 * @package App\Http\Controllers\API
 */

class ProgrammeBudgetairesAPIController extends AppBaseController
{
    /** @var  ProgrammeBudgetairesRepository */
    private $programmeBudgetairesRepository;

    public function __construct(ProgrammeBudgetairesRepository $programmeBudgetairesRepo)
    {
        $this->programmeBudgetairesRepository = $programmeBudgetairesRepo;
    }

    /**
     * Display a listing of the ProgrammeBudgetaires.
     * GET|HEAD /programmeBudgetaires
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $programmeBudgetaires = $this->programmeBudgetairesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($programmeBudgetaires->toArray(), 'Programme Budgetaires retrieved successfully');
    }

    /**
     * Store a newly created ProgrammeBudgetaires in storage.
     * POST /programmeBudgetaires
     *
     * @param CreateProgrammeBudgetairesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProgrammeBudgetairesAPIRequest $request)
    {
        $input = $request->all();

        $programmeBudgetaires = $this->programmeBudgetairesRepository->create($input);

        return $this->sendResponse($programmeBudgetaires->toArray(), 'Programme Budgetaires saved successfully');
    }

    /**
     * Display the specified ProgrammeBudgetaires.
     * GET|HEAD /programmeBudgetaires/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProgrammeBudgetaires $programmeBudgetaires */
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);

        if (empty($programmeBudgetaires)) {
            return $this->sendError('Programme Budgetaires not found');
        }

        return $this->sendResponse($programmeBudgetaires->toArray(), 'Programme Budgetaires retrieved successfully');
    }

    /**
     * Update the specified ProgrammeBudgetaires in storage.
     * PUT/PATCH /programmeBudgetaires/{id}
     *
     * @param int $id
     * @param UpdateProgrammeBudgetairesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProgrammeBudgetairesAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProgrammeBudgetaires $programmeBudgetaires */
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);

        if (empty($programmeBudgetaires)) {
            return $this->sendError('Programme Budgetaires not found');
        }

        $programmeBudgetaires = $this->programmeBudgetairesRepository->update($input, $id);

        return $this->sendResponse($programmeBudgetaires->toArray(), 'ProgrammeBudgetaires updated successfully');
    }

    /**
     * Remove the specified ProgrammeBudgetaires from storage.
     * DELETE /programmeBudgetaires/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProgrammeBudgetaires $programmeBudgetaires */
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);

        if (empty($programmeBudgetaires)) {
            return $this->sendError('Programme Budgetaires not found');
        }

        $programmeBudgetaires->delete();

        return $this->sendSuccess('Programme Budgetaires deleted successfully');
    }
}
