<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRecommandationsAPIRequest;
use App\Http\Requests\API\UpdateRecommandationsAPIRequest;
use App\Models\Recommandations;
use App\Repositories\RecommandationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RecommandationsController
 * @package App\Http\Controllers\API
 */

class RecommandationsAPIController extends AppBaseController
{
    /** @var  RecommandationsRepository */
    private $recommandationsRepository;

    public function __construct(RecommandationsRepository $recommandationsRepo)
    {
        $this->recommandationsRepository = $recommandationsRepo;
    }

    /**
     * Display a listing of the Recommandations.
     * GET|HEAD /recommandations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $recommandations = $this->recommandationsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($recommandations->toArray(), 'Recommandations retrieved successfully');
    }

    /**
     * Store a newly created Recommandations in storage.
     * POST /recommandations
     *
     * @param CreateRecommandationsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRecommandationsAPIRequest $request)
    {
        $input = $request->all();

        $recommandations = $this->recommandationsRepository->create($input);

        return $this->sendResponse($recommandations->toArray(), 'Recommandations saved successfully');
    }

    /**
     * Display the specified Recommandations.
     * GET|HEAD /recommandations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Recommandations $recommandations */
        $recommandations = $this->recommandationsRepository->find($id);

        if (empty($recommandations)) {
            return $this->sendError('Recommandations not found');
        }

        return $this->sendResponse($recommandations->toArray(), 'Recommandations retrieved successfully');
    }

    /**
     * Update the specified Recommandations in storage.
     * PUT/PATCH /recommandations/{id}
     *
     * @param int $id
     * @param UpdateRecommandationsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecommandationsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Recommandations $recommandations */
        $recommandations = $this->recommandationsRepository->find($id);

        if (empty($recommandations)) {
            return $this->sendError('Recommandations not found');
        }

        $recommandations = $this->recommandationsRepository->update($input, $id);

        return $this->sendResponse($recommandations->toArray(), 'Recommandations updated successfully');
    }

    /**
     * Remove the specified Recommandations from storage.
     * DELETE /recommandations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Recommandations $recommandations */
        $recommandations = $this->recommandationsRepository->find($id);

        if (empty($recommandations)) {
            return $this->sendError('Recommandations not found');
        }

        $recommandations->delete();

        return $this->sendSuccess('Recommandations deleted successfully');
    }
}
