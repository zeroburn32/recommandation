<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTemplatemsgsAPIRequest;
use App\Http\Requests\API\UpdateTemplatemsgsAPIRequest;
use App\Models\Templatemsgs;
use App\Repositories\TemplatemsgsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TemplatemsgsController
 * @package App\Http\Controllers\API
 */

class TemplatemsgsAPIController extends AppBaseController
{
    /** @var  TemplatemsgsRepository */
    private $templatemsgsRepository;

    public function __construct(TemplatemsgsRepository $templatemsgsRepo)
    {
        $this->templatemsgsRepository = $templatemsgsRepo;
    }

    /**
     * Display a listing of the Templatemsgs.
     * GET|HEAD /templatemsgs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $templatemsgs = $this->templatemsgsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($templatemsgs->toArray(), 'Templatemsgs retrieved successfully');
    }

    /**
     * Store a newly created Templatemsgs in storage.
     * POST /templatemsgs
     *
     * @param CreateTemplatemsgsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTemplatemsgsAPIRequest $request)
    {
        $input = $request->all();

        $templatemsgs = $this->templatemsgsRepository->create($input);

        return $this->sendResponse($templatemsgs->toArray(), 'Templatemsgs saved successfully');
    }

    /**
     * Display the specified Templatemsgs.
     * GET|HEAD /templatemsgs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Templatemsgs $templatemsgs */
        $templatemsgs = $this->templatemsgsRepository->find($id);

        if (empty($templatemsgs)) {
            return $this->sendError('Templatemsgs not found');
        }

        return $this->sendResponse($templatemsgs->toArray(), 'Templatemsgs retrieved successfully');
    }

    /**
     * Update the specified Templatemsgs in storage.
     * PUT/PATCH /templatemsgs/{id}
     *
     * @param int $id
     * @param UpdateTemplatemsgsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTemplatemsgsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Templatemsgs $templatemsgs */
        $templatemsgs = $this->templatemsgsRepository->find($id);

        if (empty($templatemsgs)) {
            return $this->sendError('Templatemsgs not found');
        }

        $templatemsgs = $this->templatemsgsRepository->update($input, $id);

        return $this->sendResponse($templatemsgs->toArray(), 'Templatemsgs updated successfully');
    }

    /**
     * Remove the specified Templatemsgs from storage.
     * DELETE /templatemsgs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Templatemsgs $templatemsgs */
        $templatemsgs = $this->templatemsgsRepository->find($id);

        if (empty($templatemsgs)) {
            return $this->sendError('Templatemsgs not found');
        }

        $templatemsgs->delete();

        return $this->sendSuccess('Templatemsgs deleted successfully');
    }
}
