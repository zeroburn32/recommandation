<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateActeurRequest;
use App\Http\Requests\UpdateActeurRequest;
use App\Models\Acteur;
use App\Repositories\ActeurRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;
use Notify;

class ActeurController extends AppBaseController
{
    /** @var  ActeurRepository */
    private $acteurRepository;

    public function __construct(ActeurRepository $acteurRepo)
    {
        $this->acteurRepository = $acteurRepo;

        /** Middleware */
        $this->middleware(['permission:GERER_ACTEUR_INSTANCE'])->only([
            'getPageActeursInstance', 'getActeur', 'getAllActeurs', 'saveActeur', 'deleteActeur'
        ]);
        $this->middleware(['permission:GERER_ROLE_ACTEUR'])->only([
            'setCss', 'setCsr'
        ]);
    }

    /**
     * Display a listing of the Acteur.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $acteurs = $this->acteurRepository->all();

        return view('backend.acteurs.index')
            ->with('acteurs', $acteurs);
    }

    /**
     * Show the form for creating a new Acteur.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.acteurs.create');
    }

    /**
     * Store a newly created Acteur in storage.
     *
     * @param CreateActeurRequest $request
     *
     * @return Response
     */
    public function store(CreateActeurRequest $request)
    {
        $input = $request->all();

        $acteur = $this->acteurRepository->create($input);

        Flash::success('Acteur saved successfully.');

        return redirect(route('acteurs.index'));
    }

    /**
     * Display the specified Acteur.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $acteur = $this->acteurRepository->find($id);

        if (empty($acteur)) {
            Flash::error('Acteur not found');

            return redirect(route('acteurs.index'));
        }

        return view('backend.acteurs.show')->with('acteur', $acteur);
    }

    /**
     * Show the form for editing the specified Acteur.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $acteur = $this->acteurRepository->find($id);

        if (empty($acteur)) {
            Flash::error('Acteur not found');

            return redirect(route('acteurs.index'));
        }

        return view('backend.acteurs.edit')->with('acteur', $acteur);
    }

    /**
     * Update the specified Acteur in storage.
     *
     * @param int $id
     * @param UpdateActeurRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActeurRequest $request)
    {
        $acteur = $this->acteurRepository->find($id);

        if (empty($acteur)) {
            Flash::error('Acteur not found');

            return redirect(route('acteurs.index'));
        }

        $acteur = $this->acteurRepository->update($request->all(), $id);

        Flash::success('Acteur updated successfully.');

        return redirect(route('acteurs.index'));
    }

    /**
     * Remove the specified Acteur from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $acteur = $this->acteurRepository->find($id);

        if (empty($acteur)) {
            Flash::error('Acteur not found');

            return redirect(route('acteurs.index'));
        }

        $this->acteurRepository->delete($id);

        Flash::success('Acteur deleted successfully.');

        return redirect(route('acteurs.index'));
    }

    /** ******************************************PERSONNALISER*********************************************** */
    public function getPageActeursInstance($id_instance)
    {
        $instance = DB::table('instances')->where('ins_id', $id_instance)->first();

        $structures = DB::table('structures')
            ->whereNotIn('str_id',
                DB::table('structures')
                    ->join('acteurs', 'structures.str_id', '=', 'acteurs.str_id')
                    ->where('acteurs.ins_id', $id_instance)
                    ->pluck('structures.str_id')
            )
            ->orderBy('str_sigle', 'asc')->get();
        $acteurs = DB::table('acteurs as act')
            ->join('instances as ins', 'act.ins_id', '=', 'ins.ins_id')
            ->join('structures as str', 'act.str_id', '=', 'str.str_id')
            ->where('ins.ins_id', '=', $id_instance)
            ->orderBy('str.str_sigle', 'asc')
            ->get();
        return view('backend.acteurs.acteurs_instance', compact('instance', 'structures', 'acteurs'));
    }

    public function getActeur($id)
    {
        $acteur = DB::table('acteurs')->where('act_id', $id)->first();
        if (empty($acteur)) {
            return null;
        }
        return response()->json($acteur);
    }

    public function getAllActeurs()
    {
        $acteurs = DB::table('acteurs')->get();
        if (empty($acteurs)) {
            return null;
        }
        return response()->json($acteurs);
    }

    public function saveActeur()
    {
        request('str_id') ? $n = sizeof(request('str_id')) : $n = 0;
        $val_csr = null;
        if ($n > 0) {
            $user = auth()->user();
            for ($i = 0; $i < $n; $i++) {
                $cat_id = DB::table('structures as str')
                    ->join('categories as cat', 'str.str_categorie', '=', 'cat.cat_id')
                    ->where([['str.str_id', '=', request('str_id')[$i]], ['cat.cat_is_ministere', '=', 1]])
                    ->first();
                empty($cat_id) ? $val_csr = 0 : $val_csr = 1;
                $acteur = new Acteur();
                $acteur->ins_id = request('ins_id');
                $acteur->str_id = request('str_id')[$i];
                $acteur->act_is_css = 0;
                $acteur->act_is_csr = $val_csr;
                $acteur->act_is_asr = 1;
                $acteur->created_by = $user->email;
                $acteur->updated_by = $user->email;
                $acteur->save();
            }
        } else {
            Notify::error("Choisissez au moins une structure", "Opération échouée", $options = []);
            return redirect('getPageActeursInstance/' . request('ins_id'));
        }
        Notify::success("Acteur ajouté avec succès", "Opération reussie", $options = []);
        return redirect('getPageActeursInstance/' . request('ins_id'));
    }

    public function deleteActeur($id)
    {
        $acteur = DB::table('acteurs')->where('act_id', $id)->first();
        if (empty($acteur)) {
            Notify::error("Acteur non trouvé", "Erreur", $options = []);
        }
        try {
            $result = DB::table('acteurs')->where('act_id', '=', intval($acteur->act_id))->delete();
            if (!empty($result)) {
                activity('Acteur')->log('Suppression');
                Notify::success("Acteur supprimé avec succès", "Opération reussie", $options = []);
                return 1;
            }
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    public function setCss($id)
    {
        $acteur = DB::table('acteurs')->where('act_id', $id)->first();
        if (empty($acteur)) {
            Notify::error("Acteur non trouvé", "Erreur", $options = []);
        }
        try {
            $user = auth()->user();
            $acteur->act_is_css == 0 ? $value = 1 : $value = 0;
            $result = DB::table('acteurs')
                ->where('act_id', $acteur->act_id)
                ->update([
                    'act_is_css' => $value,
                    'act_is_csr' => 1,
                    'act_is_asr' => 1,
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            if (!empty($result)) {
                activity('Acteur')->log('Changement rôle CSS');
                Notify::success("Rôle chargé de suivi de sessions (C.S.S.) mise à jour avec succès", "Opération reussie", $options = []);
                return 0;
            }
        } catch (\PDOException $ex) {
            return 1;
        }
    }

    public function setCsr($id)
    {
        $acteur = DB::table('acteurs')->where('act_id', $id)->first();
        if (empty($acteur)) {
            Notify::error("Acteur non trouvé", "Erreur", $options = []);
        }
        try {
            $user = auth()->user();
            $acteur->act_is_csr == 0 ? $value = 1 : $value = 0;
            $result = DB::table('acteurs')
                ->where('act_id', $acteur->act_id)
                ->update([
                    'act_is_css' => 0,
                    'act_is_csr' => $value,
                    'act_is_asr' => 1,
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            if (!empty($result)) {
                activity('Acteur')->log('Changement rôle CSR');
                Notify::success("Rôle chargé de suivi des recommandations (C.S.R.) mise à jour avec succès", "Opération reussie", $options = []);
                return 0;
            }
        } catch (\PDOException $ex) {
            return 1;
        }
    }
}
