<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateActivitesRequest;
use App\Http\Requests\UpdateActivitesRequest;
use App\Models\Activites;
use App\Repositories\ActivitesRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;
use Notify;

class ActivitesController extends AppBaseController
{
    /** @var  ActivitesRepository */
    private $activitesRepository;

    public function __construct(ActivitesRepository $activitesRepo)
    {
        $this->activitesRepository = $activitesRepo;

        /** Middleware */
        $this->middleware(['permission:PLANIFIER_RECOMMANDATION'])->only([
            'index', 'getActivite', 'getAllActivites', 'saveActivite', 'deleteActivite'
        ]);
        $this->middleware(['permission:SUIVI_RECOMMANDATION'])->only([
            'doSuiviActivite'
        ]);
    }

    /**
     * Display a listing of the Activites.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $activites = $this->activitesRepository->all();

        return view('backend.activites.index')
            ->with('activites', $activites);
    }

    /**
     * Show the form for creating a new Activites.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.activites.create');
    }

    /**
     * Store a newly created Activites in storage.
     *
     * @param CreateActivitesRequest $request
     *
     * @return Response
     */
    public function store(CreateActivitesRequest $request)
    {
        $input = $request->all();

        $activites = $this->activitesRepository->create($input);

        Flash::success('Activites saved successfully.');

        return redirect(route('activites.index'));
    }

    /**
     * Display the specified Activites.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            Flash::error('Activites not found');

            return redirect(route('activites.index'));
        }

        return view('backend.activites.show')->with('activites', $activites);
    }

    /**
     * Show the form for editing the specified Activites.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            Flash::error('Activites not found');

            return redirect(route('activites.index'));
        }

        return view('backend.activites.edit')->with('activites', $activites);
    }

    /**
     * Update the specified Activites in storage.
     *
     * @param int $id
     * @param UpdateActivitesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivitesRequest $request)
    {
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            Flash::error('Activites not found');

            return redirect(route('activites.index'));
        }

        $activites = $this->activitesRepository->update($request->all(), $id);

        Flash::success('Activites updated successfully.');

        return redirect(route('activites.index'));
    }

    /**
     * Remove the specified Activites from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $activites = $this->activitesRepository->find($id);

        if (empty($activites)) {
            Flash::error('Activites not found');

            return redirect(route('activites.index'));
        }

        $this->activitesRepository->delete($id);

        Flash::success('Activites deleted successfully.');

        return redirect(route('activites.index'));
    }

    /** ******************************************PERSONNALISER*********************************************** */

    public function getActivite($id)
    {
        $activite = DB::table('activites')
            ->join('partenaires', 'activites.ptn_id', '=', 'partenaires.ptn_id')
            ->join('structures', 'partenaires.str_id', '=', 'structures.str_id')
            ->join('recommandations', 'activites.rec_id', '=', 'recommandations.rec_id')
            ->where('activites.act_id', $id)->first();
        if (empty($activite)) {
            return null;
        }
        return response()->json($activite);
    }

    public function getAllActivites()
    {
        $activites = DB::table('activites')->get();
        if (empty($activites)) {
            return null;
        }
        return response()->json($activites);
    }

    public function saveActivite()
    {
        $sommePoids = DB::table('activites as act')
            ->join('recommandations as rec', 'act.rec_id', '=', 'rec.rec_id')
            ->where('rec.rec_id', '=', request('rec_id'))
            ->select(DB::raw('SUM(act.act_poids) as sumPoids'))
            ->first();
        $activiteExistInDB = DB::table('activites')
            ->where([
                ['rec_id', '=', request('rec_id')],
                ['act_description', '=', request('act_description')]
            ])->first();
        $user = auth()->user();
        if (is_null(request('act_id'))) {
            if (!empty($activiteExistInDB)) {
                Notify::error("La description (activité) existe déjà pour cette recommandation, merci de réessayer", "Opération échouée", $options = []);
                return redirect('getPageActiviteReco/' . request('rec_id').'?plan=true');
            }
            $sommePoids = $sommePoids->sumPoids + request('act_poids');
            if ($sommePoids > 100) {
                Notify::error("Le cumul des poids des activités ne doit pas dépassé 100", "Opération échoué", $options = []);
                return redirect('getPageActiviteReco/' . request('rec_id').'?plan=true');
            }
            $activite = new Activites();
            $activite->rec_id = request('rec_id');
            $activite->ptn_id = request('ptn_id');
            $activite->act_description = request('act_description');
            $activite->act_is_realise = 0;
            $activite->act_is_valide = 0;
            $activite->act_poids = request('act_poids');
            $activite->act_date_prevue = request('act_date_prevue');
            $activite->created_by = $user->email;
            $activite->updated_by = $user->email;
            try {
                $activite->save();
                Notify::success("Activité ajoutée avec succès", "Opération reussie", $options = []);
                return redirect('getPageActiviteReco/' . request('rec_id').'?plan=true');
            } catch (\PDOException $ex) {
                /*if (intval($ex->getCode()) == 23000) {
                    Notify::error("La description de l'activité existe déjà, merci de réessayer", "Opération échouée", $options = []);
                    return redirect('getPageActiviteReco/' . request('rec_id'));
                }*/
            }
        }
        else {
            $activite = DB::table('activites')->where('act_id', request('act_id'))->first();
            $sommePoids = ($sommePoids->sumPoids - $activite->act_poids) + request('act_poids');
            if ($sommePoids > 100) {
                Notify::error("Le cumul des poids des activités ne doit pas dépassé 100", "Opération échoué", $options = []);
                return redirect('getPageActiviteReco/' . request('rec_id').'?plan=true');
            }
            if (!empty($activiteExistInDB) && $activiteExistInDB->act_description != $activite->act_description) {
                Notify::error("La description (activité) existe déjà pour cette recommandation, merci de réessayer", "Opération échouée", $options = []);
                return redirect('getPageActiviteReco/' . request('rec_id').'?plan=true');
            }
            try {
                $result = DB::table('activites')
                    ->where('act_id', request('act_id'))
                    ->update([
                        'rec_id' => request('rec_id'),
                        'ptn_id' => request('ptn_id'),
                        'act_description' => request('act_description'),
                        'act_poids' => request('act_poids'),
                        'act_date_prevue' => request('act_date_prevue'),
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($result)) {
                    activity('Activité')->log('Modification');
                    Notify::success("Activité modifiée avec succès", "Opération reussie", $options = []);
                    return redirect('getPageActiviteReco/' . request('rec_id') . '?plan=true');
                }
            } catch (\PDOException $ex) {
                /*if (intval($ex->getCode()) == 23000) {
                    Notify::error("La description de l'activité existe déjà, merci de réessayer", "Opération échouée", $options = []);
                    return redirect('getPageActiviteReco/' . request('rec_id'));
                }*/
            }
        }
    }

    public function deleteActivite($id)
    {
        $activite = DB::table('activites')->where('act_id', $id)->first();
        if (empty($activite)) {
            return 0;
        }
        try {
            $result = DB::table('activites')->where('act_id', '=', intval($activite->act_id))->delete();
            if (!empty($result)) {
                activity('Activité')->log('Suppression');
                return 1;
            }
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    public function doSuiviActivite($id, Request $request)
    {
        $activite = DB::table('activites')->where('act_id', $id)->first();
        $user = auth()->user();
        if (!empty($activite)) {
            $files = $request->file('act_preuve');
            if (!empty($files)) {
                $ext = $files->getClientOriginalExtension();
                // taille max autorisée 4 MB (converti en byte)
                if ($files->getSize() < intval(config('app.taille_max_upload'))) {
                    if (strtolower($ext) == 'pdf' || strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg'
                        || strtolower($ext) == 'doc' || strtolower($ext) == 'docx' || strtolower($ext) == 'xls' || strtolower($ext) == 'xlsx') {
                        $chemin = public_path() . "/pj_activites/";
                        $name = 'ACT_' . $activite->act_id . '_' . Carbon::now()->format('d_M_Y_H_i_s') . '.' . $ext;
                        $request->act_preuve->move($chemin, $name);
                        try {
                            $result = DB::table('activites')
                                ->where('act_id', $activite->act_id)
                                ->update([
                                    'act_is_realise' => request('act_is_realise'),
                                    'act_observation' => request('act_observation'),
                                    'act_preuve' => $name,
                                    'act_date_realise' => Carbon::now(),
                                    'updated_by' => $user->email
                                ]);
                        } catch (PostTooLargeException $ex) {
                            $result = '';
                            Notify::error("Le fichier ne doit pas dépassé " . intval(config('app.taille_max_upload')) / (1024*1024) . "Mo", "Opération échoué", $options = []);
                        } catch (QueryException $ex) {
                        }
                        if (!empty($result)) {
                            /** @var $sommePoids : la somme des ponderations des activites realise */
                            $sommePoids = DB::table('activites as act')
                                ->join('recommandations as rec', 'act.rec_id', '=', 'rec.rec_id')
                                ->where([
                                    ['rec.rec_id', '=', $activite->rec_id],
                                    ['act.act_is_realise', '=', 1]
                                ])
                                ->select(DB::raw('SUM(act.act_poids) as sumPoids'))
                                ->first();
                            if ($sommePoids->sumPoids > 0 && $sommePoids->sumPoids < 100) {
                                $statut = 3;
                                $champ_date = 'rec_dh_en_cours';
                                $value_date = Carbon::now();
                            } else if ($sommePoids->sumPoids == 100) {
                                $statut = 4;
                                $champ_date = 'rec_dh_realise';
                                $value_date = Carbon::now();
                            } else {
                                $statut = 2;
                                $champ_date = 'rec_dh_en_cours';
                                $value_date = null;
                            }
                            $resultat = DB::table('recommandations')
                                ->where('rec_id', $activite->rec_id)
                                ->update([
                                    'rec_statut' => $statut,
                                    'rec_taux_realise' => $sommePoids->sumPoids,
                                    $champ_date => $value_date,
                                    'updated_at' => Carbon::now(),
                                    'updated_by' => $user->email
                                ]);
                            if (!empty($resultat)) {
                                activity('Activité')->log('Suivi de mise en oeuvre');
                                Notify::success("Suivi de la mise en œuvre fait avec succès", "Opération reussie", $options = []);
                            }
                        }
                    }
                    else {
                        Notify::error("Extension du fichier non valide, extension souhaité : pdf, png ou jpg", "Opération échoué", $options = []);
                    }
                } else {
                    Notify::error("Le fichier ne doit pas dépassé " . intval(config('app.taille_max_upload')) / (1024*1024) . "Mo", "Opération échoué", $options = []);
                }
            } else {
                try {
                    $result = DB::table('activites')
                        ->where('act_id', $activite->act_id)
                        ->update([
                            'act_is_realise' => request('act_is_realise'),
                            'act_observation' => request('act_observation'),
                            'act_date_realise' => Carbon::now(),
                            'updated_by' => $user->email
                        ]);
                    if (!empty($result)) {
                        /** @var $sommePoids : la somme des ponderations des activites realise */
                        $sommePoids = DB::table('activites as act')
                            ->join('recommandations as rec', 'act.rec_id', '=', 'rec.rec_id')
                            ->where([
                                ['rec.rec_id', '=', $activite->rec_id],
                                ['act.act_is_realise', '=', 1]
                            ])
                            ->select(DB::raw('SUM(act.act_poids) as sumPoids'))
                            ->first();
                        if ($sommePoids->sumPoids > 0 && $sommePoids->sumPoids < 100) {
                            $statut = 3;
                        } else if ($sommePoids->sumPoids == 100) {
                            $statut = 4;
                        } else {
                            $statut = 2;
                        }
                        $resultat = DB::table('recommandations')
                            ->where('rec_id', $activite->rec_id)
                            ->update([
                                'rec_statut' => $statut,
                                'rec_taux_realise' => $sommePoids->sumPoids,
                                'updated_at' => Carbon::now(),
                                'updated_by' => $user->email
                            ]);
                        if (!empty($resultat)) {
                            activity('Activité')->log('Suivi de mise en oeuvre');
                            Notify::success("Suivi de la mise en œuvre fait avec succès", "Opération reussie", $options = []);
                        }
                    }
                } catch (QueryException $ex) {
                }
            }
        } else {
            Notify::error("Acitivité non trouvée", "Opération échoué", $options = []);
        }
        return redirect('getPageActiviteReco/' . $activite->rec_id . '?plan=false');
    }
}
