<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class ActivityLogController extends Controller
{
    //
    public function getActivitiesLog ($id) {
        $activities = Activity::where('causer_id',$id)
            ->select('log_name as objet','description as action','email','adresse_ip as ip','created_at')
            ->limit(100)->latest()->get();

        return view('backend.activity_log.index', compact('activities','id'));
    }

    public function activityLogSearch (Request $request) {
        $id = $request->id;
        $startDate = $request->startDate;
        $endDate = $request->endDate;

        if($startDate && $endDate) {
            $activities = Activity::where('causer_id', $request->id)
                ->select('log_name as objet', 'description as action', 'email', 'adresse_ip as ip', 'created_at' )
                ->whereBetween('created_at', [$startDate, $endDate])
                ->latest()->get();
        }elseif ($startDate && !$endDate) {
            $dateTimeObject = Carbon::parse('2017-07-20 10:16:34')->startOfDay();
            //dd($now->toDateString());
            $activities = Activity::where('causer_id', $request->id)
                ->select('log_name as objet', 'description as action', 'email', 'adresse_ip as ip', 'created_at')
                ->where('created_at','>=' , $startDate)
                ->latest()->get();
        } elseif (!$startDate && $endDate) {
            $dateTimeObject = Carbon::parse('2017-07-20 10:16:34')->startOfDay();
            //dd($now->toDateString());
            $activities = Activity::where('causer_id', $request->id)
                ->select('log_name as objet', 'description as action', 'email', 'adresse_ip as ip', 'created_at')
                ->where('created_at', '<=', $endDate)
                ->latest()->get();
        } else {
            $activities = Activity::where('causer_id', $id)
                ->select('log_name as objet', 'description as action', 'email', 'adresse_ip as ip', 'created_at')
                ->limit(100)->latest()->get();
        }
        return view('backend.activity_log.index', compact('activities','id'));
    }
}
