<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Helmesvs\Notify\Facades\Notify;
use Illuminate\Support\Facades\Auth;
use App\Repositories\AgentsRepository;
use App\Http\Requests\CreateAgentsRequest;
use App\Http\Requests\UpdateAgentsRequest;
use App\Http\Controllers\AppBaseController;

class AgentsController extends AppBaseController
{
    /** @var  AgentsRepository */
    private $agentsRepository;

    public function __construct(AgentsRepository $agentsRepo)
    {
        $this->agentsRepository = $agentsRepo;
    }

    /**
     * Display a listing of the Agents.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $agents = $this->agentsRepository->all();

        return view('backend.agents.index')
            ->with('agents', $agents);
    }

    public function getAgent($id)
    {
        $agents = DB::table('agents')->where('age_id', $id)->first();
        if (empty($agents)) {
            return null;
        }
        return response()->json($agents);
    }

    public function getAgentsByStructure($id_structure, Request $request)
    {
        if(Auth::user()->str_id == $id_structure || Auth::user()->hasRole('ADMIN|CSS') ) {
        $agents = DB::table('agents as ag')
            ->leftjoin('structures as str', 'str.str_id', '=', 'ag.structure')
            ->where([
                ['str.str_id', '=', $id_structure],
            ])
            ->select('ag.*', 'str.str_id','str.str_sigle', 'str.str_nom_complet')
            ->get();
        $structure = DB::table('structures')->where('str_id', '=', $id_structure)->first();
        $structures = DB::table('structures')->pluck('str_nom_complet', 'str_id');

        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');

        if ($request->ajax()) {
            return response()->json($agents);
        }
        return view('backend.agents.liste_agents_structure', compact('roles', 'structure', 'structures'))->with('agents', $agents);;
    } else {
            abort(403);
    }
    }

    public function deleteAgent($id)
    {
        $agent = DB::table('agents')->where('age_id', $id)->first();
        if (empty($agent)) {
            return redirect(route('structures.index'));
        }
        try {
            DB::table('agents')->where('age_id', '=', intval($agent->age_id))->delete();
            return 1;
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    /**
     * Show the form for creating a new Agents.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.agents.create');
    }

    /**
     * Store a newly created Agents in storage.
     *
     * @param CreateAgentsRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentsRequest $request, UpdateAgentsRequest $updateRequest)
    {
        $input = $request->all();
        if ($request->age_id) {
            try {
                $this->update($request->age_id, $updateRequest);
                Notify::success("Agent de suivi mis à jour  !", "Opération réussie", $options = []);
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    Notify::error("Cet agent existe déjà!", "Echec", $options = []);
                }else {
                    Notify::error("Echec de la mise à jour. Veuillez réessayer!", "Echec", $options = []);
                }
            }
            
        } else {
            try {
                $this->agentsRepository->create($input);
                Notify::success("Agent de suivi ajouté avec succès !", "Opération réussie", $options = []);
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    Notify::error("Cet agent existe déjà!", "Echec", $options = []);
                } else {
                    Notify::error("Echec lors de l\'ajout. Veuillez réessayer!", "Echec", $options = []);
                }
            }
            
        }
        return back();
    }

    /**
     * Display the specified Agents.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agents = $this->agentsRepository->find($id);

        if (empty($agents)) {
            Flash::error('Agents not found');

            return redirect(route('agents.index'));
        }

        return view('backend.agents.show')->with('agents', $agents);
    }

    /**
     * Show the form for editing the specified Agents.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agents = $this->agentsRepository->find($id);

        if (empty($agents)) {
            Flash::error('Agents not found');

            return redirect(route('agents.index'));
        }

        return view('backend.agents.edit')->with('agents', $agents);
    }

    /**
     * Update the specified Agents in storage.
     *
     * @param int $id
     * @param UpdateAgentsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentsRequest $request)
    {
        $agents = $this->agentsRepository->find($id);

        if (empty($agents)) {
            Flash::error('Agents not found');

            return redirect(route('agents.index'));
        }

        $agents = $this->agentsRepository->update($request->all(), $id);

        Flash::success('Agents updated successfully.');

        return redirect(route('agents.index'));
    }

    /**
     * Remove the specified Agents from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $agents = $this->agentsRepository->find($id);

        if (empty($agents)) {
            Flash::error('Agents not found');

            return redirect(route('agents.index'));
        }

        $this->agentsRepository->delete($id);

        Flash::success('Agents deleted successfully.');

        return redirect(route('agents.index'));
    }
}
