<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAlertesRequest;
use App\Http\Requests\UpdateAlertesRequest;
use App\Models\Alertes;
use App\Models\Envoialerte;
use App\Models\EnvoiAlertes;
use App\Models\Instance;
use App\Models\ListeAlertes;
use App\Models\Session;
use App\Models\Structures;
use App\Models\Templatemsgs;
use App\Repositories\AlertesRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Notify;
use Response;

class AlertesController extends AppBaseController
{

    private $alertesRepository;

    public function __construct(AlertesRepository $alertesRepo)
    {
        $this->alertesRepository = $alertesRepo;
    }

    public function index(Request $request)
    {
        $instance_id = null;
        $session_id = null;
        $alertes = Alertes::join('sessions', 'sessions.ses_id', '=', 'alertes.ses_id')
            ->whereIn('ses_statut', [2, 3])->get();

        return view('backend.alertes.index', compact('alertes', 'instance_id', 'session_id'));
    }

    public function create()
    {
        return view('backend.alertes.create');
    }

    public function store(CreateAlertesRequest $request)
    {
        $input = $request->all();
        $alertes = $this->alertesRepository->create($input);
        Flash::success('Alertes saved successfully.');
        return redirect(route('alertes.index'));
    }

    //----routes personnelles---
    public function getSession($id)
    {
        $session = Session::join('instances', 'sessions.ins_id', '=', 'instances.ins_id')->where('sessions.ins_id', $id)->whereIn('ses_statut', [2, 3])->get();
        return response()->json($session);
    }

    public function getModele()
    {
        $modele = Templatemsgs::all();
        return response()->json($modele);
    }

    public function save()
    {
        if (request('ale_date_heure_prev') == null || request('ale_tmsg_type') == 'null') {
            Notify::warning("Alerte non enregistré", "Veuillez completer tous les champs", $options = []);
            return redirect(route('alertes.index'));
        } else {
            $alerte = new Alertes();
            $alerte->ses_id = request('session');
            $req = explode('|', request('ale_tmsg_type'));
            $alerte->tmsg_id = $req[0];
            $alerte->mail_subject = request('mail_subject');
            $alerte->ale_tmsg_type = $req[1];
            $al = Alertes::where('ses_id', request('session'))
                ->where('ale_tmsg_type', $req[1])
                ->where('ale_msg', request('ale_msg'))
                ->where('mail_subject', request('mail_subject'))->first();
            if (!empty($al)) {
                Notify::warning("Alerte non enregistré,alerte existe", "Opération echouée", $options = []);
                return redirect(route('alertes.index'));
            } else {
                // $date=Carbon::parse(request('ale_date_heure_prev'));
                $alerte->ale_date_heure_prev = Carbon::createFromFormat('d/m/Y', request('ale_date_heure_prev'))->format('Y-m-d H:i:s');
                $alerte->ale_msg = request('ale_msg');
                $alerte->ale_etat = 0;
                $alerte->ale_date_heure_envoi = null;
                $alerte->ale_destinataire = $this->setDestinataires(request('ale_destinataire'), request('instance'));

                $alerte->save();
                Notify::success("Alerte enregistré avec succès", "Opération reussie", $options = []);
                return redirect(route('alertes.index'));
            }
        }

    }

     public function delete($id)
    {
        $envoi = EnvoiAlertes::where('ale_id', $id)->first();
        if ($envoi) {
            $data = array('message' => 'error');
            return response()->json($data);
        } else {
            $alert = Alertes::where('ale_id', $id)->first();
            $alert->delete();
            $data = array('message' => 'succes');
            return response()->json($data);
        }
    }

    public function getAlerte($id)
    {
        $al = Alertes::where('ale_id', $id)->first();
        $session = Session::join('alertes', 'alertes.ses_id', '=', 'sessions.ses_id')->where('ale_id', $id)->whereIn('ses_statut', [2, 3])->first();
        $instances = Instance::where('ins_id', $session->ins_id)->first();
        $al->ale_destinataire = ($al->ale_tmsg_type == 'AUTRE') ? explode(',', $al->ale_destinataire) : array();
        $data = array('ale_tmsg_type' => $al->ale_tmsg_type,
            'ale_msg' => $al->ale_msg,
            'ale_destinataire' => $al->ale_destinataire,
            'ale_date_heure_prev' => $al->ale_date_heure_prev->format('d/m/Y'),
            'ale_date_heure_envoi' => $al->ale_date_heure_envoi,
            'ale_etat' => $al->ale_etat,
            'p' => $al->tmsg_id . '|' . $al->ale_tmsg_type,
            'ses_id' => $al->ses_id,
            'mail_subject_m' => $al->mail_subject,
            'ins_id' => $instances->ins_id
        );

        return response()->json($data);
    }

    public function updating($id)
    {
        $envoi = EnvoiAlertes::where('ale_id', $id)->first();
        if ($envoi) {
            Notify::warning("Modification impossible,alerte déjà  envoyée!", "Opération echouée", $options = []);
            return redirect(route('alertes.index'));
        } else {
            $alerte = Alertes::where('ale_id', $id)->first();
            $alerte->mail_subject = request('mail_subject_m');
            $alerte->ses_id = request('session_m');
            $req = explode('|', request('ale_tmsg_type_m'));
            $alerte->tmsg_id = $req[0];
            $alerte->ale_tmsg_type = $req[1];
            $al = Alertes::where('ses_id', request('session_m'))->where('ale_date_heure_prev', request('ale_date_heure_prev_m'))->where('ale_msg', request('ale_msg_m'))->
            where('ale_tmsg_type', $alerte->ale_tmsg_type)->first();
            if (!empty($al)) {
                Notify::warning("Alerte non enregistré,l'alerte existe", "Opération echouée", $options = []);
                return redirect(route('alertes.index'));
            } else {
                $alerte->ale_date_heure_prev = Carbon::createFromFormat('d/m/Y', request('ale_date_heure_prev_m'))->format('Y-m-d H:i:s');
                $alerte->ale_msg = request('ale_msg_m');
                $alerte->ale_etat = 0;
                $alerte->ale_destinataire = $this->setDestinataires(request('ale_destinataire_m'), request('instance_m'));
                $alerte->save();
                Notify::success("Alerte enregistré avec succès", "Opération reussie", $options = []);
                return redirect(route('alertes.index'));
            }
        }
    }

    public function show($id)
    {
        $alertes = $this->alertesRepository->find($id);
        if (empty($alertes)) {
            Flash::error('Alertes not found');
            return redirect(route('alertes.index'));
        }

        return view('backend.alertes.show')->with('alertes', $alertes);
    }

    public function edit($id)
    {
        $alertes = $this->alertesRepository->find($id);
        if (empty($alertes)) {
            Flash::error('Alertes not found');
            return redirect(route('alertes.index'));
        }

        return view('backend.alertes.edit')->with('alertes', $alertes);
    }

    public function update($id, UpdateAlertesRequest $request)
    {
        $alertes = $this->alertesRepository->find($id);
        if (empty($alertes)) {
            Flash::error('Alertes not found');
            return redirect(route('alertes.index'));
        }

        $alertes = $this->alertesRepository->update($request->all(), $id);
        Flash::success('Alertes updated successfully.');
        return redirect(route('alertes.index'));
    }

    public function destroy($id)
    {
        $alertes = $this->alertesRepository->find($id);
        if (empty($alertes)) {
            Flash::error('Alertes not found');
            return redirect(route('alertes.index'));
        }

        $this->alertesRepository->delete($id);
        Flash::success('Alertes deleted successfully.');
        return redirect(route('alertes.index'));
    }

    public function infoAlerte($id)
    {
        $notification = EnvoiAlertes::where('ena_id', $id)->first();
        $notification->ena_is_lue=1;
        $notification->save();
        return view('backend.notifications.alertInfo')->with('envoi', $notification);
    }

    public function alertInbox()
    {
        $etats = ['Lu', 'Non lu'];
        $et = null;
        $al = EnvoiAlertes::where('user_id', auth()->user()->id)->paginate(5);
        return view('backend.notifications.boiteReception')
            ->with('alerte', $al)
            ->with('etats', $etats)
            ->with('et', $et);
    }

    public function ConsulterMessage($id)
    {
        $envoi = EnvoiAlertes::where('ena_id', $id)->first();
        //return response()->json($envoi);
        return view('backend.alertes.AlertMessage')->with('envoi', $envoi);

    }

    public function filtreAlerteMessage()
    {
        $etats = ['Lu', 'Non lu'];
        $statut = request('fil_statut');
        if ($statut == 'Lu') {
            $al = EnvoiAlertes::where('user_id', auth()->user()->id)
                ->where('ena_is_lue',1)
                ->paginate(10);
            return view('backend.notifications.boiteReception')->with('alerte', $al)
                ->with('et', $statut)
                ->with('etats', $etats);
        } else if ($statut == 'Non lu') {
            $al =EnvoiAlertes::where('user_id', auth()->user()->id)
                ->where('ena_is_lue',0)
                ->paginate(10);
            return view('backend.notifications.boiteReception')->with('alerte', $al)
                ->with('et', $statut)
                ->with('etats', $etats);
        } else {
            $al =EnvoiAlertes::where('user_id', auth()->user()->id)
                ->paginate(10);
            return view('backend.notifications.boiteReception')
                ->with('alerte', $al)
                ->with('et', $statut)
                ->with('etats', $etats);
        }
    }

    public function alertSearch()
    {
        $instance_id = intval(request('instance_f'));
        $session_id = intval(request('session_f'));
        //$instances = DB::table('instances')->get();
        if ($session_id == 'null' && $instance_id == 'null') {
            $alertes = Alertes::all();
        } else if ($instance_id == 'null') {
            $alertes = Alertes::join('sessions', 'sessions.ses_id', '=', 'alertes.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where('sessions.ses_id', $session_id)->get();
        } else {
            $alertes = Alertes::join('sessions', 'sessions.ses_id', '=', 'alertes.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where('instances.ins_id', request('instance_f'))
                ->where('sessions.ses_id', request('session_f'))->get();
        }
        return view('backend.alertes.index', compact('alertes', 'instance_id', 'session_id'));
    }

    public function getMsg($val)
    {
        $req = explode('|', $val);
        $id = $req[0];
        $tmg = Templatemsgs::where('tmsg_id', $id)->first();
        return response()->json($tmg);
    }

    public function listEnvoi($id)
    {
        $envoi = EnvoiAlertes::where('ale_id', $id)->get();
        return view('backend.envoi_alertes.index')->with('msg_alertes', $envoi);
    }

    public function alertMessageSearch()
    {
        $etat = request('etat');
        $annee = request('fil_annee');
        if ($etat != 'null' && $annee != null) {
            $envoi = EnvoiAlertes::where('ena_is_lue', request('etat'))->where('ena_ses_annee', $annee)->get();
            return view('backend.envoi_alertes.index')->with('msg_alertes', $envoi);
        } else if ($etat == 'null' && $annee != null) {
            $envoi = EnvoiAlertes::where('ena_ses_annee', $annee)->get();
            return view('backend.envoi_alertes.index')->with('msg_alertes', $envoi);
        } else if ($etat != 'null' && $annee == null) {

            $envoi = EnvoiAlertes::where('ena_is_lue', $etat)->get();
            return view('backend.envoi_alertes.index')->with('msg_alertes', $envoi);
        } else {
            return back();
        }

    }

    /** ========================================== Ajouts ======================================= */
    public function getPageAlertesSession($id_session)
    {
        return view('backend.alertes.alertes_session');
    }

    private function setDestinataires($ale_destinataire, $ins_id)
    {
        if (empty($ale_destinataire)) {
            $tab_struc = MethodesStaticController::getStructuresByInstance($ins_id);
            $ale_destinataire = [];
            foreach ($tab_struc as $item_str_id) {
                $ale_destinataire[] = $item_str_id['str_id'];
            }
        }
        return implode(',', $ale_destinataire);
    }
}
