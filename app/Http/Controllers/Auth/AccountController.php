<?php

namespace App\Http\Controllers\Auth;

use DB;
use Auth;
use Hash;
use Flashy;
use App\User;
use App\Jobs\SendAccountEmailJob;
use Helmesvs\Notify\Facades\Notify;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
    }

    public function formPassword()
    {
        $user = Auth::user();
        return view('auth.changePwd')->with('user', $user);
    }

    /** Permet d'enregistrer le changement de password
     * @return redirection sur le dashboard
     */
    public function changePassword()
    {
        if (!(Hash::check(request('old_password'), Auth::user()->password))) {
            Notify::error("Ancien mot de passe incorrect!", "Echec", $options = []);
            return back();
        } else {
            if (strcmp(request('old_password'), request('password')) == 0) {
                Notify::error("Le nouveau mot de passe ne peut pas être identique à l\'ancien !", "Echec", $options = []);
                return back();
            } else {
                if (strcmp(request('password'), request('confirm_password')) == 0) {
                    if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$#', request('password'))) {
                        $user = Auth::user();
                        $user->password = bcrypt(request('password'));
                        $user->save();
                        Notify::success("Mot de passe modifié avec succès !", "Opération réussie", $options = []);
                        activity('Administration')->log('Changement de mot de passe');
                        return redirect()->back();
                        //return redirect(route('logout'));
                    } else {
                        Notify::error("Le mot de passe ne répond aux criteres de complexité réquis !", "Echec", $options = []);
                        //Flashy::error('Le mot de passe ne répond aux criteres de complexité réquis !');
                        return back();
                    }
                } else {
                    Notify::error("Le nouveau mot de passe n\'est pas identique à la confirmation !", "Echec", $options = []);
                    return back();
                }

            }
        }
    }

    public function lockAndUnlock($id)
    {
        $userConnect = auth()->user();
        $user = User::findOrFail($id);
        if (empty($user)) {
            Flashy::error('Compte non trouvé !');
            return redirect(route('admin/users'));
        }
        if ($user->is_active == true) {
            $user->is_active = false;
            $user->save();
            activity('Administration')->log('Compte désactivé');
        } else {
            $user->is_active = true;
            $user->save();
            activity('Administration')->log('Compte activé');
        }
        if ($user->id == $userConnect->id) {
            Flashy::error('Désolé vous ne pouvez pas désactivé vous même votre compte !');
            return redirect(route('admin/users'));
        }
        Flashy::success('Autorisation du compte modifié avec succès !');
    }
    public function reinitialiserMotDePasse($id)
    {
        // on recherche l'utilisateur
        $user = User::findOrFail($id);
        //$agent = Users::where('id', '=', $user->age_id)->first();
        // on genere et attribue le nouveau mot de passe
        $newPassword = "";
        if (!empty($user)) {
            $newPassword = $this->genereMotDePasse(8);
            $user->password = bcrypt($newPassword);
            $user->firstconnect = true;
            // on enregistre le changement
            $user->save();
            activity('Administration')->log('Mot de passe réinitialisé');
        }
        // on envoi un mail si l'utilisateur possede une adresse mail
        if (($user->email != null || !empty($user->email)) && $newPassword != "") {
            $view = "emails.messages.contact";
            $objet = "PSR - Réinitialisation de mot de passe";
            SendAccountEmailJob::dispatch($user->id, $newPassword, $view, $objet);
            /* Artisan::call('credentials:send', [
                'user' => $user->id, 'motDePasse' => $newPassword, 'view' => $view, 'objet' =>$objet
            ]); */
            //shell_exec ("php " . base_path() . "/artisan credentials:send ". $user->id." ". $newPassword." ".$view);
            
        } else {
            return $newPassword . " ! Echec d'envoi par email (l'utilisateur ne possède pas d'email)";
        }
        return $newPassword;
    }
    public function sendMotDePasse($id, $newPassword)
    {
        // on recherche l'utilisateur
        $user = User::findOrFail($id);
        //$agent = Agents::where('age_id','=',$age_id)->first();
        
        // on envoi un mail si l'utilisateur possede une adresse mail
        if (($user->email != null || !empty($user->email)) && $newPassword != "") {
            $view = "emails.messages.contact_create";
            $objet = "PSR - Création de compte";
            SendAccountEmailJob::dispatch($user->id, $newPassword, $view, $objet);
            /* Artisan::call('credentials:send', [
                'user' => $user->id, 'motDePasse' => $newPassword, 'view' => $view, 'objet' => $objet
            ]); */
        } else {
            return $newPassword . " ! Echec d'envoi par email (l'utilisateur ne possède pas d'email)";
        }
        return $newPassword;
    }

    /** Permet de generer un mot de passe aleatoire de longueur : $taille
     * @param $longueur
     * @return string
     */
    public function genereMotDePasse($longueur)
    {
        // chaine de caractères qui sera mis dans le désordre:
        $Chaine = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // 62 caractères au total
        // on mélange la chaine
        $Chaine = str_shuffle($Chaine);
        // ensuite on coupe à la longueur voulue
        $Chaine = substr($Chaine, 0, $longueur);
        // ensuite on retourne notre mot de passe
        return $Chaine;
    }

    /** Permet d'enregistrer le changement de password à la première connexion
     * @param $id
     * @return redirection sur la page de connexion
     */
   /*  public function firstConnect($id)
    {
        $user = $this->usersRepository->findWithoutFail($id);
        if (strcmp(request('newPassword'), request('confirmPassword')) == 0) {
            if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$#', request('newPassword'))) {
                $user->password = bcrypt(request('newPassword'));
                $user->user_isfirstconnect = false;
                $user->save();
                Flashy::success('Mot de passe modifié avec succès !');
                activity('Connexion')->log('Changement de mot de passe');
                return redirect(route('administration'));
            } else {
                Flashy::error('Le mot de passe ne répond aux criteres de complexité réquis !');
                //                return back();
                return view('backend.firstConnect')->with('user', $user);
            }
        } else {
            Flashy::error('Le nouveau mot de passe n\'est pas identique à la confirmation !');
            //            return back();
            return view('backend.firstConnect')->with('user', $user);
        }
    } */
}
