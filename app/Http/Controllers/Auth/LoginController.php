<?php

namespace App\Http\Controllers\Auth;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\LogActivity;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /** ********************************************************************************************************/
    public function logout()
    {
        DB::table('users')->where('id', Auth::user()->id)->update(['lastconnect' => now()]);
        activity('Deconnexion')->log('Utilisateur deconnecté');
        Auth::logout();
        return redirect('login');
    }
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }
    public function authenticate(Request $request)
    {
            $user = DB::table('users')->where('email', $request->email)->first();
            if (empty($user)) {
                Flashy::error('Echec de connexion, verifier vos identifiants de connexion ou veuillez contacter l\'administrateur !');
                return redirect('login');
            } else {
                if ($user->is_active == true) {
                    $userdata = array(
                        'email' => request('email'),
                        'password' => request('password')
                    );
                    if (Auth::attempt($userdata)) {
                        if ($user->firstconnect == false) {
                            activity('Connexion')->log('Connexion à la plateforme');
                            return redirect()->intended('home');
                        } else {
                            Auth::logout();
                            //Flashy::warning('Nous vous prions de changer votre mot de passe par mésure de sécurité!');
                            return view('auth.firstConnect')->with('user', $user);
                        }
                    } else {
                        Flashy::error('Echec de connexion, verifier vos identifiants de connexion ou veuillez contacter l\'administrateur !');
                        return redirect('login');
                    }
                } else {
                    Flashy::error('Echec de connexion, verifier vos identifiants de connexion ou veuillez contacter l\'administrateur !');
                    return redirect('login');
                }
            }
        
    }

    /** Permet d'enregistrer le changement de password à la première connexion
     * @param $id
     * @return redirection sur la page de connexion
     */
    public function firstConnect($id)
    {
        //        $user = $this->usersRepository->findWithoutFail($id);
        $user = DB::table('users')->where('id', $id)->get()->first();
        if (strcmp(request('newPassword'), request('confirmPassword')) == 0) {
            if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$#', request('newPassword'))) {
                DB::table('users')->where('id', $id)->update(['password' => bcrypt(request('newPassword')), 'firstconnect' => false, 'updated_at' => new Carbon()]);
                Flashy::success('Mot de passe modifié avec succès !');
                //activity('Connexion')->log('Changement de mot de passe');
                return redirect()->intended('home');
            } else {
                Flashy::error('Le mot de passe ne répond aux criteres de complexité réquis !');
                return view('auth.firstConnect')->with('user', $user);
            }
        } else {
            Flashy::error('Le nouveau mot de passe n\'est pas identique à la confirmation !');
            return view('auth.firstConnect')->with('user', $user);
        }
    }

}
