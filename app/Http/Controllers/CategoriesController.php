<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Helmesvs\Notify\Facades\Notify;
use App\Repositories\CategoriesRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateCategoriesRequest;
use App\Http\Requests\UpdateCategoriesRequest;

class CategoriesController extends AppBaseController
{
    /** @var  CategoriesRepository */
    private $categoriesRepository;

    public function __construct(CategoriesRepository $categoriesRepo)
    {
        $this->categoriesRepository = $categoriesRepo;
    }

    /**
     * Display a listing of the Categories.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categories = $this->categoriesRepository->all();

        return view('backend.categories.index')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new Categories.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created Categories in storage.
     *
     * @param CreateCategoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriesRequest $request, UpdateCategoriesRequest $updateRequest)
    {
        
        $input = $request->all();
        if ($request->cat_id) {
            try {
                /* DB::table('categories')->where('cat_id', $request->cat_id)
                ->update([
                    ['cat_libelle', $request->cat_libelle],
                    ['cat_is_ministere', $request->cat_is_ministere]
                ]); */
                $categories = $this->update($request->cat_id, $updateRequest);
                Notify::success("Catégorie mise à jour avec succès !", "Opération réussie", $options = []);
            } catch (\PDOException $ex) {
                dd($ex);
                if ($ex->getCode() == 23000) {
                    Notify::error("La catégorie existe dejà !", "Echec", $options = []);
                } else {
                    Notify::error("Echec de la mise à jour. Veuillez réessayer!", "Echec", $options = []);
                }
            }
        } else {
            try {
                $categories = $this->categoriesRepository->create($input);
                Notify::success("Catégorie ajoutée avec succès !", "Opération réussie", $options = []);
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    Notify::error("La catégorie existe dejà !", "Echec", $options = []);
                } else {
                    Notify::error("Echec de l'ajout de la catégorie. Veuillez réessayer!", "Echec", $options = []);
                }
            }
        }
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified Categories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categories = $this->categoriesRepository->find($id);

        if (empty($categories)) {
            Flash::error('Categories not found');

            return redirect(route('categories.index'));
        }

        return view('backend.categories.show')->with('categories', $categories);
    }

    /**
     * Show the form for editing the specified Categories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = $this->categoriesRepository->find($id);

        if (empty($categories)) {
            Flash::error('Categories not found');

            return redirect(route('categories.index'));
        }

        return view('backend.categories.edit')->with('categories', $categories);
    }

    /**
     * Update the specified Categories in storage.
     *
     * @param int $id
     * @param UpdateCategoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriesRequest $request)
    {
        $categories = $this->categoriesRepository->find($id);

        if (empty($categories)) {
            Flash::error('Categories not found');

            return redirect(route('categories.index'));
        }

        $categories = $this->categoriesRepository->update($request->all(), $id);

        Flash::success('Categories updated successfully.');

        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified Categories from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categories = $this->categoriesRepository->find($id);

        if (empty($categories)) {
            Flash::error('Categories not found');

            return redirect(route('categories.index'));
        }

        $this->categoriesRepository->delete($id);

        Flash::success('Categories deleted successfully.');

        return redirect(route('categories.index'));
    }
    public function getCategorie($id)
    {
        $categorie = DB::table('categories')->where('cat_id', $id)->first();
        if (empty($categorie)) {
            return null;
        }
        return response()->json($categorie);
    }

    public function deleteCategorie($id)
    {
        $categorie = DB::table('categories')->where('cat_id', $id)->first();
        if (empty($categorie)) {
            return redirect(route('categories.index'));
        }
        try {
            DB::table('categories')->where('cat_id', '=', intval($categorie->cat_id))->delete();
            return 1;
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }
}
