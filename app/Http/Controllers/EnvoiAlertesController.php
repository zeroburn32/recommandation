<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEnvoiAlertesRequest;
use App\Http\Requests\UpdateEnvoiAlertesRequest;
use App\Jobs\ReSendAlert;
use App\Jobs\SendEmail;
use App\Models\Alertes;
use App\Models\EnvoiAlertes;
use App\Repositories\EnvoiAlertesRepository;

use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Artisan;
use Response;
use Notify;

class EnvoiAlertesController extends AppBaseController
{
    /** @var  EnvoiAlertesRepository */
    private $envoiAlertesRepository;

    public function __construct(EnvoiAlertesRepository $envoiAlertesRepo)
    {
        $this->envoiAlertesRepository = $envoiAlertesRepo;
    }

    public function index(Request $request)
    {
        $envoiAlertes = $this->envoiAlertesRepository->all();

        return view('backend.envoi_alertes.index')
            ->with('envoiAlertes', $envoiAlertes);
    }

    public function create()
    {
        return view('backend.envoi_alertes.create');
    }

    public function store(CreateEnvoiAlertesRequest $request)
    {
        $input = $request->all();

        $envoiAlertes = $this->envoiAlertesRepository->create($input);

        Flash::success('Envoi Alertes saved successfully.');

        return redirect(route('envoiAlertes.index'));
    }

    public function show($id)
    {
        $envoiAlertes = $this->envoiAlertesRepository->find($id);

        if (empty($envoiAlertes)) {
            Flash::error('Envoi Alertes not found');

            return redirect(route('envoiAlertes.index'));
        }

        return view('backend.envoi_alertes.show')->with('envoiAlertes', $envoiAlertes);
    }


    public function edit($id)
    {
        $envoiAlertes = $this->envoiAlertesRepository->find($id);

        if (empty($envoiAlertes)) {
            Flash::error('Envoi Alertes not found');

            return redirect(route('envoiAlertes.index'));
        }

        return view('backend.envoi_alertes.edit')->with('envoiAlertes', $envoiAlertes);
    }

    public function update($id, UpdateEnvoiAlertesRequest $request)
    {
        $envoiAlertes = $this->envoiAlertesRepository->find($id);

        if (empty($envoiAlertes)) {
            Flash::error('Envoi Alertes not found');
            return redirect(route('envoiAlertes.index'));
        }

        $envoiAlertes = $this->envoiAlertesRepository->update($request->all(), $id);

        Flash::success('Envoi Alertes updated successfully.');

        return redirect(route('envoiAlertes.index'));
    }

    public function destroy($id)
    {
        $envoiAlertes = $this->envoiAlertesRepository->find($id);
        if (empty($envoiAlertes)) {
            Flash::error('Envoi Alertes not found');
            return redirect(route('envoiAlertes.index'));
        }
        $this->envoiAlertesRepository->delete($id);
        Flash::success('Envoi Alertes deleted successfully.');
        return redirect(route('envoiAlertes.index'));
    }

    public function alerting()
    {
        $notification = DatabaseNotification::all();
        foreach ($notification as $notif) {
            $notif->delete();
        }
    }

    public function resendAlerte($id)
    {
        activity('Alerte')->log("Lancement de renvoi d'alerte");
        Artisan::call("Realert:send ".$id);
        Notify::success("Processus de renvoi en cours", "Opération reussie", $options = []);
        return redirect(route('alertes.index'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendAlerte($id)
    {
        activity('Alerte')->log("Lancement d'envoi d'alerte");
        Artisan::call("Alert:send ".$id);
        Notify::success("Processus envoi en cours", "Opération reussie", $options = []);
        return redirect(route('alertes.index'));
    }

}
