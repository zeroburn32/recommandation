<?php

namespace App\Http\Controllers;

use DB;
use Notify;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Doctrine\DBAL\Driver\PDOException;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\AppBaseController;
use App\Models\Instance;
use App\Models\Recommandations;
use App\Repositories\RecommandationsRepository;
 
class EtatController extends AppBaseController
{
    
    public function index()
    {
        $statut = 0;
        $instance = 0;
        $sessions = [];
        $session = 0;
        $annee='';
        $acteurs = [];
        $acteur = 0;
        $instances = DB::table('instances')->pluck('ins_sigle', 'ins_id');
        $recos= '';
        //dd($recos);

        
        return view('backend.etats.index', compact('annee','recos','instances','statut','instance','sessions','session','acteurs','acteur'));
    }

    public function getSessionsByInstance($ins_id, $annee)
    {
        $sessions = DB::table('sessions')->where([['ins_id', '=', $ins_id], ['ses_annee', '=', $annee]])->get();
        return Response::json($sessions);
    }
    public function getActeursByInstance($ins_id)
    {
        $acteurs = DB::table('acteurs')
            ->join('structures', 'structures.str_id', '=', 'acteurs.str_id')
            ->where([['acteurs.ins_id', '=', $ins_id]])->get();

        return Response::json($acteurs);
    }

    public function rapportSearch(Request $request) {
        $instance = $request->fil_instance;
        $annee = $request->fil_annee;
        $session = $request->fil_session;
        $acteur = $request->fil_structure;
        $statut = $request->fil_status;

        $instanceSearch = Instance::find($request->fil_instance);
        
        $instances = DB::table('instances')->pluck('ins_sigle', 'ins_id');
        $sessions = DB::table('sessions')->where([['ins_id', '=', $instance], ['ses_annee', '=', $annee]])->pluck('ses_description', 'ses_id');;
        $acteurs = DB::table('acteurs')->join('structures', 'structures.str_id', '=', 'acteurs.str_id')->where([['acteurs.ins_id', '=', $instance]])->pluck('structures.str_sigle', 'structures.str_id');;
        
        if($instance && $annee && $session && $acteur && $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->distinct('recommandations.rec_id')
                ->get();

            }else if($instance && $annee && $session && !$acteur && !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.ptn_is_responsable', true],
                    //['partenaires.str_id', $acteur],
                    ])
                ->get();
            }else if ($instance && $annee && $session && $acteur &&  !$statut){

                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ses_id', $session],
                        ['sessions.ses_annee', $annee],
                        ['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        //['recommandations.rec_statut', $statut],
                    ])
                    ->get();
            } else if ($instance && $annee && $session && !$acteur &&  $statut) {
                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ses_id', $session],
                        ['sessions.ses_annee', $annee],
                        ['partenaires.ptn_is_responsable', true],
                        //['partenaires.str_id', $acteur],
                        ['recommandations.rec_statut', $statut],
                    ])
                    ->get();
            }
            
            else if ($instance && $annee && !$session && $acteur &&  !$statut) {
                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ins_id', $instance],
                        ['sessions.ses_annee', $annee],
                        ['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        //['recommandations.rec_statut', $statut],
                    ])
                    ->get();

             } else if (!$instance && $annee && !$session && !$acteur &&  $statut) {
                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        //['sessions.ses_id', $session],
                        ['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        ['recommandations.rec_statut', $statut],
                    ])
                    ->get();

             } else if (!$instance && $annee && !$session && !$acteur &&  !$statut) {
                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        //['sessions.ses_id', $session],
                        ['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        //['recommandations.rec_statut', $statut],
                    ])
                    ->get();

             } else if (!$instance && !$annee && !$session && !$acteur &&  $statut) {
                 $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        //['sessions.ses_id', $session],
                        //['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        ['recommandations.rec_statut', $statut],
                    ])
                    ->get(); 

             } else if (!$instance && !$annee && !$session && !$acteur &&  !$statut) { 
                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        //['sessions.ses_id', $session],
                        //['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        //['recommandations.rec_statut', $statut],
                        ['partenaires.ptn_is_responsable', true],
                    ])
                    ->get();
             }
             else if ($instance && $annee && !$session && !$acteur &&  $statut) { 
                 $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ins_id', $instance],
                        ['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        ['recommandations.rec_statut', $statut],
                    ])
                    ->get();

             } else if ($instance && $annee && !$session && !$acteur &&  !$statut) { 
                 $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ins_id', $instance],
                        ['sessions.ses_annee', $annee],
                        ['partenaires.ptn_is_responsable', true],
                        //['partenaires.str_id', $acteur],
                        //['recommandations.rec_statut', $statut],
                    ])
                    ->get();

             } else if ($instance && !$annee && !$session && !$acteur &&  $statut) { 

                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ins_id', $instance],
                        //['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        ['recommandations.rec_statut', $statut],
                    ])
                    ->get();
             } 
             else if ($instance && !$annee && !$session && !$acteur &&  !$statut) { 
                $recos = DB::table('recommandations')
                    ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                    ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                    ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                    ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where([
                        ['sessions.ins_id', $instance],
                        //['sessions.ses_annee', $annee],
                        //['partenaires.str_id', $acteur],
                        ['partenaires.ptn_is_responsable', true],
                        //['recommandations.rec_statut', $statut],
                    ])
                    ->get();
             } 
              /*else if (!$instance && $annee && !$session && $acteur &&  $statut) { 

             } else if (!$instance && $annee && $session && !$acteur &&  !$statut) { 

             } else if (!$instance && $annee && !$session && !$acteur &&  $statut) { 

             } */

        

        return view('backend.etats.index', compact('annee', 'instanceSearch','recos', 'instances','sessions' , 'acteurs', 'session', 'acteur' , 'statut', 'instance'));
    }
}
