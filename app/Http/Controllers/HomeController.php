<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EnvoiAlertes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $user = auth()->user();
        $dateDuJour = Carbon::now();
        $allInstances = DB::table('instances')->orderBy('ins_sigle')->get();
        $userInstances = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where('part.str_id', '=', $user->str_id)
            ->orderBy('ses.ses_description')
            ->distinct()
            ->get('ins.*');

        /** Récommandations non réalisées donc l'écheance est dépassé */
        $recosNonRealiseAecheanceDepasses = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['part.str_id', '=', $user->str_id],
                ['rec.rec_statut', '<>', 4],
                ['rec.rec_statut', '<>', 5],
                ['rec_date_echeance', '<', $dateDuJour->format('Y-m-d')]
            ])
            ->orderBy('ses.ses_description')
            ->distinct()
            ->get('rec.*');

        /** Récommandations non réalisées à écheance de moins de 30 Jours */
        $recosNonRealiseAecheances = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['part.str_id', '=', $user->str_id],
                ['rec.rec_statut', '<>', 4],
                ['rec.rec_statut', '<>', 5],
                ['rec_date_echeance', '>', $dateDuJour->format('Y-m-d')],
                ['rec_date_echeance', '<=', $dateDuJour->addDays(30)->format('Y-m-d')]
            ])
            ->orderBy('ses.ses_description')
            ->distinct()
            ->get('rec.*');

       $nbreAlerteNonLue=EnvoiAlertes::where('user_id', auth()->user()->id)
									->where('ena_is_lue',0)->count();

       return view('home', compact('allInstances', 'userInstances', 'recosNonRealiseAecheances',
           'recosNonRealiseAecheanceDepasses', 'nbreAlerteNonLue'));
    }
}
