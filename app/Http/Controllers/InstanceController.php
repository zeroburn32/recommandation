<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInstanceRequest;
use App\Http\Requests\UpdateInstanceRequest;
use App\Models\Instance;
use App\Repositories\InstanceRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Doctrine\DBAL\Driver\PDOException;
use Notify;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;

class InstanceController extends AppBaseController
{
    /** @var  InstanceRepository */
    private $instanceRepository;

    public function __construct(InstanceRepository $instanceRepo)
    {
        $this->instanceRepository = $instanceRepo;

        /** Middleware */
        $this->middleware(['permission:GERER_INSTANCE'])->only([
            'index', 'getInstance', 'getAllInstances', 'getAllInstancesCSS', 'saveInstance', 'deleteInstance'
        ]);
    }

    /**
     * Display a listing of the Instance.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $instances = DB::table('instances as ins')->orderBy('ins.ins_sigle', 'asc')->get();
        /*$instances = DB::table('instances as ins')
        ->join('programme_budgetaire as pbg', 'ins.bud_id', '=', 'pbg.bud_id')
        ->orderBy('ins.ins_sigle', 'asc')->get();*/
        $budges = DB::table('programme_budgetaire')->get();
        return view('backend.instances.index', compact('instances', 'budges'));
    }

    /**
     * Show the form for creating a new Instance.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.instances.create');
    }

    /**
     * Store a newly created Instance in storage.
     *
     * @param CreateInstanceRequest $request
     *
     * @return Response
     */
    public function store(CreateInstanceRequest $request)
    {
        $input = $request->all();

        $instance = $this->instanceRepository->create($input);

        Flash::success('Instance saved successfully.');

        return redirect(route('instances.index'));
    }

    /**
     * Display the specified Instance.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $instance = $this->instanceRepository->find($id);

        if (empty($instance)) {
            Flash::error('Instance not found');

            return redirect(route('instances.index'));
        }

        return view('backend.instances.show')->with('instance', $instance);
    }

    /**
     * Show the form for editing the specified Instance.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $instance = $this->instanceRepository->find($id);

        if (empty($instance)) {
            Flash::error('Instance not found');

            return redirect(route('instances.index'));
        }

        return view('backend.instances.edit')->with('instance', $instance);
    }

    /**
     * Update the specified Instance in storage.
     *
     * @param int $id
     * @param UpdateInstanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInstanceRequest $request)
    {
        $instance = $this->instanceRepository->find($id);

        if (empty($instance)) {
            Flash::error('Instance not found');

            return redirect(route('instances.index'));
        }

        $instance = $this->instanceRepository->update($request->all(), $id);

        Flash::success('Instance updated successfully.');

        return redirect(route('instances.index'));
    }

    /**
     * Remove the specified Instance from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $instance = $this->instanceRepository->find($id);

        if (empty($instance)) {
            Flash::error('Instance not found');

            return redirect(route('instances.index'));
        }

        $this->instanceRepository->delete($id);

        Flash::success('Instance deleted successfully.');

        return redirect(route('instances.index'));
    }

    /** ******************************************PERSONNALISER*********************************************** */

    public function getInstance($id)
    {
        $instance = DB::table('instances as ins')->where('ins.ins_id', $id)->first();
        /*$instance = DB::table('instances as ins')
            ->join('programme_budgetaire as pbg', 'ins.bud_id', '=', 'pbg.bud_id')
            ->select('ins.*', 'pbg.bud_code', 'pbg.bud_intitule')
            ->where('ins.ins_id', $id)->first();*/
        if (empty($instance)) {
            return null;
        }
        return response()->json($instance);
    }

    public function getAllInstances()
    {
        $instances = DB::table('instances')->get();
        if (empty($instances)) {
            return null;
        }
        return response()->json($instances);
    }

    public function getAllInstancesCSS()
    {
        $user = auth()->user();
        $instances = DB::table('instances')
            ->join('acteurs', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->where([
                ['acteurs.str_id', '=', $user->str_id],
                ['acteurs.act_is_css', '=', 1]
            ])
            ->get('instances.*');
        if (empty($instances)) {
            return null;
        }
        return response()->json($instances);
    }

    public function saveInstance() {
        $user = auth()->user();
        if (request('ins_id') == null) {
            $instance = new Instance();
            $instance->ins_sigle = request('ins_sigle');
            $instance->ins_nom_complet = request('ins_nom_complet');
            $instance->ins_nbr_session = request('ins_nbr_session');
            $instance->ins_periodicite = request('ins_periodicite');
            $instance->ins_description = request('ins_description');
            $instance->ins_ref_arrete = request('ins_ref_arrete');
            $instance->created_by = $user->email;
            $instance->updated_by = $user->email;
            try {
                $instance->save();
                Notify::success("Instance ajoutée avec succès", "Opération reussie", $options = []);
            } catch (\PDOException $ex) {
                if (intval($ex->getCode()) == 23000) {
                    Notify::error("Le sigle ou le nom complet existe déjà, merci de réessayer", "Opération échouée", $options = []);
                }
            }
        }
        else {
            try {
                $result = DB::table('instances')
                    ->where('ins_id', request('ins_id'))
                    ->update([
                        'ins_sigle' => request('ins_sigle'),
                        'ins_nom_complet' => request('ins_nom_complet'),
                        'ins_nbr_session' => request('ins_nbr_session'),
                        'ins_periodicite' => request('ins_periodicite'),
                        'ins_description' => request('ins_description'),
                        'ins_ref_arrete' => request('ins_ref_arrete'),
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($result)) {
                    activity('Instance')->log('Modification');
                    Notify::success("Instance modifiée avec succès", "Opération reussie", $options = []);
                }
            } catch (\PDOException $ex) {
                if (intval($ex->getCode()) == 23000) {
                    Notify::error("Le sigle ou le nom complet existe déjà, merci de réessayer", "Opération échouée", $options = []);
                }
            }
        }
        return redirect(route('instances.index'));
    }

    public function deleteInstance($id)
    {
        $instance = DB::table('instances')->where('ins_id', $id)->first();
        if (empty($instance)) {
            Notify::error("Instance non trouvé", "Opération échouée", $options = []);
            return redirect(route('instances.index'));
        }
        try {
            $result = DB::table('instances')->where('ins_id', '=', intval($instance->ins_id))->delete();
            if (!empty($result)) {
                activity('Instance')->log('Suppression');
                Notify::success("Instance supprimée avec succès", "Opération reussie", $options = []);
                return 1;
            }
        } catch (\PDOException $ex) {
            if($ex->getCode() == 23000){
                return 0;
            }
        }
    }
}
