<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateListeAlertesRequest;
use App\Http\Requests\UpdateListeAlertesRequest;
use App\Repositories\ListeAlertesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ListeAlertesController extends AppBaseController
{
    /** @var  ListeAlertesRepository */
    private $listeAlertesRepository;

    public function __construct(ListeAlertesRepository $listeAlertesRepo)
    {
        $this->listeAlertesRepository = $listeAlertesRepo;
    }

    /**
     * Display a listing of the ListeAlertes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $listeAlertes = $this->listeAlertesRepository->all();

        return view('backend.liste_alertes.index')
            ->with('listeAlertes', $listeAlertes);
    }

    /**
     * Show the form for creating a new ListeAlertes.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.liste_alertes.create');
    }

    /**
     * Store a newly created ListeAlertes in storage.
     *
     * @param CreateListeAlertesRequest $request
     *
     * @return Response
     */
    public function store(CreateListeAlertesRequest $request)
    {
        $input = $request->all();

        $listeAlertes = $this->listeAlertesRepository->create($input);

        Flash::success('Liste Alertes saved successfully.');

        return redirect(route('listeAlertes.index'));
    }

    /**
     * Display the specified ListeAlertes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            Flash::error('Liste Alertes not found');

            return redirect(route('listeAlertes.index'));
        }

        return view('backend.liste_alertes.show')->with('listeAlertes', $listeAlertes);
    }

    /**
     * Show the form for editing the specified ListeAlertes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            Flash::error('Liste Alertes not found');

            return redirect(route('listeAlertes.index'));
        }

        return view('backend.liste_alertes.edit')->with('listeAlertes', $listeAlertes);
    }

    /**
     * Update the specified ListeAlertes in storage.
     *
     * @param int $id
     * @param UpdateListeAlertesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateListeAlertesRequest $request)
    {
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            Flash::error('Liste Alertes not found');

            return redirect(route('listeAlertes.index'));
        }

        $listeAlertes = $this->listeAlertesRepository->update($request->all(), $id);

        Flash::success('Liste Alertes updated successfully.');

        return redirect(route('listeAlertes.index'));
    }

    /**
     * Remove the specified ListeAlertes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $listeAlertes = $this->listeAlertesRepository->find($id);

        if (empty($listeAlertes)) {
            Flash::error('Liste Alertes not found');

            return redirect(route('listeAlertes.index'));
        }

        $this->listeAlertesRepository->delete($id);

        Flash::success('Liste Alertes deleted successfully.');

        return redirect(route('listeAlertes.index'));
    }
}
