<?php
/**
 * Created by PhpStorm.
 * User: abdou
 * Date: 03/03/2020
 * Time: 14:52
 */

namespace App\Http\Controllers;

use App\Models\Alertes;
use App\Models\EnvoiAlertes;
use App\Models\ListeAlertes;
use App\Models\Structures;
use Carbon\Carbon;
use DB;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class MethodesStaticController extends AppBaseController
{
    /**
     * MethodesStaticController constructor.
     */
    public function __construct()
    {
    }

    public static function nbreActeursByInstance($id_instance) {
        $nbre = DB::table('acteurs')->where('ins_id', $id_instance)->count();
        return $nbre;
    }

    public static function nbreRecosBySession($id_session)
    {
        $nbre = DB::table('recommandations')->where('ses_id', $id_session)->count();
        return $nbre;
    }

    public static function nbreRecosOfReconductBySession($ses_id_precedant)
    {
        $nbre = DB::table('recommandations as rec')
            ->where([
                ['rec.rec_statut', '<>', 4],
                ['rec.rec_statut', '<>', 5],
                ['rec.rec_is_reconduit', '=', 0],
                ['rec.ses_id', '=', $ses_id_precedant]
            ])
            ->orWhere([
                ['rec.rec_is_permanente', '=', 1],
                ['rec.rec_is_reconduit', '=', 0],
                ['rec.ses_id', '=', $ses_id_precedant]
            ])->count();
        return $nbre;
    }

    public static function nbreAlertesBySession($id_session)
    {
        $nbre = DB::table('alertes')->where('ses_id', $id_session)->count();
        return $nbre;
    }

    public static function nbreParticipantsBySession($id_session)
    {
        $nbre = DB::table('participants')->where('ses_id', $id_session)->count();
        return $nbre;
    }

    public static function nbreActivitesByReco($id_reco)
    {
        $nbre = DB::table('activites')->where('rec_id', $id_reco)->count();
        return $nbre;
    }

    public static function nbrePartenairesByReco($id_reco)
    {
        $nbre = DB::table('partenaires')->where('rec_id', $id_reco)->count();
        return $nbre;
    }

    public static function structureUserConnect()
    {
        $userConnect = auth()->user();
        if (!empty($userConnect)) {
            $structure = DB::table('users as us')
                ->join('structures as st', 'us.str_id', '=', 'st.str_id')
                ->where('us.id', '=', $userConnect->id)
                ->select('st.*')
                ->first();
        } else {
            $structure = null;
        }
        return $structure;
    }

    public static function checkLibelleReco($ses_id, $libelleReco) {
        $result = DB::table('recommandations')->where([
            ['rec_intitule', '=', $libelleReco],
            ['ses_id', '=', $ses_id]
        ])->first();
        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }

    public static function verifyEmailNonEnvoyeByAlerte($id_alerte) {
        $nbr = EnvoiAlertes::where('envoialertes.ale_id',$id_alerte)
            ->where('envoialertes.ena_isenvoi',0)->get();
        return $nbr->count();
    }
    public static function nbrEnvoyeByAlerte($id_alerte) {
        $alerte = Alertes::where('ale_id',$id_alerte)->select('ale_id')->first();
        $liste = ListeAlertes::where('ses_id',$alerte->ses_id)->get();
        return $liste->count();
    }

    public static function verifyEnvoyeByAlerte($id_alerte) {
        $alerte=Alertes::where('ale_id',$id_alerte)->first();
        $liste=ListeAlertes::where('ses_id',$alerte->ses_id)->get();
        return ($liste->count()==0) ? false : true;
    }

    public static function templateHasAlerte($tmsg_id)
    {
        $nbr_alertes = Alertes::where('tmsg_id',$tmsg_id)->get();
        return ($nbr_alertes->count()==0) ? false : true;
    }

    public static function getStructuresByInstance($ins_id)
    {
        $structures = Structures::join('acteurs', 'acteurs.str_id', '=', 'structures.str_id')
            ->join('instances', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->where([['instances.ins_id','=', $ins_id], ['act_is_css','=',1]])
            ->orwhere([['instances.ins_id','=', $ins_id], ['act_is_csr','=',1]])
            ->select('structures.str_id', 'structures.str_nom_complet', 'structures.str_sigle')
            ->get();
        return $structures;
    }

    public static function sendEmail($envoialerte, $envoi_id = 0){
        $data = array('session_annee'=>$envoialerte->ena_ses_annee,
            'session_lieu'=>$envoialerte->ena_ses_lieu,
            'session_date_debut'=>$envoialerte->ena_ses_debut,
            'session_date_fin'=>$envoialerte->ena_ses_debut,
            'session_description'=>$envoialerte->ena_ses_description,
            'contenu_email'=>$envoialerte->ena_msg,
            'user_nom'=>$envoialerte->user_nom,
            'user_prenom'=>$envoialerte->user_prenom,
            'user_email'=>$envoialerte->user_email,
            'mail_subject'=>$envoialerte->ale_mail_subject
        );

        Mail::send('emails.messages.email', $data, function ($message) use ($envoialerte) {
            $message->from(config('mail.mail_from'));
            $message->to($envoialerte->user_email)->subject($envoialerte->ale_mail_subject);
        });

        if($envoi_id != 0){
            $envoialerte = EnvoiAlertes::where('ena_id', $envoi_id)->first();
            $envoialerte->updated_by = is_object(Auth::user()) ? Auth::user()->nom : 'system@system.sys';
        }
        $envoialerte->ena_date_heure_recu = Carbon::now();
        $envoialerte->ena_isenvoi = 1;
        $envoialerte->save();
    }
	
	public static function getEtatReco($etat)
    {
		if($etat==1) return "En préparation";
		if($etat==2) return "Non réalisée";
		if($etat==3) return "En cours";
		if($etat==4) return "Réalisée";
		return "Abandonnée";
	}
}