<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateModelHasPermissionRequest;
use App\Http\Requests\UpdateModelHasPermissionRequest;
use App\Repositories\ModelHasPermissionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ModelHasPermissionController extends AppBaseController
{
    /** @var  ModelHasPermissionRepository */
    private $modelHasPermissionRepository;

    public function __construct(ModelHasPermissionRepository $modelHasPermissionRepo)
    {
        $this->modelHasPermissionRepository = $modelHasPermissionRepo;
    }

    /**
     * Display a listing of the ModelHasPermission.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $modelHasPermissions = $this->modelHasPermissionRepository->all();

        return view('backend.model_has_permissions.index')
            ->with('modelHasPermissions', $modelHasPermissions);
    }

    /**
     * Show the form for creating a new ModelHasPermission.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.model_has_permissions.create');
    }

    /**
     * Store a newly created ModelHasPermission in storage.
     *
     * @param CreateModelHasPermissionRequest $request
     *
     * @return Response
     */
    public function store(CreateModelHasPermissionRequest $request)
    {
        $input = $request->all();

        $modelHasPermission = $this->modelHasPermissionRepository->create($input);

        Flash::success('Model Has Permission saved successfully.');

        return redirect(route('modelHasPermissions.index'));
    }

    /**
     * Display the specified ModelHasPermission.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modelHasPermission = $this->modelHasPermissionRepository->find($id);

        if (empty($modelHasPermission)) {
            Flash::error('Model Has Permission not found');

            return redirect(route('modelHasPermissions.index'));
        }

        return view('backend.model_has_permissions.show')->with('modelHasPermission', $modelHasPermission);
    }

    /**
     * Show the form for editing the specified ModelHasPermission.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modelHasPermission = $this->modelHasPermissionRepository->find($id);

        if (empty($modelHasPermission)) {
            Flash::error('Model Has Permission not found');

            return redirect(route('modelHasPermissions.index'));
        }

        return view('backend.model_has_permissions.edit')->with('modelHasPermission', $modelHasPermission);
    }

    /**
     * Update the specified ModelHasPermission in storage.
     *
     * @param int $id
     * @param UpdateModelHasPermissionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateModelHasPermissionRequest $request)
    {
        $modelHasPermission = $this->modelHasPermissionRepository->find($id);

        if (empty($modelHasPermission)) {
            Flash::error('Model Has Permission not found');

            return redirect(route('modelHasPermissions.index'));
        }

        $modelHasPermission = $this->modelHasPermissionRepository->update($request->all(), $id);

        Flash::success('Model Has Permission updated successfully.');

        return redirect(route('modelHasPermissions.index'));
    }

    /**
     * Remove the specified ModelHasPermission from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modelHasPermission = $this->modelHasPermissionRepository->find($id);

        if (empty($modelHasPermission)) {
            Flash::error('Model Has Permission not found');

            return redirect(route('modelHasPermissions.index'));
        }

        $this->modelHasPermissionRepository->delete($id);

        Flash::success('Model Has Permission deleted successfully.');

        return redirect(route('modelHasPermissions.index'));
    }
}
