<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePartenairesRequest;
use App\Http\Requests\UpdatePartenairesRequest;
use App\Models\Partenaires;
use App\Repositories\PartenairesRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;
use Notify;

class PartenairesController extends AppBaseController
{
    /** @var  PartenairesRepository */
    private $partenairesRepository;

    public function __construct(PartenairesRepository $partenairesRepo)
    {
        $this->partenairesRepository = $partenairesRepo;

        /** Middleware */
        $this->middleware(['permission:GERER_RECOMMANDATION'])->only([
            'getPagePartenaireReco', 'getListPartenairesByReco'
        ]);
        $this->middleware(['permission:SAISIE_RECOMMANDATION'])->only([
            'getPartenaire', 'getAllPartenaires', 'savePartenaire', 'deletePartenaire', 'setResponsable'
        ]);
    }

    /**
     * Display a listing of the Partenaires.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $partenaires = $this->partenairesRepository->all();
        return view('backend.partenaires.index', compact('partenaires'));
    }

    /**
     * Show the form for creating a new Partenaires.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.partenaires.create');
    }

    /**
     * Store a newly created Partenaires in storage.
     *
     * @param CreatePartenairesRequest $request
     *
     * @return Response
     */
    public function store(CreatePartenairesRequest $request)
    {
        $input = $request->all();
        $partenaires = $this->partenairesRepository->create($input);
        Flash::success('Partenaires saved successfully.');
        return redirect(route('partenaires.index'));
    }

    /**
     * Display the specified Partenaires.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $partenaires = $this->partenairesRepository->find($id);
        if (empty($partenaires)) {
            Flash::error('Partenaires not found');
            return redirect(route('partenaires.index'));
        }
        return view('backend.partenaires.show')->with('partenaires', $partenaires);
    }

    /**
     * Show the form for editing the specified Partenaires.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $partenaires = $this->partenairesRepository->find($id);
        if (empty($partenaires)) {
            Flash::error('Partenaires not found');
            return redirect(route('partenaires.index'));
        }
        return view('backend.partenaires.edit')->with('partenaires', $partenaires);
    }

    /**
     * Update the specified Partenaires in storage.
     *
     * @param int $id
     * @param UpdatePartenairesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartenairesRequest $request)
    {
        $partenaires = $this->partenairesRepository->find($id);
        if (empty($partenaires)) {
            Flash::error('Partenaires not found');
            return redirect(route('partenaires.index'));
        }
        $partenaires = $this->partenairesRepository->update($request->all(), $id);
        Flash::success('Partenaires updated successfully.');
        return redirect(route('partenaires.index'));
    }

    /**
     * Remove the specified Partenaires from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $partenaires = $this->partenairesRepository->find($id);
        if (empty($partenaires)) {
            Flash::error('Partenaires not found');
            return redirect(route('partenaires.index'));
        }
        $this->partenairesRepository->delete($id);
        Flash::success('Partenaires deleted successfully.');
        return redirect(route('partenaires.index'));
    }

    /** ******************************************PERSONNALISER*********************************************** */
    public function getPagePartenaireReco($id_reco)
    {
        $reco = DB::table('recommandations')->where('rec_id', $id_reco)->first();
        $structures = DB::table('structures')
            ->join('acteurs', 'structures.str_id', '=', 'acteurs.str_id')
            ->join('sessions', 'acteurs.ins_id', '=', 'sessions.ins_id')
            ->join('recommandations', 'sessions.ses_id', '=', 'recommandations.ses_id')
            ->where('recommandations.rec_id', '=', $reco->rec_id)
            ->whereNotIn('structures.str_id',
                DB::table('structures')
                    ->join('partenaires', 'structures.str_id', '=', 'partenaires.str_id')
                    ->where('partenaires.rec_id', $reco->rec_id)
                    ->pluck('structures.str_id')
            )
            ->orderBy('structures.str_sigle', 'asc')->get();
        $partenaires = DB::table('recommandations as rec')
            ->join('partenaires as ptn', 'rec.rec_id', '=', 'ptn.rec_id')
            ->join('structures as str', 'ptn.str_id', '=', 'str.str_id')
            ->where('rec.rec_id', '=', $reco->rec_id)
            ->orderBy('str.str_sigle', 'asc')
            ->get();
        return view('backend.partenaires.partenaire_reco', compact('reco', 'structures', 'partenaires'));
    }

    public function getListPartenairesByReco($id_reco)
    {
        $partenaires = DB::table('recommandations as rec')
            ->join('partenaires as ptn', 'rec.rec_id', '=', 'ptn.rec_id')
            ->join('structures as str', 'ptn.str_id', '=', 'str.str_id')
            ->where('rec.rec_id', '=', $id_reco)
            ->orderBy('str.str_sigle', 'asc')
            ->get();
        return $partenaires;
    }

    public function getPartenaire($id)
    {
        $partenaire = DB::table('partenaires')->where('ptn_id', $id)->first();
        if (empty($partenaire)) {
            return null;
        }
        return response()->json($partenaire);
    }

    public function getAllPartenaires()
    {
        $partenaires = DB::table('partenaires')->get();
        if (empty($partenaires)) {
            return null;
        }
        return response()->json($partenaires);
    }

    public function savePartenaire()
    {
        $nbResponsable = DB::table('recommandations as rec')
            ->join('partenaires as ptn', 'rec.rec_id', '=', 'ptn.rec_id')
            ->where([['ptn.ptn_is_responsable', '=', 1], ['rec.rec_id', '=', request('rec_id')]])
            ->select('ptn.*')
            ->count();
        request('str_id') ? $n = sizeof(request('str_id')) : $n = 0;
        if ($n > 0) {
            $user = auth()->user();
            for ($i = 0; $i < $n; $i++) {
                $partenaire = new Partenaires();
                $partenaire->rec_id = request('rec_id');
                $partenaire->str_id = request('str_id')[$i];
                if ($nbResponsable == 0 && $i == 0) {
                    $partenaire->ptn_is_responsable = 1;
                }
                $partenaire->created_by = $user->email;
                $partenaire->updated_by = $user->email;
                $partenaire->save();
            }
        }
        else {
            Notify::error("Choisissez au moins une structure", "Opération échouée", $options = []);
            return redirect('getPagePartenaireReco/' . request('rec_id'));
        }
        Notify::success("Partenaire ajouté avec succès", "Opération reussie", $options = []);
        return redirect('getPagePartenaireReco/' . request('rec_id'));
    }

    public function deletePartenaire($id)
    {
        $partenaire = DB::table('partenaires')->where('ptn_id', $id)->first();
        if (empty($partenaire)) {
            return 0;
        }
        try {
            $result = DB::table('partenaires')->where('ptn_id', '=', intval($partenaire->ptn_id))->delete();
            if (!empty($result)) {
                activity('Partenaire')->log('Suppression');
                return 1;
            }
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    public function setResponsable($id)
    {
        $partenaire = DB::table('partenaires')->where('ptn_id', $id)->first();
        if (empty($partenaire)) {
            Notify::error("Partenaire non trouvé", "Erreur", $options = []);
        }
        DB::table('partenaires')
            ->where('rec_id', $partenaire->rec_id)
            ->update([
                'ptn_is_responsable' => 0
            ]);
        try {
            $user = auth()->user();
            $partenaire->ptn_is_responsable == 0 ? $value = 1 : $value = 0;
            DB::table('partenaires')
                ->where('ptn_id', $partenaire->ptn_id)
                ->update([
                    'ptn_is_responsable' => $value,
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            Notify::success("Rôle responsable de la recommandation mise à jour avec succès", "Opération reussie", $options = []);
            return 0;
        } catch (\PDOException $ex) {
            return 1;
        }
    }
}
