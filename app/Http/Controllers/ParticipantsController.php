<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParticipantsRequest;
use App\Http\Requests\UpdateParticipantsRequest;
use App\Repositories\ParticipantsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ParticipantsController extends AppBaseController
{
    /** @var  ParticipantsRepository */
    private $participantsRepository;

    public function __construct(ParticipantsRepository $participantsRepo)
    {
        $this->participantsRepository = $participantsRepo;
    }

    /**
     * Display a listing of the Participants.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $participants = $this->participantsRepository->all();

        return view('backend.participants.index')
            ->with('participants', $participants);
    }

    /**
     * Show the form for creating a new Participants.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.participants.create');
    }

    /**
     * Store a newly created Participants in storage.
     *
     * @param CreateParticipantsRequest $request
     *
     * @return Response
     */
    public function store(CreateParticipantsRequest $request)
    {
        $input = $request->all();

        $participants = $this->participantsRepository->create($input);

        Flash::success('Participants saved successfully.');

        return redirect(route('participants.index'));
    }

    /**
     * Display the specified Participants.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            Flash::error('Participants not found');

            return redirect(route('participants.index'));
        }

        return view('backend.participants.show')->with('participants', $participants);
    }

    /**
     * Show the form for editing the specified Participants.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            Flash::error('Participants not found');

            return redirect(route('participants.index'));
        }

        return view('backend.participants.edit')->with('participants', $participants);
    }

    /**
     * Update the specified Participants in storage.
     *
     * @param int $id
     * @param UpdateParticipantsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParticipantsRequest $request)
    {
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            Flash::error('Participants not found');

            return redirect(route('participants.index'));
        }

        $participants = $this->participantsRepository->update($request->all(), $id);

        Flash::success('Participants updated successfully.');

        return redirect(route('participants.index'));
    }

    /**
     * Remove the specified Participants from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $participants = $this->participantsRepository->find($id);

        if (empty($participants)) {
            Flash::error('Participants not found');

            return redirect(route('participants.index'));
        }

        $this->participantsRepository->delete($id);

        Flash::success('Participants deleted successfully.');

        return redirect(route('participants.index'));
    }
}
