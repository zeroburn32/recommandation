<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProgrammeBudgetairesRequest;
use App\Http\Requests\UpdateProgrammeBudgetairesRequest;
use App\Models\ProgrammeBudgetaires;
use App\Repositories\ProgrammeBudgetairesRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;
use Notify;

class ProgrammeBudgetairesController extends AppBaseController
{
    /** @var  ProgrammeBudgetairesRepository */
    private $programmeBudgetairesRepository;

    public function __construct(ProgrammeBudgetairesRepository $programmeBudgetairesRepo)
    {
        $this->programmeBudgetairesRepository = $programmeBudgetairesRepo;

        /** Middleware */
        $this->middleware(['permission:GERER_PROGRAMME_BUDGETAIRE'])->only(['index', 'getProgBug', 'saveProgBug', 'saveProgBug', 'deleteProgBug']);
    }

    /**
     * Display a listing of the ProgrammeBudgetaires.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $programmeBudgetaires = DB::table('programme_budgetaire')->orderBy('bud_intitule')->get();
        $programmeBudgetaire = null;
        return view('backend.programme_budgetaires.index', compact('programmeBudgetaires', 'programmeBudgetaire'));
    }

    /**
     * Show the form for creating a new ProgrammeBudgetaires.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.programme_budgetaires.create');
    }

    /**
     * Store a newly created ProgrammeBudgetaires in storage.
     *
     * @param CreateProgrammeBudgetairesRequest $request
     *
     * @return Response
     */
    public function store(CreateProgrammeBudgetairesRequest $request)
    {
        $input = $request->all();

        $programmeBudgetaires = $this->programmeBudgetairesRepository->create($input);

        Flash::success('Programme Budgetaires saved successfully.');

        return redirect(route('programmeBudgetaires.index'));
    }

    /**
     * Display the specified ProgrammeBudgetaires.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);

        if (empty($programmeBudgetaires)) {
            Flash::error('Programme Budgetaires not found');

            return redirect(route('programmeBudgetaires.index'));
        }

        return view('backend.programme_budgetaires.show')->with('programmeBudgetaires', $programmeBudgetaires);
    }

    /**
     * Show the form for editing the specified ProgrammeBudgetaires.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);

        if (empty($programmeBudgetaires)) {
            Flash::error('Programme Budgetaires not found');

            return redirect(route('programmeBudgetaires.index'));
        }

        return view('backend.programme_budgetaires.edit')->with('programmeBudgetaires', $programmeBudgetaires);
    }

    /**
     * Update the specified ProgrammeBudgetaires in storage.
     *
     * @param int $id
     * @param UpdateProgrammeBudgetairesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProgrammeBudgetairesRequest $request)
    {
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);

        if (empty($programmeBudgetaires)) {
            Flash::error('Programme Budgetaires not found');

            return redirect(route('programmeBudgetaires.index'));
        }

        $programmeBudgetaires = $this->programmeBudgetairesRepository->update($request->all(), $id);

        Flash::success('Programme Budgetaires updated successfully.');

        return redirect(route('programmeBudgetaires.index'));
    }

    /**
     * Remove the specified ProgrammeBudgetaires from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $programmeBudgetaires = $this->programmeBudgetairesRepository->find($id);
        if (empty($programmeBudgetaires)) {
            Flash::error('Programme Budgetaires not found');
            return redirect(route('programmeBudgetaires.index'));
        }

        $this->programmeBudgetairesRepository->delete($id);
        Flash::success('Programme Budgetaires deleted successfully.');
        return redirect(route('programmeBudgetaires.index'));
    }

    /** ****************************************** PERSONNALISER *********************************************** */

    public function getProgBug($id)
    {
        $programme = DB::table('programme_budgetaire')->where('bud_id', $id)->first();
        if (empty($programme)) {
            return null;
        }
        return response()->json($programme);
    }

    public function getAllProgBugs()
    {
        $programmes = DB::table('programme_budgetaire')->get();
        if (empty($programmes)) {
            return null;
        }
        return $programmes;
    }

    public function saveProgBug() {
        if (request('bud_id') == null) {
            $programme = new ProgrammeBudgetaires();
            $programme->bud_code = request('bud_code');
            $programme->bud_intitule = request('bud_intitule');
            try {
                $programme->save();
                Notify::success("Programme budgétaire ajoutée avec succès", "Opération reussie", $options = []);
            } catch (\PDOException $ex) {
                if (intval($ex->getCode()) == 23000) {
                    Notify::error("Le code ou intitulé existe déjà, merci de réessayer", "Opération échouée", $options = []);
                }
            }
        } else {
            try {
                $result = DB::table('programme_budgetaire')
                    ->where('bud_id', request('bud_id'))
                    ->update([
                        'bud_code' => request('bud_code'),
                        'bud_intitule' => request('bud_intitule'),
                        'updated_at' => Carbon::now(),
                    ]);
                if (!empty($result)) {
                    activity('Programme budgétaire')->log('Modification');
                    Notify::success("Programme budgétaire modifiée avec succès", "Opération reussie", $options = []);
                }
            } catch (\PDOException $ex) {
                if (intval($ex->getCode()) == 23000) {
                    Notify::error("Le code ou intitulé existe déjà, merci de réessayer", "Opération échouée", $options = []);
                }
            }
        }
        return redirect(route('programmeBudgetaires.index'));
    }

    public function deleteProgBug($id)
    {
        $programme = DB::table('programme_budgetaire')->where('bud_id', $id)->first();
        if (empty($programme)) {
            Notify::error("Programme budgétaire non trouvé", "Opération échouée", $options = []);
            return redirect(route('programmeBudgetaires.index'));
        }
        try {
            $result = DB::table('programme_budgetaire')->where('bud_id', '=', intval($programme->bud_id))->delete();
            if (!empty($result)) {
                activity('Programme budgétaire')->log('Modification');
                Notify::success("Programme budgétaire supprimée avec succès", "Opération reussie", $options = []);
                return 1;
            }
        } catch (\PDOException $ex) {
            if($ex->getCode() == 23000){
                return 0;
            }
        }
    }

    public function reload(Request $request)
    {
        $programmeBudgetaires = DB::table('programme_budgetaire')->orderBy('bud_annee')->get();
        if (empty($programmeBudgetaires)) {
            Flash::info('Aucun programme budgétaire trouvé')->important();
        }
        return view('backend.programme_budgetaires.table', compact('programmeBudgetaires'));
    }
}
