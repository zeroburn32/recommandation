<?php

namespace App\Http\Controllers;

use DB;
use Notify;
use Carbon\Carbon;
use App\Models\Instance;
use App\Models\Structures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\AppBaseController;

class RapportController extends AppBaseController
{
    public function index()
    {
        $statut = 0;
        $instance = 0;
        $sessions = [];
        $session = 0;
        $annee = '';
        $acteurs = [];
        $acteur = 0;
        $instances = DB::table('instances')->orderby('ins_sigle')->pluck('ins_sigle', 'ins_id');
        $structures = DB::table('structures')->pluck(DB::raw("concat(str_sigle,' - ',str_nom_complet) as str_nom_complet"), 'str_id');
        $recos = '';


        return view('backend.rapport.index', compact('annee', 'structures', 'recos', 'instances', 'statut', 'instance', 'sessions', 'session', 'acteurs', 'acteur'));
    }

    public function getSessionsByInstance($ins_id, $annee)
    {
        $sessions = DB::table('sessions')->where([['ins_id', '=', $ins_id], ['ses_annee', '=', $annee]])->get();
        return Response::json($sessions);
    }
    public function getActeursByInstance($ins_id)
    {
        if ($ins_id == -1) {
            $acteurs = DB::table('structures')->orderby('str_sigle')->get();
        } else {
            $acteurs = DB::table('acteurs')
                ->join('structures', 'structures.str_id', '=', 'acteurs.str_id')
                ->where([['acteurs.ins_id', '=', $ins_id]])->orderby('str_sigle')->get();
        }

        return Response::json($acteurs);
    }

    public function getListPartenaires($id_reco)
    {
        $partenaires = DB::table('recommandations as rec')
            ->join('partenaires as ptn', 'rec.rec_id', '=', 'ptn.rec_id')
            ->join('structures as str', 'ptn.str_id', '=', 'str.str_id')
            ->where('rec.rec_id', '=', $id_reco)
            ->orderBy('str.str_sigle', 'asc')
            ->get();
        return $partenaires;
    }
    public function getListActivites($id_reco)
    {
        $reco = DB::table('recommandations')
            ->where('recommandations.rec_id', $id_reco)->first();

        $activites = DB::table('activites')
            ->join('partenaires', 'activites.ptn_id', '=', 'partenaires.ptn_id')
            ->join('structures', 'partenaires.str_id', '=', 'structures.str_id')
            ->where('activites.rec_id', $id_reco)->get();
        return response()->json(array(
            'reco' => $reco,
            'activites' => $activites
        ));
        //return $activites;
    }

    public function rapportSearch(Request $request)
    {
        $instance = $request->fil_instance;
        $annee = $request->fil_annee;
        $session = $request->fil_session;
        $acteur = $request->fil_structure;
        $statut = $request->fil_status;

        $instanceSearch = Instance::find($request->fil_instance);

        $instances = DB::table('instances')->orderby('ins_sigle')->pluck('ins_sigle', 'ins_id');
        $structures = DB::table('structures')->pluck(DB::raw("concat(str_sigle,' - ',str_nom_complet) as str_nom_complet"), 'str_id');
        $sessions = DB::table('sessions')->where([['ins_id', '=', $instance], ['ses_annee', '=', $annee]])->pluck('ses_description', 'ses_id');;
        $acteurs = DB::table('acteurs')->join('structures', 'structures.str_id', '=', 'acteurs.str_id')->where([['acteurs.ins_id', '=', $instance]])->orderby('str_sigle')->pluck('structures.str_sigle', 'structures.str_id');;

        if ($instance && $annee && $session && $acteur && $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->distinct('recommandations.rec_id')
                ->get();
        } else if ($instance && $annee && $session && !$acteur && !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.ptn_is_responsable', true],
                    //['partenaires.str_id', $acteur],
                ])
                ->get();
        } else if ($instance && $annee && $session && $acteur &&  !$statut) {

            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && $annee && $session && !$acteur &&  $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.ptn_is_responsable', true],
                    //['partenaires.str_id', $acteur],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && $annee && !$session && $acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if (!$instance && $annee && !$session && !$acteur &&  $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    //['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if (!$instance && $annee && !$session && !$acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    //['sessions.ses_id', $session],
                    ['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if (!$instance && !$annee && !$session && !$acteur &&  $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    //['sessions.ses_id', $session],
                    //['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if (!$instance && !$annee && !$session && !$acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    //['sessions.ses_id', $session],
                    //['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    //['recommandations.rec_statut', $statut],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->get();
        } else if ($instance && $annee && !$session && !$acteur &&  $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && $annee && !$session && !$acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.ptn_is_responsable', true],
                    //['partenaires.str_id', $acteur],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && !$annee && !$session && !$acteur &&  $statut) {

            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && !$annee && !$session && !$acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                    //['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && $annee && !$session && $acteur &&  $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && !$annee && !$session && $acteur &&  $statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    ['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if ($instance && !$annee && !$session && $acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if (!$instance && !$annee && !$session && $acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    //['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        } else if (!$instance && $annee && !$session && $acteur &&  !$statut) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->where([
                    //['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                    //['recommandations.rec_statut', $statut],
                ])
                ->get();
        }
        
        return view('backend.rapport.index', compact('annee', 'structures', 'instanceSearch', 'recos', 'instances', 'sessions', 'acteurs', 'session', 'acteur', 'statut', 'instance'));
    }
}
