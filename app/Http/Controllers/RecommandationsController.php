<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecommandationsRequest;
use App\Http\Requests\UpdateRecommandationsRequest;
use App\Models\ImportRecos;
use App\Models\Recommandations;
use App\Repositories\RecommandationsRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Response;
use Notify;
use Excel;
use PhpOffice;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class RecommandationsController extends AppBaseController
{
    /** @var  RecommandationsRepository */
    private $recommandationsRepository;

    public function __construct(RecommandationsRepository $recommandationsRepo)
    {
        $this->recommandationsRepository = $recommandationsRepo;

        /** Middleware */
        $this->middleware(['permission:GERER_RECOMMANDATION'])->only([
            'index', 'getPageRecosBySession', 'getPageActiviteReco', 'getReco', 'getAllRecos',
            'recosSearch', 'recosSearchForUserConnect'
        ]);
        $this->middleware(['permission:ABANDONNER_RECOMMANDATION'])->only([
            'abandonnerReco'
        ]);
        $this->middleware(['permission:SAISIE_RECOMMANDATION'])->only([
            'getPageSaisieRecos', 'saveReco', 'importer', 'downloadTemplateImport', 'deleteReco'
        ]);
        $this->middleware(['permission:PLANIFIER_RECOMMANDATION'])->only([
            'getPagePlaningRecos', 'changerModeSuivi', 'getPageActiviteReco'
        ]);
        $this->middleware(['permission:SUIVI_RECOMMANDATION'])->only([
            'getPageSuiviRecos', 'doSuiviReco'
        ]);
        $this->middleware(['permission:VALIDER_PLANIFICATION_RECO'])->only([
            'validerPlanningReco'
        ]);
    }

    /**
     * Display a listing of the Recommandations.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $recommandations = $this->recommandationsRepository->all();

        return view('backend.recommandations.index')
            ->with('recommandations', $recommandations);
    }

    /**
     * Show the form for creating a new Recommandations.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.recommandations.create');
    }

    /**
     * Store a newly created Recommandations in storage.
     *
     * @param CreateRecommandationsRequest $request
     *
     * @return Response
     */
    public function store(CreateRecommandationsRequest $request)
    {
        $input = $request->all();

        $recommandations = $this->recommandationsRepository->create($input);

        Flash::success('Recommandations saved successfully.');

        return redirect(route('recommandations.index'));
    }

    /**
     * Display the specified Recommandations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recommandations = $this->recommandationsRepository->find($id);

        if (empty($recommandations)) {
            Flash::error('Recommandations not found');

            return redirect(route('recommandations.index'));
        }

        return view('backend.recommandations.show')->with('recommandations', $recommandations);
    }

    /**
     * Show the form for editing the specified Recommandations.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recommandations = $this->recommandationsRepository->find($id);

        if (empty($recommandations)) {
            Flash::error('Recommandations not found');

            return redirect(route('recommandations.index'));
        }

        return view('backend.recommandations.edit')->with('recommandations', $recommandations);
    }

    /**
     * Update the specified Recommandations in storage.
     *
     * @param int $id
     * @param UpdateRecommandationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecommandationsRequest $request)
    {
        $recommandations = $this->recommandationsRepository->find($id);

        if (empty($recommandations)) {
            Flash::error('Recommandations not found');

            return redirect(route('recommandations.index'));
        }

        $recommandations = $this->recommandationsRepository->update($request->all(), $id);

        Flash::success('Recommandations updated successfully.');

        return redirect(route('recommandations.index'));
    }

    /**
     * Remove the specified Recommandations from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recommandations = $this->recommandationsRepository->find($id);
        if (empty($recommandations)) {
            Flash::error('Recommandations not found');
            return redirect(route('recommandations.index'));
        }
        $this->recommandationsRepository->delete($id);
        Flash::success('Recommandations deleted successfully.');
        return redirect(route('recommandations.index'));
    }

    /** ****************************************** PERSONNALISER *********************************************** */
    public function getPageSaisieRecos()
    {
        $mode = 'SAISIE';
        $statut = 0;
        $instance_id = 0;
        $session_id = 0;
        $annee = 0;
        $sessions = [];
        $instances = DB::table('instances')->get();
        /** @var $recos : recommandations en cours de realisation */
        $recos = DB::table('recommandations')
            ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->where('recommandations.rec_statut', '=', 1)
            ->orWhere('recommandations.rec_statut', '=', 3)
            ->get();
        return view('backend.recommandations.index', compact(
            'recos',
            'instances',
            'statut',
            'instance_id',
            'session_id',
            'annee',
            'sessions',
            'mode'
        ));
    }

    public function getPagePlaningRecos()
    {
        $user = auth()->user();
        $mode = 'PLAN';
        $statut = 0;
        $instance_id = 0;
        $session_id = 0;
        $annee = 0;
        $sessions = [];
        $instances = DB::table('instances')
            ->join('acteurs', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->where('acteurs.str_id', '=', $user->str_id)
            ->get('instances.*');
        /** @var $recos : recommandations en cours de realisation  */
        $recos = DB::table('recommandations')
            ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
            ->where([
                ['recommandations.rec_statut', '=', 1],
                ['partenaires.str_id', '=', $user->str_id],
                ['partenaires.ptn_is_responsable', '=', 1]
            ])
            ->orWhere([
                ['recommandations.rec_statut', '=', 3],
                ['partenaires.str_id', '=', $user->str_id],
                ['partenaires.ptn_is_responsable', '=', 1]
            ])
            ->get();
        return view('backend.recommandations.index', compact(
            'recos',
            'instances',
            'statut',
            'instance_id',
            'session_id',
            'annee',
            'sessions',
            'mode'
        ));
    }

    public function getPageSuiviRecos()
    {
        $user = auth()->user();
        $mode = 'SUIVI';
        $statut = 0;
        $instance_id = 0;
        $session_id = 0;
        $annee = 0;
        $sessions = [];
        $instances = DB::table('instances')
            ->join('acteurs', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->where('acteurs.str_id', '=', $user->str_id)
            ->get('instances.*');
        /** @var $recos : recommandations en cours de realisation */
        $recos = DB::table('recommandations')
            ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
            ->where([
                ['recommandations.rec_statut', '=', 1],
                ['partenaires.str_id', '=', $user->str_id],
                ['partenaires.ptn_is_responsable', '=', 1]
            ])
            ->orWhere([
                ['recommandations.rec_statut', '=', 3],
                ['partenaires.str_id', '=', $user->str_id],
                ['partenaires.ptn_is_responsable', '=', 1]
            ])
            ->get();
        return view('backend.recommandations.index', compact(
            'recos',
            'instances',
            'statut',
            'instance_id',
            'session_id',
            'annee',
            'sessions',
            'mode'
        ));
    }

    public function getPageRecosBySession($id_session)
    {
        if ($id_session != 'null') {
            $session = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where('ses_id', $id_session)
                ->first();
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where('recommandations.ses_id', '=', $id_session)->get();
        } else {
            $session = null;
            $recos = null;
        }
        return view('backend.recommandations.reco_session', compact('session', 'recos'));
    }

    public function getPageActiviteReco($id_reco)
    {
        $reco = DB::table('recommandations')
            ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->where('recommandations.rec_id', $id_reco)->first();
        $partenaires = DB::table('recommandations')
            ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
            ->join('structures', 'partenaires.str_id', '=', 'structures.str_id')
            ->where('partenaires.rec_id', $id_reco)
            ->orderBy('structures.str_sigle', 'asc')
            ->get();
        $activites = DB::table('activites')
            ->join('partenaires', 'activites.ptn_id', '=', 'partenaires.ptn_id')
            ->join('structures', 'partenaires.str_id', '=', 'structures.str_id')
            ->where('activites.rec_id', $id_reco)->get();
        return view('backend.recommandations.suivi_activites', compact('reco', 'partenaires', 'activites'));
    }

    public function getReco($id)
    {
        $reco = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('programme_budgetaire as pb', 'rec.bud_id', '=', 'pb.bud_id')
            ->where('rec.rec_id', '=', $id)
            ->select(
                'rec.*',
                'ins.ins_sigle',
                'ins.ins_nom_complet',
                'ins.ins_periodicite',
                'ins.ins_description',
                'ses.ses_description',
                'ses.ins_id',
                'ses.ses_statut',
                'ses.ses_annee',
                'ses.ses_date_debut',
                'ses.ses_date_fin',
                'ses.ses_lieu',
                'ses.ses_datefin_planification',
                'ses.ses_datefin_realisation',
                'pb.bud_intitule'
            )
            ->first();
        if (empty($reco)) {
            return null;
        }
        return response()->json($reco);
    }

    public function getAllRecos()
    {
        $recos = DB::table('recommandations')->get();
        if (empty($recos)) {
            return [];
        }
        return response()->json($recos);
    }

    public function saveReco()
    {
        $user = auth()->user();
        $check = MethodesStaticController::checkLibelleReco(request('ses_id'), request('rec_intitule'));
        if (request('rec_id') == null) {
            $reco = new Recommandations();
            $reco->ses_id = request('ses_id');
            $reco->bud_id = request('bud_id');
            $reco->rec_intitule = request('rec_intitule');
            $reco->rec_date_echeance = request('rec_date_echeance');
            $reco->rec_statut = 1;
            $reco->rec_is_mode_standard = 0;
            $reco->rec_personne_formule = request('rec_personne_formule');
            $reco->rec_personne_tel = request('rec_personne_tel');
            $reco->rec_is_permanente = request('rec_is_permanente');
            $reco->created_by = $user->email;
            $reco->updated_by = $user->email;
            if ($check == null) {
                try {
                    $reco->save();
                    Notify::success("Recommandation ajoutée avec succès", "Opération reussie", $options = []);
                } catch (\PDOException $ex) {
                    if (intval($ex->getCode()) == 23000) {
                        Notify::error("Erreur de données veuillez contacter le responsable système, merci", "Opération échouée", $options = []);
                    }
                }
            } else {
                Notify::error("Intitulé de la recommandation existe déjà, merci de réessayer", "Opération échouée", $options = []);
            }
        } else {
            $reco = DB::table('recommandations')->where('rec_id', request('rec_id'))->first();
            if ($check == null || (!empty($check) && $reco->rec_intitule == request('rec_intitule'))) {
                try {
                    $result = DB::table('recommandations')
                        ->where('rec_id', request('rec_id'))
                        ->update([
                            'ses_id' => request('ses_id'),
                            'bud_id' => request('bud_id'),
                            'rec_intitule' => request('rec_intitule'),
                            'rec_date_echeance' => request('rec_date_echeance'),
                            'rec_personne_formule' => request('rec_personne_formule'),
                            'rec_personne_tel' => request('rec_personne_tel'),
                            'rec_is_permanente' => request('rec_is_permanente'),
                            'updated_at' => Carbon::now(),
                            'updated_by' => $user->email
                        ]);
                    if (!empty($result)) {
                        activity('Recommandation')->log('Modification');
                        Notify::success("Recommandation modifiée avec succès", "Opération reussie", $options = []);
                    }
                } catch (\PDOException $ex) {
                    if (intval($ex->getCode()) == 23000) {
                        Notify::error("Erreur de données veuillez contacter le responsable système, merci", "Opération échouée", $options = []);
                    }
                }
            } else {
                Notify::error("Intitulé de la recommandation existe déjà, merci de réessayer", "Opération échouée", $options = []);
            }
        }
        return redirect(route('getPageSaisieRecos'));
    }

    public function modifierObservationReco()
    {
        $user = auth()->user();
        $reco = DB::table('recommandations')->where('rec_id', request('rec_id'))->first();
        if (empty($reco)) {
            Notify::error("Recommandation non trouvé", "Opération échouée", $options = []);
            return redirect(route('getPageSuiviRecos'));
        }
        try {
            $result = DB::table('recommandations')
                ->where('rec_id', request('rec_id'))
                ->update([
                    'rec_observation' => request('rec_observation'),
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            activity('Recommandation')->log('Modification observation');
            if (!empty($result)) {
                activity('Recommandation')->log('Modification de l\'observation');
                Notify::success("Observation de la recommandation modifiée avec succès", "Opération reussie", $options = []);
            }
        } catch (\PDOException $ex) {
            if (intval($ex->getCode()) == 23000) {
                Notify::error("Erreur survenu lors de la modification veuillez contacter le responsable système, merci", "Opération échouée", $options = []);
            }
        }
        return redirect(route('getPageSuiviRecos'));
    }

    public function deleteReco($id)
    {
        $reco = DB::table('recommandations')->where('rec_id', $id)->first();
        if (empty($reco)) {
            return redirect(route('recommandations.index'));
        }
        try {
            $result = DB::table('recommandations')->where('rec_id', '=', intval($reco->rec_id))->delete();
            if (!empty($result)) {
                activity('Recommandation')->log('Suppression');
                return 1;
            }
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    public function validerPlanningReco($id)
    {
        $reco = DB::table('recommandations')->where('rec_id', $id)->first();
        if (!empty($reco)) {
            $user = auth()->user();
            /** si le mode de suivi de la reco est par taux */
            if ($reco->rec_is_mode_standard == 0) {
                $sommePoids = DB::table('activites as act')
                    ->join('recommandations as rec', 'act.rec_id', '=', 'rec.rec_id')
                    ->where('rec.rec_id', '=', $reco->rec_id)
                    ->select(DB::raw('SUM(act.act_poids) as sumPoids'))
                    ->first();
                if ($sommePoids->sumPoids < 100) {
                    return ['status' => 'moins_cent'];
                } else {
                    $result = DB::table('recommandations')
                        ->where('rec_id', $id)
                        ->update([
                            'rec_is_planning_valide' => 1,
                            'rec_dh_planning_valide' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'updated_by' => $user->email
                        ]);
                    if (!empty($result)) {
                        DB::table('activites')
                            ->where('rec_id', $reco->rec_id)
                            ->update([
                                'act_is_valide' => 1,
                                'updated_at' => Carbon::now(),
                                'updated_by' => $user->email
                            ]);
                        activity('Recommandation')->log('Validation de la planification');
                        return ['status' => 'success'];
                    }
                }
            } else {
                $result = DB::table('recommandations')
                    ->where('rec_id', $id)
                    ->update([
                        'rec_is_planning_valide' => 1,
                        'rec_dh_planning_valide' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($result)) {
                    activity('Recommandation')->log('Validation de la planification');
                    return ['status' => 'success'];
                }
            }
        } else {
            return ['status' => 'not_found'];
        }
    }

    public function validerSuiviReco($id)
    {
        $reco = DB::table('recommandations')->where('rec_id', $id)->first();
        if (!empty($reco)) {
            $user = auth()->user();
            $result = DB::table('recommandations')
                ->where('rec_id', $id)
                ->update([
                    'rec_is_suivi_valide' => 1,
                    'rec_dh_valide' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            if (!empty($result)) {
                activity('Recommandation')->log('Validation du suivi');
            }
            return 0;
        } else {
            return 1;
        }
    }

    public function abandonnerReco($id, $motif)
    {
        $reco = DB::table('recommandations')->where('rec_id', $id)->first();
        if (!empty($reco)) {
            $user = auth()->user();
            $result = DB::table('recommandations')
                ->where('rec_id', $id)
                ->update([
                    'rec_statut' => 5,
                    'rec_motif_abandon' => $motif,
                    'rec_dh_abandon' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            if (!empty($result)) {
                activity('Recommandation')->log('Abandon');
            }
            return 0;
        } else {
            return 1;
        }
    }

    public function changerModeSuivi($id, $value)
    {
        $value = intval($value);
        $user = auth()->user();
        if (!is_null($value)) {
            $result = DB::table('recommandations')
                ->where('rec_id', $id)
                ->update([
                    'rec_is_mode_standard' => $value,
                    'updated_at' => Carbon::now(),
                    'updated_by' => $user->email
                ]);
            if (!empty($result)) {
                activity('Recommandation')->log('Abandon');
                return $result;
            }
        } else {
            return null;
        }
    }

    public function recosSearch()
    {
        $statut = intval(request('fil_status'));
        $instance_id = intval(request('fil_instance'));
        $session_id = intval(request('fil_session'));
        $annee = intval(request('fil_annee'));
        $mode = request('mode');
        $sessions = [];
        $instances = DB::table('instances')->get();

        /** afficher les recommendations selon le statut choisi */
        if ($statut && !$instance_id && !$session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut]
                ])->get();
        } else if (!$statut && $instance_id && !$session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id]
                ])->get();
        } else if (!$statut && !$instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_id', '=', $session_id]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if (!$statut && !$instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
        } else if ($statut && $instance_id && !$session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id]
                ])->get();
        }

        /** afficher les recommendations selon le statut & la session choisis */
        else if ($statut && !$instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['sessions.ses_id', '=', $session_id]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if ($statut && !$instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
        } else if (!$statut && $instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if (!$statut && $instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
        } else if (!$statut && !$instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if ($statut && $instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if ($statut && $instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
        }

        /** afficher les recommendations selon le statut, la session et l'annee choisis */
        else if ($statut && !$instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if (!$statut && $instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if ($statut && $instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee]
                ])->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        }

        /** afficher les recommendations en cours par defaut */
        else {
            $statut = 0;
            $instance_id = 0;
            $session_id = 0;
            $annee = 0;
            $sessions = [];
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                //                ->leftJoin('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->get();
        }
        return view('backend.recommandations.index', compact(
            'recos',
            'instances',
            'statut',
            'instance_id',
            'session_id',
            'annee',
            'sessions',
            'mode'
        ));
    }

    public function recosSearchForUserConnect()
    {
        $user = auth()->user();
        $statut = intval(request('fil_status'));
        $instance_id = intval(request('fil_instance'));
        $session_id = intval(request('fil_session'));
        $annee = intval(request('fil_annee'));
        $mode = request('mode');
        $sessions = [];
        $instances = DB::table('instances')
            ->join('acteurs', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->where('acteurs.str_id', '=', $user->str_id)
            ->get('instances.*');

        /** afficher les recommendations selon le statut choisi */
        if ($statut && !$instance_id && !$session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if (!$statut && $instance_id && !$session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if (!$statut && !$instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['sessions.ses_id', '=', $session_id],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if (!$statut && !$instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        }
        /** *********************************************************/
        else if ($statut && $instance_id && !$session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if ($statut && !$instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['sessions.ses_id', '=', $session_id],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if ($statut && !$instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if (!$statut && $instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if (!$statut && $instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if (!$statut && !$instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        }
        /** *********************************************************/
        else if ($statut && $instance_id && $session_id && !$annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if ($statut && $instance_id && !$session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if ($statut && !$instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        } else if (!$statut && $instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else if ($statut && $instance_id && $session_id && $annee) {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['recommandations.rec_statut', '=', $statut],
                    ['instances.ins_id', '=', $instance_id],
                    ['sessions.ses_id', '=', $session_id],
                    ['sessions.ses_annee', '=', $annee],
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
        } else {
            $recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->join('partenaires', 'recommandations.rec_id', '=', 'partenaires.rec_id')
                ->where([
                    ['partenaires.str_id', '=', $user->str_id],
                    ['partenaires.ptn_is_responsable', '=', 1]
                ])
                ->get();
            $sessions = DB::table('sessions')->where('ins_id', $instance_id)->get();
        }

        return view('backend.recommandations.index', compact(
            'recos',
            'instances',
            'statut',
            'instance_id',
            'session_id',
            'annee',
            'sessions',
            'mode'
        ));
    }

    public function doSuiviReco($id, Request $request)
    {
        $reco = DB::table('recommandations')->where('rec_id', $id)->first();
        $user = auth()->user();
        $tailleMo = intval(config('app.taille_max_upload')) / (1024 * 1024);
        if (!empty($reco)) {
            if (request('rec_statut') == 2) {
                $tauxRealisation = 0;
                $champ_date = 'rec_dh_nonrealise';
                $value_date = Carbon::now();
                $champ_preuve = 'rec_preuve_nonrealise';
                $champ_obs = 'rec_obs_nonrealise';
                $preuve = $reco->rec_preuve_nonrealise;
            } else if (request('rec_statut') == 3) {
                $tauxRealisation = 0;
                $champ_date = 'rec_dh_en_cours';
                $value_date = Carbon::now();
                $champ_preuve = 'rec_preuve_encours';
                $champ_obs = 'rec_obs_encours';
                $preuve = $reco->rec_preuve_encours;
            } else if (request('rec_statut') == 4) {
                $tauxRealisation = 100;
                $champ_date = 'rec_dh_realise';
                $value_date = Carbon::now();
                $champ_preuve = 'rec_preuve_realise';
                $champ_obs = 'rec_obs_realise';
                $preuve = $reco->rec_preuve_realise;
            } else {
                $tauxRealisation = 0;
                $champ_date = 'rec_dh_en_cours';
                $value_date = null;
                $champ_preuve = 'rec_preuve_nonrealise';
                $champ_obs = 'rec_obs_nonrealise';
                $preuve = $reco->rec_preuve_nonrealise;
            }
            if (!empty(request('preuve'))) {
                $files = $request->file('preuve');
                if ($files) {
                    $ext = $files->getClientOriginalExtension();
                    // taille max autorisée 4 MB (converti en byte)
                    if ($files->getSize() < intval(config('app.taille_max_upload'))) {
                        if (
                            strtolower($ext) == 'pdf' || strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg'
                            || strtolower($ext) == 'doc' || strtolower($ext) == 'docx' || strtolower($ext) == 'xls' || strtolower($ext) == 'xlsx'
                        ) {
                            $chemin = public_path() . "/pj_recos/";
                            $name = 'RECO_' . $reco->rec_id . '_' . request('rec_statut') . '_' . Carbon::now()->format('d_M_Y_H_i_s') . '.' . $ext;
                            if (!is_null($preuve)) {
                                $pathToDelete = $chemin . '' . $preuve;
                                File::delete($pathToDelete);
                            }
                            $request->preuve->move($chemin, $name);
                            try {
                                $result = DB::table('recommandations')
                                    ->where('rec_id', $reco->rec_id)
                                    ->update([
                                        'rec_statut' => request('rec_statut'),
                                        $champ_obs => request('observation'),
                                        'rec_taux_realise' => $tauxRealisation,
                                        $champ_date => $value_date,
                                        $champ_preuve => $name,
                                        'updated_at' => Carbon::now(),
                                        'updated_by' => $user->email
                                    ]);
                                if (!empty($result)) {
                                    activity('Recommandation')->log('Suivi de mise en oeuvre');
                                    Notify::success("Suivi de la mise en œuvre de la recommandation fait avec succès", "Opération reussie", $options = []);
                                    return redirect(route('getPageSuiviRecos'));
                                }
                            } catch (PostTooLargeException $ex) {
                                Notify::error("Le fichier ne doit pas dépassé " . $tailleMo . "Mo", "Opération échoué", $options = []);
                                return redirect(route('getPageSuiviRecos'));
                            } catch (QueryException $ex) {
                            }
                        } else {
                            Notify::error("Extension du fichier non valide, extension souhaité : pdf, png ou jpg", "Opération échoué", $options = []);
                            return redirect(route('getPageSuiviRecos'));
                        }
                    } else {
                        Notify::error("Le fichier ne doit pas dépassé " . $tailleMo . "Mo", "Opération échoué", $options = []);
                        return redirect(route('getPageSuiviRecos'));
                    }
                } else {
                    Notify::error("Veuillez selectionner une pièce jointe", "Opération échoué", $options = []);
                    return redirect(route('getPageSuiviRecos'));
                }
            } else {
                $result = DB::table('recommandations')
                    ->where('rec_id', $reco->rec_id)
                    ->update([
                        'rec_statut' => request('rec_statut'),
                        $champ_obs => request('observation'),
                        'rec_taux_realise' => $tauxRealisation,
                        $champ_date => $value_date,
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($result)) {
                    activity('Recommandation')->log('Suivi de mise en oeuvre');
                    Notify::success("Suivi de la mise en œuvre de la recommandation fait avec succès", "Opération reussie", $options = []);
                    return redirect(route('getPageSuiviRecos'));
                }
            }
        }
    }

    public function importer(Request $request)
    {
        if ($request->hasFile('fichier')) {
            $ext = $request->fichier->getClientOriginalExtension();
            $file = $request->file('fichier');
            if ($file->getSize() < intval(config('app.taille_max_upload'))) {
                if (strtolower($ext) == 'xlsx') {
                    $import = new ImportRecos();
                    $import->onlySheets(0);
                    Excel::import($import, $file);
                    activity('Recommandation')->log('Importation');
                    return ['status' => 'success'];
                } else {
                    return ['status' => 'ext-no-valide'];
                }
            } else {
                return ['status' => 'taille-no-valide'];
            }
        } else {
            return ['status' => 'no-file'];
        }
    }

    public function downloadTemplateImport()
    {
        $oldEncoding = '';
        if (function_exists('mb_internal_encoding')) {
            $oldEncoding = mb_internal_encoding();
            mb_internal_encoding('latin1');
        }

        $user = auth()->user();

        $spreadsheet = new \PHPExcel();
        $spreadsheet->createSheet();

        /** Feuille parametre */
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Parametre');

        /** @var $sessions : liste des sessions dont l'utilisateur connecté est CSS et en formulation */
        $sessions = DB::table('sessions')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->join('acteurs', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->where([
                ['acteurs.str_id', '=', $user->str_id],
                ['acteurs.act_is_css', '=', 1],
                ['sessions.ses_statut', '=', 1]
            ])
            ->orderBy('sessions.ses_description')
            ->get();

        /** @var $budges : programme budgetaire */
        $budges = DB::table('programme_budgetaire')->get();

        $sheet->setCellValue('A1', "Instance");
        $sheet->setCellValue('B1', "Année session");
        $sheet->setCellValue('C1', "Intitulé session");

        $ligne = 2;
        foreach ($sessions as $session) {
            $sheet->setCellValue('A' . $ligne, $session->ins_nom_complet . '(' . $session->ins_sigle . ')');
            $sheet->setCellValue('B' . $ligne, $session->ses_annee);
            $sheet->setCellValue('C' . $ligne, $session->ses_description);
            $ligne++;
        }

        $sheet->setCellValue('E1', "Is permanente");
        $sheet->setCellValue('E2', "Oui");
        $sheet->setCellValue('F2', 1);
        $sheet->setCellValue('E3', "Non");
        $sheet->setCellValue('F3', 0);
        $sheet->mergeCells("E1:F1");

        $sheet->setCellValue('H1', "Code Programme budgétaire");
        $sheet->setCellValue('I1', "Libellé Programme budgétaire");

        $lig = 2;
        foreach ($budges as $budge) {
            $sheet->setCellValue('H' . $lig, $budge->bud_code);
            $sheet->setCellValue('I' . $lig, $budge->bud_intitule);
            $lig++;
        }

        $this->setCellColor($sheet, "A1:C1", "58D3F7");
        $this->setCellColor($sheet, "E1:F1", "58D3F7");
        $this->setCellColor($sheet, "H1:I1", "58D3F7");

        $sheet->getProtection()->setSheet(true);

        /** Feuille Données à charger */
        $spreadsheet->setActiveSheetIndex(0);
        $sheet_1 = $spreadsheet->getActiveSheet();
        $sheet_1->setTitle('Données à charger');
        $sheet_1->setCellValue('A1', "session_description");
        $sheet_1->setCellValue('B1', "rec_intitule");
        $sheet_1->setCellValue('C1', "rec_date_echeance");
        $sheet_1->setCellValue('D1', "rec_personne_formule");
        $sheet_1->setCellValue('E1', "rec_personne_tel");
        $sheet_1->setCellValue('F1', "rec_is_permanente");
        $sheet_1->setCellValue('G1', "programme_budgetaire");

        $sheet_1->setCellValue('I2', "Champs obligatoire");
        $sheet_1->setCellValue('I3', "Champs optionnel");
        $sheet_1->setCellValue('I5', "NB: format de date");
        $sheet_1->setCellValue('J5', "dd/mm/aaaa");

        $this->setCellColor($sheet_1, "A1:G1", "FAAC28");
        $this->setCellColor($sheet_1, "J2", "FAAC28");
        $this->setCellColor($sheet_1, "J3", "58D3F7");
        $this->setCellColor($sheet_1, "I5:J5", "FF0000");

        $val_session = $sheet_1->getCell('A2')->getDataValidation();
        $val_session->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
        $val_session->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
        $val_session->setAllowBlank(true);
        $val_session->setShowInputMessage(true);
        $val_session->setShowErrorMessage(true);
        $val_session->setShowDropDown(true);
        $val_session->setErrorTitle('Donnée incorrecte');
        $val_session->setError('Description non trouvée');
        $val_session->setPromptTitle('Veuillez choisir une valeur dans la liste !');
        $val_session->setFormula1('=\'Parametre\'!$C$2:$C$' . ($ligne - 1));

        $val_permanent = $sheet_1->getCell('F2')->getDataValidation();
        $val_permanent->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
        $val_permanent->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
        $val_permanent->setAllowBlank(true);
        $val_permanent->setShowInputMessage(true);
        $val_permanent->setShowErrorMessage(true);
        $val_permanent->setShowDropDown(true);
        $val_permanent->setErrorTitle('Donnée incorrecte');
        $val_permanent->setError('Valeur non trouvée');
        $val_permanent->setPromptTitle('Veuillez choisir une valeur dans la liste !');
        $val_permanent->setFormula1('=\'Parametre\'!$F$2:$F$3');

        $val_budge = $sheet_1->getCell('G2')->getDataValidation();
        $val_budge->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
        $val_budge->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
        $val_budge->setAllowBlank(true);
        $val_budge->setShowInputMessage(true);
        $val_budge->setShowErrorMessage(true);
        $val_budge->setShowDropDown(true);
        $val_budge->setErrorTitle('Donnée incorrecte');
        $val_budge->setError('Valeur non trouvée');
        $val_budge->setPromptTitle('Veuillez choisir une valeur dans la liste !');
        $val_budge->setFormula1('=\'Parametre\'!$H$2:$H$' . ($lig - 1));

        $sheet_1->getProtection()->setSheet(true);
        for ($i = 2; $i < config('app.max_ligne_export_reco'); $i++) {
            $sheet_1->getCell('A' . $i)->setDataValidation(clone $val_session);
            $sheet_1->getCell('F' . $i)->setDataValidation(clone $val_permanent);
            $sheet_1->getCell('G' . $i)->setDataValidation(clone $val_budge);
            $sheet_1->getStyle('A' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
            $sheet_1->getStyle('B' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
            $sheet_1->getStyle('C' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
            $sheet_1->getStyle('D' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
            $sheet_1->getStyle('E' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
            $sheet_1->getStyle('F' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
            $sheet_1->getStyle('G' . $i)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
        }

        /** mettre la colonne date écheance en format text */
        $sheet_1->getStyle('C2:C52')
            ->getNumberFormat()
            ->setFormatCode(
                \PHPExcel_Style_NumberFormat::FORMAT_TEXT
            );

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="canevas_import_recos.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = \PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    public function setCellColor($sheet, $cell, $color)
    {
        $sheet->getStyle($cell)->applyFromArray(
            array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => $color)
                )
            )
        );
    }

    public function getChartDataByInstanceByStructure($ins_id)
    {
        $user = auth()->user();
        /** @var $preparations : Nbre de recommandations en preparation par instance
         * et pour la structure de l'utilisateur connecte
         */
        $preparations = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 1],
                ['ins.ins_id', '=', $ins_id],
                //                ['part.ptn_is_responsable', '=', 1],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $nonRealises : Nbre de recommandations non realisees par instance
         * et pour la structure de l'utilisateur connecte
         */
        $nonRealises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 2],
                ['ins.ins_id', '=', $ins_id],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $enCours : Nbre de recommandations en cours par instance
         * et pour la structure de l'utilisateur connecte
         */
        $enCours = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 3],
                ['ins.ins_id', '=', $ins_id],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $realises : Nbre de recommandations realisees par instance
         * et pour la structure de l'utilisateur connecte
         */
        $realises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 4],
                ['ins.ins_id', '=', $ins_id],
                //                ['part.ptn_is_responsable', '=', 1],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $abandonnes : Nbre de recommandations abandonnees par instance
         * et pour la structure de l'utilisateur connecte
         */
        $abandonnes = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 5],
                ['ins.ins_id', '=', $ins_id],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        return compact('preparations', 'nonRealises', 'enCours', 'realises', 'abandonnes');
    }

    public function getChartDataAllInstanceByStructure()
    {
        $user = auth()->user();
        /** @var $preparations : Nbre de recommandations En preparation de toute les instances
         * et pour la structure de l'utilisateur connecte
         */
        $preparations = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 1],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $nonRealises : Nbre de recommandations non realisees de toute les instances
         * et pour la structure de l'utilisateur connecte
         */
        $nonRealises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 2],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $enCours : Nbre de recommandations en cours de toute les instances
         * et pour la structure de l'utilisateur connecte
         */
        $enCours = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 3],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $realises : Nbre de recommandations realisees de toute les instances
         * et pour la structure de l'utilisateur connecte
         */
        $realises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 4],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        /** @var $abandonnes : Nbre de recommandations abandonnees de toute les instances
         * et pour la structure de l'utilisateur connecte
         */
        $abandonnes = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->join('partenaires as part', 'rec.rec_id', '=', 'part.rec_id')
            ->where([
                ['rec.rec_statut', '=', 5],
                ['part.str_id', '=', $user->str_id]
            ])
            ->count();

        return compact('preparations', 'nonRealises', 'enCours', 'realises', 'abandonnes');
    }

    public function getChartDataByInstance($ins_id)
    {
        /** @var $preparations : Nbre de recommandations en preparations par instance pour toutes les structures */
        $preparations = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['ins.ins_id', '=', $ins_id],
                ['rec.rec_statut', '=', 1]
            ])
            ->count();

        /** @var $nonRealises : Nbre de recommandations non realisees par instance pour toutes les structures */
        $nonRealises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['ins.ins_id', '=', $ins_id],
                ['rec.rec_statut', '=', 2]
            ])
            ->count();

        /** @var $enCours : Nbre de recommandations en cours par instance pour toutes les structures */
        $enCours = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['ins.ins_id', '=', $ins_id],
                ['rec.rec_statut', '=', 3]
            ])
            ->count();

        /** @var $realises : Nbre de recommandations realisees par instance pour toutes les structures */
        $realises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['ins.ins_id', '=', $ins_id],
                ['rec.rec_statut', '=', 4]
            ])
            ->count();

        /** @var $abandonnes : Nbre de recommandations abandonnees par instance pour toutes les structures */
        $abandonnes = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['ins.ins_id', '=', $ins_id],
                ['rec.rec_statut', '=', 5]
            ])
            ->count();

        return compact('preparations', 'nonRealises', 'enCours', 'realises', 'abandonnes');
    }

    public function getChartDataAllInstance()
    {
        /** @var $preparations : Nbre de recommandations en preparation pour toutes les instances */
        $preparations = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['rec.rec_statut', '=', 1]
            ])
            ->count();

        /** @var $nonRealises : Nbre de recommandations non realisees pour toutes les instances */
        $nonRealises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['rec.rec_statut', '=', 2]
            ])
            ->count();

        /** @var $enCours : Nbre de recommandations en cours pour toutes les instances */
        $enCours = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['rec.rec_statut', '=', 3]
            ])
            ->count();

        /** @var $realises : Nbre de recommandations realisees pour toutes les instances */
        $realises = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['rec.rec_statut', '=', 4]
            ])
            ->count();

        /** @var $abandonnes : Nbre de recommandations abandonnees pour toutes les instances */
        $abandonnes = DB::table('recommandations as rec')
            ->join('sessions as ses', 'rec.ses_id', '=', 'ses.ses_id')
            ->join('instances as ins', 'ses.ins_id', '=', 'ins.ins_id')
            ->where([
                ['rec.rec_statut', '=', 5]
            ])
            ->count();

        return compact('preparations', 'nonRealises', 'enCours', 'realises', 'abandonnes');
    }

    public function reload(Request $request)
    {
        $programmeBudgetaires = DB::table('programme_budgetaire')->orderBy('bud_annee')->get();
        if (empty($programmeBudgetaires)) {
            Flash::info('Aucun programme budgétaire trouvé')->important();
        }
        return view('backend.programme_budgetaires.table', compact('programmeBudgetaires'));
    }
}
