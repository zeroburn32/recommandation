<?php

namespace App\Http\Controllers;

use Flashy;
use Response;
 use App\Models\Roles;
use App\Models\Permissions; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Helmesvs\Notify\Facades\Notify;
use App\Repositories\RolesRepository;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\CreateRolesRequest;
use App\Http\Requests\UpdateRolesRequest;
use App\Http\Controllers\AppBaseController;

class RolesController extends AppBaseController
{
    /** @var  RolesRepository */
    private $rolesRepository;

    public function __construct(RolesRepository $rolesRepo)
    {
        $this->rolesRepository = $rolesRepo;
    }

    /**
     * Display a listing of the Roles.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $roles = $this->rolesRepository->all();

        return view('backend.roles.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new Roles.
     *
     * @return Response
     */
    public function create()
    {
        $permissions = Permission::select('id', 'name', 'label')->get();
        return view('backend.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created Roles in storage.
     *
     * @param CreateRolesRequest $request
     *
     * @return Response
     */
    public function store(CreateRolesRequest $request)
    {
        try {
            $this->validate($request, ['name' => 'required']);

            $inputs = $request->except('permissions');
            $role = Role::create($inputs);
            //$role->permissions()->detach();

            if ($request->has('permissions')) {
                foreach ($request->permissions as $permission_name) {
                    $permission =  Permission::findByName($permission_name);
                    $role->givePermissionTo($permission);
                }
            }

            Notify::success("Profil ajouté avec succès", "Opération reussie", $options = []);
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                Notify::error("Cet profil existe déjà!", "Echec", $options = []);
            } else {
                Notify::error("Echec lors de la création du profil. Veuillez réessayer!", "Echec", $options = []);
            }
        } catch (\Spatie\Permission\Exceptions\RoleAlreadyExists $ex1 ) {
            Notify::error("Cet profil existe déjà!", "Echec", $options = []);
        }
        

        return redirect(route('roles.index'));
    }

    /**
     * Display the specified Roles.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roles = $this->rolesRepository->find($id);

        if (empty($roles)) {
            Notify::error("Profil non trouvé", "Echec !", $options = []);

            return redirect(route('roles.index'));
        }

        return view('backend.roles.show')->with('roles', $roles);
    }

    /**
     * Show the form for editing the specified Roles.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = Roles::findOrFail($id);
        $permissions = DB::table('permissions')
            ->leftJoin('role_has_permissions as pr', function ($join) use ($id) {
                $join->on('permissions.id', '=', 'pr.permission_id')
                    ->where('pr.role_id', '=', $id);
            })
            ->select('permissions.*', 'pr.role_id as id_role')
            ->orderBy('permissions.name', 'asc')
            ->get();

        return view('backend.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified Roles in storage.
     *
     * @param int $id
     * @param UpdateRolesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRolesRequest $request)
    {
        try {
            $role = Role::findOrFail($id);
            DB::table('roles')->where('id', $role->id)->limit(1)
                ->update(array('name' => $request->name, 'label' => $request->label));

            $role->permissions()->detach();

            if ($request->has('permissions')) {
                foreach ($request->permissions as $permission_name) {
                    $permission =  Permission::findByName($permission_name);
                    $role->givePermissionTo($permission);
                }
            }


            Notify::success("Profil mis à jour avec succès", "Opération reussie", $options = []);
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                Notify::error("Cet profil existe déjà!", "Echec", $options = []);
            } else {
                Notify::error("Echec lors de la mise à jour du profil. Veuillez réessayer!", "Echec", $options = []);
            }
        }
        
        return redirect(route('roles.index'));
    }

    /**
     * Remove the specified Roles from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roles = $this->rolesRepository->find($id);

        if (empty($roles)) {
            Flashy::error('Roles not found');

            return redirect(route('roles.index'));
        }

        $this->rolesRepository->delete($id);

        Flashy::success('Roles deleted successfully.');

        return redirect(route('roles.index'));
    }
    public function supprimerProfil($id)
    {
        $role = DB::table('roles')->where('id', $id)->first();
        if (empty($role)) {
            return redirect(route('roles.index'));
        }
        try {
            DB::table('roles')->where('id', '=', intval($role->id))->delete();
            Notify::success("Profil supprimé avec succès", "Opération reussie", $options = []);
            return 1;
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }
}
