<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSessionRequest;
use App\Http\Requests\UpdateSessionRequest;
use App\Models\ListeAlertes;
use App\Models\Recommandations;
use App\Models\Session;
use App\Repositories\SessionRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Activites;
use App\Models\Partenaires;
use Carbon\Carbon;
use Doctrine\DBAL\Driver\PDOException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;
use Notify;

class SessionController extends AppBaseController
{
    /** @var  SessionRepository */
    private $sessionRepository;

    public function __construct(SessionRepository $sessionRepo)
    {
        $this->sessionRepository = $sessionRepo;

        /** Middleware */
        $this->middleware(['permission:GERER_SESSION'])->only([
            'index', 'getSession', 'getAllSessions', 'getAllSessionsEnFormulation', 'saveSession',
            'deleteSession', 'joindreListParticipant', 'sessionsSearch', 'getRecosAreconduire'
        ]);
        $this->middleware(['permission:RECONDUIRE_RECOMMANDATION'])->only([
            'getRecosAreconduire', 'reconduireRecos'
        ]);
        $this->middleware(['permission:VALIDER_FORMULATION', 'permission:VALIDER_PLANIFICATION', 'permission:VALIDER_REALISATION'])->only([
            'structuresWhereRecoNotPlan', 'changeStatus'
        ]);
    }

    /**
     * Display a listing of the Session.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $statut = 0;
        $instance_id = 0;
        $annee = 0;
        $sessions = DB::table('sessions')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->where('sessions.ses_statut', '=', 1)
            ->orWhere('sessions.ses_statut', '=', 2)
            ->orderBy('sessions.ses_date_debut', 'asc')
            ->get();
        $instances = DB::table('instances')->get();
        return view('backend.sessions.index', compact('sessions', 'instances', 'statut', 'instance_id', 'annee'));
    }

    /**
     * Show the form for creating a new Session.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.sessions.create');
    }

    /**
     * Store a newly created Session in storage.
     *
     * @param CreateSessionRequest $request
     *
     * @return Response
     */
    public function store(CreateSessionRequest $request)
    {
        $input = $request->all();

        $session = $this->sessionRepository->create($input);

        Flash::success('Session saved successfully.');

        return redirect(route('sessions.index'));
    }

    /**
     * Display the specified Session.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $session = $this->sessionRepository->find($id);

        if (empty($session)) {
            Flash::error('Session not found');

            return redirect(route('sessions.index'));
        }

        return view('backend.sessions.show')->with('session', $session);
    }

    /**
     * Show the form for editing the specified Session.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $session = $this->sessionRepository->find($id);

        if (empty($session)) {
            Flash::error('Session not found');

            return redirect(route('sessions.index'));
        }

        return view('backend.sessions.edit')->with('session', $session);
    }

    /**
     * Update the specified Session in storage.
     *
     * @param int $id
     * @param UpdateSessionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSessionRequest $request)
    {
        $session = $this->sessionRepository->find($id);

        if (empty($session)) {
            Flash::error('Session not found');

            return redirect(route('sessions.index'));
        }

        $session = $this->sessionRepository->update($request->all(), $id);

        Flash::success('Session updated successfully.');

        return redirect(route('sessions.index'));
    }

    /**
     * Remove the specified Session from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $session = $this->sessionRepository->find($id);

        if (empty($session)) {
            Flash::error('Session not found');

            return redirect(route('sessions.index'));
        }

        $this->sessionRepository->delete($id);

        Flash::success('Session deleted successfully.');

        return redirect(route('sessions.index'));
    }

    /** ******************************************PERSONNALISER*********************************************** */
    public function getSession($id)
    {
        $session = DB::table('sessions')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->select('instances.ins_sigle', 'instances.ins_nom_complet', 'sessions.*')
            ->where('sessions.ses_id', $id)->first();
        if (empty($session)) {
            return null;
        }
        return response()->json($session);
    }

    public function getAllSessions()
    {
        $sessions = DB::table('sessions')->get();
        if (empty($sessions)) {
            return [];
        }
        return response()->json($sessions);
    }

    public function getAllSessionsEnFormulation()
    {
        $sessions = DB::table('sessions')->where('ses_statut', '=', 1)->get();
        if (empty($sessions)) {
            return [];
        }
        return $sessions;
    }

    public function saveSession()
    {
        $user = auth()->user();
        $ses_id_precedant = null;
        /** Ajout */
        if (request('ses_id') == null) {
            /** Recuperation de la date début la plus grande de l'instance de la session*/
            $maxDateDebut = DB::table('sessions')
                ->select(DB::raw('MAX(ses_date_debut) as ses_date_debut'))
                ->where('ins_id', '=', request('ins_id'))
                ->first();
            /** Recuperation de la session precedante */
            $lastSession = DB::table('sessions')->where([
                ['ses_date_debut', '=', $maxDateDebut->ses_date_debut],
                ['ins_id', '=', request('ins_id')]
            ])->first();
            /** Recuperation des recommandations a reconduire de la derniere session, si la session a une precedante */
            if (!empty($lastSession)) {
                $ses_id_precedant = $lastSession->ses_id;
                /*$recoAreconduire = DB::table('recommandations as rec')
                    ->where([
                        ['rec.rec_statut', '<>', 4],
                        ['rec.rec_statut', '<>', 5],
                        ['rec.rec_is_reconduit', '=', 0],
                        ['rec.ses_id', '=', $ses_id_precedant]
                    ])
                    ->orWhere([
                        ['rec.rec_is_permanente', '=', 1],
                        ['rec.rec_is_reconduit', '=', 0],
                        ['rec.ses_id', '=', $ses_id_precedant]
                    ])->get();*/
            } else {
                $recoAreconduire = [];
                $ses_id_precedant = null;
            }
            /** Creation et enregistrement de la session */
            if (request('ses_date_debut') > request('ses_date_fin')) {
                Notify::error("La date fin session doit être supérieur à la date début de session", "Opération échouée", $options = []);
                return redirect(route('sessions.index'));
            }
            if (request('ses_date_fin') > request('ses_datefin_planification')) {
                Notify::error("La date fin planification doit être supéreieur à la date fin session", "Opération échouée", $options = []);
                return redirect(route('sessions.index'));
            }
            if (request('ses_datefin_planification') > request('ses_datefin_realisation')) {
                Notify::error("La date fin réalisation doit être supéreieur à la date planification", "Opération échouée", $options = []);
                return redirect(route('sessions.index'));
            }
            $session = new Session();
            $session->ins_id = request('ins_id');
            $session->ses_description = request('ses_description');
            $session->ses_annee = request('ses_annee');
            $session->ses_date_debut = request('ses_date_debut');
            $session->ses_lieu = request('ses_lieu');
            $session->ses_date_fin = request('ses_date_fin');
            $session->ses_statut = 1;
            $session->ses_id_precedante = $ses_id_precedant;
            $session->ses_datefin_planification = request('ses_datefin_planification');
            $session->ses_datefin_realisation = request('ses_datefin_realisation');
            $session->created_by = $user->email;
            $session->updated_by = $user->email;
            try {
                $session->save();
                Notify::success("Session ajoutée avec succès", "Opération reussie", $options = []);
                return redirect(route('sessions.index'));
                /** Enregistrement des recommandations a reconduire s'il existe au moins une */
                /*if (count($recoAreconduire) > 0) {
                    for ($i = 0; $i < count($recoAreconduire); $i++) {
                        $reco = new Recommandations();
                        $reco->ses_id = $session->ses_id;
                        $reco->rec_id_ref = $recoAreconduire[$i]->rec_id;
                        $reco->rec_intitule = $recoAreconduire[$i]->rec_intitule;
                        $reco->rec_date_echeance = $recoAreconduire[$i]->rec_date_echeance;
                        $reco->rec_statut = 1;
                        $reco->rec_is_suivi_valide = 0;
                        $reco->rec_is_mode_standard = $recoAreconduire[$i]->rec_is_mode_standard;
                        $reco->rec_personne_formule = $recoAreconduire[$i]->rec_personne_formule;
                        $reco->rec_personne_tel = $recoAreconduire[$i]->rec_personne_tel;
                        $reco->rec_is_permanente = $recoAreconduire[$i]->rec_is_permanente;
                        $reco->created_by = $user->email;
                        $reco->updated_by = $user->email;
                        $reco->save();
                        if ($reco->rec_id) {
                            DB::table('recommandations')
                                ->where('rec_id', $recoAreconduire[$i]->rec_id)
                                ->update([
                                    'rec_is_reconduit' => 1,
                                    'updated_at' => Carbon::now(),
                                    'updated_by' => $user->email
                                ]);
                        }
                    }
                }*/
            } catch (\PDOException $ex) {
                if (intval($ex->getCode()) == 23000) {
                    Notify::error("Intitulé de la session existe déjà, merci de réessayer", "Opération échouée", $options = []);
                    return redirect(route('sessions.index'));
                }
            }
        }
        /** Modification */
        else {
            if (request('ses_date_debut') > request('ses_date_fin')) {
                Notify::error("La date fin session doit être supérieur à la date début de session", "Opération échouée", $options = []);
                return redirect(route('sessions.index'));
            }
            if (request('ses_date_fin') > request('ses_datefin_planification')) {
                Notify::error("La date fin planification doit être supéreieur à la date fin session", "Opération échouée", $options = []);
                return redirect(route('sessions.index'));
            }
            if (request('ses_datefin_planification') > request('ses_datefin_realisation')) {
                Notify::error("La date fin réalisation doit être supéreieur à la date planification", "Opération échouée", $options = []);
                return redirect(route('sessions.index'));
            }
            try {
                $result = DB::table('sessions')
                    ->where('ses_id', request('ses_id'))
                    ->update([
                        'ins_id' => request('ins_id'),
                        'ses_description' => request('ses_description'),
                        'ses_annee' => request('ses_annee'),
                        'ses_date_debut' => request('ses_date_debut'),
                        'ses_lieu' => request('ses_lieu'),
                        'ses_date_fin' => request('ses_date_fin'),
                        'ses_datefin_planification' => request('ses_datefin_planification'),
                        'ses_datefin_realisation' => request('ses_datefin_realisation'),
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($result)) {
                    activity('Session')->log('Modification');
                    Notify::success("Session modifiée avec succès", "Opération reussie", $options = []);
                    return redirect(route('sessions.index'));
                }
            } catch (\PDOException $ex) {
                if (intval($ex->getCode()) == 23000) {
                    Notify::error("Intitulé de la session existe déjà, merci de réessayer", "Opération échouée", $options = []);
                    return redirect(route('sessions.index'));
                }
            }
        }
    }

    public function joindreListParticipant(Request $request)
    {
        $session_id = request('session_id');
        $session = DB::table('sessions')
            ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
            ->where('ses_id', $session_id)->first();
        if (!empty($session)) {
            $user = auth()->user();
            $files = $request->file('ses_liste_participants');
            $ext = $files->getClientOriginalExtension();
            // taille max autorisée 4 MB (converti en byte)
            if ($files->getSize() < intval(config('app.taille_max_upload'))) {
                if (
                    strtolower($ext) == 'pdf' || strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg'
                    || strtolower($ext) == 'doc' || strtolower($ext) == 'docx' || strtolower($ext) == 'xls' || strtolower($ext) == 'xlsx'
                ) {
                    $chemin = public_path() . "/list_participants/";
                    $list_name = $session->ins_sigle . '_' . Carbon::parse($session->ses_date_debut)->format('d_m_Y') . '.' . $ext;
                    $request->ses_liste_participants->move($chemin, $list_name);
                    try {
                        $result = DB::table('sessions')
                            ->where('ses_id', $session_id)
                            ->update([
                                'ses_liste_participants' => $list_name,
                                'updated_at' => Carbon::now(),
                                'updated_by' => $user->email
                            ]);
                        if (!empty($result)) {
                            activity('Session')->log('Liste des participants');
                            Notify::success("Liste des participants joint avec succès", "Opération reussie", $options = []);
                        }
                    } catch (PostTooLargeException $ex) {
                        Notify::error("Le fichier ne doit pas dépassé " . intval(config('app.taille_max_upload')) / (1024 * 1024) . "Mo", "Opération échoué", $options = []);
                    } catch (\PDOException $ex) {
                    }
                } else {
                    Notify::error("Extension du fichier non valide", "Opération échoué", $options = []);
                }
            } else {
                Notify::error("Le fichier ne doit pas dépassé " . intval(config('app.taille_max_upload')) / (1024 * 1024) . "Mo", "Opération échoué", $options = []);
            }
        } else {
            Notify::error("Session non trouvée", "Opération échoué", $options = []);
        }
        return redirect(route('sessions.index'));
    }

    public function deleteSession($id)
    {
        $session = DB::table('sessions')->where('ses_id', $id)->first();
        if (empty($session)) {
            return redirect(route('sessions.index'));
        }
        try {
            $result = DB::table('sessions')->where('ses_id', '=', intval($session->ses_id))->delete();
            if (!empty($result)) {
                activity('Session')->log('Modification');
                return 1;
            }
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    public function changeStatus($id, $value)
    {
        $user = auth()->user();
        $session = DB::table('sessions')->where('ses_id', $id)->first();
        if (!empty($session)) {
            /** @var  $result : maj du statut de la session */
            if ($value == 3) {
                $result = DB::table('sessions')
                    ->where('ses_id', $id)
                    ->update([
                        'ses_statut' => $value,
                        'ses_is_planning_valide' => 1,
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
            } else {
                $result = DB::table('sessions')
                    ->where('ses_id', $id)
                    ->update([
                        'ses_statut' => $value,
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
            }
            /** A la validation de la formulation */
            if (!empty($result) && $value == 2) {
                activity('Session')->log('Validation de la formulation');
            }

            /** A la validation de la planification */
            else if (!empty($result) && $value == 3) {
                /** @var  $rec : maj de chaque recommandation associee a non realise et planifier */
                $resultat = DB::table('recommandations')
                    ->where([
                        ['ses_id', '=', $session->ses_id],
                        ['rec_statut', '=', 1],
                        ['rec_is_planning_valide', '=', 1]
                    ])
                    ->update([
                        'rec_statut' => 2,
                        'rec_dh_nonrealise' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($resultat)) {
                    activity('Session')->log('Validation de la planification');
                }
            }

            /** A la validation de la realisation */
            else if (!empty($result) && $value == 4) {
                /** @var  $rec : maj de chaque recommandation associee a en cours et suivi valider */
                $resultat = DB::table('recommandations')
                    ->where([
                        ['ses_id', '=', $session->ses_id],
                        ['rec_is_suivi_valide', '=', 0]/*,
                        ['rec_taux_realise', '=', 100]*/
                    ])
                    ->update([
                        'rec_is_suivi_valide' => 1,
                        'rec_dh_valide' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'updated_by' => $user->email
                    ]);
                if (!empty($resultat)) {
                    activity('Session')->log('Validation de la réalisation');
                }
            }
            return $result;
        } else {
            return null;
        }
    }

    public function sessionsSearch()
    {
        $statut = intval(request('fil_status'));
        $instance_id = intval(request('fil_instance'));
        $annee = intval(request('fil_annee'));
        $instances = DB::table('instances')->get();
        /** Par instance */
        if ($instance_id != 0 && $statut == 0 && $annee == 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where('sessions.ins_id', '=', $instance_id)
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        /** Par instance et statut */
        else if ($instance_id != 0 && $statut != 0 && $annee == 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_statut', '=', $statut],
                    ['sessions.ins_id', '=', $instance_id]
                ])
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        /** Par instance, statut et annee */
        else if ($instance_id != 0 && $statut != 0 && $annee != 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_statut', '=', $statut],
                    ['sessions.ins_id', '=', $instance_id],
                    ['sessions.ses_annee', '=', $annee]
                ])
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        /** Par statut */
        else if ($instance_id == 0 && $statut != 0 && $annee == 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_statut', '=', $statut]
                ])
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        /** Par statut et annee */
        else if ($instance_id == 0 && $statut != 0 && $annee != 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_statut', '=', $statut],
                    ['sessions.ses_annee', '=', $annee]
                ])
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        /** Par annee */
        else if ($instance_id == 0 && $statut == 0 && $annee != 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ses_annee', '=', $annee]
                ])
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        /** Par instance et annee */
        else if ($instance_id != 0 && $statut == 0 && $annee != 0) {
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->where([
                    ['sessions.ins_id', '=', $instance_id],
                    ['sessions.ses_annee', '=', $annee]
                ])
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        } else {
            $statut = 0;
            $instance_id = 0;
            $annee = 0;
            $sessions = DB::table('sessions')
                ->join('instances', 'sessions.ins_id', '=', 'instances.ins_id')
                ->orderBy('sessions.ses_date_debut', 'asc')
                ->get();
        }
        return view('backend.sessions.index', compact('sessions', 'instances', 'statut', 'instance_id', 'annee'));
    }

    public function getPageParticipants($id_session)
    {
        $participants = DB::table('participants as part')
            ->leftjoin('structures as str', 'str.str_id', '=', 'part.str_id')
            ->leftjoin('agents as ag', 'ag.structure', '=', 'str.str_id')
            ->join('sessions as ses', 'ses.ses_id', '=', 'part.ses_id')
            ->where([
                ['ses.ses_id', '=', $id_session],
            ])
            ->select('ses.*', 'str.*', DB::raw('COUNT(ag.age_id) as nb_agents'))
            ->groupby('str.str_id')
            ->orderby('str.str_sigle', 'desc')
            ->get();

        $session = DB::table('sessions as ses')
            ->join('instances as ins', 'ins.ins_id', '=', 'ses.ins_id')
            ->where('ses_id', '=', $id_session)->first();

        return view('backend.sessions.participants', compact('participants', 'session'));
    }

    public function getRecosAreconduire($ses_id_precedant)
    {
        $recoAreconduire = [];
        if ($ses_id_precedant != null) {
            $recoAreconduire = DB::table('recommandations as rec')
                ->where([
                    ['rec.rec_statut', '<>', 4],
                    ['rec.rec_statut', '<>', 5],
                    ['rec.rec_is_reconduit', '=', 0],
                    ['rec.ses_id', '=', $ses_id_precedant]
                ])
                ->orWhere([
                    ['rec.rec_is_permanente', '=', 1],
                    ['rec.rec_statut', '<>', 5],
                    ['rec.rec_is_reconduit', '=', 0],
                    ['rec.ses_id', '=', $ses_id_precedant]
                ])->get();
        }
        if (count($recoAreconduire) > 0) {
            return $recoAreconduire;
        } else {
            return null;
        }
    }

    public function reconduireRecos()
    {
        request('recos') ? $n = sizeof(request('recos')) : $n = 0;
        if ($n > 0) {
            $user = auth()->user();
            for ($i = 0; $i < $n; $i++) {
                $recommandation = DB::table('recommandations')->where('rec_id', request('recos')[$i])->first();
                $reco = new Recommandations();
                $reco->ses_id = request('session');
                $reco->bud_id = $recommandation->bud_id;
                $reco->rec_id_ref = $recommandation->rec_id;
                $reco->rec_intitule = $recommandation->rec_intitule;
                $reco->rec_date_echeance = $recommandation->rec_date_echeance;
                $reco->rec_statut = $recommandation->rec_statut;
                $reco->rec_is_suivi_valide = $recommandation->rec_is_suivi_valide;
                $reco->rec_is_mode_standard = $recommandation->rec_is_mode_standard;
                $reco->rec_is_planning_valide = $recommandation->rec_is_planning_valide;
                $reco->rec_observation = $recommandation->rec_observation;
                $reco->rec_obs_nonrealise = $recommandation->rec_obs_nonrealise;
                $reco->rec_obs_encours = $recommandation->rec_obs_encours;
                $reco->rec_obs_realise = $recommandation->rec_obs_realise;
                $reco->rec_personne_formule = $recommandation->rec_personne_formule;
                $reco->rec_personne_tel = $recommandation->rec_personne_tel;
                $reco->rec_dh_valide = $recommandation->rec_dh_valide;
                $reco->rec_dh_abandon = $recommandation->rec_dh_abandon;
                $reco->rec_dh_nonrealise = $recommandation->rec_dh_nonrealise;
                $reco->rec_dh_en_cours = $recommandation->rec_dh_en_cours;
                $reco->rec_dh_realise = $recommandation->rec_dh_realise;
                $reco->rec_dh_planning_valide = $recommandation->rec_dh_planning_valide;
                $reco->rec_motif_abandon = $recommandation->rec_motif_abandon;
                $reco->rec_preuve_nonrealise = $recommandation->rec_preuve_nonrealise;
                $reco->rec_preuve_encours = $recommandation->rec_preuve_encours;
                $reco->rec_preuve_realise = $recommandation->rec_preuve_realise;
                $reco->rec_is_permanente = $recommandation->rec_is_permanente;
                $reco->rec_is_reconduit = $recommandation->rec_is_reconduit;
                $reco->created_by = $user->email;
                $reco->updated_by = $user->email;
                if ($reco->rec_is_permanente == 0 || ($reco->rec_is_permanente == 1 && $reco->rec_statut != 4)) {
                    $reco->rec_taux_realise = $recommandation->rec_taux_realise;
                }
                try {
                    $reco->save();
                    /** si la recommandation a bien ete reconduit et le suivi est par taux */
                    if (!empty($reco->rec_id)) {
                        /** on recupere egalement les partenaires de la recommandation a reconduire */
                        $partenaires = DB::table('partenaires')->where([['rec_id', '=', $recommandation->rec_id]])->get();
                        if (count($partenaires) > 0) {
                            foreach ($partenaires as $partenaire) {
                                $part = new Partenaires();
                                $part->str_id = $partenaire->str_id;
                                $part->rec_id = $reco->rec_id;
                                $part->ptn_is_responsable = $partenaire->ptn_is_responsable;
                                $part->created_by = $user->email;
                                $part->updated_by = $user->email;
                                $part->save();
                            }
                        }

                        if ($reco->rec_is_mode_standard == 0) {
                            /** on recupere les activites non realiser de la recommandation a reconduire */
                            $activites = DB::table('activites')->where([['rec_id', '=', $recommandation->rec_id]/*, ['act_is_realise', '=', 0]*/])->get();
                            if (count($activites) > 0) {
                                foreach ($activites as $activite) {
                                    $act = new Activites();
                                    $act->rec_id = $reco->rec_id;
                                    $act->ptn_id = $activite->ptn_id;
                                    $act->act_description = $activite->act_description;
                                    $act->act_is_realise = $activite->act_is_realise;
                                    $act->act_is_valide = $activite->act_is_valide;
                                    $act->act_poids = $activite->act_poids;
                                    $act->act_date_prevue = $activite->act_date_prevue;
                                    $act->act_date_realise = $activite->act_date_realise;
                                    $act->act_preuve = $activite->act_preuve;
                                    $act->act_observation = $activite->act_observation;
                                    $act->created_by = $user->email;
                                    $act->updated_by = $user->email;
                                    $act->save();
                                }
                            }
                        }
                    }
                    if (!empty($reco->rec_id)) {
                        $result = DB::table('recommandations')
                            ->where('rec_id', $recommandation->rec_id)
                            ->update([
                                'rec_is_reconduit' => 1,
                                'updated_at' => Carbon::now(),
                                'updated_by' => $user->email
                            ]);
                        if (!empty($result)) {
                            activity('Session')->log('Reconduction de recommandation(s)');
                        }
                    }
                } catch (\PDOException $ex) {
                    return 1;
                }
            }
            return 0;
        } else {
            return 2;
        }
    }

    public function reconduireRecos_old($ses_id, $ses_id_precedant)
    {
        $session = DB::table('sessions')->where('ses_id', $ses_id)->first();
        $user = auth()->user();
        /** Recuperation des recommandations a reconduire de la derniere session, si la session a une precedante */
        if ($ses_id_precedant != null) {
            $recoAreconduire = DB::table('recommandations as rec')
                ->where([
                    ['rec.rec_statut', '<>', 4],
                    ['rec.rec_statut', '<>', 5],
                    ['rec.rec_is_reconduit', '=', 0],
                    ['rec.ses_id', '=', $ses_id_precedant]
                ])
                ->orWhere([
                    ['rec.rec_is_permanente', '=', 1],
                    ['rec.rec_is_reconduit', '=', 0],
                    ['rec.ses_id', '=', $ses_id_precedant]
                ])->get();
            /** Enregistrement des recommandations a reconduire s'il existe au moins une */
            if (count($recoAreconduire) > 0) {
                for ($i = 0; $i < count($recoAreconduire); $i++) {
                    $reco = new Recommandations();
                    $reco->ses_id = $session->ses_id;
                    $reco->bud_id = $recoAreconduire[$i]->bud_id;
                    $reco->rec_id_ref = $recoAreconduire[$i]->rec_id;
                    $reco->rec_intitule = $recoAreconduire[$i]->rec_intitule;
                    $reco->rec_date_echeance = $recoAreconduire[$i]->rec_date_echeance;
                    $reco->rec_statut = 1;
                    $reco->rec_is_suivi_valide = 0;
                    $reco->rec_is_mode_standard = $recoAreconduire[$i]->rec_is_mode_standard;
                    $reco->rec_personne_formule = $recoAreconduire[$i]->rec_personne_formule;
                    $reco->rec_personne_tel = $recoAreconduire[$i]->rec_personne_tel;
                    $reco->rec_is_permanente = $recoAreconduire[$i]->rec_is_permanente;
                    $reco->created_by = $user->email;
                    $reco->updated_by = $user->email;
                    try {
                        $reco->save();
                        if ($reco->rec_id) {
                            DB::table('recommandations')
                                ->where('rec_id', $recoAreconduire[$i]->rec_id)
                                ->update([
                                    'rec_is_reconduit' => 1,
                                    'updated_at' => Carbon::now(),
                                    'updated_by' => $user->email
                                ]);
                        }
                    } catch (\PDOException $ex) {
                        return 1;
                    }
                }
                return 0;
            } else {
                return 2;
            }
        } else {
            return 2;
        }
    }

    /**
     * @param $ses_id
     * @return liste des structures partenaires responsables donc la planification des reco ne sont pas encore validees
     */
    public function structuresWhereRecoNotPlan($ses_id)
    {
        $structures = DB::table('sessions as ses')
            ->join('recommandations as reco', 'ses.ses_id', '=', 'reco.ses_id')
            ->join('partenaires as part', 'reco.rec_id', '=', 'part.rec_id')
            ->join('structures as str', 'part.str_id', '=', 'str.str_id')
            ->where([
                ['ses.ses_id', '=', intval($ses_id)],
                ['reco.rec_is_planning_valide', '=', 0],
                ['part.ptn_is_responsable', '=', 1]
            ])
            ->select('str.*')
            ->orderby('str.str_sigle', 'asc')
            ->groupBy('str.str_sigle', 'str.str_nom_complet')
            ->distinct()->get();
        /*$structures = DB::table('structures as str')
            ->join('partenaires as part', 'str.str_id', '=', 'part.str_id')
            ->join('recommandations as reco', 'part.rec_id', '=', 'part.rec_id')
            ->join('sessions as ses', 'ses.ses_id', '=', 'reco.ses_id')
            ->where([
                ['ses.ses_id', '=', intval($ses_id)],
                ['reco.rec_is_planning_valide', '=', 0],
                ['part.ptn_is_responsable', '=', 1]
            ])
            ->select('str.*')
            ->orderby('str.str_sigle', 'asc')
            ->groupBy('str.str_sigle', 'str.str_nom_complet')
            ->distinct()->get();*/
        if (count($structures) > 0) {
            return $structures;
        } else {
            return null;
        }
    }
}
