<?php

namespace App\Http\Controllers;

use App\Models\Instance;
use App\Models\Recommandations;
use App\Models\Structures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatistiquesController extends AppBaseController
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $statut = 0;
        $instance = 0;
        $sessions = [];
        $session = 0;
        $annee='';
        $acteurs = [];
        $acteur = 0;
        $instances = DB::table('instances')->pluck('ins_sigle', 'ins_id');
        $structures = DB::table('structures')->pluck(DB::raw("concat(str_sigle,' - ',str_nom_complet) as str_nom_complet"), 'str_id');
        $recos = '';
        $title_table_nombre =''; $title_table_taux=''; $title_col=""; $title_table_statut="";

            return view('backend.statistiques.index', compact('annee','recos', 'instances', 'statut', 'instance',
            'sessions', 'session', 'acteurs', 'acteur','structures',
            'title_table_nombre', 'title_table_taux','title_col','title_table_statut'
        ));
    }

    

    public function statistiqueSearch(Request $request)
    {
        $instance = $request->fil_instance;
        $annee = $request->fil_annee;
        $session = $request->fil_session;
        $acteur = $request->fil_structure;
        $statut = $request->fil_status;
        $title_table_nombre = "";
        $title_table_taux = "";
        $title_table_statut = "";
        $title_col = "";

        $instanceSearch = Instance::find($request->fil_instance);

        $id_rec_reconduits = DB::table('recommandations')->where('rec_id_ref','!=',null)->pluck('rec_id_ref');
        $id_rec_reconduit = [];
        foreach ($id_rec_reconduits as $key => $value) {
            array_push($id_rec_reconduit, $value);
        }

        $instances = DB::table('instances')->orderby('ins_sigle')->pluck('ins_sigle', 'ins_id');
        $structures = DB::table('structures')->pluck(DB::raw("concat(str_sigle,' - ',str_nom_complet) as str_nom_complet"), 'str_id');
        $sessions = DB::table('sessions')->where([['ins_id', '=', $instance], ['ses_annee', '=', $annee]])->pluck('ses_description', 'ses_id');;
        $acteurs = DB::table('acteurs')->join('structures', 'structures.str_id', '=', 'acteurs.str_id')->where([['acteurs.ins_id', '=', $instance]])->orderby('str_sigle')->pluck('structures.str_sigle', 'structures.str_id');;

        if (!$instance && !$annee && !$session && !$acteur &&  !$statut) {
            $title_table_nombre = "Nombre de recommandations par instance";
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par instance";
            $title_table_statut = "Mise en oeuvre des recommandations par statut";
            $title_col = "Instance";
            
            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('instances.ins_sigle')
                ->groupBy('instances.ins_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('instances.ins_sigle')
                ->groupBy('instances.ins_id')
                ->get();


                // Construction du tableau en fonction du statut
                /* $id_impl= DB::select('select rc_id from recommandations where rec_id_ref != null', [1]) */
                $statut_recos =  $nb_recos;
                foreach ($statut_recos as $key => $value) {
                    $statut_recos[$key]->non_realise = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ins_id` =:instance
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null) )
                                        group by
                                            `instances`.`ins_id`
                                        order by
                                            `instances`.`ins_sigle` asc
                                        limit 1', [$value->ins_id,2]);
                        
                
                    $statut_recos[$key]->realise = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ins_id` =:instance
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `instances`.`ins_id`
                                        order by
                                            `instances`.`ins_sigle` asc
                                        limit 1', [$value->ins_id, 4]);

                 $statut_recos[$key]->en_cours = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ins_id` =:instance
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `instances`.`ins_id`
                                        order by
                                            `instances`.`ins_sigle` asc
                                        limit 1', [$value->ins_id, 3]);

                    $statut_recos[$key]->abandonne = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ins_id` =:instance
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `instances`.`ins_id`
                                        order by
                                            `instances`.`ins_sigle` asc
                                        limit 1', [$value->ins_id, 5]);
                        
                }

            } 
            else if (!$instance && $annee && !$session && !$acteur && !$statut) {
                    $title_table_nombre = "Nombre de recommandations par instance de ".$annee;
                    $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par instance de " . $annee;
                    $title_table_statut = "Mise en oeuvre par statut de " . $annee;
                    $title_col = "Instance";

                    $nb_recos = DB::table('recommandations')
                        ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                        ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                        ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                        ->where([
                            ['sessions.ses_annee', $annee],
                        ])
                        ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                        ->orderBy('instances.ins_sigle')
                        ->groupBy('instances.ins_id')
                        ->get();
                    $tx_recos = DB::table('recommandations')
                        ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                        ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                        ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                        ->where([
                            ['sessions.ses_annee', $annee],
                        ])
                        ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                        ->orderBy('instances.ins_sigle')
                        ->groupBy('instances.ins_id')
                        ->get();

                    // Construction du tableau en fonction du statut
                    $statut_recos =  $nb_recos;
                    foreach ($statut_recos as $key => $value) {

                        $statut_recos[$key]->non_realise = DB::select('select
                                                    count(recommandations.rec_id) as valeur1
                                                from
                                                    `recommandations`
                                                inner join `sessions` on
                                                    `recommandations`.`ses_id` = `sessions`.`ses_id`
                                                inner join `instances` on
                                                    `instances`.`ins_id` = `sessions`.`ins_id`
                                                where
                                                    (`sessions`.`ins_id` =:instance
                                                    and `recommandations`.`rec_statut` =:statut
                                                    and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null)
                                                    and `sessions`.`ses_annee` =:annee)
                                                group by
                                                    `instances`.`ins_id`
                                                order by
                                                    `instances`.`ins_sigle` asc
                                                limit 1', [$value->ins_id, 2, $annee]);


                        $statut_recos[$key]->realise = DB::select('select
                                                    
                                                    count(recommandations.rec_id) as valeur1
                                                from
                                                    `recommandations`
                                                inner join `sessions` on
                                                    `recommandations`.`ses_id` = `sessions`.`ses_id`
                                                inner join `instances` on
                                                    `instances`.`ins_id` = `sessions`.`ins_id`
                                                where
                                                    (`sessions`.`ins_id` =:instance
                                                    and `recommandations`.`rec_statut` =:statut 
                                                    and `sessions`.`ses_annee` =:annee
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                                group by
                                                    `instances`.`ins_id`
                                                order by
                                                    `instances`.`ins_sigle` asc
                                                limit 1', [$value->ins_id, 4, $annee]);

                        $statut_recos[$key]->en_cours = DB::select('select
                                                    
                                                    count(recommandations.rec_id) as valeur1
                                                from
                                                    `recommandations`
                                                inner join `sessions` on
                                                    `recommandations`.`ses_id` = `sessions`.`ses_id`
                                                inner join `instances` on
                                                    `instances`.`ins_id` = `sessions`.`ins_id`
                                                where
                                                    (`sessions`.`ins_id` =:instance
                                                    and `recommandations`.`rec_statut` =:statut
                                                    and `sessions`.`ses_annee` =:annee
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                                group by
                                                    `instances`.`ins_id`
                                                order by
                                                    `instances`.`ins_sigle` asc
                                                limit 1', [$value->ins_id, 3, $annee]);

                        $statut_recos[$key]->abandonne = DB::select('select
                                                    
                                                    count(recommandations.rec_id) as valeur1
                                                from
                                                    `recommandations`
                                                inner join `sessions` on
                                                    `recommandations`.`ses_id` = `sessions`.`ses_id`
                                                inner join `instances` on
                                                    `instances`.`ins_id` = `sessions`.`ins_id`
                                                where
                                                    (`sessions`.`ins_id` =:instance
                                                    and `recommandations`.`rec_statut` =:statut
                                                    and `sessions`.`ses_annee` =:annee
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                                group by
                                                    `instances`.`ins_id`
                                                order by
                                                    `instances`.`ins_sigle` asc
                                                limit 1', [$value->ins_id, 5, $annee]);
                    }
                    
        }  else if ($instance && $annee && !$session && !$acteur && !$statut) {
            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $title_table_nombre = "Nombre de recommandations par session du " . $ins_sigle . " de " . $annee ;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle . " de " . $annee;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle ." de ".$annee;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id,  2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id,  3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id,  5]);
            }
        
        } else if ($instance && $annee && $session && !$acteur && !$statut) {
            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $title_table_nombre = "Nombre de recommandations par session du " . $ins_sigle . " de " . $annee;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle . " de " . $annee;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle . " de " . $annee;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    ['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id,  2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id,  3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id,  5]);
            }
        } else if ($instance && !$annee && $session && !$acteur && !$statut) {
            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $title_table_nombre = "Nombre de recommandations par session du " . $ins_sigle ;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle ;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle ;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [ $value->ses_id,  5]);
            }
        } else if ($instance && !$annee && !$session && !$acteur && !$statut) {
            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $title_table_nombre = "Nombre de recommandations par session du " . $ins_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ins_id', $instance],
                    //['sessions.ses_annee', $annee],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_id')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `recommandations`.`rec_statut` =:statut
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id,  5]);
            }
        }
         else if (!$instance && !$annee && !$session && $acteur &&  !$statut) {

            $str_sigle = DB::table('structures')->where('str_id', $acteur)->value('str_sigle');
            $title_table_nombre = "Nombre de recommandations par instance de ".$str_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par instance de " . $str_sigle;
            $title_table_statut = "Mise en oeuvre par statut de " . $str_sigle;
            $title_col = "Instance";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('instances.ins_sigle')
                ->groupBy('instances.ins_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    //['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('instances.ins_sigle')
                ->groupBy('instances.ins_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            ( `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$value->ins_id, $acteur, 2]);


                $statut_recos[$key]->realise = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            ( `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$value->ins_id, $acteur, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            ( `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$value->ins_id, $acteur, 3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            ( `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$value->ins_id, $acteur, 5]);
            }

        } else if (!$instance && $annee && !$session && $acteur &&  !$statut) {

            $str_sigle = DB::table('structures')->where('str_id', $acteur)->value('str_sigle');
            $title_table_nombre = "Nombre de recommandations par instance de " . $str_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par instance de " . $str_sigle;
            $title_table_statut = "Mise en oeuvre par statut de " . $str_sigle;
            $title_col = "Instance";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('instances.ins_sigle')
                ->groupBy('instances.ins_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('instances.ins_id', 'instances.ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ses_annee', $annee],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('instances.ins_sigle')
                ->groupBy('instances.ins_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$annee,$value->ins_id, $acteur, 2]);


                $statut_recos[$key]->realise = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$annee, $value->ins_id, $acteur, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$annee, $value->ins_id, $acteur, 3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ins_id` =:instance
                                            and `partenaires`.`str_id` =:structure
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_statut` =:statut
                                            
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ins_id`
                                        limit 1', [$annee, $value->ins_id, $acteur, 5]);
            }
        }

         else if ($instance && $annee && !$session && $acteur &&  !$statut) {
            
            $ins_sigle = DB::table('instances')->where('ins_id',$instance)->value('ins_sigle');
            $str_sigle = DB::table('structures')->where('str_id',$acteur)->value('str_sigle');
            $title_table_nombre = "Nombre de recommandations par session de " . $ins_sigle . " pour "  . $str_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session de "  . $ins_sigle . " pour " . $str_sigle;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " .$ins_sigle." pour " . $str_sigle;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, $acteur, 2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, $acteur, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id ,$acteur, 3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id ,$acteur, 5]);
            }
        } else if ($instance && $annee && $session && $acteur &&  !$statut) {

            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $str_sigle = DB::table('structures')->where('str_id', $acteur)->value('str_sigle');
            $title_table_nombre = "Nombre de recommandations par session de " . $ins_sigle . " pour "  . $str_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session de "  . $ins_sigle . " pour " . $str_sigle;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle . " pour " . $str_sigle;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    ['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    ['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, $acteur, 2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, $acteur, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, $acteur, 3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_annee` =:annee
                                            and `sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$annee, $value->ses_id, $acteur, 5]);
            }
        } else if ($instance && !$annee && !$session && $acteur &&  !$statut) {
            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $str_sigle = DB::table('structures')->where('str_id', $acteur)->value('str_sigle');
            $title_table_nombre = "Nombre de recommandations par session de " . $ins_sigle . " pour "  . $str_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session de "  . $ins_sigle . " pour " . $str_sigle;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle . " pour " . $str_sigle;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    //['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    //['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [ $value->ses_id, $acteur, 2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [ $value->ses_id, $acteur, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [ $value->ses_id, $acteur, 3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id, $acteur, 5]);
            }
        } else if ($instance && !$annee && $session && $acteur &&  !$statut) {
            $ins_sigle = DB::table('instances')->where('ins_id', $instance)->value('ins_sigle');
            $str_sigle = DB::table('structures')->where('str_id', $acteur)->value('str_sigle');
            $title_table_nombre = "Nombre de recommandations par session de " . $ins_sigle . " pour "  . $str_sigle;
            $title_table_taux = "Taux moyen de mise en oeuvre des recommandations par session de "  . $ins_sigle . " pour " . $str_sigle;
            $title_table_statut = "Taux moyen de mise en oeuvre des recommandations par session du " . $ins_sigle . " pour " . $str_sigle;
            $title_col = "Session";

            $nb_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('count(recommandations.rec_id) as valeur'))
                ->where([
                    //['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();
            $tx_recos = DB::table('recommandations')
                ->join('sessions', 'recommandations.ses_id', '=', 'sessions.ses_id')
                ->join('instances', 'instances.ins_id', '=', 'sessions.ins_id')
                ->join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('structures', 'structures.str_id', '=', 'partenaires.str_id')
                ->select('sessions.ses_id', 'sessions.ses_description as ins_sigle', DB::raw('avg(recommandations.rec_taux_realise) as valeur'))
                ->where([
                    //['sessions.ses_annee', $annee],
                    ['instances.ins_id', $instance],
                    ['partenaires.str_id', $acteur],
                    ['partenaires.ptn_is_responsable', true],
                ])
                ->whereNotIn('recommandations.rec_id', $id_rec_reconduit)
                ->orderBy('sessions.ses_description')
                ->groupBy('sessions.ses_id')
                ->get();

            // Construction du tableau en fonction du statut
            $statut_recos =  $nb_recos;
            foreach ($statut_recos as $key => $value) {

                $statut_recos[$key]->non_realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id, $acteur, 2]);


                $statut_recos[$key]->realise = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id, $acteur, 4]);

                $statut_recos[$key]->en_cours = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id, $acteur, 3]);

                $statut_recos[$key]->abandonne = DB::select('select
                                            
                                            count(recommandations.rec_id) as valeur1
                                        from
                                            `recommandations`
                                        inner join `sessions` on
                                            `recommandations`.`ses_id` = `sessions`.`ses_id`
                                        inner join `instances` on
                                            `instances`.`ins_id` = `sessions`.`ins_id`
                                        inner join `partenaires` on
                                            `partenaires`.`rec_id` = `recommandations`.`rec_id`
                                        inner join `structures` on
                                            `structures`.`str_id` = `partenaires`.`str_id`
                                        where
                                            (`sessions`.`ses_id` =:session
                                            and `partenaires`.`str_id` =:structure
                                            and `recommandations`.`rec_statut` =:statut
                                            and `partenaires`.`ptn_is_responsable` =true
                                            and `recommandations`.`rec_id` NOT IN (select `rec_id_ref` from `recommandations` where `rec_id_ref` is not null))
                                        group by
                                            `sessions`.`ses_id`
                                        order by
                                            `sessions`.`ses_description` asc
                                        limit 1', [$value->ses_id, $acteur, 5]);
            }
        }
       
        return view('backend.statistiques.index', compact('annee','instanceSearch', 'nb_recos',
        'tx_recos' ,'instances', 'sessions', 'acteurs', 'session', 'acteur', 'statut', 'instance',
        'title_table_nombre', 'title_table_taux','title_table_statut','title_col','statut_recos','structures'
    ));
    }
}
