<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStructuresRequest;
use App\Http\Requests\UpdateStructuresRequest;
use App\Models\Structures;
use App\Repositories\StructuresRepository;
use Flash;
use Helmesvs\Notify\Facades\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;
use Response;

class StructuresController extends AppBaseController
{
    /** @var  StructuresRepository */
    private $structuresRepository;

    public function __construct(StructuresRepository $structuresRepo)
    {
        $this->structuresRepository = $structuresRepo;
    }

    /**
     * Display a listing of the Structures.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $structures = DB::table('structures as str')
            ->leftjoin('categories as cat', 'cat.cat_id', '=', 'str.str_categorie')
            ->leftjoin('users as u', 'u.str_id', '=', 'str.str_id')
            ->select('str.*', 'cat.cat_libelle', DB::raw('COUNT(u.id) as nb_agents'))
            ->groupby('str.str_id')
            ->orderby('str.str_sigle', 'asc')
            ->get();

        $categories = DB::table('categories')->pluck('cat_libelle', 'cat_id');
        return view('backend.structures.index', compact('categories'))
            ->with('structures', $structures);
    }

    public function structuresSearch()
    {
        $categorie = request('fil_categorie');
        if ($categorie != -1) {
            $structures = DB::table('structures as str')
                ->leftjoin('categories as cat', 'cat.cat_id', '=', 'str.str_categorie')
                ->leftjoin('users as u', 'u.str_id', '=', 'str.str_id')
                ->select('str.*', 'cat.cat_libelle', DB::raw('COUNT(u.id) as nb_agents'))
                ->where([
                    ['str.str_categorie', '=', $categorie]
                ])
                ->groupby('str.str_id')
                ->orderby('str.str_sigle', 'asc')
                ->get();
        } else {
            $structures = DB::table('structures as str')
                ->leftjoin('categories as cat', 'cat.cat_id', '=', 'str.str_categorie')
                ->leftjoin('users as u', 'u.str_id', '=', 'str.str_id')
                ->select('str.*', 'cat.cat_libelle', DB::raw('COUNT(u.id) as nb_agents'))
                ->groupby('str.str_id')
                ->orderby('str.str_sigle', 'asc')
                ->get();
        }

        $categories = DB::table('categories')->pluck('cat_libelle', 'cat_id');

        return view('backend.structures.index', compact('categories', 'categorie'))
            ->with('structures', $structures);
    }

    /**
     * Show the form for creating a new Structures.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.structures.create');
    }

    /**
     * Store a newly created Structures in storage.
     *
     * @param CreateStructuresRequest $request
     *
     * @return Response
     */
    public function store(CreateStructuresRequest $request, UpdateStructuresRequest $updateRequest)
    {
        $input = $request->all();
        if ($request->str_id) {
            try {
                $structures = $this->update($request->str_id, $updateRequest);
                Notify::success("Structure mise à jour avec succès !", "Opération réussie", $options = []);
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    Notify::error("La structure existe dejà !", "Echec", $options = []);
                } else {
                    Notify::error("Echec de la mise à jour. Veuillez réessayer!", "Echec", $options = []);
                }
            }

        } else {
            try {
                $structures = $this->structuresRepository->create($input);
                Notify::success("Structure ajoutée avec succès !", "Opération réussie", $options = []);
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    Notify::error("La structure existe dejà !", "Echec", $options = []);
                } else {
                    Notify::error("Echec de l'ajout de la structure. Veuillez réessayer!", "Echec", $options = []);
                }
            }
        }
        return redirect(route('structures.index'));
    }

    /**
     * Display the specified Structures.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $structures = $this->structuresRepository->find($id);

        if (empty($structures)) {
            Flashy::error('Structures not found');

            return redirect(route('structures.index'));
        }

        return view('backend.structures.show')->with('structures', $structures);
    }

    /**
     * Show the form for editing the specified Structures.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $structures = $this->structuresRepository->find($id);

        if (empty($structures)) {
            Notify::success("Structure non trouvée !", "Echec !", $options = []);

            return redirect(route('structures.index'));
        }

        return view('backend.structures.edit')->with('structures', $structures);
    }

    /**
     * Update the specified Structures in storage.
     *
     * @param int $id
     * @param UpdateStructuresRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStructuresRequest $request)
    {
        $structures = $this->structuresRepository->find($id);

        if (empty($structures)) {
            Flashy::error('Structures not found');

            return redirect(route('structures.index'));
        }

        $structures = $this->structuresRepository->update($request->all(), $id);

        Notify::success("Structure mise à jour avec succès !", "Opération réussie", $options = []);

        return redirect(route('structures.index'));
    }

    /**
     * Remove the specified Structures from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $structures = $this->structuresRepository->find($id);

        if (empty($structures)) {
            Flashy::error('Structures not found');

            return redirect(route('structures.index'));
        }

        $this->structuresRepository->delete($id);

        Notify::success("Structure supprimée !", "Opération réussie", $options = []);

        return redirect(route('structures.index'));
    }

    public function getStructure($id)
    {
        $structure = DB::table('structures')->where('str_id', $id)->first();
        if (empty($structure)) {
            return null;
        }
        return response()->json($structure);
    }

    public function deleteStructure($id)
    {
        $structure = DB::table('structures')->where('str_id', $id)->first();
        if (empty($structure)) {
            return redirect(route('structures.index'));
        }
        try {
            DB::table('structures')->where('str_id', '=', intval($structure->str_id))->delete();
            return 1;
        } catch (\PDOException $ex) {
            if ($ex->getCode() == 23000) {
                return 0;
            }
        }
    }

    public function getStructuresByInstance($ins_id)
    {
        return MethodesStaticController::getStructuresByInstance($ins_id);
    }
}
