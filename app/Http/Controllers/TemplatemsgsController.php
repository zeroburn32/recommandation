<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateTemplatemsgsRequest;
use App\Models\Alertes;
use App\Models\Templatemsgs;
use App\Repositories\TemplatemsgsRepository;
use Flash;
use Illuminate\Http\Request;
use Notify;
use Response;

class TemplatemsgsController extends AppBaseController
{

    private $templatemsgsRepository;

    public function __construct(TemplatemsgsRepository $templatemsgsRepo)
    {
        $this->templatemsgsRepository = $templatemsgsRepo;
    }

    public function index(Request $request)
    {
        $templatemsgs = $this->templatemsgsRepository->all();
        return view('backend.templatemsgs.index')
            ->with('templatemsgs', $templatemsgs);
    }

    public function create()
    {
        return view('backend.templatemsgs.create');
    }


    public function save()
    {
        $type = Templatemsgs::where('tmsg_type', request('tmsg_type'))->first();
        if ($type) {
            Notify::warning("Modele message non enregistré,le modèle existe", "Opération echouée", $options = []);
        } else {
            $tmp = new Templatemsgs();
            $tmp->tmsg_msg = request('tmsg_msg');
            $tmp->tmsg_type = request('tmsg_type');
            $tmp->tmsg_periodicite = request('tmsg_periodicite');
            $tmp->save();
            Notify::success("Modele message enregistré avec succès", "Opération reussie", $options = []);

        }
        return redirect(route('templatemsgs.index'));
    }

    public function show($id)
    {
        $templatemsgs = $this->templatemsgsRepository->find($id);

        if (empty($templatemsgs)) {
            Flash::error('Templatemsgs not found');
            return redirect(route('templatemsgs.index'));
        }

        return view('backend.templatemsgs.show')->with('templatemsgs', $templatemsgs);
    }

    public function detailTemplate($id)
    {
        $templatemsgs = $this->templatemsgsRepository->find($id);
        return view('backend.templatemsgs.show')->with('templatemsgs', $templatemsgs);
    }


    public function getTemplate($id)
    {
        $templatemsgs = Templatemsgs::where('tmsg_id', $id)->first();
        //dd($templatemsgs);
        $data = array('tmsg_type' => $templatemsgs->tmsg_type,
            'tmsg_msg' => $templatemsgs->tmsg_msg,
            'periodicite' => $templatemsgs->tmsg_periodicite
        );


        return response()->json($data);
    }

    public function updating($id)
    {
        $templatemsgs = Templatemsgs::where('tmsg_id', $id)->first();
        $type = Templatemsgs::where('tmsg_type', request('tmsg_type'))->first();
        if ($type && $templatemsgs->tmsg_type != request('tmsg_type')) {
            Notify::warning("Modele message non enregistré,le modèle existe", "Opération echouée", $options = []);
        } else {
            $templatemsgs->tmsg_type = request('tmsg_type');
            $templatemsgs->tmsg_msg = request('tmsg_msg');
            $templatemsgs->tmsg_periodicite = request('tmsg_periodicite');
            $templatemsgs->save();
            Notify::success("Le modèle a été enregistré", "Opération reussie", $options = []);
        }
        return redirect(route('templatemsgs.index'));

    }

    public function edit($id)
    {
        $templatemsgs = $this->templatemsgsRepository->find($id);

        if (empty($templatemsgs)) {
            Flash::error('Templatemsgs not found');
            return redirect(route('templatemsgs.index'));
        }

        return view('backend.templatemsgs.edit')->with('templatemsgs', $templatemsgs);
    }

    public function update($id, UpdateTemplatemsgsRequest $request)
    {
        $templatemsgs = $this->templatemsgsRepository->find($id);
        if (empty($templatemsgs)) {
            Flash::error('Templatemsgs not found');
            return redirect(route('templatemsgs.index'));
        }

        $templatemsgs = $this->templatemsgsRepository->update($request->all(), $id);
        Flash::success('Templatemsgs updated successfully.');
        return redirect(route('templatemsgs.index'));
    }


    public function delete($id)
    {

        $alert = Alertes::where('tmsg_id', $id)->first();
        if ($alert) {
            $data = array('message' => 'error');
            return response()->json($data);

        } else {
            $data = array('message' => 'succes');
            $t = Templatemsgs::where('tmsg_id', $id)->first();
            $t->delete();
            return response()->json($data);
        }
    }

}
