<?php

namespace App\Http\Controllers;

use Flashy;
use App\User;
use Response;
use App\Models\Users;
use App\Models\Agents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Helmesvs\Notify\Facades\Notify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Repositories\UsersRepository;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CreateUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Auth\AccountController;

class UsersController extends AppBaseController
{
    /** @var  UsersRepository */
    private $usersRepository;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the Users.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = DB::table('users as u')
            ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
            ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
            ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
            ->select('u.*','r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
            ->get();

        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        $user = new Users();
        $structures = DB::table('structures')->pluck(DB::raw("concat(str_sigle,' - ',str_nom_complet) as str_nom_complet"), 'str_id');
        
        return view('backend.users.index', compact('roles','user','structures'))
            ->with('users', $users);
    }

    public function usersSearch()
    {
        $structure = request('fil_structure1');
        $role = request('fil_profil1');
        $statut = request('fil_statut1');

        if ($structure && $role && isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['str.str_id',$structure],
                    ['r.name',$role],
                    ['u.is_active',$statut],
                    ])
                ->get();
        } else if ($structure && $role && ! isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['str.str_id', $structure],
                    ['r.name', $role],
                ])
                ->get();
            
        } else if ($structure && !$role && isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['str.str_id', $structure],
                    ['u.is_active', $statut],
                ])
                ->get();
        } else if ($structure && !$role && ! isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['str.str_id', $structure],
                ])
                ->get();
            
        } else if (!$structure && $role && ! isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['r.name', $role],
                ])
                ->get();
            
        } else if (!$structure && !$role && isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['u.is_active','=', $statut],
                ])
                ->get();
                
        }else if (!$structure && $role && isset($statut)) {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['r.name', $role],
                    ['u.is_active', $statut],
                ])
                ->get();
        } else {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->get();
        }
       

        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        $user = new Users();
        $structures = DB::table('structures')->pluck(DB::raw("concat(str_sigle,' - ',str_nom_complet) as str_nom_complet"), 'str_id');
        

        return view('backend.users.index', compact('role','roles', 'statut','structure','structures'))
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new Users.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        $user = new Users();
        $structures = DB::table('structures')->pluck('str_sigle', 'str_id');
        return view('backend.users.create', compact('roles', 'user', 'structures'));
    }

    /**
     * Store a newly created Users in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersRequest $request, UpdateUsersRequest $updateRequest)
    {
        $statutCode=0;
        $password="";
        if($request->id) {
            $user = DB::table('users')->where('id', '=', $request->id)->first();
            //$agent = DB::table('agents')->where('age_id', '=', $user->age_id)->first();
            $data = $request->all();
            try {
                $this->update($request->id, $updateRequest);
                $statutCode = 1;
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    $statutCode = 2;
                } else {
                    $statutCode = 3;
                }
            }
        } else {
            $data = $request->except('password');
            $newPassword = app('App\Http\Controllers\Auth\AccountController')->genereMotDePasse(8);
            $data['password'] = bcrypt($newPassword);
            $user = User::create($data);
            //$agent = DB::table('agents')->where('age_id', '=', $user->age_id)->first();
            try {
                foreach ($request->roles as $role) {
                    $user->assignRole($role);
                }
                $statutCode = 0;
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    $statutCode = 2;
                } else {
                    $statutCode = 3;
                }
            }
            $password = app('App\Http\Controllers\Auth\AccountController')->sendMotDePasse($user->id, $newPassword);
        }
        if ($request->ajax()) {
            return response()->json(array(
                'user' => $user,
                'password' => $password,
                'statutCode' => $statutCode,

            ));
        }
        return back();
        //return redirect(route('users.index'));
    }

    /**
     * Display the specified Users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        
        $users = DB::select('select   str.str_sigle, str.str_nom_complet, u.*, r.name as user_role 
                from users u, roles r, model_has_roles ru , structures str
                where u.id = ru.model_id 
                and r.id= ru.role_id
                and str.str_id= u.str_id
                and u.id=:id_user', ['id_user' => $id]);
        
        $permissions = DB::select('select p.* from permissions p, role_has_permissions pr, roles r, model_has_roles ru where pr.role_id=r.id and pr.permission_id=p.id and r.id=ru.role_id and ru.model_id=:utilisateur;', ['utilisateur' => $id]);
        
        //dd($users);
        if ($users) {
            $users = $users[0];
        }

       
        return view('backend.users.show', compact('users', 'permissions'));
    }

    /**
     * Show the form for editing the specified Users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');

        $user = User::with('roles')->findOrFail($id);
        //$agent = Agents::where('age_id','=',$user->age_id)->first();
        /* $user->nom= $agent->nom_prenom;
        $user->matricule = $agent->matricule;
        $user->telephone = $agent->telephone;
        $user->structure = $agent->structure; */
        
        $structures = DB::table('structures')->pluck('str_sigle', 'str_id');
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }
        $editMode = 'profil';

        if ($request->ajax()) {
            return response()->json($user);
        }

        return view('backend.users.edit', compact('editMode', 'structures','user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified Users in storage.
     *
     * @param int $id
     * @param UpdateUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersRequest $request)
    {
        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);

         $user->roles()->detach();
        foreach ($request->roles as $role) {
            $user->assignRole($role);
        } 

        Flashy::success('Mise à jour réussie');
        return back();
        //return redirect(route('users.index'));
    }
    // Modifier mes informations
    public function update_account(Request $request)
    {
        
        $user = User::findOrFail($request->id);
        $data = $request->except('password');

        $user->nom_prenom = $request->nom_prenom;
        $user->matricule = $request->matricule;
        $user->telephone = $request->telephone;
        $user->email = $request->email;

        $user->save();

        Flashy::success('Mise à jour réussie');
        return back();
        //return redirect(route('users.index'));
    }

    /**
     * Remove the specified Users from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Notify::success("Compte non trouvé !", "Echec !", $options = []);

            return redirect(route('users.index'));
        }

        $this->usersRepository->delete($id);
        if ($request->ajax()) {
            return response()->json(1);
        }

        Notify::success("Compte supprimé avec succès !", "Opération réussie", $options = []);

        return back();
    }

    /*********************************************    ***************************************************************/
    /**
     * @param $id (ID de l'utilisateur)
     * @return user
     */
    public function showUser($id)
    {
        $users = DB::select('select   str.str_sigle, str.str_nom_complet, u.*,  r.name as user_role 
                from users u, roles r, model_has_roles ru ,  structures str
                where u.id = ru.model_id 
                and r.id= ru.role_id
                and str.str_id= u.str_id
                and u.id=:id_user', ['id_user' => $id]);

                //dd($users);

        $permissions = DB::select('select p.* from permissions p, role_has_permissions pr, roles r, model_has_roles ru where pr.role_id=r.id and pr.permission_id=p.id and r.id=ru.role_id and ru.model_id=:utilisateur;', ['utilisateur' => $id]);

        //dd($users);
        if ($users) {
            $users = $users[0];
        }

        return view('backend.users.show_account', compact('users', 'permissions'));
    }

    /**
     * @param $id
     * @return affichage de l'ecran de modification
     */
    public function editUser($id)
    {
        $utilisateurs = $this->usersRepository->findWithoutFail($id);
        if (empty($utilisateurs)) {
            Flashy::error('Utilisateurs inexistant');
            return redirect(route('users.index'));
        }
        $profils = DB::table('Profil')->get();
        return view('backend.users.edit_account', compact('utilisateurs', 'profils'));
    }

    /** permet d'enregistrement de la modification
     * @param $id
     * @param UpdateUsersRequest $request
     * @return redirection sur le dashboard
     */
    public function modifier($id, UpdateUsersRequest $request)
    {
        $users = $this->usersRepository->findWithoutFail($id);
        $email = DB::select('SELECT * FROM "users" WHERE UPPER(email) = UPPER(?)', [request('email')]);
        if (empty($users)) {
            Flashy::error('Compte non trouvé !');
            return redirect(route('users.index'));
        }
        if (!empty($email)) {
            if (request('email') != $users->email) {
                Flashy::error('L\'email saisi existe déjà!');
                return Redirect::back()->withInput(Input::all());
            }
        }
        $users = $this->usersRepository->update($request->all(), $id);
        Flashy::success('Information du compte modifié avec succès !');
        return redirect(route('administration'));
    }

    
    /** permet d'afficher le formulaire de changement de password
     * @param $id
     * @return redirection sur le formulaire de saisie
     */
    public function formPassword($id)
    {
        $user = Users::findOrFail($id);
        return view('backend.users.changePwd')->with('user', $user);
    }

    public function getAgentsStructure($id_structure, Request $request)
    {
            $users = DB::table('users as u')
                ->leftjoin('structures as str', 'str.str_id', '=', 'u.str_id')
                ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
                ->leftjoin('roles as r', 'r.id', '=', 'ru.role_id')
                ->select('u.*', 'r.name as user_role', 'str.str_sigle', 'str.str_nom_complet')
                ->where([
                    ['str.str_id', '=', $id_structure],
                ])
                ->get();

            $roles = Role::select('id', 'name', 'label')->get();
            $roles = $roles->pluck('label', 'name');
            $user = new Users();
            $structures = DB::table('structures')->pluck('str_nom_complet', 'str_id');
            $structure = DB::table('structures')->where('str_id', '=', $id_structure)->first();

            if ($request->ajax()) {
                return response()->json($users);
            }
            return view('backend.structures.liste_agents_structure', compact('roles', 'user','structure', 'structures'))->with('users', $users);;
        
    }
    
    public function getPageAgentSuivi($id_structure)
    {    
        $users=DB::table('users as u')
            ->leftjoin('structures as str', 'str.str_id', '=', 'u.structure')
            ->leftjoin('model_has_roles as ru', 'u.id', '=', 'ru.model_id')
            ->leftjoin('roles as r', 'r.id','=', 'ru.role_id')
            ->where( [
                ['str.str_id','=', $id_structure],
            ])
            ->select('u.*','r.name as user_role', 'str.str_sigle', 'str.str_nom_complet' )
            ->get();

        $structure = DB::table('structures')->where('str_id', '=',$id_structure)->first();
        $structures = DB::table('structures')->pluck('str_sigle','str_id');

        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        return view('backend.structures.agents_suivi', compact('roles','structure','structures'))->with('users', $users);;
    }
}


