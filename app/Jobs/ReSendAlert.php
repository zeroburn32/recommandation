<?php

namespace App\Jobs;

use App\Http\Controllers\MethodesStaticController;
use App\Models\EnvoiAlertes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReSendAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $alerte) {
            $messageAlert = new EnvoiAlertes();
            $messageAlert->ena_msg = $alerte->ena_msg;
            $messageAlert->ena_ses_description = $alerte->ena_ses_description;
            $messageAlert->ena_ses_annee = $alerte->ena_ses_annee;
            $messageAlert->ena_ses_debut = $alerte->ena_ses_debut;
            $messageAlert->ena_ses_fin = $alerte->ena_ses_fin;
            $messageAlert->ena_ses_lieu = $alerte->ena_ses_lieu;
            $messageAlert->user_nom = $alerte->user_nom;
            $messageAlert->user_prenom = $alerte->user_prenom;
            $messageAlert->user_email = $alerte->user_email;
            $messageAlert->ale_mail_subject = $alerte->ale_mail_subject;
            MethodesStaticController::sendEmail($messageAlert, $alerte->ena_id);
        }
    }
}
