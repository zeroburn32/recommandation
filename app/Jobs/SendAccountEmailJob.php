<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendAccountEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $motDePasse;
    protected $view;
    protected $objet;

    public $tries = 3;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($user, $motDePasse,$view, $objet)
    {
        $this->user = $user;
        $this->motDePasse = $motDePasse;
        $this->view = $view;
        $this->objet = $objet;
    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $newPassword = $this->motDePasse;
        $user = User::find($this->user);
        $contenu = "Votre nouveau mot de passe est : " . $newPassword;
        $objet = $this->objet; 
        $email = $user->email;
        $data = ['nom' => $user->nom, 'prenom' => $user->prenom, "email" => $user->email, "contenu" => $contenu];
            Mail::send($this->view, $data, function ($message) use ($objet, $email) {
                $message->to($email)->subject($objet);
            });
    }
}
