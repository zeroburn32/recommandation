<?php

namespace App\Jobs;

use App\Http\Controllers\MethodesStaticController;
use App\Models\Acteur;
use App\Models\Alertes;
use App\Models\EnvoiAlertes;
use App\Models\Recommandations;
use App\Models\Session;
use App\Models\Templatemsgs;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SendAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($_id)
    {
       $this->id = $_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id == 0) {
            $available_alertes = Alertes::where(DB::raw('date(ale_date_heure_prev)'), DB::raw('date(now())'))->get();
        } else {
            $available_alertes = Alertes::where("ale_id", $this->id)
                ->where(DB::raw('date(ale_date_heure_prev)'), DB::raw('date(now())'))
                ->get();
        }

        foreach ($available_alertes as $alerte) {
            $cpt = 0;
            if ($this->isReadyForSend($alerte)) {
                $session = $this->getSession($alerte->ses_id);
                $list_structures = $this->getListStructures($alerte);

                foreach ($list_structures as $structure) {
                    $tamplate = $alerte->ale_msg;
                    $need_list_reco = false;
                    $list_recommandations = [];

                    if (preg_match('#LISTE_RECO#', $tamplate)) {
                        $need_list_reco = true;

                        if($structure->act_is_css==0)
                            $list_recommandations = $this->getListRecommandations($alerte->ses_id, $structure->str_id, $alerte->ale_tmsg_type);
                        else
                            $list_recommandations = $this->getListRecommandations($alerte->ses_id, 0, $alerte->ale_tmsg_type);

                        $table_recos = $this->formatRecoToTable($list_recommandations);
                        $tamplate = str_replace('[LISTE_RECO]', $table_recos, $tamplate);
                    }

                    if (($need_list_reco && count($list_recommandations) > 0) || (!$need_list_reco)) {
                        $list_destinataires = $this->getListeDestinataires($structure->str_id);
                        foreach ($list_destinataires as $destinataire) {
                            $messageAlert = new EnvoiAlertes();
                            $messageAlert->ena_msg = $this->setTamplateParamsValue($tamplate, $destinataire, $structure, $session);
                            $messageAlert->user_id = $destinataire->id;
                            $messageAlert->ale_id = $alerte->ale_id;
                            $messageAlert->ena_date_heure_recu = Carbon::now();
                            $messageAlert->ena_is_lue = 0;
                            $messageAlert->ena_ses_description = $session->ses_description;
                            $messageAlert->ena_ses_annee = $session->ses_annee;
                            $messageAlert->ena_ses_debut = $session->ses_date_debut;
                            $messageAlert->ena_ses_fin = $session->ses_date_fin;
                            $messageAlert->ena_ses_lieu = $session->ses_lieu;
                            $messageAlert->ena_isenvoi = 0;
                            $messageAlert->user_nom = $destinataire->nom;
                            $messageAlert->user_prenom = $destinataire->prenom;
                            $messageAlert->ale_date_heure_prev = $alerte->ale_date_heure_prev;
                            $messageAlert->user_email = $destinataire->email;

                            $messageAlert->str_id = $structure->str_id;
                            $messageAlert->str_sigle = $structure->str_sigle;
                            $messageAlert->str_nom_complet = $structure->str_nom_complet;

                            $messageAlert->ale_mail_subject = $alerte->mail_subject;
                            $messageAlert->created_by = is_object(Auth::user()) ? Auth::user()->nom : 'system@system.sys';
                            $messageAlert->updated_by = is_object(Auth::user()) ? Auth::user()->nom : 'system@system.sys';
                            $messageAlert->save();
                            MethodesStaticController::sendEmail($messageAlert);

                            echo $messageAlert->ena_msg;
                            $cpt = $cpt+1;
                            echo "\n-----------------------------------------------------------\n\n";

                        }
                    }
                }
            }
            //echo "\n\n".$cpt." ALERTES ENVOYES EN TOUT POUR L'ALERTE N°".$alerte->ale_id."\n\n";
            $this->updateAlerte($alerte);
        }
    }

    private function isReadyForSend($alerte)
    {
        $envoialertes = EnvoiAlertes::where('ale_id', $alerte->ale_id)
            ->where('ale_date_heure_prev', $alerte->ale_date_heure_prev)->first();

        if (empty($envoialertes)) {
            if (Carbon::parse($alerte->ale_date_heure_prev)->lte(Carbon::now())) {
                return true;
            }
        }
        return false;
    }

    private function getSession($ses_id)
    {
        return Session::where('ses_id', $ses_id)->first();
    }

    private function getListStructures($alerte)
    {
        $list_str_id = explode(",", $alerte->ale_destinataire);
        $acteursObj = Acteur::join('structures', 'structures.str_id', '=', 'acteurs.str_id')
            ->join('instances', 'instances.ins_id', '=', 'acteurs.ins_id')
            ->join('sessions', 'sessions.ins_id', '=', 'instances.ins_id')
            ->where('sessions.ses_id', $alerte->ses_id)
            ->whereIn('acteurs.str_id', $list_str_id)
            ->select('structures.str_id', 'structures.str_sigle', 'structures.str_nom_complet', 'acteurs.act_is_css', 'acteurs.act_is_csr');

        if ($alerte->ale_tmsg_type == 'PLANIFICATION') {
            $acteursObj->where('sessions.ses_statut', 2)
                ->where(DB::raw('date(ses_datefin_planification)'), '>=', DB::raw('date(now())'));
        } elseif ($alerte->ale_tmsg_type == 'SUIVI') {
            $acteursObj->where('sessions.ses_statut', 3)
                ->where(DB::raw('date(ses_datefin_realisation)'), '>=', DB::raw('date(now())'));
        }
        $acteurs = $acteursObj->get();

        return $acteurs;
    }

    private function getListeDestinataires($str_id)
    {
        return Users::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->whereIn('roles.name', ['CSS', 'CSR'])
            ->where('users.str_id', $str_id)
            ->select('users.*')
            ->get();
    }

    private function getListRecommandations($ses_id, $str_id, $type_alerte)
    {
        if($str_id>0) {
            $recoObj = Recommandations::join('partenaires', 'partenaires.rec_id', '=', 'recommandations.rec_id')
                ->join('sessions', 'sessions.ses_id', '=', 'recommandations.ses_id')
                ->where('sessions.ses_id', $ses_id)
                ->where('partenaires.str_id', $str_id);
        }else {
            $recoObj = Recommandations::join('sessions', 'sessions.ses_id', '=', 'recommandations.ses_id')
                ->where('sessions.ses_id', $ses_id);
        }

        if ($type_alerte == 'PLANIFICATION') {
            $recoObj->where('recommandations.rec_is_planning_valide', 0);
        } elseif ($type_alerte == 'SUIVI') {
            $recoObj->whereIn('recommandations.rec_statut', [2, 3]);
        }
        return $recoObj->select('recommandations.*')->get();
    }

    private function formatRecoToTable($list_recommandations)
    {
        $table = '<table class="table table-cl " border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                      <thead class="bg-secondary" style="background-color: #868e96;">
                        <tr>
                          <th class="text-center th-cl" align="center" valign="top">#</th>
                          <th class="th-cl" align="left" valign="top">Recommandation</th> 
                          <th class="th-cl" align="left" valign="top">Echeance</th>
                          <th class="th-cl" align="left" valign="top">Etat</th>
                          <th class="th-cl" align="left" valign="top">Taux</th>
                        </tr>
                      </thead>
                      <tbody>';
        $i = 1;
        foreach ($list_recommandations as $reco) {

            $table = $table . '<tr>
                                    <td class="td-cl" align="left" valign="top"> '.$i.' </td>
                                    <td class="td-cl" align="left" valign="top"> '.$reco->rec_intitule.' </td>
                                    <td class="td-cl" align="left" valign="top"> '.$reco->rec_date_echeance.'</td>
                                    <td class="td-cl" align="left" valign="top"> '.$reco->rec_stat.' </td>
                                    <td class="td-cl" align="left" valign="top"> '.$reco->rec_taux_realise.'</td>
                                </tr>';
            $i++;
        }
        return $table . '</tbody></table>';
    }

    private function setTamplateParamsValue($tamplate, $destinataire, $structure, $session)
    {
        $message = str_replace("[USER_NOM]", $destinataire->nom, $tamplate);
        $message = str_replace("[USER_PRENOM]", $destinataire->prenom, $message);
        $message = str_replace("[USER_EMAIL]", $destinataire->lia_email, $message);
        $message = str_replace("[USER_SEXE]", $destinataire->lia_sexe, $message);
        $message = str_replace("[USER_FONCTION]", $destinataire->lia_fonction, $message);
        $message = str_replace("[USER_MATRICULE]", $destinataire->lia_matricule, $message);
        $message = str_replace("[STRUCTURE_SIGLE]", $structure->str_sigle, $message);
        $message = str_replace("[STRUCTURE_NOM_COMPLET]", $structure->str_nom_complet, $message);
        $message = str_replace("[DESCRITION_SESSION]", $session->ses_description, $message);
        $message = str_replace("[ANNEE_SESSION]", $session->ses_annee, $message);
        $message = str_replace("[DATE_DEBUT_SESSION]", $session->ses_date_debut, $message);
        $message = str_replace("[DATE_FIN_SESSION]", $session->ses_date_fin, $message);
        $message = str_replace("[LIEU_SESSION]", $session->ses_lieu, $message);
        $message = str_replace("[DATE_FIN_PLANIFICATION]", $session->ses_datefin_planification, $message);
        $message = str_replace("[DATE_FIN_SUIVI]", $session->ses_datefin_realisation, $message);
        return $message;
    }

    private function updateAlerte($alerte)
    {
        $templ = Templatemsgs::where('tmsg_id', $alerte->tmsg_id)->select('tmsg_periodicite')->first();
        $alerte->ale_etat += 1;
        $alerte->ale_date_heure_envoi = Carbon::now();
        if ($templ->tmsg_periodicite == 'Mensuelle') {
            $alerte->ale_date_heure_prev = $alerte->ale_date_heure_prev->addMonths(1);
        } else if ($templ->tmsg_periodicite == 'Trimestrielle') {
            $alerte->ale_date_heure_prev = $alerte->ale_date_heure_prev->addMonths(3);
        } else if ($templ->tmsg_periodicite == 'Semestrielle') {
            $alerte->ale_date_heure_prev = $alerte->ale_date_heure_prev->addMonths(6);
        } else if ($templ->tmsg_periodicite == 'Annuelle') {
            $alerte->ale_date_heure_prev = $alerte->ale_date_heure_prev->addYears(1);
        } else if ($templ->tmsg_periodicite == 'Hebdomadaire') {
            $alerte->ale_date_heure_prev = $alerte->ale_date_heure_prev->addWeeks(1);
        }
        $alerte->save();
    }
}
