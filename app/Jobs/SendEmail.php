<?php

namespace App\Jobs;

use App\Models\EnvoiAlertes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
       $this->data=$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id=$this->data['id'];
        $to_email=$this->data['email'];
        $subject=$this->data['subject'];
        Mail::send('emails.messages.email', $this->data, function ($message) use ($to_email,$subject) {
            $message->from(config('mail.mail_from'));
            $message->to($to_email)->subject($subject);
        });
        $envoi=EnvoiAlertes::where('ena_id',$id)->first();
        $envoi->ena_isenvoi=1;
        $envoi->save();
    }
}
