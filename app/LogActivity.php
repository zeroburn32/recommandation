<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Spatie\Activitylog\Models\Activity;

class LogActivity extends Activity
{
    //
    public $fillable = [
        'email',
        'adresse_ip',
        'user_agent',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user = Auth::user();
            $model->email = is_object($user) ? $user->email : 'system@system.sys';
            $model->adresse_ip = Request::ip();
            $model->user_agent = Request::header('user-agent');
        });
        static::updating(function ($model) {
            $user = Auth::user();
            $model->email = is_object($user) ? $user->email : 'system@system.sys';
            $model->adresse_ip = Request::ip();
            $model->user_agent = Request::header('user-agent');
        });
    }
}
