<?php

/**
 * Created by PhpStorm.
 * User: Guillaume NYANTUDRE
 * Date: 27/09/2019
 * Time: 11:27
 */

namespace App;

use DB;
use Illuminate\Support\Facades\Auth;

class Methods
{
    static function getAgent($age_id)
    {
        $agent = DB::table('agents')
            //->join('Agents', 'Agents.structure', '=', 'structures.str_id')
            ->where('age_id', '=', $age_id)
            ->first();

        return $agent;
    }
    static function getStructure($id)
    {
        $structure = DB::table('structures')
            //->join('Agents', 'Agents.structure', '=', 'structures.str_id')
            ->where('str_id', '=', $id)
            ->first();

        return $structure;
    }

}
