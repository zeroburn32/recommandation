<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Acteur
 * @package App\Models
 * @version February 17, 2020, 11:36 pm UTC
 *
 * @property integer ins_id
 * @property integer str_id
 * @property integer act_is_css
 * @property integer act_is_csr
 * @property integer act_is_asr
 */
class Acteur extends Model
{

    use LogsActivity;
    public $table = 'acteurs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $primaryKey = 'act_id';

    public $fillable = [
        'ins_id',
        'str_id',
        'act_is_css',
        'act_is_csr',
        'act_is_asr'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'act_id' => 'integer',
        'ins_id' => 'integer',
        'str_id' => 'integer',
        'act_is_css' => 'integer',
        'act_is_csr' => 'integer',
        'act_is_asr' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ins_id' => 'required',
        'str_id' => 'required'
    ];

    protected static $logName = 'Acteur';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
}
