<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Activites
 * @package App\Models
 * @version February 26, 2020, 1:29 pm UTC
 *
 * @property integer rec_id
 * @property integer ptn_id
 * @property string act_description
 * @property integer act_is_realise
 * @property integer act_is_valide
 * @property integer act_poids
 * @property string act_date_prevue
 * @property string act_date_realise
 * @property string act_preuve
 * @property string act_observation
 * @property string created_by
 * @property string updated_by
 */
class Activites extends Model
{

    use LogsActivity;
    public $table = 'activites';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'act_id';

    public $fillable = [
        'rec_id',
        'ptn_id',
        'act_description',
        'act_is_realise',
        'act_is_valide',
        'act_poids',
        'act_date_prevue',
        'act_date_realise',
        'act_preuve',
        'act_observation',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'act_id' => 'integer',
        'rec_id' => 'integer',
        'ptn_id' => 'integer',
        'act_description' => 'string',
        'act_is_realise' => 'integer',
        'act_is_valide' => 'integer',
        'act_poids' => 'integer',
        'act_date_prevue' => 'date',
        'act_date_realise' => 'date',
        'act_preuve' => 'string',
        'act_observation' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rec_id' => 'required',
        'ptn_id' => 'required',
        'act_description' => 'required',
        'act_is_realise' => 'required',
        'act_is_valide' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    protected static $logName = 'Activités';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
    
}
