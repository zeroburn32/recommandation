<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;


/**
 * Class Alertes
 * @package App\Models
 * @version February 24, 2020, 10:13 am UTC
 *
 * @property integer ses_id
 * @property integer tmsg_id
 * @property string ale_msg
 * @property string|\Carbon\Carbon ale_date_heure_prev
 * @property integer ale_etat
 * @property string ale_tmsg_type
 * @property string|\Carbon\Carbon ale_date_heure_envoi
 * @property string created_by
 * @property string updated_by
 */
class Alertes extends Model
{

   // use LogsActivity;
    public $table = 'alertes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'ale_id';

    public $fillable = [
        'ses_id',
        'tmsg_id',
        'ale_msg',
        'ale_date_heure_prev',
        'ale_etat',
        'ale_tmsg_type',
        'ale_date_heure_envoi',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ale_id' => 'integer',
        'ses_id' => 'integer',
        'tmsg_id' => 'integer',
        'ale_msg' => 'string',
        'ale_date_heure_prev' => 'dateTime',
        'ale_etat' => 'integer',
        'ale_tmsg_type' => 'string',
        'ale_date_heure_envoi' => 'dateTime',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ses_id' => 'required',
        'tmsg_id' => 'required',
        'ale_msg' => 'required',
        'ale_date_heure_prev' => 'required',
        'ale_etat' => 'required',
        'ale_tmsg_type' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];




}
