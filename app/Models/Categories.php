<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Categories
 * @package App\Models
 * @version April 16, 2020, 3:10 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection structures
 * @property string cat_libelle
 * @property boolean cat_is_ministere
 */
class Categories extends Model
{
    use LogsActivity;

    public $table = 'categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'cat_id';
    protected static $logName = 'Paramètre';


    public $fillable = [
        'cat_libelle',
        'cat_is_ministere'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'cat_id' => 'integer',
        'cat_libelle' => 'string',
        'cat_is_ministere' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cat_libelle' => 'required',
        'cat_is_ministere' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function structures()
    {
        return $this->hasMany(\App\Models\Structure::class, 'str_categorie');
    }


    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création de categorie structure";
        } elseif ($eventName == 'updated') {
            return "Modification de categorie structure";
        } elseif ($eventName == 'deleted') {
            return "Suppression de categorie structure";
        }
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user = Auth::user();
            $model->created_by = $user->email;
            //$model->updated_by = $user->email;
        });
        static::updating(function ($model) {
            $user = Auth::user();
            $model->updated_by = $user->email;
        });
    }
}
