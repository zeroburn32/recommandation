<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class EnvoiAlertes
 * @package App\Models
 * @version April 30, 2020, 1:05 pm UTC
 *
 * @property \App\Models\ListeAlerte user
 * @property \App\Models\Alerte ale
 * @property integer user_id
 * @property integer ale_id
 * @property string ena_msg
 * @property string ale_mail_subject
 * @property string|\Carbon\Carbon ale_date_heure_prev
 * @property string|\Carbon\Carbon ena_date_heure_recu
 * @property integer ena_isenvoi
 * @property integer ena_is_lue
 * @property string|\Carbon\Carbon ena_read_at
 * @property string ena_ses_description
 * @property integer ena_ses_annee
 * @property string ena_ses_debut
 * @property string|\Carbon\Carbon ena_ses_fin
 * @property string ena_ses_lieu
 * @property string user_nom
 * @property string user_prenom
 * @property string user_email
 * @property string str_id
 * @property string str_sigle
 * @property string str_nom_complet
 * @property string created_by
 * @property string updated_by
 * @property string created_at
 * @property string updated_at
 */

class EnvoiAlertes extends Model
{

    public $table = 'envoialertes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'ena_id';

    public $fillable = [
        'user_id',
        'ale_id',
        'ena_msg',
        'ale_mail_subject',
        'ale_date_heure_prev',
        'ena_date_heure_recu',
        'ena_isenvoi',
        'ena_is_lue',
        'ena_read_at',
        'ena_ses_description',
        'ena_ses_annee',
        'ena_ses_debut',
        'ena_ses_fin',
        'ena_ses_lieu',
        'user_nom',
        'user_prenom',
        'user_email',
        'str_id',
        'str_sigle',
        'str_nom_complet',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ena_id' => 'integer',
        'user_id' => 'integer',
        'ale_id' => 'integer',
        'ena_msg' => 'string',
        'ale_mail_subject' => 'string',
        'ale_date_heure_prev' => 'datetime',
        'ena_date_heure_recu' => 'datetime',
        'ena_isenvoi' => 'integer',
        'ena_is_lue' => 'integer',
        'ena_read_at' => 'datetime',
        'ena_ses_description' => 'string',
        'ena_ses_annee' => 'integer',
        'ena_ses_debut' => 'date',
        'ena_ses_fin' => 'datetime',
        'ena_ses_lieu' => 'string',
        'user_nom' => 'string',
        'user_prenom' => 'string',
        'user_email' => 'string',
        'str_id' => 'integer',
        'str_sigle' => 'string',
        'str_nom_complet' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'ale_id' => 'required',
        'ena_date_heure_recu' => 'required',
        'ena_isenvoi' => 'required',
        'ena_is_lue' => 'required',
        'ena_ses_description' => 'required',
        'ena_ses_annee' => 'required',
        'ena_ses_debut' => 'required',
        'ena_ses_lieu' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\ListeAlerte::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ale()
    {
        return $this->belongsTo(\App\Models\Alerte::class, 'ale_id');
    }
}
