<?php
/**
 * Created by PhpStorm.
 * User: abdou
 * Date: 27/03/2020
 * Time: 10:07
 */
namespace App\Models;

use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets ;

class ImportRecos implements WithMultipleSheets
{
    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            0 => new SheetImprotRecos(),
            1 => new SheetImportParams()
        ];
    }
}