<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Instance
 * @package App\Models
 * @version February 17, 2020, 11:39 pm UTC
 *
 * @property string ins_sigle
 * @property string ins_nom_complet
 * @property integer ins_nbr_session
 * @property string ins_periodicite
 * @property string ins_description
 * @property string ins_ref_arrete
 * @property string created_by
 * @property string updated_by
 */
class Instance extends Model
{
    use LogsActivity; 

    public $table = 'instances';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'ins_id';

    public $fillable = [
//        'bud_id',
        'ins_sigle',
        'ins_nom_complet',
        'ins_nbr_session',
        'ins_periodicite',
        'ins_description',
        'ins_ref_arrete',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ins_id' => 'integer',
//        'bud_id' => 'integer',
        'ins_sigle' => 'string',
        'ins_nom_complet' => 'string',
        'ins_nbr_session' => 'integer',
        'ins_periodicite' => 'string',
        'ins_description' => 'string',
        'ins_ref_arrete' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'bud_id' => 'required',
        'ins_sigle' => 'required',
        'ins_nom_complet' => 'required',
        'ins_nbr_session' => 'required',
        'ins_periodicite' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    protected static $logName = 'Instance';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
}
