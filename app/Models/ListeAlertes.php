<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class ListeAlertes
 * @package App\Models
 * @version April 1, 2020, 10:25 am UTC
 *
 * @property \App\Models\Structure str
 * @property \App\Models\Session ses
 * @property \App\Models\User use
 * @property \Illuminate\Database\Eloquent\Collection alertes
 * @property integer ses_id
 * @property integer use_id
 * @property integer str_id
 * @property string lia_nom_prenom
 * @property string lia_email
 * @property string lia_sexe
 * @property string lia_fonction
 * @property string lia_matricule
 * @property string created_by
 * @property string updated_by
 */
class ListeAlertes extends Model
{

    use LogsActivity;

    public $table = 'liste_alertes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'lia_id';

    public $fillable = [
        'ses_id',
        'use_id',
        'str_id',
        'lia_nom_prenom',
        'lia_email',
        'lia_sexe',
        'lia_fonction',
        'lia_matricule',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'lia_id' => 'integer',
        'ses_id' => 'integer',
        'use_id' => 'integer',
        'str_id' => 'integer',
        'lia_nom_prenom' => 'string',
        'lia_email' => 'string',
        'lia_sexe' => 'string',
        'lia_fonction' => 'string',
        'lia_matricule' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ses_id' => 'required',
        'use_id' => 'required',
        'str_id' => 'required',
        'lia_nom_prenom' => 'required',
        'lia_email' => 'required',
//        'lia_sexe' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function str()
    {
        return $this->belongsTo(\App\Models\Structure::class, 'str_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ses()
    {
        return $this->belongsTo(\App\Models\Session::class, 'ses_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function age()
    {
        return $this->belongsTo(\App\Models\Agents::class, 'age_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function alertes()
    {
        return $this->belongsToMany(\App\Models\Alerte::class, 'envoialertes');
    }

    protected static $logName = 'Liste Alerte';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
}
