<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Partenaires
 * @package App\Models
 * @version February 27, 2020, 11:05 am UTC
 *
 * @property integer str_id
 * @property integer rec_id
 * @property integer ptn_is_responsable
 * @property string created_by
 * @property string updated_by
 */
class Partenaires extends Model
{

    use LogsActivity;

    public $table = 'partenaires';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'ptn_id';

    public $fillable = [
        'str_id',
        'rec_id',
        'ptn_is_responsable',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ptn_id' => 'integer',
        'str_id' => 'integer',
        'rec_id' => 'integer',
        'ptn_is_responsable' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'str_id' => 'required',
        'rec_id' => 'required',
        'ptn_is_responsable' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    protected static $logName = 'Partenaires';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
}
