<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Participants
 * @package App\Models
 * @version February 24, 2020, 10:14 am UTC
 *
 * @property integer str_id
 * @property integer ses_id
 */
class Participants extends Model
{

    use LogsActivity;

    public $table = 'participants';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'par_id';

    public $fillable = [
        'str_id',
        'ses_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'par_id' => 'integer',
        'str_id' => 'integer',
        'ses_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'str_id' => 'required',
        'ses_id' => 'required'
    ];

    protected static $logName = 'Participants';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
}
