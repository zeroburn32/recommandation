<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ProgrammeBudgetaires
 * @package App\Models
 * @version April 17, 2020, 2:57 pm UTC
 *
 * @property string bud_code
 * @property string bud_intitule
 */
class ProgrammeBudgetaires extends Model
{

    public $table = 'programme_budgetaire';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'bud_id';

    public $fillable = [
        'bud_code',
        'bud_intitule'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bud_id' => 'integer',
        'bud_code' => 'string',
        'bud_intitule' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bud_code' => 'required',
        'bud_intitule' => 'required'
    ];

    protected static $logName = 'Programme Budgetaire';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 

    
}
