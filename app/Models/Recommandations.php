<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Recommandations
 * @package App\Models
 * @version February 24, 2020, 10:14 am UTC
 *
 * @property integer ses_id
 * @property integer bud_id
 * @property integer rec_id_ref
 * @property string rec_intitule
 * @property string rec_date_echeance
 * @property integer rec_statut
 * @property integer rec_is_suivi_valide
 * @property integer rec_is_mode_standard
 * @property integer rec_is_planning_valide
 * @property integer rec_is_reconduit
 * @property string rec_observation
 * @property string rec_obs_nonrealise
 * @property string rec_obs_encours
 * @property string rec_obs_realise
 * @property string rec_personne_formule
 * @property string rec_personne_tel
 * @property string|\Carbon\Carbon rec_dh_valide
 * @property string|\Carbon\Carbon rec_dh_abandon
 * @property string|\Carbon\Carbon rec_dh_nonrealise
 * @property string|\Carbon\Carbon rec_dh_en_cours
 * @property string|\Carbon\Carbon rec_dh_realise
 * @property string|\Carbon\Carbon rec_dh_planning_valide
 * @property integer rec_taux_realise
 * @property integer rec_is_permanente
 * @property string rec_motif_abandon
 * @property string rec_preuve_nonrealise
 * @property string rec_preuve_encours
 * @property string rec_preuve_realise
 * @property string created_by
 * @property string updated_by
 */
class Recommandations extends Model
{

    use LogsActivity;

    public $table = 'recommandations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'rec_id';
    protected static $logName = 'Recommandation';

    public $fillable = [
        'ses_id',
        'bud_id',
        'rec_id_ref',
        'rec_intitule',
        'rec_date_echeance',
        'rec_statut',
        'rec_is_suivi_valide',
        'rec_is_mode_standard',
        'rec_is_planning_valide',
        'rec_is_reconduit',
        'rec_observation',
        'rec_obs_nonrealise',
        'rec_obs_encours',
        'rec_obs_realise',
        'rec_personne_formule',
        'rec_personne_tel',
        'rec_dh_valide',
        'rec_dh_abandon',
        'rec_dh_nonrealise',
        'rec_dh_en_cours',
        'rec_dh_realise',
        'rec_dh_planning_valide',
        'rec_taux_realise',
        'rec_is_permanente',
        'rec_motif_abandon',
        'rec_preuve_nonrealise',
        'rec_preuve_encours',
        'rec_preuve_realise',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rec_id' => 'integer',
        'bud_id' => 'integer',
        'rec_id_ref' => 'integer',
        'ses_id' => 'integer',
        'rec_intitule' => 'string',
        'rec_date_echeance' => 'date',
        'rec_statut' => 'integer',
        'rec_is_suivi_valide' => 'integer',
        'rec_is_mode_standard' => 'integer',
        'rec_is_planning_valide' => 'integer',
        'rec_is_reconduit' => 'integer',
        'rec_observation' => 'string',
        'rec_obs_nonrealise' => 'string',
        'rec_obs_encours' => 'string',
        'rec_obs_realise' => 'string',
        'rec_personne_formule' => 'string',
        'rec_personne_tel' => 'string',
        'rec_dh_valide' => 'datetime',
        'rec_dh_abandon' => 'datetime',
        'rec_dh_nonrealise' => 'datetime',
        'rec_dh_en_cours' => 'datetime',
        'rec_dh_realise' => 'datetime',
        'rec_dh_planning_valide' => 'datetime',
        'rec_taux_realise' => 'integer',
        'rec_is_permanente' => 'integer',
        'rec_motif_abandon' => 'string',
        'rec_preuve_nonrealise' => 'string',
        'rec_preuve_encours' => 'string',
        'rec_preuve_realise' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ses_id' => 'required',
        'bud_id' => 'required',
        'rec_intitule' => 'required',
        'rec_date_echeance' => 'required',
        'rec_statut' => 'required',
        'rec_is_suivi_valide' => 'required',
        'rec_is_mode_standard' => 'required',
        'rec_is_planning_valide' => 'required',
        'rec_is_permanente' => 'required',
        'rec_is_reconduit' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 

    
}
