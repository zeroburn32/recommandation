<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Session
 * @package App\Models
 * @version February 21, 2020, 10:57 am UTC
 *
 * @property integer ins_id
 * @property integer ses_statut
 * @property integer ses_id_precedante
 * @property string ses_description
 * @property integer ses_annee
 * @property string ses_date_debut
 * @property string ses_lieu
 * @property string ses_date_fin
 * @property string ses_datefin_planification
 * @property string ses_datefin_realisation
 * @property string ses_liste_participants
 * @property integer ses_is_planning_valide
 * @property string created_by
 * @property string updated_by
 */
class Session extends Model
{
    use LogsActivity;

    public $table = 'sessions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $primaryKey = 'ses_id';

    public $fillable = [
        'ins_id',
        'ses_statut',
        'ses_id_precedante',
        'ses_description',
        'ses_annee',
        'ses_date_debut',
        'ses_lieu',
        'ses_date_fin',
        'ses_datefin_planification',
        'ses_datefin_realisation',
        'ses_liste_participants',
        'ses_is_planning_valide',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ses_id' => 'integer',
        'ins_id' => 'integer',
        'ses_statut' => 'integer',
        'ses_id_precedante' => 'integer',
        'ses_description' => 'string',
        'ses_annee' => 'integer',
        'ses_date_debut' => 'date',
        'ses_lieu' => 'string',
        'ses_date_fin' => 'date',
        'ses_datefin_planification' => 'date',
        'ses_datefin_realisation' => 'date',
        'ses_liste_participants' => 'string',
        'ses_is_planning_valide' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ins_id' => 'required',
        'ses_statut' => 'required',
        'ses_description' => 'required',
        'ses_annee' => 'required',
        'ses_date_debut' => 'required',
        'ses_datefin_planification' => 'required',
        'ses_datefin_realisation' => 'required',
        'ses_lieu' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    protected static $logName = 'Session';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 

    
}
