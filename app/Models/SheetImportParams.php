<?php
/**
 * Created by PhpStorm.
 * User: abdou
 * Date: 06/04/2020
 * Time: 13:41
 */

namespace App\Models;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class SheetImportParams implements SkipsUnknownSheets
{

    /**
     * @param string|int $sheetName
     */
    public function onUnknownSheet($sheetName)
    {
        // TODO: Implement onUnknownSheet() method.
        info("Sheet {$sheetName} was skipped");
    }
}