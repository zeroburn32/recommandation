<?php
/**
 * Created by PhpStorm.
 * User: abdou
 * Date: 06/04/2020
 * Time: 12:52
 */

namespace App\Models;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SheetImprotRecos implements ToCollection
{
    public function collection(Collection $rows)
    {
        file_put_contents(public_path() . '/errors/erreurs_importation_recos.log', '');
        Log::channel('donneesLog')->info('Fichier ouvert : importation des recommandations du ' . new Carbon() . ' !');
        Log::channel('importsLog')->info('###############################################################################################################################"');
        if (!empty($rows) && $rows->count()) {
            $user = auth()->user();
            $lignes = '';
            $nbre = 1;
            $updated_row = 0;
            $created_row = 0;
            $unsed_row = 0;
            foreach ($rows as $index=>$row) {
                /** verifier que le n° le code de la session est different de null */
                $ses_description = $row[0];
                if (!is_null($ses_description) && $index > 0) {
                    $rec_personne_formule = $row[3];
                    $rec_intitule = $row[1];
                    $rec_date_echeance = $row[2];
                    $rec_personne_tel = $row[4];
                    $rec_is_permanente = intval($row[5]);
                    $codePB = $row[6];
                    /** recuperation de la session */
                    $session = DB::table('sessions')->where('ses_description', '=', $ses_description)->first();
                    /** recuperation du programme budgetaire */
                    $pBudge = DB::table('programme_budgetaire')->where('bud_code', '=', $codePB)->first();
                    if (!is_null($session)) {
                        if (!is_null($pBudge)) {
                            $check = \App\Http\Controllers\MethodesStaticController::checkLibelleReco($session->ses_id, $rec_intitule);
                            $reco = DB::table('recommandations')->where('rec_intitule', '=', $rec_intitule)->first();
                            if (is_null($check)) {
                                $recommandation = new Recommandations();
                                $recommandation->ses_id = $session->ses_id;
                                $recommandation->bud_id = $pBudge->bud_id;
                                $recommandation->rec_intitule = $rec_intitule;
                                $recommandation->rec_statut = 1;
                                $recommandation->rec_is_mode_standard = 0;
                                $recommandation->rec_date_echeance = Carbon::createFromFormat('d/m/Y', $rec_date_echeance);
                                $recommandation->rec_personne_formule = $rec_personne_formule;
                                $recommandation->rec_personne_tel = $rec_personne_tel;
                                $recommandation->rec_is_permanente = $rec_is_permanente;
                                $recommandation->created_by = $user->name;
                                $recommandation->updated_by = $user->name;
                                if ($check == null) {
                                    try {
                                        $result = $recommandation->save();
                                        if (!empty($result)) {
                                            Log::channel('importsLog')->info('La recommandation de la ligne ' . $nbre . '. a été enregistrer avec succès');
                                            $created_row++;
                                        }
                                    } catch (QueryException $e) {
                                        Log::channel('importsLog')->info('Erreur à la ligne ' . $nbre . '. Certaines données sur cette ligne sont invalide pour l\'enregistrement, merci de les corrigées.');
                                        $lignes = $lignes . ' ' . $nbre;
                                        $unsed_row++;
                                    }
                                } else {
                                    Log::channel('importsLog')->info('Erreur à la ligne ' . $nbre . '. Intitulé de la recommandation existe déjà, merci de le corrigé puis réessayer.');
                                    $lignes = $lignes . ' ' . $nbre;
                                    $unsed_row++;
                                }
                            } else {
                                if ($check == null || (!empty($check) && $reco->rec_intitule == $rec_intitule)) {
                                    try {
                                        $result = DB::table('recommandations')->where('rec_id', $reco->rec_id)->update([
                                            'ses_id' => $session->ses_id,
                                            'bud_id' => $pBudge->bud_id,
                                            'rec_intitule' => $rec_intitule,
                                            'rec_date_echeance' => Carbon::createFromFormat('d/m/Y', $rec_date_echeance),
                                            'rec_personne_formule' => $rec_personne_formule,
                                            'rec_personne_tel' => $rec_personne_tel,
                                            'rec_is_permanente' => $rec_is_permanente,
                                            'updated_by' => $user->name,
                                            'updated_at' => Carbon::now()
                                        ]);
                                        if (!empty($result)) {
                                            Log::channel('importsLog')->info('La recommandation de la ligne ' . $nbre . '. a été modifier avec succès');
                                            $updated_row++;
                                        }
                                    } catch (QueryException $e) {
                                        Log::channel('importsLog')->info('Erreur à la ligne ' . $nbre . '. Certaines données sur cette ligne sont invalide pour la modification, merci de les corrigées.');
                                        $lignes = $lignes . ' ' . $nbre;
                                        $unsed_row++;
                                    }
                                } else {
                                    Log::channel('importsLog')->info('Erreur à la ligne ' . $nbre . '. Intitulé de la recommandation existe déjà, merci de le corrigé puis réessayer.');
                                    $lignes = $lignes . ' ' . $nbre;
                                    $unsed_row++;
                                }
                            }
                        }
                        else {
                            Log::channel('importsLog')->info('Erreur la ligne ' . $nbre . '. Le programme budgétaire : ' . $codePB . ' n\'existe pas dans la base de données !');
                            $lignes = $lignes . ' ' . $nbre;
                            $unsed_row++;
                        }
                    }
                    else {
                        Log::channel('importsLog')->info('Erreur la ligne ' . $nbre . '. Une session avec la description : ' . $ses_description . ' n\'existe pas dans la base de données !');
                        $lignes = $lignes . ' ' . $nbre;
                        $unsed_row++;
                    }
                } else {
                    Log::channel('importsLog')->info('Erreur la ligne ' . $nbre . '. La description de la session ne peut pas être vide !');
                    $lignes = $lignes . ' ' . $nbre;
                    $unsed_row++;
                }
                $nbre = $nbre + 1;
            }
            Log::channel('importsLog')->info('###############################################################################################################################');
            Log::channel('importsLog')->info('Nombre de recommandations ajouter : ' . $created_row);
            Log::channel('importsLog')->info('Nombre de recommandations mise à jour : ' . $updated_row);
            Log::channel('importsLog')->info('Nombre de recommandations non traitées : ' . $unsed_row);
        }
        else {
            Log::channel('importsLog')->info('Fichier non ouvert !');
            Log::channel('importsLog')->info('###############################################################################################################################"');
        }
    }
}