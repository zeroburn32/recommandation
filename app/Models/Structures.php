<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

 /**
 * Class Structures
 * @package App\Models
 * @version February 20, 2020, 11:22 am UTC
 *
 * @property string str_sigle
 * @property string str_nom_complet
 * @property integer str_categorie
 * @property string created_by
 * @property string updated_by
 */
class Structures extends Model
{

    use LogsActivity;

    public $table = 'structures';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'str_id';
    //protected static $logFillable = true;
    protected static $logName = 'Paramètre';

    public $fillable = [
        'str_sigle',
        'str_nom_complet',
        'str_categorie',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'str_id' => 'integer',
        'str_sigle' => 'string',
        'str_nom_complet' => 'string',
        'str_categorie' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'str_sigle' => 'required',
        'str_nom_complet' => 'required',
        'str_categorie' => 'required',
        //'created_by' => 'required',
        //'created_at' => 'required'
    ];

    public function getDescriptionForEvent(string $eventName): string
    {
        if($eventName == 'created') {
            return "Création de structure";
        } elseif($eventName == 'updated') {
            return "Modification de structure";
        } elseif ($eventName == 'deleted') {
            return "Suppression de structure";
        }
        
    } 

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user = Auth::user();
            $model->created_by = $user->email;
            //$model->updated_by = $user->email;
        });
        static::updating(function ($model) { 
            $user = Auth::user();
            $model->updated_by = $user->email;
        });
    }
    
}
