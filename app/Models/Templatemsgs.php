<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Templatemsgs
 * @package App\Models
 * @version February 24, 2020, 9:42 am UTC
 *
 * @property string tmsg_type
 * @property string tmsg_msg
 * @property string tmsg_periodicite
 * @property string created_by
 * @property string updated_by
 */
class Templatemsgs extends Model
{

    use LogsActivity;

    public $table = 'templatemsgs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    protected $primaryKey = 'tmsg_id';

    public $fillable = [
        'tmsg_type',
        'tmsg_msg',
        'tmsg_periodicite',
        'hashmsg',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tmsg_id' => 'integer',
        'tmsg_type' => 'string',
        'tmsg_msg' => 'string',
        'tmsg_periodicite' => 'string',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tmsg_type' => 'required',
        'tmsg_msg' => 'required',
        'tmsg_periodicite' => 'required',
        'created_by' => 'required',
        'created_at' => 'required'
    ];

    protected static $logName = 'Template message';
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création";
        } elseif ($eventName == 'updated') {
            return "Modification";
        } elseif ($eventName == 'deleted') {
            return "Suppression";
        }
    } 
}
