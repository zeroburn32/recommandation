<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Traits\CausesActivity;

/**
 * Class Users
 * @package App\Models
 * @version February 13, 2020, 1:00 am UTC
 *
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string remember_token
 */
class Users extends Model
{
    use Notifiable, HasRoles , CausesActivity, LogsActivity;
    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    //protected static $logFillable = true;
    protected static $logName = 'Administration';




    public $fillable = [
        'str_id',
        'name',
        'email',
        'password',
        'nom',
        'prenom',
        'sexe',
        'fonction',
        'matricule',
        'telephone',
        'lastconnect',
        'fails_connect',
        'is_blocked'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'str_id' => 'integer',
        'id' => 'integer',
        'name' => 'string',
        'nom' => 'string',
        'prenom' => 'string',
        'sexe' => 'string',
        'fonction' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required',
        'email' => 'required',
        //'password' => 'required'
    ];
    
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création de compte";
        } elseif ($eventName == 'updated') {
            return "Modification de compte";
        } elseif ($eventName == 'deleted') {
            return "Suppression de compte";
        }
    }
    
}
