<?php

namespace App\Notifications;


use App\Models\EnvoiAlertes;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AlerteNotification extends Notification
{
    use Queueable;
    private $envoialerte;


    public function __construct($envoialerte)
    {
        $this->envoialerte=$envoialerte;
    }

    public function via($notifiable)
    {
        return ['database'];
    }
    public function toDatabase()
    {
      /*  return [
            'id'=>$this->envoialerte->ena_id,
            'msg'=>$this->envoialerte->ena_msg,
            'date_recu'=>$this->envoialerte->ena_date_heure_recu->format('d-m-Y H:i:s'),
            'ses_annee'=>$this->envoialerte->ena_ses_annee,
        'description'=>$this->envoialerte->ena_ses_description,
            'ses_lieu'=>$this->envoialerte->ena_ses_lieu,
            'ses_debut'=>$this->envoialerte->ena_ses_debut->format('d-m-Y'),
        ]; */
      return $this->envoialerte;
    }


    public function toArray($notifiable)
    {
        return [

        ];
    }
}
