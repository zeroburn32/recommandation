<?php

namespace App\Providers;

use App\Models\EnvoiAlertes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\MethodesStaticController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $periodicites = ['Mensuelle', 'Trimestrielle', 'Semestrielle', 'Annuelle', 'Biennale', 'Quatriennale'];
        $periodicite_msg = ['Aucune','Hebdomadaire','Mensuelle', 'Trimestrielle', 'Semestrielle', 'Annuelle'];
        $type_msg = ['PLANIFICATION','SUIVI', 'AUTRE'];
        $statuts = [
            1 => 'Préparation',
            2 => 'Non réalisée',
            3 => 'En cours',
            4 => 'Réalisée',
            5 => 'Abandonnée'
        ];
        $statutsSessions = [
            1 => 'En formulation',
            2 => 'En planification',
            3 => 'En réalisation',
            4 => 'Suivi validé'
        ];
        $categories = array(
            'key' => '1', 'value' => 'Direction Centrale',
            'key' => '2', 'value' => 'Direction Générale',
            'key' => '3', 'value' => 'DREA',
            'key' => '4', 'value' => 'Agence de l\'eau',
            'key' => '5', 'value' => 'Structure rattachée',
            'key' => '6', 'value' => 'PTF',
            'key' => '7', 'value' => 'OSC',
            'key' => '8', 'value' => 'Privé',
            'key' => '9', 'value' => 'Autres',
        );
        $categories = "DREA";
//        dd($statuts);
        View::share('periodicites', $periodicites);
        View::share('categories', $categories);
        View::share('statuts', $statuts);
        View::share('statutsSessions', $statutsSessions);
        View::share('periodicite_msg', $periodicite_msg);
        View::share('type_msg', $type_msg);
        View::composer('*', function($view) {
            if (Auth::check()) {
                $nbr_alerte=EnvoiAlertes::where('user_id', Auth::id())->where('ena_is_lue',0)->get();
                $list_alerte=EnvoiAlertes::where('user_id', Auth::id())->where('ena_is_lue',0)->limit(5)->get();
                //dd($list_alerte);
                View::share('nbr_alerte', $nbr_alerte);
                View::share('list_alerte', $list_alerte);
            }
        });

    }
}
