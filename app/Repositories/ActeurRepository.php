<?php

namespace App\Repositories;

use App\Models\Acteur;
use App\Repositories\BaseRepository;

/**
 * Class ActeurRepository
 * @package App\Repositories
 * @version February 17, 2020, 11:36 pm UTC
*/

class ActeurRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ins_id',
        'str_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Acteur::class;
    }
}
