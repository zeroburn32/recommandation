<?php

namespace App\Repositories;

use App\Models\Activites;
use App\Repositories\BaseRepository;

/**
 * Class ActivitesRepository
 * @package App\Repositories
 * @version February 26, 2020, 1:29 pm UTC
*/

class ActivitesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rec_id',
        'ptn_id',
        'act_description',
        'act_is_realise',
        'act_is_valide',
        'act_poids',
        'act_date_prevue',
        'act_date_realise',
        'act_preuve',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Activites::class;
    }
}
