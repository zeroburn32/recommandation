<?php

namespace App\Repositories;

use App\Models\Agents;
use App\Repositories\BaseRepository;

/**
 * Class AgentsRepository
 * @package App\Repositories
 * @version March 7, 2020, 12:14 pm UTC
*/

class AgentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'nom_prenom',
        'structure',
        'matricule',
        'telephone',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agents::class;
    }
}
