<?php

namespace App\Repositories;

use App\Models\Alertes;
use App\Repositories\BaseRepository;

/**
 * Class AlertesRepository
 * @package App\Repositories
 * @version February 24, 2020, 10:13 am UTC
*/

class AlertesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ses_id',
        'tmsg_id',
        'ale_msg',
        'ale_date_heure_prev',
        'ale_etat',
        'ale_tmsg_type',
        'ale_date_heure_envoi',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Alertes::class;
    }
}
