<?php

namespace App\Repositories;

use App\Models\EnvoiAlertes;
use App\Repositories\BaseRepository;

/**
 * Class EnvoiAlertesRepository
 * @package App\Repositories
 * @version April 30, 2020, 1:05 pm UTC
*/

class EnvoiAlertesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'ale_id',
        'ena_msg',
        'ale_mail_subject',
        'ena_date_heure_recu',
        'ena_isenvoi',
        'ena_is_lue',
        'ena_read_at',
        'ena_ses_description',
        'ena_ses_annee',
        'ena_ses_debut',
        'ena_ses_fin',
        'ena_ses_lieu',
        'user_nom_prenom',
        'user_email',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EnvoiAlertes::class;
    }
}
