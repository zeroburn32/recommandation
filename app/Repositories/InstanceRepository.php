<?php

namespace App\Repositories;

use App\Models\Instance;
use App\Repositories\BaseRepository;

/**
 * Class InstanceRepository
 * @package App\Repositories
 * @version February 17, 2020, 11:39 pm UTC
*/

class InstanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ins_sigle',
        'ins_nom_complet',
        'ins_nbr_session',
        'ins_periodicite',
        'ins_description',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Instance::class;
    }
}
