<?php

namespace App\Repositories;

use App\Models\ListeAlertes;
use App\Repositories\BaseRepository;

/**
 * Class ListeAlertesRepository
 * @package App\Repositories
 * @version April 1, 2020, 10:25 am UTC
*/

class ListeAlertesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ses_id',
        'use_id',
        'str_id',
        'lia_nom',
        'lia_prenom',
        'lia_email',
        'lia_sexe',
        'lia_fonction',
        'lia_matricule',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ListeAlertes::class;
    }
}
