<?php

namespace App\Repositories;

use App\Models\ModelHasPermission;
use App\Repositories\BaseRepository;

/**
 * Class ModelHasPermissionRepository
 * @package App\Repositories
 * @version February 15, 2020, 4:38 pm UTC
*/

class ModelHasPermissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'model_type',
        'model_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ModelHasPermission::class;
    }
}
