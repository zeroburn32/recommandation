<?php

namespace App\Repositories;

use App\Models\Partenaires;
use App\Repositories\BaseRepository;

/**
 * Class PartenairesRepository
 * @package App\Repositories
 * @version February 27, 2020, 11:05 am UTC
*/

class PartenairesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'str_id',
        'rec_id',
        'ptn_is_responsable',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Partenaires::class;
    }
}
