<?php

namespace App\Repositories;

use App\Models\Participants;
use App\Repositories\BaseRepository;

/**
 * Class ParticipantsRepository
 * @package App\Repositories
 * @version February 24, 2020, 10:14 am UTC
*/

class ParticipantsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'str_id',
        'ses_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Participants::class;
    }
}
