<?php

namespace App\Repositories;

use App\Models\ProgrammeBudgetaires;
use App\Repositories\BaseRepository;

/**
 * Class ProgrammeBudgetairesRepository
 * @package App\Repositories
 * @version April 17, 2020, 2:57 pm UTC
*/

class ProgrammeBudgetairesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bud_code',
        'bud_intitule',
        'bud_annee'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProgrammeBudgetaires::class;
    }
}
