<?php

namespace App\Repositories;

use App\Models\Recommandations;
use App\Repositories\BaseRepository;

/**
 * Class RecommandationsRepository
 * @package App\Repositories
 * @version February 24, 2020, 10:14 am UTC
*/

class RecommandationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ses_id',
        'rec_intitule',
        'rec_date_echeance',
        'rec_statut',
        'rec_etat',
        'rec_is_suivi_valide',
        'rec_is_mode_standard',
        'rec_observation',
        'rec_personne_formule',
        'rec_personne_tel',
        'rec_dh_valide',
        'rec_dh_abandon',
        'rec_taux_realise',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recommandations::class;
    }
}
