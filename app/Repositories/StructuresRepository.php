<?php

namespace App\Repositories;

use App\Models\Structures;
use App\Repositories\BaseRepository;

/**
 * Class StructuresRepository
 * @package App\Repositories
 * @version February 20, 2020, 11:22 am UTC
*/

class StructuresRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'str_sigle',
        'str_nom_complet',
        'str_categorie',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Structures::class;
    }
}
