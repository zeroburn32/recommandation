<?php

namespace App\Repositories;

use App\Models\Templatemsgs;
use App\Repositories\BaseRepository;

/**
 * Class TemplatemsgsRepository
 * @package App\Repositories
 * @version February 24, 2020, 9:42 am UTC
*/

class TemplatemsgsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tmsg_type',
        'tmsg_msg',
        'tmsg_periodicite',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Templatemsgs::class;
    }
}
