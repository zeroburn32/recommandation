<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Traits\CausesActivity;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles, CausesActivity, LogsActivity;

    //protected static $logFillable = true;
    protected static $logName = 'Administration';

    protected $dates = [
        'created_at',
        'updated_at',
        // your other new column
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'age_id',
        'str_id',
        'name',
        'email',
        'password',
        'nom',
        'prenom',
        'sexe',
        'fonction',
        'matricule',
        'telephone', 
        'lastconnect',
        'fails_connect',
        'is_blocked'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return "Création de compte";
        } elseif ($eventName == 'updated') {
            return "Modification de compte";
        } elseif ($eventName == 'deleted') {
            return "Suppression de compte";
        }
    }
}
