<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Acteur;
use Faker\Generator as Faker;

$factory->define(Acteur::class, function (Faker $faker) {

    return [
        'ins_id' => $faker->word,
        'str_id' => $faker->word
    ];
});
