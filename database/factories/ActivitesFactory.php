<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Activites;
use Faker\Generator as Faker;

$factory->define(Activites::class, function (Faker $faker) {

    return [
        'rec_id' => $faker->word,
        'ptn_id' => $faker->word,
        'act_description' => $faker->text,
        'act_is_realise' => $faker->word,
        'act_is_valide' => $faker->word,
        'act_poids' => $faker->word,
        'act_date_prevue' => $faker->word,
        'act_date_realise' => $faker->word,
        'act_preuve' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
