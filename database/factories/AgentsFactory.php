<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Agents;
use Faker\Generator as Faker;

$factory->define(Agents::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'email' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'nom_prenom' => $faker->word,
        'structure' => $faker->word,
        'matricule' => $faker->word,
        'telephone' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word
    ];
});
