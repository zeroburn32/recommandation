<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Alertes;
use Faker\Generator as Faker;

$factory->define(Alertes::class, function (Faker $faker) {

    return [
        'ses_id' => $faker->word,
        'tmsg_id' => $faker->word,
        'ale_msg' => $faker->word,
        'ale_date_heure_prev' => $faker->date('Y-m-d H:i:s'),
        'ale_etat' => $faker->word,
        'ale_tmsg_type' => $faker->word,
        'ale_date_heure_envoi' => $faker->date('Y-m-d H:i:s'),
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
