<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EnvoiAlertes;
use Faker\Generator as Faker;

$factory->define(EnvoiAlertes::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'ale_id' => $faker->word,
        'ena_msg' => $faker->text,
        'ale_mail_subject' => $faker->word,
        'ena_date_heure_recu' => $faker->date('Y-m-d H:i:s'),
        'ena_isenvoi' => $faker->word,
        'ena_is_lue' => $faker->word,
        'ena_read_at' => $faker->date('Y-m-d H:i:s'),
        'ena_ses_description' => $faker->word,
        'ena_ses_annee' => $faker->randomDigitNotNull,
        'ena_ses_debut' => $faker->word,
        'ena_ses_fin' => $faker->date('Y-m-d H:i:s'),
        'ena_ses_lieu' => $faker->word,
        'user_nom_prenom' => $faker->word,
        'user_email' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
