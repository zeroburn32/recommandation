<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Instance;
use Faker\Generator as Faker;

$factory->define(Instance::class, function (Faker $faker) {

    return [
        'ins_sigle' => $faker->word,
        'ins_nom_complet' => $faker->word,
        'ins_nbr_session' => $faker->randomDigitNotNull,
        'ins_periodicite' => $faker->word,
        'ins_description' => $faker->text,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
