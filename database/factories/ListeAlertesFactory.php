<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ListeAlertes;
use Faker\Generator as Faker;

$factory->define(ListeAlertes::class, function (Faker $faker) {

    return [
        'ses_id' => $faker->word,
        'use_id' => $faker->word,
        'str_id' => $faker->word,
        'lia_nom' => $faker->word,
        'lia_prenom' => $faker->word,
        'lia_email' => $faker->word,
        'lia_sexe' => $faker->word,
        'lia_fonction' => $faker->word,
        'lia_matricule' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
