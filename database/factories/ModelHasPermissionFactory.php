<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ModelHasPermission;
use Faker\Generator as Faker;

$factory->define(ModelHasPermission::class, function (Faker $faker) {

    return [
        'model_type' => $faker->word,
        'model_id' => $faker->word
    ];
});
