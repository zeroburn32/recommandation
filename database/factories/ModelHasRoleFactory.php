<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ModelHasRole;
use Faker\Generator as Faker;

$factory->define(ModelHasRole::class, function (Faker $faker) {

    return [
        'model_type' => $faker->word,
        'model_id' => $faker->word
    ];
});
