<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Partenaires;
use Faker\Generator as Faker;

$factory->define(Partenaires::class, function (Faker $faker) {

    return [
        'str_id' => $faker->word,
        'rec_id' => $faker->word,
        'ptn_is_responsable' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
