<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Participants;
use Faker\Generator as Faker;

$factory->define(Participants::class, function (Faker $faker) {

    return [
        'str_id' => $faker->word,
        'ses_id' => $faker->word
    ];
});
