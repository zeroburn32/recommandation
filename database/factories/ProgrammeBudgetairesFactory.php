<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProgrammeBudgetaires;
use Faker\Generator as Faker;

$factory->define(ProgrammeBudgetaires::class, function (Faker $faker) {

    return [
        'bud_code' => $faker->word,
        'bud_intitule' => $faker->word,
        'bud_annee' => $faker->word
    ];
});
