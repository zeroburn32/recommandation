<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProgrammePbudgetaires;
use Faker\Generator as Faker;

$factory->define(ProgrammePbudgetaires::class, function (Faker $faker) {

    return [
        'bud_code' => $faker->word,
        'bud_intitule' => $faker->word,
        'bud_annee' => $faker->word
    ];
});
