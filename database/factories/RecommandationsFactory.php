<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Recommandations;
use Faker\Generator as Faker;

$factory->define(Recommandations::class, function (Faker $faker) {

    return [
        'ses_id' => $faker->word,
        'rec_intitule' => $faker->text,
        'rec_date_echeance' => $faker->word,
        'rec_statut' => $faker->word,
        'rec_etat' => $faker->word,
        'rec_is_suivi_valide' => $faker->word,
        'rec_is_mode_standard' => $faker->word,
        'rec_observation' => $faker->text,
        'rec_personne_formule' => $faker->word,
        'rec_personne_tel' => $faker->word,
        'rec_dh_valide' => $faker->date('Y-m-d H:i:s'),
        'rec_dh_abandon' => $faker->date('Y-m-d H:i:s'),
        'rec_taux_realise' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
