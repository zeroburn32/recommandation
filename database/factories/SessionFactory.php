<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Session;
use Faker\Generator as Faker;

$factory->define(Session::class, function (Faker $faker) {

    return [
        'ins_id' => $faker->word,
        'ses_description' => $faker->word,
        'ses_annee' => $faker->randomDigitNotNull,
        'ses_date_debut' => $faker->word,
        'ses_lieu' => $faker->word,
        'ses_date_fin' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
