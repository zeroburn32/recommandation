<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Templatemsgs;
use Faker\Generator as Faker;

$factory->define(Templatemsgs::class, function (Faker $faker) {

    return [
        'tmsg_type' => $faker->word,
        'tmsg_msg' => $faker->word,
        'tmsg_periodicite' => $faker->word,
        'created_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_by' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
