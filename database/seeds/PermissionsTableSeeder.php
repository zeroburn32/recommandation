<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permissions')->insert(['name' => 'CONSULTER_RAPPORT', 'label' =>'Génerer des rapports']);
        DB::table('permissions')->insert(['name' => 'CONSULTER_STATS', 'label' =>'Consulter les statistiques']);
        DB::table('permissions')->insert(['name' => 'AJOUTER_STRUCTURE', 'label' =>'Ajouter une structure']);
        DB::table('permissions')->insert(['name' => 'MODIFIER_STRUCTURE', 'label' =>'Modifier une structure']);
    }
}
