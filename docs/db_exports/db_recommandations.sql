-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 18 Février 2020 à 08:28
-- Version du serveur :  5.7.29-0ubuntu0.18.04.1
-- Version de PHP :  7.2.27-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `recommandations`
--

-- --------------------------------------------------------

--
-- Structure de la table `ACTEURS`
--

CREATE TABLE `ACTEURS` (
  `act_id` bigint(20) UNSIGNED NOT NULL,
  `ins_id` bigint(20) UNSIGNED NOT NULL,
  `str_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ACTIVITES`
--

CREATE TABLE `ACTIVITES` (
  `act_id` bigint(20) UNSIGNED NOT NULL,
  `rec_id` bigint(20) UNSIGNED NOT NULL,
  `ptn_id` bigint(20) UNSIGNED NOT NULL,
  `act_description` text NOT NULL,
  `act_is_realise` smallint(6) NOT NULL DEFAULT '0',
  `act_is_valide` smallint(6) NOT NULL,
  `act_poids` smallint(6) DEFAULT NULL,
  `act_date_prevue` date DEFAULT NULL,
  `act_date_realise` date DEFAULT NULL,
  `act_preuve` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ALERTES`
--

CREATE TABLE `ALERTES` (
  `ale_id` bigint(20) UNSIGNED NOT NULL,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `tmsg_id` bigint(20) UNSIGNED NOT NULL,
  `ale_msg` varchar(255) NOT NULL,
  `ale_date_heure_prev` datetime NOT NULL,
  `ale_etat` smallint(6) NOT NULL DEFAULT '0' COMMENT '0: non envoy?; 1:envoy',
  `ale_tmsg_type` varchar(50) NOT NULL COMMENT 'PREPARATION;SUIVI;AUTRE',
  `ale_date_heure_envoi` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ENVOIALERTES`
--

CREATE TABLE `ENVOIALERTES` (
  `ena_id` bigint(20) UNSIGNED NOT NULL,
  `lia_id` bigint(20) UNSIGNED NOT NULL,
  `ale_id` bigint(20) UNSIGNED NOT NULL,
  `ena_msg` varchar(255) NOT NULL,
  `ena_date_heure_recu` datetime NOT NULL,
  `ena_isenvoi` smallint(6) NOT NULL DEFAULT '0',
  `ena_isl_u` smallint(6) NOT NULL DEFAULT '0',
  `ena_ses_description` varchar(255) NOT NULL,
  `ena_ses_annee` int(11) NOT NULL,
  `ena_ses_debut` date NOT NULL,
  `ena_ses_lieu` varchar(255) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `INSTANCES`
--

CREATE TABLE `INSTANCES` (
  `ins_id` bigint(20) UNSIGNED NOT NULL,
  `ins_sigle` varchar(20) NOT NULL,
  `ins_nom_complet` varchar(255) NOT NULL,
  `ins_nbr_session` int(11) NOT NULL,
  `ins_periodicite` varchar(100) NOT NULL,
  `ins_description` text,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `LISTE_ALERTES`
--

CREATE TABLE `LISTE_ALERTES` (
  `lia_id` bigint(20) UNSIGNED NOT NULL,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `use_id` bigint(20) UNSIGNED NOT NULL,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `lia_nom` varchar(20) NOT NULL,
  `lia_prenom` varchar(255) NOT NULL,
  `lia_email` varchar(100) NOT NULL,
  `lia_sexe` char(1) NOT NULL,
  `lia_fonction` varchar(255) DEFAULT NULL,
  `lia_matricule` varchar(20) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_13_160023_create_permission_tables', 2),
(5, '2020_02_15_201905_create_permission_tables', 3);

-- --------------------------------------------------------

--
-- Structure de la table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'App\\User',
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 2),
(2, 'App\\User', 4),
(2, 'App\\User', 7);

-- --------------------------------------------------------

--
-- Structure de la table `PARTENAIRES`
--

CREATE TABLE `PARTENAIRES` (
  `ptn_id` bigint(20) UNSIGNED NOT NULL,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `rec_id` bigint(20) UNSIGNED NOT NULL,
  `ptn_is_responsable` smallint(6) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PARTICIPANTS`
--

CREATE TABLE `PARTICIPANTS` (
  `par_id` bigint(20) UNSIGNED NOT NULL,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `ses_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'AJOUTER', 'Ajout d\'un élement', 'web', NULL, NULL),
(2, 'MODIFIER', 'Modification d\'un élement', 'web', NULL, NULL),
(3, 'SUPPRIMER', 'Suppression d\'un élément', 'web', NULL, NULL),
(4, 'CONSULTER', 'Consultation d\'un élement', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `RECOMMANDATIONS`
--

CREATE TABLE `RECOMMANDATIONS` (
  `rec_id` bigint(20) UNSIGNED NOT NULL,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `rec_intitule` text NOT NULL,
  `rec_date_echeance` date NOT NULL,
  `rec_statut` smallint(6) NOT NULL COMMENT '1:Proposition;2:Valid?;3:Abandonn?; 4:Reconduit',
  `rec_etat` smallint(6) NOT NULL DEFAULT '0' COMMENT '-1:Non r?alis?; 0:Encours; 1:R?alis',
  `rec_is_suivi_valide` smallint(6) NOT NULL DEFAULT '0',
  `rec_is_mode_standard` smallint(6) NOT NULL DEFAULT '1' COMMENT '1: suivi par statut; 0: suivi par taux',
  `rec_observation` text,
  `rec_personne_formule` varchar(255) DEFAULT NULL,
  `rec_personne_tel` varchar(100) DEFAULT NULL,
  `rec_dh_valide` datetime DEFAULT NULL,
  `rec_dh_abandon` datetime DEFAULT NULL,
  `rec_taux_realise` smallint(6) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrateur', 'web', '2020-02-15 20:51:05', '2020-02-15 20:51:05'),
(2, 'Invite', 'Invité', 'web', '2020-02-15 20:58:08', '2020-02-15 20:58:08'),
(3, 'test2', 'tesr2', 'web', '2020-02-15 21:07:25', '2020-02-15 21:07:25');

-- --------------------------------------------------------

--
-- Structure de la table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(2, 2),
(1, 3),
(3, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `SESSIONS`
--

CREATE TABLE `SESSIONS` (
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `ins_id` bigint(20) UNSIGNED NOT NULL,
  `ses_description` varchar(255) NOT NULL,
  `ses_annee` int(11) NOT NULL,
  `ses_date_debut` date NOT NULL,
  `ses_lieu` varchar(255) NOT NULL,
  `ses_date_fin` date DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `STRUCTURES`
--

CREATE TABLE `STRUCTURES` (
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `str_sigle` varchar(20) NOT NULL,
  `str_nom_complet` varchar(255) NOT NULL,
  `str_categorie` int(11) NOT NULL COMMENT '1:Direction centrale; 2:DR; 3:AE; 4:PTF; 5:OSC; 6:Priv?; 7:Collectivit?; 8:Autres',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `TEMPLATEMSGS`
--

CREATE TABLE `TEMPLATEMSGS` (
  `tmsg_id` bigint(20) UNSIGNED NOT NULL,
  `tmsg_type` varchar(30) NOT NULL COMMENT 'PREPARATION;SUIVI;AUTRE',
  `tmsg_msg` varchar(255) NOT NULL,
  `tmsg_periodicite` varchar(50) NOT NULL COMMENT 'MENSUELLE; TRIMESTRIELLE;SEMESTRIELLE;ANNUELLE',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `firstconnect` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `nom_prenom` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `structure` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `matricule` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastconnect` timestamp NULL DEFAULT NULL,
  `fails_connect` int(11) DEFAULT '0',
  `is_blocked` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `firstconnect`, `is_active`, `nom_prenom`, `structure`, `matricule`, `telephone`, `lastconnect`, `fails_connect`, `is_blocked`) VALUES
(1, 'Administrateur', 'admin@admin.com', NULL, '$2y$10$TQX/FZ862Zx5Fvb0V2IiKuXpt7Ajsp5mY1GpaInV3dbi/gHUlJjbK', NULL, '2020-02-13 00:15:16', '2020-02-17 00:48:38', 1, 1, 'Administrateur', 'MEA', '45623 K', '+5522555', NULL, 0, 0),
(2, 'isidore', 'isidore@gmail.com', '2020-02-13 00:00:00', '$2y$10$.HWloo9KuWfJCS0AFTqwUeb0e5lIZZsHrSxKillaSWj6G1mm6i/6O', NULL, '2020-02-15 13:42:32', '2020-02-17 01:11:09', 1, 0, 'KABORE Isidore', 'DGIH', '5588440 L', '+566666666', NULL, 0, 0),
(4, 'souley02', 'alassane@asso.com', NULL, '$2y$10$2TFBb6nyw4CchNO6ulETfOZVMaZXoyYU5iJLnOsLfEHbFCILqaCa.', NULL, '2020-02-15 14:20:00', '2020-02-17 01:12:24', 1, 1, 'KONE Souley 02', 'MASN', 'PT588', '+332225555', NULL, 0, 0),
(5, 'barbe', 'barbe@bb.bb', NULL, '$2y$10$pzUXaDyPIbkDfQMcDN9MDOkAvvhFrp8n5mdpQ8pRnx7iQXYha8s6q', NULL, '2020-02-15 14:43:46', '2020-02-15 14:43:46', 1, 1, 'COULIBALY Barbe', 'Bolloré Inc', 'MP5632', '+8895621', NULL, 0, 0),
(7, 'alassane', 'asso@asso.com', NULL, '$2y$10$Dbzgpm9CHRecsPpS0Nh7QugFe6B99zZ6LSWQunM.jwSIPzR6R6DRO', NULL, '2020-02-15 14:54:22', '2020-02-15 14:54:22', 1, 1, 'KABORE Alassane', 'DGIH', '12556 J', '+5566332255', NULL, 0, 0),
(8, 'alassane', 'asso2@asso.com', NULL, '$2y$10$IsDgnbgBndfB9khcarQhJO6xeGnPelDbcr5plP6KP0JBP/w9CXacK', NULL, '2020-02-15 14:58:03', '2020-02-15 14:58:03', 1, 1, 'KABORE Alassane', 'DGIH', '12556 J', '+55663322454', NULL, 0, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ACTEURS`
--
ALTER TABLE `ACTEURS`
  ADD PRIMARY KEY (`act_id`),
  ADD UNIQUE KEY `act_id` (`act_id`),
  ADD KEY `ins_id` (`ins_id`),
  ADD KEY `str_id` (`str_id`);

--
-- Index pour la table `ACTIVITES`
--
ALTER TABLE `ACTIVITES`
  ADD PRIMARY KEY (`act_id`),
  ADD UNIQUE KEY `act_id` (`act_id`),
  ADD KEY `rec_id` (`rec_id`),
  ADD KEY `ptn_id` (`ptn_id`);

--
-- Index pour la table `ALERTES`
--
ALTER TABLE `ALERTES`
  ADD PRIMARY KEY (`ale_id`),
  ADD UNIQUE KEY `ale_id` (`ale_id`),
  ADD KEY `ses_id` (`ses_id`),
  ADD KEY `tmsg_id` (`tmsg_id`);

--
-- Index pour la table `ENVOIALERTES`
--
ALTER TABLE `ENVOIALERTES`
  ADD PRIMARY KEY (`ena_id`),
  ADD UNIQUE KEY `ena_id` (`ena_id`),
  ADD KEY `lia_id` (`lia_id`),
  ADD KEY `ale_id` (`ale_id`);

--
-- Index pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `INSTANCES`
--
ALTER TABLE `INSTANCES`
  ADD PRIMARY KEY (`ins_id`),
  ADD UNIQUE KEY `ins_id` (`ins_id`),
  ADD UNIQUE KEY `ins_sigle` (`ins_sigle`);

--
-- Index pour la table `LISTE_ALERTES`
--
ALTER TABLE `LISTE_ALERTES`
  ADD PRIMARY KEY (`lia_id`),
  ADD UNIQUE KEY `lia_id` (`lia_id`),
  ADD KEY `str_id` (`str_id`),
  ADD KEY `ses_id` (`ses_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Index pour la table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Index pour la table `PARTENAIRES`
--
ALTER TABLE `PARTENAIRES`
  ADD PRIMARY KEY (`ptn_id`),
  ADD UNIQUE KEY `ptn_id` (`ptn_id`),
  ADD KEY `str_id` (`str_id`),
  ADD KEY `rec_id` (`rec_id`);

--
-- Index pour la table `PARTICIPANTS`
--
ALTER TABLE `PARTICIPANTS`
  ADD PRIMARY KEY (`par_id`),
  ADD UNIQUE KEY `par_id` (`par_id`),
  ADD KEY `str_id` (`str_id`),
  ADD KEY `ses_id` (`ses_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `RECOMMANDATIONS`
--
ALTER TABLE `RECOMMANDATIONS`
  ADD PRIMARY KEY (`rec_id`),
  ADD UNIQUE KEY `rec_id` (`rec_id`),
  ADD KEY `ses_id` (`ses_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Index pour la table `SESSIONS`
--
ALTER TABLE `SESSIONS`
  ADD PRIMARY KEY (`ses_id`),
  ADD UNIQUE KEY `ses_id` (`ses_id`),
  ADD KEY `ins_id` (`ins_id`);

--
-- Index pour la table `STRUCTURES`
--
ALTER TABLE `STRUCTURES`
  ADD PRIMARY KEY (`str_id`),
  ADD UNIQUE KEY `str_id` (`str_id`),
  ADD UNIQUE KEY `str_sigle` (`str_sigle`),
  ADD UNIQUE KEY `str_nom_complet` (`str_nom_complet`);

--
-- Index pour la table `TEMPLATEMSGS`
--
ALTER TABLE `TEMPLATEMSGS`
  ADD PRIMARY KEY (`tmsg_id`),
  ADD UNIQUE KEY `tmsg_id` (`tmsg_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `ACTEURS`
--
ALTER TABLE `ACTEURS`
  MODIFY `act_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ACTIVITES`
--
ALTER TABLE `ACTIVITES`
  MODIFY `act_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ALERTES`
--
ALTER TABLE `ALERTES`
  MODIFY `ale_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ENVOIALERTES`
--
ALTER TABLE `ENVOIALERTES`
  MODIFY `ena_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `INSTANCES`
--
ALTER TABLE `INSTANCES`
  MODIFY `ins_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `LISTE_ALERTES`
--
ALTER TABLE `LISTE_ALERTES`
  MODIFY `lia_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `PARTENAIRES`
--
ALTER TABLE `PARTENAIRES`
  MODIFY `ptn_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PARTICIPANTS`
--
ALTER TABLE `PARTICIPANTS`
  MODIFY `par_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `RECOMMANDATIONS`
--
ALTER TABLE `RECOMMANDATIONS`
  MODIFY `rec_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `SESSIONS`
--
ALTER TABLE `SESSIONS`
  MODIFY `ses_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `STRUCTURES`
--
ALTER TABLE `STRUCTURES`
  MODIFY `str_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `TEMPLATEMSGS`
--
ALTER TABLE `TEMPLATEMSGS`
  MODIFY `tmsg_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
