-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: recommandations
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acteurs`
--

DROP TABLE IF EXISTS `acteurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acteurs` (
  `act_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ins_id` bigint(20) unsigned NOT NULL,
  `str_id` bigint(20) unsigned NOT NULL,
  `act_is_acteur_suivi` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`act_id`),
  UNIQUE KEY `act_id` (`act_id`),
  KEY `ins_id` (`ins_id`),
  KEY `str_id` (`str_id`),
  CONSTRAINT `acteurs_instances_fk` FOREIGN KEY (`ins_id`) REFERENCES `instances` (`ins_id`),
  CONSTRAINT `acteurs_structures_fk` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acteurs`
--

LOCK TABLES `acteurs` WRITE;
/*!40000 ALTER TABLE `acteurs` DISABLE KEYS */;
INSERT INTO `acteurs` VALUES (1,4,2,1,'Administrateur','2020-03-04 14:08:29','Administrateur','2020-03-04 14:08:29'),(3,4,5,0,'Administrateur','2020-03-04 14:08:51','Administrateur','2020-03-04 14:08:51'),(5,1,3,0,'Administrateur','2020-03-04 14:10:55','Administrateur','2020-03-04 14:10:55'),(6,1,1,1,'Administrateur','2020-03-04 17:00:47','Administrateur','2020-03-04 17:00:47');
/*!40000 ALTER TABLE `acteurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activites`
--

DROP TABLE IF EXISTS `activites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activites` (
  `act_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rec_id` bigint(20) unsigned NOT NULL,
  `ptn_id` bigint(20) unsigned NOT NULL,
  `act_description` text NOT NULL,
  `act_is_realise` smallint(6) NOT NULL DEFAULT '0',
  `act_is_valide` smallint(6) NOT NULL,
  `act_poids` smallint(6) DEFAULT NULL,
  `act_date_prevue` date DEFAULT NULL,
  `act_date_realise` date DEFAULT NULL,
  `act_preuve` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `act_observation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`act_id`),
  UNIQUE KEY `act_id` (`act_id`),
  KEY `rec_id` (`rec_id`),
  KEY `ptn_id` (`ptn_id`),
  CONSTRAINT `activites_ibfk_1` FOREIGN KEY (`rec_id`) REFERENCES `recommandations` (`rec_id`),
  CONSTRAINT `activites_ibfk_2` FOREIGN KEY (`ptn_id`) REFERENCES `partenaires` (`ptn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activites`
--

LOCK TABLES `activites` WRITE;
/*!40000 ALTER TABLE `activites` DISABLE KEYS */;
/*!40000 ALTER TABLE `activites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alertes`
--

DROP TABLE IF EXISTS `alertes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertes` (
  `ale_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ses_id` bigint(20) unsigned NOT NULL,
  `tmsg_id` bigint(20) unsigned NOT NULL,
  `ale_msg` varchar(255) NOT NULL,
  `ale_date_heure_prev` datetime NOT NULL,
  `ale_etat` smallint(6) NOT NULL DEFAULT '0' COMMENT '0: non envoy?; 1:envoy',
  `ale_tmsg_type` varchar(50) NOT NULL COMMENT 'PREPARATION;SUIVI;AUTRE',
  `ale_date_heure_envoi` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ale_id`),
  UNIQUE KEY `ale_id` (`ale_id`),
  KEY `ses_id` (`ses_id`),
  KEY `tmsg_id` (`tmsg_id`),
  CONSTRAINT `alertes_ibfk_1` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`),
  CONSTRAINT `alertes_ibfk_2` FOREIGN KEY (`tmsg_id`) REFERENCES `templatemsgs` (`tmsg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertes`
--

LOCK TABLES `alertes` WRITE;
/*!40000 ALTER TABLE `alertes` DISABLE KEYS */;
/*!40000 ALTER TABLE `alertes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `cat_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cat_libelle` varchar(70) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Direction centrale','2020-01-02 12:35:40',NULL),(2,'Direction générale','2020-01-02 12:35:40',NULL),(3,'DREA','2020-01-02 12:35:40',NULL),(4,'Agence de l\'Eau','2020-01-02 12:35:40',NULL),(5,'Secteur privé','2020-01-02 12:35:40',NULL),(6,'PTF','2020-01-02 12:35:40',NULL),(9,'OSC','2020-01-02 12:35:40',NULL),(10,'Structure rattachée','2020-01-02 12:35:40',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `envoialertes`
--

DROP TABLE IF EXISTS `envoialertes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `envoialertes` (
  `ena_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lia_id` bigint(20) unsigned NOT NULL,
  `ale_id` bigint(20) unsigned NOT NULL,
  `ena_msg` varchar(255) NOT NULL,
  `ena_date_heure_recu` datetime NOT NULL,
  `ena_isenvoi` smallint(6) NOT NULL DEFAULT '0',
  `ena_isl_u` smallint(6) NOT NULL DEFAULT '0',
  `ena_ses_description` varchar(255) NOT NULL,
  `ena_ses_annee` int(11) NOT NULL,
  `ena_ses_debut` date NOT NULL,
  `ena_ses_lieu` varchar(255) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ena_id`),
  UNIQUE KEY `ena_id` (`ena_id`),
  KEY `lia_id` (`lia_id`),
  KEY `ale_id` (`ale_id`),
  CONSTRAINT `envoialertes_ibfk_1` FOREIGN KEY (`lia_id`) REFERENCES `liste_alertes` (`lia_id`),
  CONSTRAINT `envoialertes_ibfk_2` FOREIGN KEY (`ale_id`) REFERENCES `alertes` (`ale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `envoialertes`
--

LOCK TABLES `envoialertes` WRITE;
/*!40000 ALTER TABLE `envoialertes` DISABLE KEYS */;
/*!40000 ALTER TABLE `envoialertes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instances`
--

DROP TABLE IF EXISTS `instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instances` (
  `ins_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ins_sigle` varchar(20) NOT NULL,
  `ins_nom_complet` varchar(255) NOT NULL,
  `ins_nbr_session` int(11) NOT NULL,
  `ins_periodicite` varchar(100) NOT NULL,
  `ins_description` text,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ins_id`),
  UNIQUE KEY `ins_id` (`ins_id`),
  UNIQUE KEY `ins_sigle` (`ins_sigle`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instances`
--

LOCK TABLES `instances` WRITE;
/*!40000 ALTER TABLE `instances` DISABLE KEYS */;
INSERT INTO `instances` VALUES (1,'CSD','Cadre Sectoriel de Dialogue',1,'annuel','Cadre Sectoriel de Dialogue','admin','2020-02-28 08:22:41',NULL,NULL),(2,'CASEM','Conseil d\'Administration du Secteur Ministériel',2,'annuel','Conseil d\'Administration du Secteur Ministériel','admin','2020-02-28 08:22:41',NULL,NULL),(3,'CRP','Comité de Revue et de Pilotage',2,'annuel','Comité de Revue','admin','2020-02-28 08:22:41',NULL,NULL),(4,'CP','Comité de pilotage',2,'Trimestrielle','Comité de pilotage des projets et programmes','Administrateur','2020-03-04 14:07:15','Administrateur','2020-03-04 14:07:15');
/*!40000 ALTER TABLE `instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liste_alertes`
--

DROP TABLE IF EXISTS `liste_alertes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liste_alertes` (
  `lia_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ses_id` bigint(20) unsigned NOT NULL,
  `use_id` bigint(20) unsigned NOT NULL,
  `str_id` bigint(20) unsigned NOT NULL,
  `lia_nom` varchar(20) NOT NULL,
  `lia_prenom` varchar(255) NOT NULL,
  `lia_email` varchar(100) NOT NULL,
  `lia_sexe` char(1) NOT NULL,
  `lia_fonction` varchar(255) DEFAULT NULL,
  `lia_matricule` varchar(20) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`lia_id`),
  UNIQUE KEY `lia_id` (`lia_id`),
  KEY `str_id` (`str_id`),
  KEY `ses_id` (`ses_id`),
  KEY `liste_alertes_users_FK` (`use_id`),
  CONSTRAINT `liste_alertes_ibfk_1` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`),
  CONSTRAINT `liste_alertes_ibfk_2` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`),
  CONSTRAINT `liste_alertes_users_FK` FOREIGN KEY (`use_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liste_alertes`
--

LOCK TABLES `liste_alertes` WRITE;
/*!40000 ALTER TABLE `liste_alertes` DISABLE KEYS */;
/*!40000 ALTER TABLE `liste_alertes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_02_13_160023_create_permission_tables',2),(5,'2020_02_15_201905_create_permission_tables',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'App\\User',
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (2,'App\\User',1),(1,'App\\User',2),(1,'App\\User',4),(2,'App\\User',5),(2,'App\\User',7),(3,'App\\User',9),(1,'App\\User',27),(1,'App\\User',29),(1,'App\\User',30),(1,'App\\User',31),(1,'App\\User',32),(1,'App\\User',35),(3,'App\\User',36),(2,'App\\User',37),(1,'App\\User',38),(3,'App\\User',39),(2,'App\\User',40),(1,'App\\User',41),(3,'App\\User',42),(2,'App\\User',43);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaires` (
  `ptn_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `str_id` bigint(20) unsigned NOT NULL,
  `rec_id` bigint(20) unsigned NOT NULL,
  `ptn_is_responsable` smallint(6) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ptn_id`),
  UNIQUE KEY `ptn_id` (`ptn_id`),
  KEY `str_id` (`str_id`),
  KEY `rec_id` (`rec_id`),
  CONSTRAINT `partenaires_ibfk_1` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`),
  CONSTRAINT `partenaires_ibfk_2` FOREIGN KEY (`rec_id`) REFERENCES `recommandations` (`rec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaires`
--

LOCK TABLES `partenaires` WRITE;
/*!40000 ALTER TABLE `partenaires` DISABLE KEYS */;
INSERT INTO `partenaires` VALUES (1,2,1,1,'Administrateur','2020-03-04 14:13:50','Administrateur','2020-03-04 14:13:50'),(2,4,1,0,'Administrateur','2020-03-04 14:13:54','Administrateur','2020-03-04 14:13:54'),(3,5,1,0,'Administrateur','2020-03-04 14:14:01','Administrateur','2020-03-04 14:14:01');
/*!40000 ALTER TABLE `partenaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participants` (
  `par_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `str_id` bigint(20) unsigned NOT NULL,
  `ses_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`par_id`),
  UNIQUE KEY `par_id` (`par_id`),
  KEY `str_id` (`str_id`),
  KEY `ses_id` (`ses_id`),
  CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`),
  CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participants`
--

LOCK TABLES `participants` WRITE;
/*!40000 ALTER TABLE `participants` DISABLE KEYS */;
INSERT INTO `participants` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,1,2),(6,2,2),(7,3,2),(8,4,2),(9,1,3),(10,2,3),(11,3,3),(12,4,3),(13,1,4),(14,2,4),(15,3,4),(16,4,4);
/*!40000 ALTER TABLE `participants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'AJOUTER','Ajout d\'un élement','web',NULL,NULL),(2,'MODIFIER','Modification d\'un élement','web',NULL,NULL),(3,'SUPPRIMER','Suppression d\'un élément','web',NULL,NULL),(4,'CONSULTER','Consultation d\'un élement','web',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommandations`
--

DROP TABLE IF EXISTS `recommandations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommandations` (
  `rec_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ses_id` bigint(20) unsigned NOT NULL,
  `rec_intitule` text NOT NULL,
  `rec_date_echeance` date NOT NULL,
  `rec_statut` smallint(6) NOT NULL COMMENT '1:Proposition;2:Valid?;3:Abandonn?; 4:Reconduit',
  `rec_etat` smallint(6) NOT NULL DEFAULT '0' COMMENT '-1:Non r?alis?; 0:Encours; 1:R?alis',
  `rec_is_suivi_valide` smallint(6) NOT NULL DEFAULT '0',
  `rec_is_mode_standard` smallint(6) NOT NULL DEFAULT '1' COMMENT '1: suivi par statut; 0: suivi par taux',
  `rec_observation` text,
  `rec_personne_formule` varchar(255) DEFAULT NULL,
  `rec_personne_tel` varchar(100) DEFAULT NULL,
  `rec_dh_valide` datetime DEFAULT NULL,
  `rec_dh_abandon` datetime DEFAULT NULL,
  `rec_taux_realise` smallint(6) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `rec_is_permanente` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rec_id`),
  UNIQUE KEY `rec_id` (`rec_id`),
  KEY `ses_id` (`ses_id`),
  CONSTRAINT `recommandations_ibfk_1` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommandations`
--

LOCK TABLES `recommandations` WRITE;
/*!40000 ALTER TABLE `recommandations` DISABLE KEYS */;
INSERT INTO `recommandations` VALUES (1,2,'Dsiponibiliser l\'argent à temps','2020-03-14',1,0,0,0,'Dsiponibiliser l\'argent à temps','DAF','+22677889966',NULL,NULL,NULL,'Administrateur','2020-03-04 14:12:24','Administrateur','2020-03-04 14:12:24',0),(2,2,'Plannifier toutes les activités','2020-05-02',1,0,0,1,'Plannifier toutes les activités','DGESS','+22688556633',NULL,NULL,NULL,'Administrateur','2020-03-04 14:13:10','Administrateur','2020-03-04 14:13:10',0);
/*!40000 ALTER TABLE `recommandations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(2,2),(1,3),(3,3),(4,3);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','Administrateur','web','2020-02-15 20:51:05','2020-02-15 20:51:05'),(2,'Invite','Invité','web','2020-02-15 20:58:08','2020-02-15 20:58:08'),(3,'test2','test2','web','2020-02-15 21:07:25','2020-02-15 21:07:25');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `ses_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ins_id` bigint(20) unsigned NOT NULL,
  `ses_description` varchar(255) NOT NULL,
  `ses_annee` int(11) NOT NULL,
  `ses_date_debut` date NOT NULL,
  `ses_lieu` varchar(255) NOT NULL,
  `ses_date_fin` date DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ses_statut` int(11) NOT NULL DEFAULT '0' COMMENT '0: EN PREPARATION; 1: EN COURS; 2: TERMINER',
  PRIMARY KEY (`ses_id`),
  UNIQUE KEY `ses_id` (`ses_id`),
  KEY `ins_id` (`ins_id`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`ins_id`) REFERENCES `instances` (`ins_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,1,'1ère session CSD',2020,'2020-02-21','Salle de conférence MEA',NULL,'admin','2020-02-23 07:02:33',NULL,NULL,0),(2,1,'2è session CSD',2021,'2021-02-21','Salle de conférence MEA',NULL,'admin','2020-02-23 07:02:33',NULL,NULL,0),(3,2,'1ère session CASEM',2020,'2020-02-21','Salle de conférence MEA',NULL,'admin','2020-02-23 07:02:33',NULL,NULL,0),(4,2,'2è session CASEM',2021,'2021-02-21','Salle de conférence MEA',NULL,'admin','2020-02-23 07:02:33',NULL,NULL,0),(5,3,'1ère session CRP',2020,'2020-02-21','Salle de conférence MEA',NULL,'admin','2020-02-23 07:02:33','Administrateur',NULL,2);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `str_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `str_sigle` varchar(20) NOT NULL,
  `str_nom_complet` varchar(255) NOT NULL,
  `str_categorie` bigint(20) unsigned DEFAULT NULL COMMENT '1:Direction centrale; 2:DR; 3:AE; 4:PTF; 5:OSC; 6:Priv?; 7:Collectivit?; 8:Autres',
  `created_by` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`str_id`),
  UNIQUE KEY `str_id` (`str_id`),
  UNIQUE KEY `str_sigle` (`str_sigle`),
  UNIQUE KEY `str_nom_complet` (`str_nom_complet`),
  KEY `structures_categories_FK` (`str_categorie`),
  CONSTRAINT `structures_categories_FK` FOREIGN KEY (`str_categorie`) REFERENCES `categories` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
INSERT INTO `structures` VALUES (1,'SG','Secrétariat Général',1,'admin','2019-10-08 23:28:59',NULL,NULL),(2,'DGESS','Direction Générale des Etudes et des Statistiques Sectorielles',2,'admin','2019-10-08 23:28:59',NULL,NULL),(3,'DGEP','Direction Générale de l\'Eau Potable',2,'admin','2019-10-08 23:28:59',NULL,NULL),(4,'DGA','Direction Générale de l\'Assainissement',2,'admin','2019-10-08 23:28:59',NULL,NULL),(5,'DGIH','Direction Générale des Infrastructures Hydrauliques',2,'admin','2019-10-08 23:28:59',NULL,NULL),(6,'HBS','Hauts-Bassins',3,'admin','2018-10-12 08:42:58',NULL,'2018-10-12 08:42:58'),(7,'BMH','Boucle du Mouhoun',3,'admin','2018-10-12 08:43:41',NULL,'2018-10-12 08:43:41'),(8,'SHL','Sahel',3,'admin','2018-10-12 08:44:09',NULL,'2019-02-28 12:34:41'),(9,'EST','Est',3,'admin','2018-10-12 08:44:28',NULL,'2018-10-12 08:44:28'),(10,'SUO','Sud-Ouest',3,'admin','2018-10-12 08:44:50',NULL,'2019-02-28 12:34:33'),(11,'CNO','Centre-Nord',3,'admin','2018-10-12 08:45:08',NULL,'2018-10-12 08:45:08'),(12,'COU','Centre-Ouest',3,'admin','2018-10-12 08:45:27',NULL,'2018-10-12 08:45:27'),(13,'PCE','Plateau-Central',3,'admin','2018-10-12 08:45:48',NULL,'2018-10-12 08:45:48'),(14,'NOR','Nord',3,'admin','2018-10-12 08:46:03',NULL,'2018-10-12 08:46:03'),(15,'CES','Centre-Est',3,'admin','2018-10-12 08:46:17',NULL,'2018-10-12 08:46:17'),(16,'CEN','Centre',3,'admin','2018-10-12 08:46:35',NULL,'2018-10-12 08:46:35'),(17,'CAS','Cascades',3,'admin','2018-10-12 08:46:58',NULL,'2018-10-12 08:46:58'),(18,'CSD','Centre-Sud',3,'admin','2018-10-12 08:47:16',NULL,'2019-02-28 12:35:13'),(19,'ONEA','ONEA',10,'admin','2018-10-15 07:20:14',NULL,'2019-01-18 09:22:59'),(21,'AEN','Agence de l\'Eau du Nakanbé',4,'admin','2019-01-18 09:42:24',NULL,'2019-06-25 15:18:08'),(22,'AEC','Agence de l\'Eau des Cascades',4,'admin','2019-01-18 09:42:49',NULL,'2019-01-18 09:42:49'),(23,'AEL','Agence de l\'Eau du Liptako',4,'admin','2019-01-18 09:43:11',NULL,'2019-01-18 09:43:11'),(24,'AEG','Agence de l\'Eau du Gourma',4,'admin','2019-01-18 09:43:32',NULL,'2019-01-18 09:43:32'),(25,'AEM','Agence de l\'Eau du Mouhoun',4,'admin','2019-01-18 09:43:58',NULL,'2019-01-18 09:43:58');
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatemsgs`
--

DROP TABLE IF EXISTS `templatemsgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatemsgs` (
  `tmsg_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tmsg_type` varchar(30) NOT NULL COMMENT 'PREPARATION;SUIVI;AUTRE',
  `tmsg_msg` varchar(255) NOT NULL,
  `tmsg_periodicite` varchar(50) NOT NULL COMMENT 'MENSUELLE; TRIMESTRIELLE;SEMESTRIELLE;ANNUELLE',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`tmsg_id`),
  UNIQUE KEY `tmsg_id` (`tmsg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatemsgs`
--

LOCK TABLES `templatemsgs` WRITE;
/*!40000 ALTER TABLE `templatemsgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatemsgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `firstconnect` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `nom_prenom` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `structure` bigint(20) unsigned DEFAULT NULL,
  `matricule` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastconnect` timestamp NULL DEFAULT NULL,
  `fails_connect` int(11) DEFAULT '0',
  `is_blocked` tinyint(1) DEFAULT '0',
  `current_connect` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `structures_fk1` (`id`),
  KEY `users_structures_FK` (`structure`),
  CONSTRAINT `users_structures_FK` FOREIGN KEY (`structure`) REFERENCES `structures` (`str_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrateur','admin@admin.com',NULL,'$2y$10$NGQG6LJl9CeUDvuLeoEbbuhjaW6.BwqZZd0Oof1izyOrZ3Y6DqUJa',NULL,'2020-02-13 00:15:16','2020-03-06 10:32:39',0,1,'BARRA Inoussa kkk',1,'489633L','+22679885666','2020-03-05 16:57:45',0,0,'2020-03-06 10:32:39'),(2,'isidore','isidore@gmail.com','2020-02-13 00:00:00','$2y$10$BT0xtqb.xaBBteA7NUUtsOFEthJAzbJ50VpY.NI0BywhyGf0O3O1C',NULL,'2020-02-15 13:42:32','2020-02-28 11:04:37',1,0,'KABORE Isidore',2,'55880 L','+22679855633',NULL,0,0,NULL),(4,'souley','alassane@asso.com',NULL,'$2y$10$Hd4A9vhTzOq9aepXc3hAsuu/Pg5oV7vGf0RPcmEcHTRfaPQ9Pm8ye',NULL,'2020-02-15 14:20:00','2020-02-29 13:34:45',1,1,'KONE Souleymane Sou',3,'855996M','+22679883314',NULL,0,0,NULL),(5,'barbe','barbe@yahoo.fr',NULL,'$2y$10$pzUXaDyPIbkDfQMcDN9MDOkAvvhFrp8n5mdpQ8pRnx7iQXYha8s6q',NULL,'2020-02-15 14:43:46','2020-02-29 22:37:22',1,1,'COULIBALY Toua B',2,'785423H','+22789885633',NULL,0,0,NULL),(37,'pelag','pelag@gmail.com',NULL,'$2y$10$uixAx.tLPT5S1j3zBViIXe0LNPiARi/XBzKqODZVlDRwsLe.TGdQy',NULL,'2020-02-28 23:48:57','2020-02-29 13:34:11',0,0,'OUATTARA Pelagie',3,'899345G','+22845885636','2020-02-29 00:18:25',0,0,'2020-02-29 00:04:37'),(38,'sitadeni','sitadeni@gmail.com',NULL,'$2y$10$jpbGZZbiix59hU5ogiRu9.rq7Lhzc/MTqN2KfYAOmv3W7Yl9ND6QO',NULL,'2020-02-29 00:18:08','2020-02-29 00:18:08',1,1,'TRAORE Sita',2,'144523T','+22679885697',NULL,0,0,NULL),(40,'imma','imma@gmail.com',NULL,'$2y$10$/w.xYg56Rwk0BA9Ukmv7dujqa0hq/PanZNMLUou1rIdG.3jtuA8Hu',NULL,'2020-02-29 21:20:24','2020-02-29 21:20:24',1,1,'KONDOMBO Imma',3,'788563K','+22678554412',NULL,0,0,NULL),(41,'yameogo','yameogo@yahoo.fr',NULL,'$2y$10$kw6vUjowQ3Gp1tFDkDhaIekPDzb.B6Hy9Brsuq.DQUWP6HW5/PI3.',NULL,'2020-02-29 21:43:30','2020-02-29 22:56:54',1,1,'YAMEOGO Adama',1,'652142 Y','+322562244',NULL,0,0,NULL),(42,'Rolande','rolande@rolande.com',NULL,'$2y$10$7KOFxOsVNV6UaRVQR69ZUedYgGzJaRsPKSM7DD8iNKTCi1BLfqoOW',NULL,'2020-02-29 21:47:15','2020-03-04 16:42:34',1,1,'COMPAORO Rolande',3,'589621 R','+887744523',NULL,0,0,NULL),(43,'gaetan','gaetan@gmail.com',NULL,'$2y$10$lKSjAtRVe/CtVOz6y4HjHO4ru6uYGKGjNXUponKvWqZf6ROXX/6nO',NULL,'2020-03-04 11:15:35','2020-03-04 11:15:35',1,1,'BAMBARA Gaetan',5,'856975 O','+226335544',NULL,0,0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'recommandations'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-06 12:00:11
