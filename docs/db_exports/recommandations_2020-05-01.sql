-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 01, 2020 at 04:34 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recommandations`
--

-- --------------------------------------------------------

--
-- Table structure for table `acteurs`
--

DROP TABLE IF EXISTS `acteurs`;
CREATE TABLE IF NOT EXISTS `acteurs` (
  `act_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ins_id` bigint(20) UNSIGNED NOT NULL,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `act_is_css` smallint(1) NOT NULL DEFAULT '0',
  `act_is_csr` smallint(1) NOT NULL DEFAULT '0',
  `act_is_asr` smallint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`act_id`),
  UNIQUE KEY `act_id` (`act_id`),
  KEY `ins_id` (`ins_id`),
  KEY `str_id` (`str_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acteurs`
--

INSERT INTO `acteurs` (`act_id`, `ins_id`, `str_id`, `act_is_css`, `act_is_csr`, `act_is_asr`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 4, 2, 1, 1, 1, 'Administrateur', '2020-03-04 14:08:29', 'gaetan@gmail.com', '2020-04-20 10:16:49'),
(3, 4, 5, 0, 1, 1, 'Administrateur', '2020-03-04 14:08:51', 'gaetan@gmail.com', '2020-04-20 10:17:02'),
(7, 2, 23, 1, 1, 1, 'Administrateur', '2020-03-24 14:38:54', 'Administrateur', '2020-04-09 14:02:25'),
(8, 2, 25, 0, 1, 1, 'Administrateur', '2020-03-24 14:38:54', 'Administrateur', '2020-04-09 14:02:41'),
(9, 2, 21, 0, 0, 1, 'Administrateur', '2020-03-24 14:38:54', 'Administrateur', '2020-04-09 14:02:49'),
(18, 1, 22, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(19, 1, 18, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(20, 1, 4, 1, 1, 1, 'Administrateur', '2020-04-14 11:57:53', 'gaetan', '2020-04-15 16:02:04'),
(21, 1, 3, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(22, 1, 2, 0, 1, 1, 'Administrateur', '2020-04-14 11:57:53', 'gaetan', '2020-04-15 16:02:33'),
(23, 1, 5, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(24, 1, 19, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(25, 1, 13, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(26, 1, 1, 0, 0, 1, 'Administrateur', '2020-04-14 11:57:53', 'Administrateur', '2020-04-14 11:57:53'),
(27, 4, 18, 0, 1, 1, 'gaetan@gmail.com', '2020-04-20 10:16:40', 'gaetan@gmail.com', '2020-04-20 10:16:40'),
(28, 4, 4, 1, 1, 1, 'gaetan@gmail.com', '2020-04-20 10:16:40', 'gaetan@gmail.com', '2020-04-20 10:16:57'),
(29, 4, 3, 1, 1, 1, 'gaetan@gmail.com', '2020-04-20 10:16:40', 'gaetan@gmail.com', '2020-04-22 16:35:24'),
(30, 4, 19, 0, 1, 1, 'gaetan@gmail.com', '2020-04-20 10:16:40', 'gaetan@gmail.com', '2020-04-20 10:16:40'),
(31, 4, 1, 0, 1, 1, 'gaetan@gmail.com', '2020-04-20 10:16:40', 'gaetan@gmail.com', '2020-04-20 10:16:40'),
(32, 2, 3, 1, 1, 1, 'gaetan@gmail.com', '2020-04-22 16:36:16', 'gaetan@gmail.com', '2020-04-22 16:36:22'),
(33, 5, 3, 0, 1, 1, 'guetikila@gmail.com', '2020-04-28 11:24:01', 'guetikila@gmail.com', '2020-04-28 11:24:01'),
(34, 5, 2, 1, 1, 1, 'guetikila@gmail.com', '2020-04-28 11:24:01', 'guetikila@gmail.com', '2020-04-28 11:25:18'),
(36, 5, 26, 0, 1, 1, 'guetikila@gmail.com', '2020-04-28 11:27:39', 'guetikila@gmail.com', '2020-04-28 11:27:39'),
(37, 5, 24, 0, 0, 1, 'gaetan@gmail.com', '2020-04-29 16:37:49', 'gaetan@gmail.com', '2020-04-29 16:37:58'),
(38, 5, 7, 0, 0, 1, 'gaetan@gmail.com', '2020-04-29 16:37:49', 'gaetan@gmail.com', '2020-04-29 16:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `activites`
--

DROP TABLE IF EXISTS `activites`;
CREATE TABLE IF NOT EXISTS `activites` (
  `act_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rec_id` bigint(20) UNSIGNED NOT NULL,
  `ptn_id` bigint(20) UNSIGNED NOT NULL,
  `act_description` varchar(1024) NOT NULL,
  `act_is_realise` smallint(6) NOT NULL DEFAULT '0',
  `act_is_valide` smallint(6) NOT NULL,
  `act_poids` smallint(6) DEFAULT NULL,
  `act_date_prevue` date DEFAULT NULL,
  `act_date_realise` date DEFAULT NULL,
  `act_preuve` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `act_observation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`act_id`),
  UNIQUE KEY `act_id` (`act_id`),
  KEY `rec_id` (`rec_id`),
  KEY `ptn_id` (`ptn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activites`
--

INSERT INTO `activites` (`act_id`, `rec_id`, `ptn_id`, `act_description`, `act_is_realise`, `act_is_valide`, `act_poids`, `act_date_prevue`, `act_date_realise`, `act_preuve`, `created_by`, `created_at`, `updated_by`, `updated_at`, `act_observation`) VALUES
(1, 2, 4, 'Etape 1', 1, 0, 10, '2020-03-31', '2020-04-09', 'Etape 1.pdf', 'Administrateur', '2020-03-23 12:55:17', 'Administrateur', '2020-03-23 12:55:17', 'RAS'),
(2, 2, 5, 'Etape 2', 0, 0, 25, '2020-03-31', NULL, NULL, 'Administrateur', '2020-03-23 12:55:42', 'Administrateur', '2020-03-23 12:55:42', NULL),
(3, 36, 11, 'Activité 1', 0, 0, 35, '2020-05-12', NULL, NULL, 'Administrateur', '2020-04-14 12:00:45', 'Administrateur', '2020-04-14 12:19:54', NULL),
(4, 36, 12, 'Activité 2', 0, 0, 46, '2020-05-22', NULL, NULL, 'Administrateur', '2020-04-14 12:01:15', 'Administrateur', '2020-04-14 12:01:15', NULL),
(5, 3, 18, 'Rédaction des TDR', 1, 1, 10, '2020-04-20', '2020-04-16', NULL, 'pelag', '2020-04-16 10:53:01', 'pelag', '2020-04-16 11:27:36', 'Activité 1 valididée'),
(6, 3, 18, 'Recrutement des prestataires', 1, 1, 30, '2020-04-23', '2020-04-16', NULL, 'pelag', '2020-04-16 10:53:48', 'pelag', '2020-04-16 11:27:36', 'Activité 2 réalisée'),
(7, 3, 18, 'Livraison des licences', 1, 1, 60, '2020-05-04', '2020-04-16', 'Livraison des licences.pdf', 'pelag', '2020-04-16 10:56:08', 'pelag', '2020-04-16 11:27:36', 'Activité 3 réalisée'),
(8, 40, 24, 'Activité 1', 1, 1, 25, '2020-04-30', '2020-04-23', NULL, 'pelag@gmail.com', '2020-04-20 10:18:31', 'pelag@gmail.com', '2020-04-20 11:18:49', 'Réalisé'),
(9, 40, 24, 'Activité 2', 1, 1, 45, '2020-05-02', '2020-04-23', 'ACT_9_23_Apr_2020_09_55_17.pdf', 'pelag@gmail.com', '2020-04-20 10:22:16', 'pelag@gmail.com', '2020-04-20 11:18:49', 'Ok'),
(10, 40, 24, 'Activité 3', 1, 1, 30, '2020-05-10', '2020-04-24', 'ACT_10_24_Apr_2020_15_33_59.pdf', 'pelag@gmail.com', '2020-04-20 10:27:52', 'pelag@gmail.com', '2020-04-20 11:18:49', 'Réalisée'),
(11, 54, 30, 'Rédaction des TDR', 1, 1, 10, '2020-05-04', '2020-04-28', 'ACT_11_28_Apr_2020_13_52_46.pdf', 'barbe@yahoo.fr', '2020-04-28 13:23:15', 'barbe@yahoo.fr', '2020-04-28 13:27:45', 'Nous accusé un petit retard ....'),
(12, 54, 30, 'Elaborer les DAO', 1, 1, 15, '2020-02-01', '2020-04-28', NULL, 'lessilassina@gmail.com', '2020-04-28 13:23:32', 'barbe@yahoo.fr', '2020-04-28 13:27:45', 'RAS'),
(13, 54, 30, 'Recruter le cabinent de réalisation', 1, 1, 10, '2020-05-06', '2020-04-28', NULL, 'barbe@yahoo.fr', '2020-04-28 13:23:55', 'barbe@yahoo.fr', '2020-04-28 13:27:45', 'RAS'),
(14, 54, 30, 'Réaliser l\'intranet', 1, 1, 55, '2020-06-30', '2020-04-28', NULL, 'barbe@yahoo.fr', '2020-04-28 13:24:59', 'barbe@yahoo.fr', '2020-04-28 13:27:45', 'RAS'),
(15, 54, 30, 'Réceptionner la plateforme', 1, 1, 10, '2020-07-04', '2020-04-28', NULL, 'barbe@yahoo.fr', '2020-04-28 13:25:34', 'barbe@yahoo.fr', '2020-04-28 13:27:45', 'RAS'),
(16, 43, 28, 'Acquerir les accessoires', 1, 1, 45, '2020-12-30', '2020-04-28', 'ACT_16_28_Apr_2020_13_54_14.pdf', 'lessilassina@gmail.com', '2020-04-28 13:45:47', 'lessilassina@gmail.com', '2020-04-28 13:47:01', 'Les accessoires sont acquis'),
(17, 43, 27, 'Lancer l\'appel d\'offre', 1, 1, 25, '2020-05-23', '2020-04-28', NULL, 'lessilassina@gmail.com', '2020-04-28 13:46:20', 'lessilassina@gmail.com', '2020-04-28 13:47:01', 'L\'appel d\'offre N° DMP/12/MEA/CAB'),
(18, 43, 28, 'Assurer la maintenance', 0, 1, 30, '2020-11-30', '2020-04-28', NULL, 'lessilassina@gmail.com', '2020-04-28 13:46:46', 'lessilassina@gmail.com', '2020-04-28 13:47:01', 'En attente du matériel');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `log_name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse_ip` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`),
  KEY `subject` (`subject_id`,`subject_type`(191)),
  KEY `causer` (`causer_id`,`causer_type`)
) ENGINE=InnoDB AUTO_INCREMENT=567 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `email`, `adresse_ip`, `user_agent`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:11:02', '2020-04-20 10:11:02'),
(2, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:11:09', '2020-04-20 10:11:09'),
(3, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:11:09', '2020-04-20 10:11:09'),
(4, 'default', 'created', 14, 'App\\Models\\Session', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:11', '2020-04-20 10:12:11'),
(5, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:40', '2020-04-20 10:12:40'),
(6, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:46', '2020-04-20 10:12:46'),
(7, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:46', '2020-04-20 10:12:46'),
(8, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:51', '2020-04-20 10:12:51'),
(9, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:56', '2020-04-20 10:12:56'),
(10, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:12:56', '2020-04-20 10:12:56'),
(11, 'default', 'created', 40, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:13:32', '2020-04-20 10:13:32'),
(12, 'default', 'created', 22, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:13:57', '2020-04-20 10:13:57'),
(13, 'default', 'created', 23, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:13:57', '2020-04-20 10:13:57'),
(14, 'default', 'created', 19, 'App\\Models\\ListeAlertes', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:14:49', '2020-04-20 10:14:49'),
(15, 'default', 'created', 20, 'App\\Models\\ListeAlertes', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:14:49', '2020-04-20 10:14:49'),
(16, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:15:22', '2020-04-20 10:15:22'),
(17, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:15:33', '2020-04-20 10:15:33'),
(18, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:15:34', '2020-04-20 10:15:34'),
(19, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:02', '2020-04-20 10:16:02'),
(20, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:09', '2020-04-20 10:16:09'),
(21, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:09', '2020-04-20 10:16:09'),
(22, 'default', 'created', 27, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:40', '2020-04-20 10:16:40'),
(23, 'default', 'created', 28, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:40', '2020-04-20 10:16:40'),
(24, 'default', 'created', 29, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:40', '2020-04-20 10:16:40'),
(25, 'default', 'created', 30, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:40', '2020-04-20 10:16:40'),
(26, 'default', 'created', 31, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:16:40', '2020-04-20 10:16:40'),
(27, 'default', 'created', 24, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:17:31', '2020-04-20 10:17:31'),
(28, 'default', 'created', 25, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:17:31', '2020-04-20 10:17:31'),
(29, 'default', 'created', 26, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:17:31', '2020-04-20 10:17:31'),
(30, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:17:48', '2020-04-20 10:17:48'),
(31, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:17:53', '2020-04-20 10:17:53'),
(32, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:17:53', '2020-04-20 10:17:53'),
(33, 'default', 'created', 8, 'App\\Models\\Activites', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:18:31', '2020-04-20 10:18:31'),
(34, 'default', 'created', 9, 'App\\Models\\Activites', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 10:22:16', '2020-04-20 10:22:16'),
(35, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 10:26:58', '2020-04-20 10:26:58'),
(36, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 10:26:58', '2020-04-20 10:26:58'),
(37, 'default', 'created', 10, 'App\\Models\\Activites', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 10:27:52', '2020-04-20 10:27:52'),
(38, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 11:19:04', '2020-04-20 11:19:04'),
(39, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 11:19:08', '2020-04-20 11:19:08'),
(40, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 11:19:08', '2020-04-20 11:19:08'),
(41, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 11:19:11', '2020-04-20 11:19:11'),
(42, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 11:19:15', '2020-04-20 11:19:15'),
(43, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 11:19:15', '2020-04-20 11:19:15'),
(44, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 13:43:17', '2020-04-20 13:43:17'),
(45, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-20 13:43:17', '2020-04-20 13:43:17'),
(46, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 14:38:24', '2020-04-20 14:38:24'),
(47, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 14:38:29', '2020-04-20 14:38:29'),
(48, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 14:38:29', '2020-04-20 14:38:29'),
(49, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 17:20:59', '2020-04-20 17:20:59'),
(50, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 17:21:03', '2020-04-20 17:21:03'),
(51, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 17:21:03', '2020-04-20 17:21:03'),
(52, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 21:01:36', '2020-04-20 21:01:36'),
(53, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-20 21:01:36', '2020-04-20 21:01:36'),
(54, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 09:21:05', '2020-04-21 09:21:05'),
(55, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 09:21:05', '2020-04-21 09:21:05'),
(56, 'default', 'deleted', 1, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 11:35:17', '2020-04-21 11:35:17'),
(57, 'default', 'created', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 11:35:33', '2020-04-21 11:35:33'),
(58, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 12:17:26', '2020-04-21 12:17:26'),
(59, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 12:17:32', '2020-04-21 12:17:32'),
(60, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 12:17:32', '2020-04-21 12:17:32'),
(61, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 13:15:38', '2020-04-21 13:15:38'),
(62, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 13:15:48', '2020-04-21 13:15:48'),
(63, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 13:15:48', '2020-04-21 13:15:48'),
(64, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 16:21:36', '2020-04-21 16:21:36'),
(65, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '[]', '2020-04-21 16:21:36', '2020-04-21 16:21:36'),
(66, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 08:52:08', '2020-04-22 08:52:08'),
(67, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 08:52:08', '2020-04-22 08:52:08'),
(68, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 14:24:56', '2020-04-22 14:24:56'),
(69, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 14:44:14', '2020-04-22 14:44:14'),
(70, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 14:44:14', '2020-04-22 14:44:14'),
(71, 'Instance', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 15:36:42', '2020-04-22 15:36:42'),
(72, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-22 16:11:13', '2020-04-22 16:11:13'),
(73, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-22 16:11:13', '2020-04-22 16:11:13'),
(74, 'Acteur', 'Changement rôle CSS', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 16:32:47', '2020-04-22 16:32:47'),
(75, 'Acteur', 'Changement rôle CSS', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 16:35:03', '2020-04-22 16:35:03'),
(76, 'Acteur', 'Changement rôle CSS', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 16:35:24', '2020-04-22 16:35:24'),
(77, 'default', 'created', 32, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 16:36:16', '2020-04-22 16:36:16'),
(78, 'Acteur', 'Changement rôle CSS', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 16:36:22', '2020-04-22 16:36:22'),
(79, 'Recommandation', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 17:50:07', '2020-04-22 17:50:07'),
(80, 'default', 'created', 41, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 18:05:08', '2020-04-22 18:05:08'),
(81, 'Recommandation', 'Importation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-22 18:05:08', '2020-04-22 18:05:08'),
(82, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 09:22:27', '2020-04-23 09:22:27'),
(83, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 09:22:27', '2020-04-23 09:22:27'),
(84, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:31:54', '2020-04-23 09:31:54'),
(85, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:31:54', '2020-04-23 09:31:54'),
(86, 'Programme budgétaire', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 09:39:05', '2020-04-23 09:39:05'),
(87, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:52:16', '2020-04-23 09:52:16'),
(88, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:53:04', '2020-04-23 09:53:04'),
(89, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:54:22', '2020-04-23 09:54:22'),
(90, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:55:17', '2020-04-23 09:55:17'),
(91, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:58:07', '2020-04-23 09:58:07'),
(92, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 09:58:28', '2020-04-23 09:58:28'),
(93, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 12:47:35', '2020-04-23 12:47:35'),
(94, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 12:47:35', '2020-04-23 12:47:35'),
(95, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:19:01', '2020-04-23 15:19:01'),
(96, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:19:01', '2020-04-23 15:19:01'),
(97, 'Session', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:42:22', '2020-04-23 15:42:22'),
(98, 'Session', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:45:23', '2020-04-23 15:45:23'),
(99, 'Session', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:46:45', '2020-04-23 15:46:45'),
(100, 'default', 'created', 21, 'App\\Models\\ListeAlertes', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:59:09', '2020-04-23 15:59:09'),
(101, 'Session', 'Validation de la formulation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 15:59:09', '2020-04-23 15:59:09'),
(102, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 15:59:51', '2020-04-23 15:59:51'),
(103, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 15:59:51', '2020-04-23 15:59:51'),
(104, 'Session', 'Validation de la réalisation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-23 16:00:28', '2020-04-23 16:00:28'),
(105, 'default', 'created', 42, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 16:55:01', '2020-04-23 16:55:01'),
(106, 'default', 'created', 43, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 16:55:46', '2020-04-23 16:55:46'),
(107, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-23 16:55:57', '2020-04-23 16:55:57'),
(108, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 08:13:17', '2020-04-24 08:13:17'),
(109, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 08:13:17', '2020-04-24 08:13:17'),
(110, 'Programme budgétaire', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 10:30:20', '2020-04-24 10:30:20'),
(111, 'Programme budgétaire', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 10:30:39', '2020-04-24 10:30:39'),
(112, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-24 10:39:08', '2020-04-24 10:39:08'),
(113, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-24 10:39:08', '2020-04-24 10:39:08'),
(114, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 12:22:39', '2020-04-24 12:22:39'),
(115, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 12:25:43', '2020-04-24 12:25:43'),
(116, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 12:25:43', '2020-04-24 12:25:43'),
(117, 'Session', 'Liste des participants', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-24 15:07:26', '2020-04-24 15:07:26'),
(118, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-24 15:33:59', '2020-04-24 15:33:59'),
(119, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-24 17:16:16', '2020-04-24 17:16:16'),
(120, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-25 16:55:33', '2020-04-25 16:55:33'),
(121, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-25 16:55:33', '2020-04-25 16:55:33'),
(122, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-25 16:56:42', '2020-04-25 16:56:42'),
(123, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 08:50:46', '2020-04-27 08:50:46'),
(124, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 08:50:46', '2020-04-27 08:50:46'),
(125, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 10:26:46', '2020-04-27 10:26:46'),
(126, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 11:13:55', '2020-04-27 11:13:55'),
(127, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 11:13:55', '2020-04-27 11:13:55'),
(128, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 11:24:51', '2020-04-27 11:24:51'),
(129, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 11:24:56', '2020-04-27 11:24:56'),
(130, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 11:24:56', '2020-04-27 11:24:56'),
(131, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 13:43:09', '2020-04-27 13:43:09'),
(132, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 14:00:35', '2020-04-27 14:00:35'),
(133, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 14:02:41', '2020-04-27 14:02:41'),
(134, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 14:05:49', '2020-04-27 14:05:49'),
(135, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 14:07:44', '2020-04-27 14:07:44'),
(136, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 14:21:58', '2020-04-27 14:21:58'),
(137, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 14:22:03', '2020-04-27 14:22:03'),
(138, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 14:22:03', '2020-04-27 14:22:03'),
(139, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 14:46:57', '2020-04-27 14:46:57'),
(140, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 14:47:01', '2020-04-27 14:47:01'),
(141, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 14:47:01', '2020-04-27 14:47:01'),
(142, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 15:07:28', '2020-04-27 15:07:28'),
(143, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 15:08:08', '2020-04-27 15:08:08'),
(144, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 37, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 15:09:10', '2020-04-27 15:09:10'),
(145, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 15:25:28', '2020-04-27 15:25:28'),
(146, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 15:25:33', '2020-04-27 15:25:33'),
(147, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 15:25:33', '2020-04-27 15:25:33'),
(148, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', '', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-27 16:00:10', '2020-04-27 16:00:10'),
(149, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 17:00:36', '2020-04-27 17:00:36'),
(150, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 17:00:40', '2020-04-27 17:00:40'),
(151, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-27 17:00:40', '2020-04-27 17:00:40'),
(152, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:32:24', '2020-04-27 20:32:24'),
(153, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:32:24', '2020-04-27 20:32:24'),
(154, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:37:16', '2020-04-27 20:37:16'),
(155, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:37:19', '2020-04-27 20:37:19'),
(156, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:37:19', '2020-04-27 20:37:19'),
(157, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:38:02', '2020-04-27 20:38:02'),
(158, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:38:16', '2020-04-27 20:38:16'),
(159, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:38:16', '2020-04-27 20:38:16'),
(160, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:38:29', '2020-04-27 20:38:29'),
(161, 'Administration', 'Modification de compte', 37, 'App\\User', 37, 'App\\User', 'pelag@gmail.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:38:44', '2020-04-27 20:38:44'),
(162, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:38:44', '2020-04-27 20:38:44'),
(163, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 37, 'App\\User', 'pelag@gmail.com', '41.138.122.199', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-27 20:39:53', '2020-04-27 20:39:53'),
(164, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-27 22:19:28', '2020-04-27 22:19:28'),
(165, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-27 22:19:28', '2020-04-27 22:19:28'),
(166, 'Administration', 'Création de compte', 46, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-27 22:47:38', '2020-04-27 22:47:38'),
(167, 'Administration', 'Modification de compte', 40, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-27 23:50:20', '2020-04-27 23:50:20'),
(168, 'Administration', 'Modification de compte', 40, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-27 23:50:35', '2020-04-27 23:50:35'),
(169, 'Administration', 'Mot de passe réinitialisé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-27 23:50:35', '2020-04-27 23:50:35'),
(170, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 08:05:41', '2020-04-28 08:05:41'),
(171, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 08:05:41', '2020-04-28 08:05:41'),
(172, 'Administration', 'Modification de compte', 40, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 08:06:09', '2020-04-28 08:06:09'),
(173, 'Administration', 'Mot de passe réinitialisé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.178.64.24', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 08:06:09', '2020-04-28 08:06:09'),
(174, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 08:28:39', '2020-04-28 08:28:39'),
(175, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 08:28:39', '2020-04-28 08:28:39'),
(176, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 08:34:48', '2020-04-28 08:34:48'),
(177, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 09:11:00', '2020-04-28 09:11:00'),
(178, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 09:11:00', '2020-04-28 09:11:00'),
(179, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 09:22:21', '2020-04-28 09:22:21'),
(180, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '154.66.162.142', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 09:22:21', '2020-04-28 09:22:21'),
(181, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.73', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 09:34:22', '2020-04-28 09:34:22'),
(182, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.73', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 09:34:22', '2020-04-28 09:34:22'),
(183, 'Administration', 'Modification de compte', 37, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 09:34:29', '2020-04-28 09:34:29'),
(184, 'Administration', 'Mot de passe réinitialisé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 09:34:29', '2020-04-28 09:34:29'),
(185, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:08:53', '2020-04-28 10:08:53'),
(186, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:10:26', '2020-04-28 10:10:26'),
(187, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:10:26', '2020-04-28 10:10:26'),
(188, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:10:44', '2020-04-28 10:10:44'),
(189, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:10:44', '2020-04-28 10:10:44'),
(190, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:11:05', '2020-04-28 10:11:05');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `email`, `adresse_ip`, `user_agent`, `properties`, `created_at`, `updated_at`) VALUES
(191, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:11:05', '2020-04-28 10:11:05'),
(192, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.123.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:27:37', '2020-04-28 10:27:37'),
(193, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.123.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:27:52', '2020-04-28 10:27:52'),
(194, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.123.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:27:52', '2020-04-28 10:27:52'),
(195, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.123.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:29:08', '2020-04-28 10:29:08'),
(196, 'Administration', 'Modification de compte', 2, 'App\\User', 2, 'App\\User', 'isidore@gmail.com', '41.138.123.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:29:31', '2020-04-28 10:29:31'),
(197, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 2, 'App\\User', 'isidore@gmail.com', '41.138.123.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:29:31', '2020-04-28 10:29:31'),
(198, 'Administration', 'Création de compte', 47, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:39:59', '2020-04-28 10:39:59'),
(199, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.203.230.154', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0', '[]', '2020-04-28 10:42:07', '2020-04-28 10:42:07'),
(200, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.203.230.154', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0', '[]', '2020-04-28 10:42:07', '2020-04-28 10:42:07'),
(201, 'Administration', 'Création de compte', 48, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:42:39', '2020-04-28 10:42:39'),
(202, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:42:56', '2020-04-28 10:42:56'),
(203, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:42:56', '2020-04-28 10:42:56'),
(204, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Linux; Android 8.1.0; Infinix X606B) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.111 Mobile Safari/537.36', '[]', '2020-04-28 10:43:29', '2020-04-28 10:43:29'),
(205, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Linux; Android 8.1.0; Infinix X606B) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.111 Mobile Safari/537.36', '[]', '2020-04-28 10:43:29', '2020-04-28 10:43:29'),
(206, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:43:36', '2020-04-28 10:43:36'),
(207, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:43:36', '2020-04-28 10:43:36'),
(208, 'Administration', 'Création de compte', 49, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:44:42', '2020-04-28 10:44:42'),
(209, 'Paramètre', 'Création de structure', 26, 'App\\Models\\Structures', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:46:16', '2020-04-28 10:46:16'),
(210, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:46:37', '2020-04-28 10:46:37'),
(211, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:46:37', '2020-04-28 10:46:37'),
(212, 'Administration', 'Modification de compte', 49, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:47:35', '2020-04-28 10:47:35'),
(213, 'Administration', 'Création de compte', 50, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.203.230.154', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0', '[]', '2020-04-28 10:47:56', '2020-04-28 10:47:56'),
(214, 'Paramètre', 'Modification de categorie structure', 5, 'App\\Models\\Categories', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:47:56', '2020-04-28 10:47:56'),
(215, 'Administration', 'Création de compte', 51, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:48:27', '2020-04-28 10:48:27'),
(216, 'Administration', 'Création de compte', 52, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:48:36', '2020-04-28 10:48:36'),
(217, 'Administration', 'Création de compte', 53, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:48:40', '2020-04-28 10:48:40'),
(218, 'Administration', 'Modification de compte', 48, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:48:44', '2020-04-28 10:48:44'),
(219, 'Administration', 'Modification de compte', 53, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:52:36', '2020-04-28 10:52:36'),
(220, 'Administration', 'Modification de compte', 52, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:53:07', '2020-04-28 10:53:07'),
(221, 'Administration', 'Modification de compte', 52, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:53:57', '2020-04-28 10:53:57'),
(222, 'Administration', 'Compte désactivé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:53:57', '2020-04-28 10:53:57'),
(223, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:55:03', '2020-04-28 10:55:03'),
(224, 'Administration', 'Modification de compte', 47, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:55:24', '2020-04-28 10:55:24'),
(225, 'Administration', 'Mot de passe réinitialisé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 10:55:24', '2020-04-28 10:55:24'),
(226, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:55:39', '2020-04-28 10:55:39'),
(227, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:55:41', '2020-04-28 10:55:41'),
(228, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:55:41', '2020-04-28 10:55:41'),
(229, 'Administration', 'Modification de compte', 53, 'App\\User', 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:55:55', '2020-04-28 10:55:55'),
(230, 'Administration', 'Modification de compte', 53, 'App\\User', 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:56:43', '2020-04-28 10:56:43'),
(231, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 10:56:43', '2020-04-28 10:56:43'),
(232, 'Administration', 'Modification de compte', 52, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:57:11', '2020-04-28 10:57:11'),
(233, 'Administration', 'Compte activé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.177.132', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 10:57:11', '2020-04-28 10:57:11'),
(234, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 10:59:12', '2020-04-28 10:59:12'),
(235, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 10:59:12', '2020-04-28 10:59:12'),
(236, 'Administration', 'Création de compte', 54, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 11:01:18', '2020-04-28 11:01:18'),
(237, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 11:02:31', '2020-04-28 11:02:31'),
(238, 'Administration', 'Modification de compte', 54, 'App\\User', 54, 'App\\User', 'philippe.zabsonre@gmail.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 11:03:31', '2020-04-28 11:03:31'),
(239, 'Administration', 'Modification de compte', 54, 'App\\User', 54, 'App\\User', 'philippe.zabsonre@gmail.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 11:04:33', '2020-04-28 11:04:33'),
(240, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 54, 'App\\User', 'philippe.zabsonre@gmail.com', '196.13.207.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '[]', '2020-04-28 11:04:33', '2020-04-28 11:04:33'),
(241, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.203.230.154', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 11:09:27', '2020-04-28 11:09:27'),
(242, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.203.230.154', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 11:09:27', '2020-04-28 11:09:27'),
(243, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 2, 'App\\User', 'isidore@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 11:11:16', '2020-04-28 11:11:16'),
(244, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 11:11:46', '2020-04-28 11:11:46'),
(245, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 11:11:46', '2020-04-28 11:11:46'),
(246, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:14:55', '2020-04-28 11:14:55'),
(247, 'Administration', 'Modification de compte', 47, 'App\\User', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:15:08', '2020-04-28 11:15:08'),
(248, 'Administration', 'Modification de compte', 47, 'App\\User', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:16:04', '2020-04-28 11:16:04'),
(249, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:16:04', '2020-04-28 11:16:04'),
(250, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 11:17:59', '2020-04-28 11:17:59'),
(251, 'Administration', 'Modification de compte', 51, 'App\\User', 51, 'App\\User', 'michaelclaver14@gmail.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 11:18:12', '2020-04-28 11:18:12'),
(252, 'Administration', 'Modification de compte', 51, 'App\\User', 51, 'App\\User', 'michaelclaver14@gmail.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 11:19:11', '2020-04-28 11:19:11'),
(253, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 51, 'App\\User', 'michaelclaver14@gmail.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 11:19:11', '2020-04-28 11:19:11'),
(254, 'default', 'created', 5, 'App\\Models\\Instance', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:21:03', '2020-04-28 11:21:03'),
(255, 'default', 'created', 33, 'App\\Models\\Acteur', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:24:01', '2020-04-28 11:24:01'),
(256, 'default', 'created', 34, 'App\\Models\\Acteur', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:24:01', '2020-04-28 11:24:01'),
(257, 'default', 'created', 35, 'App\\Models\\Acteur', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:24:01', '2020-04-28 11:24:01'),
(258, 'Acteur', 'Changement rôle CSS', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:25:18', '2020-04-28 11:25:18'),
(259, 'Acteur', 'Changement rôle CSR', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:26:04', '2020-04-28 11:26:04'),
(260, 'Acteur', 'Changement rôle CSR', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:26:30', '2020-04-28 11:26:30'),
(261, 'Acteur', 'Suppression', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:27:26', '2020-04-28 11:27:26'),
(262, 'default', 'created', 36, 'App\\Models\\Acteur', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:27:39', '2020-04-28 11:27:39'),
(263, 'default', 'created', 15, 'App\\Models\\Session', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:36:54', '2020-04-28 11:36:54'),
(264, 'Session', 'Liste des participants', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:41:26', '2020-04-28 11:41:26'),
(265, 'Session', 'Modification', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 11:48:42', '2020-04-28 11:48:42'),
(266, 'default', 'created', 43, 'App\\Models\\Recommandations', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 12:13:08', '2020-04-28 12:13:08'),
(267, 'default', 'created', 44, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:17:11', '2020-04-28 12:17:11'),
(268, 'default', 'created', 45, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:17:11', '2020-04-28 12:17:11'),
(269, 'Recommandation', 'Importation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:17:11', '2020-04-28 12:17:11'),
(270, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:18:56', '2020-04-28 12:18:56'),
(271, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:18:56', '2020-04-28 12:18:56'),
(272, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:19:28', '2020-04-28 12:19:28'),
(273, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:19:39', '2020-04-28 12:19:39'),
(274, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:19:54', '2020-04-28 12:19:54'),
(275, 'default', 'created', 27, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 12:20:45', '2020-04-28 12:20:45'),
(276, 'default', 'created', 28, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 12:20:45', '2020-04-28 12:20:45'),
(277, 'default', 'created', 46, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:26:10', '2020-04-28 12:26:10'),
(278, 'default', 'created', 47, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:26:10', '2020-04-28 12:26:10'),
(279, 'Recommandation', 'Importation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:26:10', '2020-04-28 12:26:10'),
(280, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:28:04', '2020-04-28 12:28:04'),
(281, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:29:29', '2020-04-28 12:29:29'),
(282, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:30:05', '2020-04-28 12:30:05'),
(283, 'default', 'created', 48, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:30:21', '2020-04-28 12:30:21'),
(284, 'default', 'created', 49, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:30:21', '2020-04-28 12:30:21'),
(285, 'default', 'created', 50, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:30:21', '2020-04-28 12:30:21'),
(286, 'Recommandation', 'Importation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:30:21', '2020-04-28 12:30:21'),
(287, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:31:41', '2020-04-28 12:31:41'),
(288, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:31:47', '2020-04-28 12:31:47'),
(289, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:31:55', '2020-04-28 12:31:55'),
(290, 'Recommandation', 'Importation', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 12:53:59', '2020-04-28 12:53:59'),
(291, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:53:59', '2020-04-28 12:53:59'),
(292, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:53:59', '2020-04-28 12:53:59'),
(293, 'default', 'created', 51, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:56:40', '2020-04-28 12:56:40'),
(294, 'default', 'created', 52, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:56:40', '2020-04-28 12:56:40'),
(295, 'default', 'created', 53, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:56:40', '2020-04-28 12:56:40'),
(296, 'Recommandation', 'Importation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:56:40', '2020-04-28 12:56:40'),
(297, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:57:22', '2020-04-28 12:57:22'),
(298, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:57:32', '2020-04-28 12:57:32'),
(299, 'Recommandation', 'Suppression', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 12:57:43', '2020-04-28 12:57:43'),
(300, 'default', 'created', 54, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:06:25', '2020-04-28 13:06:25'),
(301, 'default', 'created', 55, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:06:25', '2020-04-28 13:06:25'),
(302, 'default', 'created', 56, 'App\\Models\\Recommandations', 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:06:25', '2020-04-28 13:06:25'),
(303, 'Recommandation', 'Importation', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:06:25', '2020-04-28 13:06:25'),
(304, 'default', 'created', 29, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:10:18', '2020-04-28 13:10:18'),
(305, 'default', 'created', 30, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:10:18', '2020-04-28 13:10:18'),
(306, 'default', 'created', 31, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:10:42', '2020-04-28 13:10:42'),
(307, 'default', 'created', 32, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:10:42', '2020-04-28 13:10:42'),
(308, 'default', 'created', 33, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:11:39', '2020-04-28 13:11:39'),
(309, 'default', 'created', 34, 'App\\Models\\Partenaires', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:11:39', '2020-04-28 13:11:39'),
(310, 'default', 'created', 1, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(311, 'default', 'created', 2, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(312, 'default', 'created', 3, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(313, 'default', 'created', 4, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(314, 'default', 'created', 5, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(315, 'default', 'created', 6, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(316, 'default', 'created', 7, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(317, 'default', 'created', 8, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(318, 'default', 'created', 9, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(319, 'default', 'created', 10, 'App\\Models\\ListeAlertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(320, 'Session', 'Validation de la formulation', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:12:48', '2020-04-28 13:12:48'),
(321, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:14:51', '2020-04-28 13:14:51'),
(322, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:14:58', '2020-04-28 13:14:58'),
(323, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:14:58', '2020-04-28 13:14:58'),
(324, 'Administration', 'Modification de compte', 5, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:16:12', '2020-04-28 13:16:12'),
(325, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:16:21', '2020-04-28 13:16:21'),
(326, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:16:27', '2020-04-28 13:16:27'),
(327, 'Administration', 'Modification de compte', 5, 'App\\User', 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:16:28', '2020-04-28 13:16:28'),
(328, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:16:28', '2020-04-28 13:16:28'),
(329, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:16:54', '2020-04-28 13:16:54'),
(330, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:16:54', '2020-04-28 13:16:54'),
(331, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 51, 'App\\User', 'michaelclaver14@gmail.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:17:25', '2020-04-28 13:17:25'),
(332, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:17:45', '2020-04-28 13:17:45'),
(333, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:17:45', '2020-04-28 13:17:45'),
(334, 'Administration', 'Modification de compte', 49, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:19:07', '2020-04-28 13:19:07'),
(335, 'Administration', 'Mot de passe réinitialisé', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:19:07', '2020-04-28 13:19:07'),
(336, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:19:14', '2020-04-28 13:19:14'),
(337, 'Recommandation', 'Abandon', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:19:14', '2020-04-28 13:19:14'),
(338, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:19:29', '2020-04-28 13:19:29'),
(339, 'Administration', 'Modification de compte', 51, 'App\\User', 51, 'App\\User', 'michaelclaver14@gmail.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:19:48', '2020-04-28 13:19:48'),
(340, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 51, 'App\\User', 'michaelclaver14@gmail.com', '196.13.207.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:19:48', '2020-04-28 13:19:48'),
(341, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:20:24', '2020-04-28 13:20:24'),
(342, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:20:51', '2020-04-28 13:20:51'),
(343, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:20:51', '2020-04-28 13:20:51'),
(344, 'Recommandation', 'Validation de la planification', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:21:44', '2020-04-28 13:21:44'),
(345, 'default', 'created', 11, 'App\\Models\\Activites', 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:23:15', '2020-04-28 13:23:15'),
(346, 'default', 'created', 12, 'App\\Models\\Activites', 49, 'App\\User', 'lessilassina@gmail.com', '196.13.207.158', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:23:32', '2020-04-28 13:23:32'),
(347, 'default', 'created', 13, 'App\\Models\\Activites', 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:23:55', '2020-04-28 13:23:55'),
(348, 'default', 'created', 14, 'App\\Models\\Activites', 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:24:59', '2020-04-28 13:24:59'),
(349, 'default', 'created', 15, 'App\\Models\\Activites', 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:25:34', '2020-04-28 13:25:34'),
(350, 'Recommandation', 'Validation de la planification', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:27:45', '2020-04-28 13:27:45'),
(351, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:28:19', '2020-04-28 13:28:19'),
(352, 'Administration', 'Modification de compte', 47, 'App\\User', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:28:26', '2020-04-28 13:28:26'),
(353, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:28:26', '2020-04-28 13:28:26'),
(354, 'Session', 'Modification', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:29:39', '2020-04-28 13:29:39'),
(355, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:30:37', '2020-04-28 13:30:37'),
(356, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:31:10', '2020-04-28 13:31:10'),
(357, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:31:10', '2020-04-28 13:31:10'),
(358, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:31:49', '2020-04-28 13:31:49'),
(359, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:32:21', '2020-04-28 13:32:21'),
(360, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:32:21', '2020-04-28 13:32:21'),
(361, 'Administration', 'Modification de compte', 49, 'App\\User', 1, 'App\\User', 'admin@admin.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:33:25', '2020-04-28 13:33:25'),
(362, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:33:34', '2020-04-28 13:33:34'),
(363, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:33:46', '2020-04-28 13:33:46'),
(364, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:33:46', '2020-04-28 13:33:46'),
(365, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '[]', '2020-04-28 13:36:18', '2020-04-28 13:36:18'),
(366, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '[]', '2020-04-28 13:36:18', '2020-04-28 13:36:18'),
(367, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '[]', '2020-04-28 13:38:52', '2020-04-28 13:38:52'),
(368, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '[]', '2020-04-28 13:38:52', '2020-04-28 13:38:52'),
(369, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '[]', '2020-04-28 13:39:19', '2020-04-28 13:39:19'),
(370, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:39:27', '2020-04-28 13:39:27'),
(371, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:39:41', '2020-04-28 13:39:41'),
(372, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:39:45', '2020-04-28 13:39:45'),
(373, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:39:47', '2020-04-28 13:39:47'),
(374, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '154.66.167.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 13:39:47', '2020-04-28 13:39:47'),
(375, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:39:48', '2020-04-28 13:39:48'),
(376, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:39:48', '2020-04-28 13:39:48'),
(377, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:09', '2020-04-28 13:40:09'),
(378, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:09', '2020-04-28 13:40:09'),
(379, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:17', '2020-04-28 13:40:17');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `email`, `adresse_ip`, `user_agent`, `properties`, `created_at`, `updated_at`) VALUES
(380, 'Administration', 'Modification de compte', 49, 'App\\User', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:40:32', '2020-04-28 13:40:32'),
(381, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:40:32', '2020-04-28 13:40:32'),
(382, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:39', '2020-04-28 13:40:39'),
(383, 'Administration', 'Modification de compte', 50, 'App\\User', 1, 'App\\User', 'admin@admin.com', '41.203.230.154', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:40', '2020-04-28 13:40:40'),
(384, 'Administration', 'Modification de compte', 53, 'App\\User', 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:48', '2020-04-28 13:40:48'),
(385, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:48', '2020-04-28 13:40:48'),
(386, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:51', '2020-04-28 13:40:51'),
(387, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:40:51', '2020-04-28 13:40:51'),
(388, 'Session', 'Modification', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:42:35', '2020-04-28 13:42:35'),
(389, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:43:32', '2020-04-28 13:43:32'),
(390, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:43:46', '2020-04-28 13:43:46'),
(391, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:43:46', '2020-04-28 13:43:46'),
(392, 'Session', 'Modification', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:43:55', '2020-04-28 13:43:55'),
(393, 'Administration', 'Modification de compte', 53, 'App\\User', 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:44:14', '2020-04-28 13:44:14'),
(394, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:44:22', '2020-04-28 13:44:22'),
(395, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:44:29', '2020-04-28 13:44:29'),
(396, 'Administration', 'Modification de compte', 53, 'App\\User', 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:44:33', '2020-04-28 13:44:33'),
(397, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:44:33', '2020-04-28 13:44:33'),
(398, 'Recommandation', 'Abandon', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:45:02', '2020-04-28 13:45:02'),
(399, 'default', 'created', 16, 'App\\Models\\Activites', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:45:47', '2020-04-28 13:45:47'),
(400, 'default', 'created', 17, 'App\\Models\\Activites', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:46:20', '2020-04-28 13:46:20'),
(401, 'default', 'created', 18, 'App\\Models\\Activites', 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:46:46', '2020-04-28 13:46:46'),
(402, 'Recommandation', 'Validation de la planification', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:47:01', '2020-04-28 13:47:01'),
(403, 'Recommandation', 'Validation de la planification', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:47:09', '2020-04-28 13:47:09'),
(404, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:48:20', '2020-04-28 13:48:20'),
(405, 'Session', 'Modification', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:48:22', '2020-04-28 13:48:22'),
(406, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:48:23', '2020-04-28 13:48:23'),
(407, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:48:23', '2020-04-28 13:48:23'),
(408, 'Administration', 'Modification de compte', 53, 'App\\User', 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:48:34', '2020-04-28 13:48:34'),
(409, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:48:34', '2020-04-28 13:48:34'),
(410, 'Session', 'Validation de la planification', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:48:40', '2020-04-28 13:48:40'),
(411, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:50:06', '2020-04-28 13:50:06'),
(412, 'Administration', 'Modification de compte', 5, 'App\\User', 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:50:12', '2020-04-28 13:50:12'),
(413, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:50:12', '2020-04-28 13:50:12'),
(414, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 53, 'App\\User', 'nikiemacamille2015@gmail.com', '196.13.207.163', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 13:50:57', '2020-04-28 13:50:57'),
(415, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:51:20', '2020-04-28 13:51:20'),
(416, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:52:07', '2020-04-28 13:52:07'),
(417, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:52:33', '2020-04-28 13:52:33'),
(418, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:52:46', '2020-04-28 13:52:46'),
(419, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:53:04', '2020-04-28 13:53:04'),
(420, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:53:11', '2020-04-28 13:53:11'),
(421, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:53:35', '2020-04-28 13:53:35'),
(422, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:54:09', '2020-04-28 13:54:09'),
(423, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:54:14', '2020-04-28 13:54:14'),
(424, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:54:28', '2020-04-28 13:54:28'),
(425, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:54:52', '2020-04-28 13:54:52'),
(426, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:55:32', '2020-04-28 13:55:32'),
(427, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '102.179.86.231', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0', '[]', '2020-04-28 13:55:41', '2020-04-28 13:55:41'),
(428, 'Recommandation', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:57:04', '2020-04-28 13:57:04'),
(429, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:59:23', '2020-04-28 13:59:23'),
(430, 'Activité', 'Suivi de mise en oeuvre', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 13:59:36', '2020-04-28 13:59:36'),
(431, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 5, 'App\\User', 'barbe@yahoo.fr', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:09:42', '2020-04-28 14:09:42'),
(432, 'Administration', 'Modification de compte', 47, 'App\\User', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:09:50', '2020-04-28 14:09:50'),
(433, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:09:50', '2020-04-28 14:09:50'),
(434, 'Session', 'Validation de la réalisation', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:10:44', '2020-04-28 14:10:44'),
(435, 'default', 'created', 16, 'App\\Models\\Session', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:13:06', '2020-04-28 14:13:06'),
(436, 'default', 'created', 58, 'App\\Models\\Recommandations', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:15:02', '2020-04-28 14:15:02'),
(437, 'Session', 'Reconduction de recommandation(s)', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:15:02', '2020-04-28 14:15:02'),
(438, 'default', 'created', 59, 'App\\Models\\Recommandations', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:15:02', '2020-04-28 14:15:02'),
(439, 'Session', 'Reconduction de recommandation(s)', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:15:02', '2020-04-28 14:15:02'),
(440, 'Recommandation', 'Abandon', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:19:16', '2020-04-28 14:19:16'),
(441, 'default', 'created', 2, 'App\\Models\\Templatemsgs', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:24:26', '2020-04-28 14:24:26'),
(442, 'default', 'created', 3, 'App\\Models\\Templatemsgs', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:26:42', '2020-04-28 14:26:42'),
(443, 'default', 'created', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:42:31', '2020-04-28 14:42:31'),
(444, 'default', 'updated', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:50:01', '2020-04-28 14:50:01'),
(445, 'default', 'updated', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 14:50:01', '2020-04-28 14:50:01'),
(446, 'default', 'updated', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 15:07:43', '2020-04-28 15:07:43'),
(447, 'default', 'updated', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 15:07:43', '2020-04-28 15:07:43'),
(448, 'default', 'updated', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 15:21:34', '2020-04-28 15:21:34'),
(449, 'default', 'updated', 1, 'App\\Models\\Alertes', 47, 'App\\User', 'guetikila@gmail.com', '41.138.102.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 15:21:34', '2020-04-28 15:21:34'),
(450, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 49, 'App\\User', 'lessilassina@gmail.com', '154.66.171.104', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 15:37:08', '2020-04-28 15:37:08'),
(451, 'default', 'created', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:11', '2020-04-28 15:39:11'),
(452, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(453, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(454, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(455, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(456, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(457, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(458, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(459, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(460, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(461, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(462, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(463, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(464, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(465, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(466, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(467, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(468, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(469, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(470, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(471, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
(472, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 16:46:37', '2020-04-28 16:46:37'),
(473, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '[]', '2020-04-28 16:46:37', '2020-04-28 16:46:37'),
(474, 'default', 'created', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:08', '2020-04-28 16:52:08'),
(475, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(476, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(477, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(478, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(479, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(480, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(481, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(482, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(483, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(484, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
(485, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(486, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(487, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(488, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(489, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(490, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(491, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(492, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(493, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(494, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '41.138.102.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
(495, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 47, 'App\\User', 'guetikila@gmail.com', '154.66.171.104', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', '[]', '2020-04-28 16:59:26', '2020-04-28 16:59:26'),
(496, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'gaetan@gmail.com', '154.66.171.217', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 08:14:12', '2020-04-29 08:14:12'),
(497, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '154.66.171.217', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 08:14:12', '2020-04-29 08:14:12'),
(498, 'default', 'updated', 1, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 10:56:47', '2020-04-29 10:56:47'),
(499, 'default', 'created', 4, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 12:20:00', '2020-04-29 12:20:00'),
(500, 'default', 'created', 5, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 13:04:53', '2020-04-29 13:04:53'),
(501, 'default', 'updated', 4, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 13:29:59', '2020-04-29 13:29:59'),
(502, 'default', 'updated', 5, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 15:20:20', '2020-04-29 15:20:20'),
(503, 'default', 'created', 1, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:08:53', '2020-04-29 16:08:53'),
(504, 'default', 'created', 2, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:17:53', '2020-04-29 16:17:53'),
(505, 'default', 'updated', 1, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:18:28', '2020-04-29 16:18:28'),
(506, 'default', 'updated', 1, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:23:25', '2020-04-29 16:23:25'),
(507, 'default', 'updated', 2, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:28:50', '2020-04-29 16:28:50'),
(508, 'default', 'updated', 1, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:29:39', '2020-04-29 16:29:39'),
(509, 'default', 'created', 3, 'App\\Models\\Templatemsgs', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:33:25', '2020-04-29 16:33:25'),
(510, 'default', 'created', 37, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:37:49', '2020-04-29 16:37:49'),
(511, 'default', 'created', 38, 'App\\Models\\Acteur', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:37:49', '2020-04-29 16:37:49'),
(512, 'Acteur', 'Changement rôle CSR', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:37:58', '2020-04-29 16:37:58'),
(513, 'Acteur', 'Changement rôle CSR', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:38:17', '2020-04-29 16:38:17'),
(514, 'default', 'created', 35, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:46:50', '2020-04-29 16:46:50'),
(515, 'default', 'created', 36, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:46:50', '2020-04-29 16:46:50'),
(516, 'default', 'created', 37, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:46:50', '2020-04-29 16:46:50'),
(517, 'default', 'created', 38, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:46:50', '2020-04-29 16:46:50'),
(518, 'default', 'created', 39, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:46:50', '2020-04-29 16:46:50'),
(519, 'default', 'created', 40, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:48:35', '2020-04-29 16:48:35'),
(520, 'default', 'created', 41, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:48:35', '2020-04-29 16:48:35'),
(521, 'default', 'created', 42, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:48:35', '2020-04-29 16:48:35'),
(522, 'default', 'created', 43, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:48:35', '2020-04-29 16:48:35'),
(523, 'default', 'created', 44, 'App\\Models\\Partenaires', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:48:35', '2020-04-29 16:48:35'),
(524, 'Session', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:51:12', '2020-04-29 16:51:12'),
(525, 'Session', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:51:26', '2020-04-29 16:51:26'),
(526, 'Session', 'Modification', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:52:39', '2020-04-29 16:52:39'),
(527, 'default', 'created', 1, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:54:21', '2020-04-29 16:54:21'),
(528, 'default', 'created', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:55:44', '2020-04-29 16:55:44'),
(529, 'default', 'updated', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:56:12', '2020-04-29 16:56:12'),
(530, 'default', 'deleted', 2, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 16:58:04', '2020-04-29 16:58:04'),
(531, 'default', 'created', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 17:28:48', '2020-04-29 17:28:48'),
(532, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 17:39:58', '2020-04-29 17:39:58'),
(533, 'default', 'updated', 3, 'App\\Models\\Alertes', 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 17:40:23', '2020-04-29 17:40:23'),
(534, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 43, 'App\\User', 'gaetan@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 19:11:16', '2020-04-29 19:11:16'),
(535, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 19:11:24', '2020-04-29 19:11:24'),
(536, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 19:11:24', '2020-04-29 19:11:24'),
(537, 'Administration', 'Modification de compte', 43, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 19:13:11', '2020-04-29 19:13:11'),
(538, 'Administration', 'Modification de compte', 43, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-29 19:15:04', '2020-04-29 19:15:04'),
(539, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:06:10', '2020-04-30 08:06:10'),
(540, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:06:10', '2020-04-30 08:06:10'),
(541, 'default', 'created', 1, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:28', '2020-04-30 08:44:28'),
(542, 'default', 'created', 2, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:28', '2020-04-30 08:44:28'),
(543, 'default', 'created', 3, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:28', '2020-04-30 08:44:28'),
(544, 'default', 'created', 4, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:28', '2020-04-30 08:44:28'),
(545, 'default', 'created', 5, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:28', '2020-04-30 08:44:28'),
(546, 'default', 'created', 6, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:28', '2020-04-30 08:44:28'),
(547, 'default', 'created', 7, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(548, 'default', 'created', 8, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(549, 'default', 'created', 9, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(550, 'default', 'created', 10, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(551, 'default', 'created', 11, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(552, 'default', 'created', 12, 'App\\Models\\ListeAlertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(553, 'Session', 'Validation de la formulation', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 08:44:29', '2020-04-30 08:44:29'),
(554, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 13:20:49', '2020-04-30 13:20:49'),
(555, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 13:20:49', '2020-04-30 13:20:49'),
(556, 'default', 'updated', 1, 'App\\Models\\Alertes', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 13:22:43', '2020-04-30 13:22:43'),
(557, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 20:18:25', '2020-04-30 20:18:25'),
(558, 'Administration', 'Modification de compte', 43, 'App\\User', 43, 'App\\User', 'kizitoouedraogo12@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 20:19:02', '2020-04-30 20:19:02'),
(559, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 43, 'App\\User', 'kizitoouedraogo12@gmail.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-04-30 20:19:02', '2020-04-30 20:19:02'),
(560, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 09:24:21', '2020-05-01 09:24:21'),
(561, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 09:24:21', '2020-05-01 09:24:21'),
(562, 'Deconnexion', 'Utilisateur deconnecté', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 11:25:20', '2020-05-01 11:25:20'),
(563, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 11:27:11', '2020-05-01 11:27:11'),
(564, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 11:27:11', '2020-05-01 11:27:11'),
(565, 'Administration', 'Modification de compte', 1, 'App\\User', 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 14:33:19', '2020-05-01 14:33:19'),
(566, 'Connexion', 'Connexion à la plateforme', NULL, NULL, 1, 'App\\User', 'admin@admin.com', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '[]', '2020-05-01 14:33:19', '2020-05-01 14:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `alertes`
--

DROP TABLE IF EXISTS `alertes`;
CREATE TABLE IF NOT EXISTS `alertes` (
  `ale_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `tmsg_id` bigint(20) UNSIGNED NOT NULL,
  `ale_msg` text,
  `ale_destinataire` text,
  `mail_subject` varchar(255) DEFAULT NULL,
  `ale_date_heure_prev` datetime NOT NULL,
  `ale_etat` smallint(6) NOT NULL DEFAULT '0' COMMENT '0: non envoy?; 1:envoy',
  `ale_tmsg_type` varchar(50) NOT NULL COMMENT 'PREPARATION;SUIVI;AUTRE',
  `ale_date_heure_envoi` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ale_id`),
  UNIQUE KEY `ale_id` (`ale_id`),
  KEY `ses_id` (`ses_id`),
  KEY `tmsg_id` (`tmsg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alertes`
--

INSERT INTO `alertes` (`ale_id`, `ses_id`, `tmsg_id`, `ale_msg`, `ale_destinataire`, `mail_subject`, `ale_date_heure_prev`, `ale_etat`, `ale_tmsg_type`, `ale_date_heure_envoi`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 16, 1, 'Bonjour [USER_NOM] [USER_PRENOM]! Votre structure a des recommandations issues de la \"[DESCRITION_SESSION]\" qui a eu lieu du [DATE_DEBUT_SESSION] au [DATE_FIN_SESSION] à [LIEU_SESSION]. Vous êtes désignés pour la planification et la réalisation de ces recommandations.\r\nLa phase de planification prend fin au plus tard le [DATE_FIN_PLANIFICATION].\r\nCi-dessous la liste des recommandations en attentes de planification :\r\n [LISTE_RECO]\r\nMerci de prendre toutes les dispositions pour le respect de cette date qui sera de rigueure.', '3,2,26', 'Invitation à la planification des recommandations', '2020-05-01 13:22:43', 0, 'PLANIFICATION', NULL, '', '2020-04-29 16:54:21', NULL, '2020-05-01 15:18:13'),
(3, 16, 3, 'Bonjour [USER_NOM] [USER_PRENOM]! Juste vous informer que  [DESCRITION_SESSION] est reporté(e) pour se tenir du [DATE_DEBUT_SESSION] au [DATE_FIN_SESSION] à  [LIEU_SESSION].\r\nTrès cordialement!', '3,2', 'Repport de la tenu de la session du forum de l\'eau 2020', '2020-05-01 14:00:23', 0, 'AUTRE', NULL, '', '2020-04-29 17:28:48', NULL, '2020-05-01 16:26:17');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_libelle` varchar(70) NOT NULL,
  `cat_is_ministere` smallint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  UNIQUE KEY `categories_UN` (`cat_libelle`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_libelle`, `cat_is_ministere`, `created_at`, `updated_at`, `updated_by`, `created_by`) VALUES
(1, 'Direction centrale', 1, '2020-01-02 12:35:40', NULL, NULL, NULL),
(2, 'Direction générale', 1, '2020-01-02 12:35:40', NULL, NULL, NULL),
(3, 'DREA', 1, '2020-01-02 12:35:40', NULL, NULL, NULL),
(4, 'Agence de l\'Eau', 1, '2020-01-02 12:35:40', NULL, NULL, NULL),
(5, 'Secteur privé', 0, '2020-01-02 12:35:40', '2020-04-28 10:47:56', 'admin@admin.com', NULL),
(6, 'PTF', 0, '2020-01-02 12:35:40', NULL, NULL, NULL),
(9, 'OSC', 0, '2020-01-02 12:35:40', NULL, NULL, NULL),
(10, 'Structure rattachée', 1, '2020-01-02 12:35:40', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `envoialertes`
--

DROP TABLE IF EXISTS `envoialertes`;
CREATE TABLE IF NOT EXISTS `envoialertes` (
  `ena_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `ale_id` bigint(20) UNSIGNED NOT NULL,
  `ena_msg` text,
  `ale_mail_subject` varchar(255) DEFAULT NULL,
  `ale_date_heure_prev` datetime DEFAULT NULL,
  `ena_date_heure_recu` datetime NOT NULL,
  `ena_isenvoi` smallint(6) NOT NULL DEFAULT '0',
  `ena_is_lue` smallint(6) NOT NULL DEFAULT '0',
  `ena_read_at` datetime DEFAULT NULL,
  `ena_ses_description` varchar(255) NOT NULL,
  `ena_ses_annee` int(11) NOT NULL,
  `ena_ses_debut` date NOT NULL,
  `ena_ses_fin` date DEFAULT NULL,
  `ena_ses_lieu` varchar(255) NOT NULL,
  `user_nom` varchar(100) DEFAULT NULL,
  `user_prenom` varchar(100) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `str_id` bigint(20) DEFAULT NULL,
  `str_sigle` varchar(255) DEFAULT NULL,
  `str_nom_complet` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ena_id`),
  UNIQUE KEY `ena_id` (`ena_id`),
  KEY `ale_id` (`ale_id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `failed_jobs`
--

INSERT INTO `failed_jobs` (`id`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(60, 'database', 'default', '{\"displayName\":\"App\\\\Jobs\\\\ReSendAlert\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\ReSendAlert\",\"command\":\"O:20:\\\"App\\\\Jobs\\\\ReSendAlert\\\":9:{s:26:\\\"\\u0000App\\\\Jobs\\\\ReSendAlert\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\EnvoiAlertes\\\";s:2:\\\"id\\\";a:1:{i:0;i:21;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ErrorException: Creating default object from empty value in C:\\wamp64\\www\\psr_12\\psr\\app\\Http\\Controllers\\MethodesStaticController.php:162\nStack trace:\n#0 C:\\wamp64\\www\\psr_12\\psr\\app\\Http\\Controllers\\MethodesStaticController.php(162): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'Creating defaul...\', \'C:\\\\wamp64\\\\www\\\\p...\', 162, Array)\n#1 C:\\wamp64\\www\\psr_12\\psr\\app\\Jobs\\ReSendAlert.php(49): App\\Http\\Controllers\\MethodesStaticController::sendEmail(Object(stdClass), \'ReSend\')\n#2 [internal function]: App\\Jobs\\ReSendAlert->handle()\n#3 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)\n#4 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#5 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#6 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#7 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#8 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#9 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#10 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#11 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#12 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\ReSendAlert), false)\n#13 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#14 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#15 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#16 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\ReSendAlert))\n#17 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(88): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#18 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(368): Illuminate\\Queue\\Jobs\\Job->fire()\n#19 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(314): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#20 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(134): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#21 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#22 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#23 [internal function]: Illuminate\\Queue\\Console\\WorkCommand->handle()\n#24 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)\n#25 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#26 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#27 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#28 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#29 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#30 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#31 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#32 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Application.php(1001): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Application.php(271): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Application.php(147): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#36 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#37 C:\\wamp64\\www\\psr_12\\psr\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#38 {main}', '2020-05-01 15:24:51'),
(61, 'database', 'default', '{\"displayName\":\"App\\\\Jobs\\\\ReSendAlert\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\ReSendAlert\",\"command\":\"O:20:\\\"App\\\\Jobs\\\\ReSendAlert\\\":9:{s:26:\\\"\\u0000App\\\\Jobs\\\\ReSendAlert\\u0000data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:23:\\\"App\\\\Models\\\\EnvoiAlertes\\\";s:2:\\\"id\\\";a:2:{i:0;i:29;i:1;i:35;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ErrorException: Creating default object from empty value in C:\\wamp64\\www\\psr_12\\psr\\app\\Http\\Controllers\\MethodesStaticController.php:161\nStack trace:\n#0 C:\\wamp64\\www\\psr_12\\psr\\app\\Http\\Controllers\\MethodesStaticController.php(161): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'Creating defaul...\', \'C:\\\\wamp64\\\\www\\\\p...\', 161, Array)\n#1 C:\\wamp64\\www\\psr_12\\psr\\app\\Jobs\\ReSendAlert.php(47): App\\Http\\Controllers\\MethodesStaticController::sendEmail(Object(stdClass), \'ReSend\')\n#2 [internal function]: App\\Jobs\\ReSendAlert->handle()\n#3 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)\n#4 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#5 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#6 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#7 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#8 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#9 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#10 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#11 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#12 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\ReSendAlert), false)\n#13 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#14 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\ReSendAlert))\n#15 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#16 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\ReSendAlert))\n#17 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(88): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#18 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(368): Illuminate\\Queue\\Jobs\\Job->fire()\n#19 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(314): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#20 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(134): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#21 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#22 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#23 [internal function]: Illuminate\\Queue\\Console\\WorkCommand->handle()\n#24 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)\n#25 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#26 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#27 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#28 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#29 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#30 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#31 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#32 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Application.php(1001): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Application.php(271): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 C:\\wamp64\\www\\psr_12\\psr\\vendor\\symfony\\console\\Application.php(147): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#36 C:\\wamp64\\www\\psr_12\\psr\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#37 C:\\wamp64\\www\\psr_12\\psr\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#38 {main}', '2020-05-01 15:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `instances`
--

DROP TABLE IF EXISTS `instances`;
CREATE TABLE IF NOT EXISTS `instances` (
  `ins_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ins_sigle` varchar(20) NOT NULL,
  `ins_nom_complet` varchar(255) NOT NULL,
  `ins_nbr_session` int(11) NOT NULL,
  `ins_periodicite` varchar(100) NOT NULL,
  `ins_description` text,
  `ins_ref_arrete` varchar(30) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ins_id`),
  UNIQUE KEY `ins_id` (`ins_id`),
  UNIQUE KEY `ins_sigle` (`ins_sigle`),
  UNIQUE KEY `instances_unique_nom_complet` (`ins_nom_complet`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instances`
--

INSERT INTO `instances` (`ins_id`, `ins_sigle`, `ins_nom_complet`, `ins_nbr_session`, `ins_periodicite`, `ins_description`, `ins_ref_arrete`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'CSD', 'Cadre Sectoriel de Dialogue', 1, 'Annuelle', 'Cadre Sectoriel de Dialogue', 'ARR2020/AG/0025', 'admin', '2020-02-28 08:22:41', 'Administrateur', NULL),
(2, 'CASEM', 'Conseil d\'Administration du Secteur Ministériel', 2, 'Annuelle', 'Conseil d\'Administration du Secteur Ministériel', 'ARR2020/AG/0026', 'admin', '2020-02-28 08:22:41', 'gaetan@gmail.com', '2020-04-22 15:36:42'),
(3, 'CRP', 'Comité de Revue et de Pilotage', 2, 'Annuelle', 'Comité de Revue', 'ARR2020/AG/0027', 'admin', '2020-02-28 08:22:41', 'Administrateur', NULL),
(4, 'CP', 'Comité de pilotage', 2, 'Trimestrielle', 'Comité de pilotage des projets et programmes', 'ARR2020/AG/0028', 'Administrateur', '2020-03-04 14:07:15', 'Administrateur', '2020-03-04 14:07:15'),
(5, 'FE', 'Forum de l\'Eau', 1, 'Quatriennale', 'Forum national de l\'eau, Burkina Faso', 'MP', 'guetikila@gmail.com', '2020-04-28 11:21:03', 'guetikila@gmail.com', '2020-04-28 11:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=MyISAM AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `liste_alertes`
--

DROP TABLE IF EXISTS `liste_alertes`;
CREATE TABLE IF NOT EXISTS `liste_alertes` (
  `lia_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `use_id` bigint(20) UNSIGNED NOT NULL,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `lia_nom_prenom` varchar(255) NOT NULL,
  `lia_email` varchar(100) NOT NULL,
  `lia_sexe` char(1) DEFAULT NULL,
  `lia_fonction` varchar(255) DEFAULT NULL,
  `lia_matricule` varchar(20) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`lia_id`),
  KEY `str_id` (`str_id`),
  KEY `ses_id` (`ses_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_13_160023_create_permission_tables', 2),
(5, '2020_02_15_201905_create_permission_tables', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL DEFAULT 'App\\User',
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(5, 'App\\User', 1),
(1, 'App\\User', 2),
(2, 'App\\User', 2),
(1, 'App\\User', 4),
(2, 'App\\User', 5),
(3, 'App\\User', 9),
(1, 'App\\User', 27),
(1, 'App\\User', 29),
(1, 'App\\User', 30),
(1, 'App\\User', 31),
(1, 'App\\User', 32),
(1, 'App\\User', 35),
(3, 'App\\User', 36),
(2, 'App\\User', 37),
(1, 'App\\User', 38),
(3, 'App\\User', 39),
(2, 'App\\User', 40),
(1, 'App\\User', 41),
(3, 'App\\User', 42),
(3, 'App\\User', 43),
(3, 'App\\User', 44),
(1, 'App\\User', 45),
(3, 'App\\User', 46),
(3, 'App\\User', 47),
(1, 'App\\User', 48),
(2, 'App\\User', 49),
(1, 'App\\User', 50),
(3, 'App\\User', 51),
(1, 'App\\User', 52),
(2, 'App\\User', 53),
(1, 'App\\User', 54);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) NOT NULL,
  `type` varchar(255) NOT NULL,
  `notifiable_type` varchar(255) NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('19dbbd62-0803-4dff-af79-16f23fc8e222', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 43, '{\"id\":1,\"msg\":\"Bonjour M.\\/Mme  BAMBARA Gaetan!\\r\\nLa date d\'\\u00e9ch\\u00e9ance des recommandations suivantes sont presque arriv\\u00e9es ou d\\u00e9pass\\u00e9es:\\r\\n <table border=\'1\'>\\r\\n                                        <thead style=\'background-color: #4CAF50;color: white\'>\\r\\n                                            <th>Ordre<\\/th>\\r\\n                                            <th>Intitul\\u00e9s recommandations<\\/th> \\r\\n                                            <th>Date d\'\\u00e9cheance<\\/th>\\r\\n                                            <th>Etat<\\/th>\\r\\n                                            <th>Taux de r\\u00e9al.<\\/th>\\r\\n                                        <\\/thead><tr>\\r\\n                                    <td> 1 <\\/td>\\r\\n                                    <td> Dsiponibiliser l\'argent \\u00e0 temps <\\/td>\\r\\n                                    <td> 2020-03-14 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 2 <\\/td>\\r\\n                                    <td> Plannifier toutes les activit\\u00e9s <\\/td>\\r\\n                                    <td> 2020-05-02 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 3 <\\/td>\\r\\n                                    <td> Aquisition d\'ordinateur portable <\\/td>\\r\\n                                    <td> 2021-02-17 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 4 <\\/td>\\r\\n                                    <td> Aquisition d\'imprimente multi-fonction laserJet <\\/td>\\r\\n                                    <td> 2021-02-19 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 5 <\\/td>\\r\\n                                    <td> Aquisition d\'un point d\'acc\\u00e8s 5G <\\/td>\\r\\n                                    <td> 2021-02-20 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 6 <\\/td>\\r\\n                                    <td> Reco pour test 1 <\\/td>\\r\\n                                    <td> 2020-05-20 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 7 <\\/td>\\r\\n                                    <td> Reco pour test 2 <\\/td>\\r\\n                                    <td> 2020-05-22 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 8 <\\/td>\\r\\n                                    <td> Reco pour test 3 <\\/td>\\r\\n                                    <td> 2020-05-24 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 9 <\\/td>\\r\\n                                    <td> Reco pour test 4 <\\/td>\\r\\n                                    <td> 2020-05-26 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 10 <\\/td>\\r\\n                                    <td> Maintenir les \\u00e9quipement informatiques <\\/td>\\r\\n                                    <td> 2021-04-28 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 70 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 11 <\\/td>\\r\\n                                    <td> Maintenir les \\u00e9quipement informatiques 2 <\\/td>\\r\\n                                    <td> 2021-04-28 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 12 <\\/td>\\r\\n                                    <td> Entretenir le syst\\u00e8me \\u00e9lectrique 2 <\\/td>\\r\\n                                    <td> 2021-03-31 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><\\/table>\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-04-2020 14:42:31\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', '2020-04-28 16:58:10', '2020-04-28 14:50:01', '2020-04-28 16:58:10'),
('e31eefff-2917-471b-8be8-e4462fed63df', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 43, '{\"id\":1,\"msg\":\"Bonjour M.\\/Mme  BAMBARA Gaetan!\\r\\nLa date d\'\\u00e9ch\\u00e9ance des recommandations suivantes sont presque arriv\\u00e9es ou d\\u00e9pass\\u00e9es:\\r\\n <table border=\'1\'>\\r\\n                                        <thead style=\'background-color: #4CAF50;color: white\'>\\r\\n                                            <th>Ordre<\\/th>\\r\\n                                            <th>Intitul\\u00e9s recommandations<\\/th> \\r\\n                                            <th>Date d\'\\u00e9cheance<\\/th>\\r\\n                                            <th>Etat<\\/th>\\r\\n                                            <th>Taux de r\\u00e9al.<\\/th>\\r\\n                                        <\\/thead><tr>\\r\\n                                    <td> 1 <\\/td>\\r\\n                                    <td> Dsiponibiliser l\'argent \\u00e0 temps <\\/td>\\r\\n                                    <td> 2020-03-14 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 2 <\\/td>\\r\\n                                    <td> Plannifier toutes les activit\\u00e9s <\\/td>\\r\\n                                    <td> 2020-05-02 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 3 <\\/td>\\r\\n                                    <td> Aquisition d\'ordinateur portable <\\/td>\\r\\n                                    <td> 2021-02-17 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 4 <\\/td>\\r\\n                                    <td> Aquisition d\'imprimente multi-fonction laserJet <\\/td>\\r\\n                                    <td> 2021-02-19 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 5 <\\/td>\\r\\n                                    <td> Aquisition d\'un point d\'acc\\u00e8s 5G <\\/td>\\r\\n                                    <td> 2021-02-20 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 6 <\\/td>\\r\\n                                    <td> Reco pour test 1 <\\/td>\\r\\n                                    <td> 2020-05-20 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 7 <\\/td>\\r\\n                                    <td> Reco pour test 2 <\\/td>\\r\\n                                    <td> 2020-05-22 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 8 <\\/td>\\r\\n                                    <td> Reco pour test 3 <\\/td>\\r\\n                                    <td> 2020-05-24 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 9 <\\/td>\\r\\n                                    <td> Reco pour test 4 <\\/td>\\r\\n                                    <td> 2020-05-26 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 10 <\\/td>\\r\\n                                    <td> Maintenir les \\u00e9quipement informatiques <\\/td>\\r\\n                                    <td> 2021-04-28 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 70 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 11 <\\/td>\\r\\n                                    <td> Maintenir les \\u00e9quipement informatiques 2 <\\/td>\\r\\n                                    <td> 2021-04-28 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 12 <\\/td>\\r\\n                                    <td> Entretenir le syst\\u00e8me \\u00e9lectrique 2 <\\/td>\\r\\n                                    <td> 2021-03-31 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><\\/table>\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-04-2020 14:42:31\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', '2020-04-28 16:55:08', '2020-04-28 15:07:43', '2020-04-28 16:55:08'),
('dc7ed272-0f65-42df-b1aa-c46dce9bbeed', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 43, '{\"id\":2,\"msg\":\"Bonjour M.\\/Mme  BAMBARA Gaetan!\\r\\nLa date d\'\\u00e9ch\\u00e9ance des recommandations suivantes sont presque arriv\\u00e9es ou d\\u00e9pass\\u00e9es:\\r\\n <table border=\'1\'>\\r\\n                                        <thead style=\'background-color: #4CAF50;color: white\'>\\r\\n                                            <th>Ordre<\\/th>\\r\\n                                            <th>Intitul\\u00e9s recommandations<\\/th> \\r\\n                                            <th>Date d\'\\u00e9cheance<\\/th>\\r\\n                                            <th>Etat<\\/th>\\r\\n                                            <th>Taux de r\\u00e9al.<\\/th>\\r\\n                                        <\\/thead><tr>\\r\\n                                    <td> 1 <\\/td>\\r\\n                                    <td> Dsiponibiliser l\'argent \\u00e0 temps <\\/td>\\r\\n                                    <td> 2020-03-14 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 2 <\\/td>\\r\\n                                    <td> Plannifier toutes les activit\\u00e9s <\\/td>\\r\\n                                    <td> 2020-05-02 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 3 <\\/td>\\r\\n                                    <td> Aquisition d\'ordinateur portable <\\/td>\\r\\n                                    <td> 2021-02-17 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 4 <\\/td>\\r\\n                                    <td> Aquisition d\'imprimente multi-fonction laserJet <\\/td>\\r\\n                                    <td> 2021-02-19 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 5 <\\/td>\\r\\n                                    <td> Aquisition d\'un point d\'acc\\u00e8s 5G <\\/td>\\r\\n                                    <td> 2021-02-20 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 6 <\\/td>\\r\\n                                    <td> Reco pour test 1 <\\/td>\\r\\n                                    <td> 2020-05-20 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 7 <\\/td>\\r\\n                                    <td> Reco pour test 2 <\\/td>\\r\\n                                    <td> 2020-05-22 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 8 <\\/td>\\r\\n                                    <td> Reco pour test 3 <\\/td>\\r\\n                                    <td> 2020-05-24 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 9 <\\/td>\\r\\n                                    <td> Reco pour test 4 <\\/td>\\r\\n                                    <td> 2020-05-26 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 10 <\\/td>\\r\\n                                    <td> Maintenir les \\u00e9quipement informatiques <\\/td>\\r\\n                                    <td> 2021-04-28 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 70 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 11 <\\/td>\\r\\n                                    <td> Maintenir les \\u00e9quipement informatiques 2 <\\/td>\\r\\n                                    <td> 2021-04-28 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><tr>\\r\\n                                    <td> 12 <\\/td>\\r\\n                                    <td> Entretenir le syst\\u00e8me \\u00e9lectrique 2 <\\/td>\\r\\n                                    <td> 2021-03-31 00:00:00<\\/td>\\r\\n                                    <td>  <\\/td>\\r\\n                                    <td> 0 <\\/td>\\r\\n                                <\\/tr><\\/table>\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-04-2020 14:42:31\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', '2020-04-28 16:57:50', '2020-04-28 15:21:34', '2020-04-28 16:57:50'),
('0b13f3f9-eb92-4a6a-aaea-47fd60629376', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 43, '{\"id\":3,\"msg\":\"Bonjour M.\\/Mme  BAMBARA Gaetan!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-04-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', '2020-04-28 16:57:33', '2020-04-28 15:39:47', '2020-04-28 16:57:33'),
('3cd32b1f-85b5-4b78-bcb7-1a4b64d9a0f7', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 51, '{\"id\":4,\"msg\":\"Bonjour M.\\/Mme  KI L. Micha\\u00ebl Claver!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-05-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('b2a492f2-c582-4549-9ceb-3e3520b418b1', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 2, '{\"id\":5,\"msg\":\"Bonjour M.\\/Mme  isidore isidore!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-06-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('2f063796-1a0d-4ccb-826b-77c8d0b25c0e', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 46, '{\"id\":6,\"msg\":\"Bonjour M.\\/Mme  KONATE Issouf!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-07-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('4901e675-06fc-44c5-a84c-515fbbf72529', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 47, '{\"id\":7,\"msg\":\"Bonjour M.\\/Mme  GUETIKILA Daouda!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-08-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('52e3d918-7d6f-4cf6-a501-84a89cc1f0e2', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 48, '{\"id\":8,\"msg\":\"Bonjour M.\\/Mme  OUIYA Brigitte!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-09-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('a89b66e5-2c51-419a-9af7-320b44f9d0ab', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 49, '{\"id\":9,\"msg\":\"Bonjour M.\\/Mme  COULIBALY Lessi Lassina!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-10-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('c783ca39-9bce-422f-99c2-00da629704e4', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 52, '{\"id\":10,\"msg\":\"Bonjour M.\\/Mme  COMPAORE Fabrice!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-11-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('a5e9025a-91f4-467d-aa25-1b63b69f87eb', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 53, '{\"id\":11,\"msg\":\"Bonjour M.\\/Mme  NIKIEMA Teddy Camille!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-12-2020 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('9b9766d4-c16b-4111-a939-2bf1605be281', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 54, '{\"id\":12,\"msg\":\"Bonjour M.\\/Mme  ZABSONRE Philippe!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-01-2021 15:39:11\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 15:39:47', '2020-04-28 15:39:47'),
('893e82e7-4175-4669-9a9b-0f10c32b125d', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 43, '{\"id\":13,\"msg\":\"Bonjour M.\\/Mme  BAMBARA Gaetan!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-04-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', '2020-04-28 16:56:49', '2020-04-28 16:52:38', '2020-04-28 16:56:49'),
('dfc4d019-edcd-49ea-a087-13c69a9b1039', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 51, '{\"id\":14,\"msg\":\"Bonjour M.\\/Mme  KI L. Micha\\u00ebl Claver!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-05-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
('703d5f1e-ef8f-4723-8ed8-60b2e557aea4', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 2, '{\"id\":15,\"msg\":\"Bonjour M.\\/Mme  isidore isidore!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-06-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
('c5432aa9-6650-4656-911b-e8e0e7010b60', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 46, '{\"id\":16,\"msg\":\"Bonjour M.\\/Mme  KONATE Issouf!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-07-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
('039b6a89-ef5c-4674-be09-a253cc0c89e1', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 47, '{\"id\":17,\"msg\":\"Bonjour M.\\/Mme  GUETIKILA Daouda!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-08-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:38', '2020-04-28 16:52:38'),
('50e1423f-fbda-4f1b-bee2-36edfabdc0b0', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 48, '{\"id\":18,\"msg\":\"Bonjour M.\\/Mme  OUIYA Brigitte!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-09-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
('8a5cd812-3f6e-4337-b6b8-0f8416284287', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 49, '{\"id\":19,\"msg\":\"Bonjour M.\\/Mme  COULIBALY Lessi Lassina!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-10-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
('5fb4de61-6470-4bcb-99f0-76e1fb1525d4', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 52, '{\"id\":20,\"msg\":\"Bonjour M.\\/Mme  COMPAORE Fabrice!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-11-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
('0b1a0112-4fb1-4baf-acfa-e78d35f7f624', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 53, '{\"id\":21,\"msg\":\"Bonjour M.\\/Mme  NIKIEMA Teddy Camille!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-12-2020 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:39', '2020-04-28 16:52:39'),
('a68fb1d2-6e22-49c2-8574-976e937fdb07', 'App\\Notifications\\AlerteNotification', 'App\\Models\\Users', 54, '{\"id\":22,\"msg\":\"Bonjour M.\\/Mme  ZABSONRE Philippe!\\r\\nVous devez planifier vos recommandations au plus tard le  2020-04-14 00:00:00.\\r\\nBonne r\\u00e9ception!\",\"date_recu\":\"28-01-2021 16:52:08\",\"ses_annee\":2022,\"description\":\"3\\u00e8 session CSD\",\"ses_lieu\":\"Salle de conf\\u00e9rence MEA\",\"ses_debut\":\"06-05-2020\"}', NULL, '2020-04-28 16:52:39', '2020-04-28 16:52:39');

-- --------------------------------------------------------

--
-- Table structure for table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
CREATE TABLE IF NOT EXISTS `partenaires` (
  `ptn_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `rec_id` bigint(20) UNSIGNED NOT NULL,
  `ptn_is_responsable` smallint(6) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ptn_id`),
  UNIQUE KEY `ptn_id` (`ptn_id`),
  KEY `str_id` (`str_id`),
  KEY `rec_id` (`rec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partenaires`
--

INSERT INTO `partenaires` (`ptn_id`, `str_id`, `rec_id`, `ptn_is_responsable`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 2, 1, 1, 'Administrateur', '2020-03-04 14:13:50', 'Administrateur', '2020-03-04 14:13:50'),
(2, 4, 1, 0, 'Administrateur', '2020-03-04 14:13:54', 'Administrateur', '2020-03-04 14:13:54'),
(4, 3, 2, 0, 'Administrateur', '2020-03-23 12:53:48', 'Administrateur', '2020-04-02 12:18:47'),
(5, 19, 2, 0, 'Administrateur', '2020-03-23 12:53:56', 'Administrateur', '2020-03-23 12:53:56'),
(8, 1, 32, 1, 'Administrateur', '2020-04-08 12:30:12', 'Administrateur', '2020-04-08 12:30:12'),
(11, 18, 36, 1, 'Administrateur', '2020-04-14 11:58:39', 'Administrateur', '2020-04-14 11:58:39'),
(12, 4, 36, 0, 'Administrateur', '2020-04-14 11:58:39', 'Administrateur', '2020-04-14 11:58:39'),
(13, 3, 36, 0, 'Administrateur', '2020-04-14 11:58:39', 'Administrateur', '2020-04-14 11:58:39'),
(14, 2, 36, 0, 'Administrateur', '2020-04-14 11:58:39', 'Administrateur', '2020-04-14 11:58:39'),
(15, 19, 36, 0, 'Administrateur', '2020-04-14 11:58:39', 'Administrateur', '2020-04-14 11:58:39'),
(16, 4, 2, 0, 'gaetan', '2020-04-15 16:04:04', 'gaetan', '2020-04-15 16:04:04'),
(17, 2, 2, 1, 'gaetan', '2020-04-15 16:04:04', 'gaetan', '2020-04-15 16:04:17'),
(18, 4, 3, 1, 'gaetan', '2020-04-15 16:05:37', 'gaetan', '2020-04-15 16:05:37'),
(19, 2, 3, 0, 'gaetan', '2020-04-15 16:05:37', 'gaetan', '2020-04-15 16:05:37'),
(20, 4, 33, 1, 'gaetan', '2020-04-15 16:06:17', 'gaetan', '2020-04-15 16:06:29'),
(21, 2, 33, 0, 'gaetan', '2020-04-15 16:06:17', 'gaetan', '2020-04-15 16:06:17'),
(22, 2, 40, 0, 'gaetan@gmail.com', '2020-04-20 10:13:57', 'gaetan@gmail.com', '2020-04-20 10:13:57'),
(23, 5, 40, 0, 'gaetan@gmail.com', '2020-04-20 10:13:57', 'gaetan@gmail.com', '2020-04-20 10:13:57'),
(24, 4, 40, 1, 'gaetan@gmail.com', '2020-04-20 10:17:31', 'gaetan@gmail.com', '2020-04-20 10:17:35'),
(25, 3, 40, 0, 'gaetan@gmail.com', '2020-04-20 10:17:31', 'gaetan@gmail.com', '2020-04-20 10:17:31'),
(26, 1, 40, 0, 'gaetan@gmail.com', '2020-04-20 10:17:31', 'gaetan@gmail.com', '2020-04-20 10:17:31'),
(27, 3, 43, 1, 'guetikila@gmail.com', '2020-04-28 12:20:45', 'guetikila@gmail.com', '2020-04-28 12:21:45'),
(28, 26, 43, 0, 'guetikila@gmail.com', '2020-04-28 12:20:45', 'guetikila@gmail.com', '2020-04-28 12:21:26'),
(29, 3, 54, 0, 'guetikila@gmail.com', '2020-04-28 13:10:18', 'guetikila@gmail.com', '2020-04-28 13:10:18'),
(30, 26, 54, 1, 'guetikila@gmail.com', '2020-04-28 13:10:18', 'guetikila@gmail.com', '2020-04-28 13:11:08'),
(31, 3, 55, 0, 'guetikila@gmail.com', '2020-04-28 13:10:42', 'guetikila@gmail.com', '2020-04-28 13:10:42'),
(32, 26, 55, 1, 'guetikila@gmail.com', '2020-04-28 13:10:42', 'guetikila@gmail.com', '2020-04-28 13:10:51'),
(33, 3, 56, 1, 'guetikila@gmail.com', '2020-04-28 13:11:39', 'guetikila@gmail.com', '2020-04-28 13:11:39'),
(34, 26, 56, 0, 'guetikila@gmail.com', '2020-04-28 13:11:39', 'guetikila@gmail.com', '2020-04-28 13:11:39'),
(35, 24, 58, 0, 'gaetan@gmail.com', '2020-04-29 16:46:50', 'gaetan@gmail.com', '2020-04-29 16:46:50'),
(36, 7, 58, 0, 'gaetan@gmail.com', '2020-04-29 16:46:50', 'gaetan@gmail.com', '2020-04-29 16:46:50'),
(37, 3, 58, 1, 'gaetan@gmail.com', '2020-04-29 16:46:50', 'gaetan@gmail.com', '2020-04-29 16:47:43'),
(38, 2, 58, 0, 'gaetan@gmail.com', '2020-04-29 16:46:50', 'gaetan@gmail.com', '2020-04-29 16:46:50'),
(39, 26, 58, 0, 'gaetan@gmail.com', '2020-04-29 16:46:50', 'gaetan@gmail.com', '2020-04-29 16:47:33'),
(40, 24, 59, 0, 'gaetan@gmail.com', '2020-04-29 16:48:34', 'gaetan@gmail.com', '2020-04-29 16:48:34'),
(41, 7, 59, 0, 'gaetan@gmail.com', '2020-04-29 16:48:35', 'gaetan@gmail.com', '2020-04-29 16:48:35'),
(42, 3, 59, 0, 'gaetan@gmail.com', '2020-04-29 16:48:35', 'gaetan@gmail.com', '2020-04-29 16:48:35'),
(43, 2, 59, 0, 'gaetan@gmail.com', '2020-04-29 16:48:35', 'gaetan@gmail.com', '2020-04-29 16:48:35'),
(44, 26, 59, 1, 'gaetan@gmail.com', '2020-04-29 16:48:35', 'gaetan@gmail.com', '2020-04-29 16:48:46');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
CREATE TABLE IF NOT EXISTS `participants` (
  `par_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `str_id` bigint(20) UNSIGNED NOT NULL,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`par_id`),
  UNIQUE KEY `par_id` (`par_id`),
  KEY `str_id` (`str_id`),
  KEY `ses_id` (`ses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`par_id`, `str_id`, `ses_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 1, 2),
(6, 2, 2),
(7, 3, 2),
(8, 4, 2),
(9, 1, 3),
(10, 2, 3),
(11, 3, 3),
(12, 4, 3),
(13, 1, 4),
(14, 2, 4),
(15, 3, 4),
(16, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `guard_name` varchar(255) NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_UN_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `guard_name`, `created_at`, `updated_at`) VALUES
(26, 'GERER_STRUCTURE', 'Peut ajouter, modifier ou supprimer une structure', 'web', NULL, NULL),
(27, 'GERER_COMPTE', 'Peut créer un compte pour un utilisateur', 'web', NULL, NULL),
(28, 'GERER_PROFIL', 'Peut ajouter, modifier ou supprimer des profils d\'utilisateurs', 'web', NULL, NULL),
(29, 'GERER_INSTANCE', 'Peut ajouter, modifier ou supprimer une instance', 'web', NULL, NULL),
(30, 'GERER_SESSION', 'Peut accéder aux sessions', 'web', NULL, NULL),
(31, 'GERER_RECOMMANDATION', 'Peut accéder aux recommandations', 'web', NULL, NULL),
(32, 'GERER_ALERTE', 'Peut gérer les alertes, notifications et modèles de message', 'web', NULL, NULL),
(33, 'GERER_PROGRAMME_BUDGETAIRE', 'Peut ajouter, modifier ou supprimer les programmes budgétaires', 'web', NULL, NULL),
(34, 'GERER_CATEGORIE_STRUCTURE', 'Peut gérer les différentes catégories de structure', 'web', NULL, NULL),
(35, 'GERER_ACTEUR_INSTANCE', 'Peut affecter des acteurs à une instance', 'web', NULL, NULL),
(36, 'GERER_ROLE_ACTEUR', 'Peut affecter ou retirer les rôles CSS ou CSR à un acteur d\'une instance', 'web', NULL, NULL),
(37, 'VALIDER_FORMULATION', 'Peut valider la formulation des recommandations d\'une session', 'web', NULL, NULL),
(38, 'VALIDER_PLANIFICATION', 'Peut valider la planification des recommandations d\'une session', 'web', NULL, NULL),
(39, 'VALIDER_REALISATION', 'Peut valider le suivi des recommandations d\'une session', 'web', NULL, NULL),
(40, 'RECONDUIRE_RECOMMANDATION', 'Peut réconduire les recommandations de la session précedente non encore réalisées, non abandonné ou permanent', 'web', NULL, NULL),
(41, 'ABANDONNER_RECOMMANDATION', 'Peut abandonner une recommandation', 'web', NULL, NULL),
(42, 'SAISIE_RECOMMANDATION', 'Peut ajouter, modifier ou supprimer une recommandation', 'web', NULL, NULL),
(43, 'PLANIFIER_RECOMMANDATION', 'Peut planifier les activités d\'une recommandation', 'web', NULL, NULL),
(44, 'SUIVI_RECOMMANDATION', 'Peut assurer le suivi des activités d\'une récommandation', 'web', NULL, NULL),
(45, 'CONSULTER_RAPPORT', 'Peut consulter les rapports de mise en oeuvre des recommandations', 'web', NULL, NULL),
(47, 'CONSULTER_STATS', 'Peut consulter les statistiques de mise en oeuvre des recommandations', 'web', NULL, NULL),
(48, 'VALIDER_PLANIFICATION_RECO', 'Peut valider la planification d\'une recommandation', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `programme_budgetaire`
--

DROP TABLE IF EXISTS `programme_budgetaire`;
CREATE TABLE IF NOT EXISTS `programme_budgetaire` (
  `bud_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bud_code` varchar(10) NOT NULL,
  `bud_intitule` varchar(150) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bud_id`),
  UNIQUE KEY `ligne_budgetaire_bud_code_uindex` (`bud_code`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `programme_budgetaire`
--

INSERT INTO `programme_budgetaire` (`bud_id`, `bud_code`, `bud_intitule`, `created_at`, `updated_at`) VALUES
(1, 'PN-AEP', 'PN-AEP', '2020-04-17 15:52:17', '2020-04-24 10:30:20'),
(2, 'PN-AEUE ', 'PN-AEUE ', '2020-04-17 15:52:17', '2020-04-17 15:52:32'),
(3, 'PN-GIRE', 'PN-GIRE', '2020-04-17 15:52:17', '2020-04-17 15:52:40'),
(20, 'PN-AH', 'PN-AH', '2020-04-17 15:52:17', '2020-04-17 15:52:32'),
(21, 'PPS', 'PPS', '2020-04-17 15:52:17', '2020-04-17 15:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `recommandations`
--

DROP TABLE IF EXISTS `recommandations`;
CREATE TABLE IF NOT EXISTS `recommandations` (
  `rec_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ses_id` bigint(20) UNSIGNED NOT NULL,
  `bud_id` bigint(20) NOT NULL,
  `rec_intitule` varchar(1024) NOT NULL,
  `rec_date_echeance` date NOT NULL,
  `rec_statut` smallint(1) NOT NULL DEFAULT '1' COMMENT '1:Préparation;2:Non réalisé;3:En cours; 4:Réalisé;5:Abandonné',
  `rec_is_suivi_valide` smallint(6) NOT NULL DEFAULT '0',
  `rec_is_mode_standard` smallint(6) DEFAULT '0' COMMENT '1: suivi par statut; 0: suivi par taux',
  `rec_is_planning_valide` smallint(1) NOT NULL DEFAULT '0',
  `rec_observation` text,
  `rec_obs_nonrealise` varchar(1024) DEFAULT NULL,
  `rec_obs_encours` varchar(1024) DEFAULT NULL,
  `rec_obs_realise` varchar(1024) DEFAULT NULL,
  `rec_personne_formule` varchar(255) DEFAULT NULL,
  `rec_personne_tel` varchar(100) DEFAULT NULL,
  `rec_dh_valide` datetime DEFAULT NULL,
  `rec_dh_abandon` datetime DEFAULT NULL,
  `rec_dh_nonrealise` datetime DEFAULT NULL COMMENT 'Date de passage au statut non réalisé',
  `rec_dh_en_cours` datetime DEFAULT NULL COMMENT 'Date de passage au statut en cours',
  `rec_dh_realise` datetime DEFAULT NULL COMMENT 'Date de passage au statut réalisé',
  `rec_dh_planning_valide` datetime DEFAULT NULL,
  `rec_taux_realise` smallint(6) NOT NULL DEFAULT '0',
  `rec_motif_abandon` varchar(255) DEFAULT NULL,
  `rec_preuve_nonrealise` varchar(255) DEFAULT NULL,
  `rec_preuve_encours` varchar(255) DEFAULT NULL,
  `rec_preuve_realise` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `rec_is_permanente` smallint(6) NOT NULL DEFAULT '0',
  `rec_id_ref` bigint(20) UNSIGNED DEFAULT NULL,
  `rec_is_reconduit` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rec_id`),
  UNIQUE KEY `rec_id` (`rec_id`),
  KEY `ses_id` (`ses_id`),
  KEY `recommandations_reconduit_fk` (`rec_id_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recommandations`
--

INSERT INTO `recommandations` (`rec_id`, `ses_id`, `bud_id`, `rec_intitule`, `rec_date_echeance`, `rec_statut`, `rec_is_suivi_valide`, `rec_is_mode_standard`, `rec_is_planning_valide`, `rec_observation`, `rec_obs_nonrealise`, `rec_obs_encours`, `rec_obs_realise`, `rec_personne_formule`, `rec_personne_tel`, `rec_dh_valide`, `rec_dh_abandon`, `rec_dh_nonrealise`, `rec_dh_en_cours`, `rec_dh_realise`, `rec_dh_planning_valide`, `rec_taux_realise`, `rec_motif_abandon`, `rec_preuve_nonrealise`, `rec_preuve_encours`, `rec_preuve_realise`, `created_by`, `created_at`, `updated_by`, `updated_at`, `rec_is_permanente`, `rec_id_ref`, `rec_is_reconduit`) VALUES
(1, 2, 1, 'Dsiponibiliser l\'argent à temps', '2020-03-14', 1, 0, 0, 0, 'Dsiponibiliser l\'argent à temps', NULL, NULL, NULL, 'DAF', '+22677889966', '2020-03-23 15:48:48', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-03-04 14:12:24', 'Administrateur', '2020-03-23 15:48:48', 0, NULL, 0),
(2, 2, 3, 'Plannifier toutes les activités', '2020-05-02', 2, 0, 1, 1, 'Plannifier toutes les activités', NULL, NULL, NULL, 'DGESS', '+22688556633', '2020-04-08 14:36:54', NULL, NULL, NULL, NULL, '2020-04-07 15:18:08', 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-03-04 14:13:10', 'gaetan', '2020-04-16 12:56:04', 0, NULL, 0),
(3, 2, 3, 'Acquisition de licence ORACLE 12C', '2020-04-30', 4, 0, 0, 1, NULL, 'Non réalisée', 'En cours de réalisation', 'Recommandation réalisée', 'DSA', '00000001', NULL, NULL, '2020-04-27 14:00:35', '2020-04-27 15:07:28', '2020-04-27 15:08:08', '2020-04-16 11:27:36', 100, '', 'RECO_3_2_27_Apr_2020_14_00_35.pdf', 'RECO_3_3_27_Apr_2020_15_07_28.pdf', 'RECO_3_4_27_Apr_2020_15_08_08.pdf', 'Administrateur', '2020-04-02 13:36:30', 'pelag@gmail.com', '2020-04-27 15:08:08', 1, NULL, 0),
(32, 1, 2, 'Aquisition d\'ordinateur portable', '2021-02-17', 1, 0, 0, 0, NULL, NULL, NULL, NULL, 'DAF', '10000001', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-06 14:42:06', 'Administrateur', '2020-04-06 14:42:06', 1, NULL, 0),
(33, 2, 2, 'Aquisition d\'écran de projection', '2021-02-18', 4, 0, 1, 1, '- Activité en cours\r\n- Activité réalisée', NULL, NULL, 'Réalisée', 'SG', '10000002', NULL, NULL, NULL, NULL, '2020-04-27 15:09:10', '2020-04-16 12:03:40', 100, NULL, NULL, NULL, 'RECO_33_4_27_Apr_2020_15_09_10.pdf', 'Administrateur', '2020-04-06 14:42:06', 'pelag@gmail.com', '2020-04-27 15:09:10', 0, NULL, 0),
(34, 3, 2, 'Aquisition d\'imprimente multi-fonction laserJet', '2021-02-19', 1, 1, 0, 0, 'Modification de l’observation de la recommandation\r\n\"Aquisition d\'imprimente multi-fonction laserJet\"', NULL, NULL, NULL, 'DSI', '10000003', '2020-04-23 16:00:28', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-06 14:42:06', 'gaetan@gmail.com', '2020-04-23 16:00:28', 1, NULL, 0),
(35, 4, 2, 'Aquisition d\'un point d\'accès 5G', '2021-02-20', 1, 0, 0, 0, NULL, NULL, NULL, NULL, 'DGESS', '10000004', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-06 14:42:06', 'gaetan@gmail.com', '2020-04-22 17:50:07', 0, NULL, 0),
(36, 13, 3, 'Reco pour test 1', '2020-05-20', 2, 0, 1, 1, NULL, NULL, NULL, NULL, 'DGESS', '10101010', NULL, NULL, NULL, NULL, NULL, '2020-04-14 12:24:21', 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-14 09:40:11', 'Administrateur', '2020-04-14 12:24:21', 1, NULL, 0),
(37, 13, 3, 'Reco pour test 2', '2020-05-22', 2, 0, 0, 1, NULL, NULL, NULL, NULL, 'SG', '10101011', NULL, NULL, NULL, NULL, NULL, '2020-04-14 12:24:21', 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-14 09:40:11', 'Administrateur', '2020-04-14 12:24:21', 1, NULL, 0),
(38, 13, 1, 'Reco pour test 3', '2020-05-24', 2, 0, 0, 1, NULL, NULL, NULL, NULL, 'DSI', '10101012', NULL, NULL, NULL, NULL, NULL, '2020-04-14 12:24:21', 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-14 09:40:11', 'Administrateur', '2020-04-14 12:24:21', 0, NULL, 0),
(39, 13, 3, 'Reco pour test 4', '2020-05-26', 2, 0, 0, 1, NULL, NULL, NULL, NULL, 'ONEA', '10101013', NULL, NULL, NULL, NULL, NULL, '2020-04-14 12:24:21', 0, NULL, NULL, NULL, NULL, 'Administrateur', '2020-04-14 09:40:11', 'Administrateur', '2020-04-14 12:24:21', 0, NULL, 0),
(40, 14, 1, 'Test CP 1', '2020-05-08', 4, 0, 0, 1, NULL, NULL, NULL, NULL, 'DSA', '00000001', NULL, NULL, NULL, '2020-04-23 00:00:00', '2020-04-24 00:00:00', '2020-04-20 11:18:49', 100, NULL, NULL, NULL, NULL, 'gaetan@gmail.com', '2020-04-20 10:13:32', 'pelag@gmail.com', '2020-04-24 15:33:59', 1, NULL, 0),
(43, 15, 1, 'Maintenir les équipement informatiques', '2021-04-28', 3, 1, 0, 1, NULL, NULL, NULL, NULL, 'KI', '70220222', '2020-04-28 14:10:44', NULL, '2020-04-28 13:48:40', '2020-04-28 13:54:14', NULL, '2020-04-28 13:47:01', 70, NULL, NULL, NULL, NULL, 'guetikila@gmail.com', '2020-04-28 12:13:08', 'guetikila@gmail.com', '2020-04-28 14:15:02', 1, NULL, 1),
(54, 15, 1, 'Doter les responsables du MEA en ordinateurs portables de marque APPLE', '2020-05-28', 4, 1, 0, 1, NULL, NULL, NULL, NULL, 'Fabrice', '70666666', '2020-04-28 14:10:44', NULL, '2020-04-28 13:48:40', '2020-04-28 13:52:46', NULL, '2020-04-28 13:27:45', 100, NULL, NULL, NULL, NULL, 'gaetan', '2020-04-28 13:06:25', 'guetikila@gmail.com', '2020-04-28 14:10:44', 0, NULL, 0),
(55, 15, 21, 'Mettre en place l\'intranet du MEA', '2020-10-30', 4, 1, 1, 1, NULL, NULL, 'Test', 'Ouf !', 'Lassina', '72000000', '2020-04-28 14:10:44', NULL, '2020-04-28 13:48:40', '2020-04-28 13:55:32', '2020-04-28 13:57:04', '2020-04-28 13:21:44', 100, NULL, NULL, 'RECO_55_3_28_Apr_2020_13_55_32.pdf', NULL, 'gaetan', '2020-04-28 13:06:25', 'guetikila@gmail.com', '2020-04-28 14:10:44', 0, NULL, 0),
(56, 15, 1, 'Entretenir le système électrique', '2021-03-31', 5, 1, 1, 1, NULL, NULL, 'Les TDR sont faits', 'Le système électrique est opérationnel', 'KI', '70022333', '2020-04-28 14:10:44', '2020-04-28 14:19:16', '2020-04-28 13:48:40', '2020-04-28 13:53:35', '2020-04-28 13:55:41', '2020-04-28 13:47:09', 100, 'Activité hors périmètres de la DGEP', NULL, 'RECO_56_3_28_Apr_2020_13_53_11.pdf', 'RECO_56_4_28_Apr_2020_13_55_41.pdf', 'gaetan', '2020-04-28 13:06:25', 'guetikila@gmail.com', '2020-04-28 14:19:16', 1, NULL, 1),
(58, 16, 1, 'Maintenir les équipement informatiques 2', '2021-04-28', 1, 0, 0, 0, NULL, NULL, NULL, NULL, 'KI', '70220222', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'guetikila@gmail.com', '2020-04-28 14:15:02', 'guetikila@gmail.com', '2020-04-28 14:15:02', 1, 43, 0),
(59, 16, 1, 'Entretenir le système électrique 2', '2021-03-31', 1, 0, 1, 0, NULL, NULL, NULL, NULL, 'KI', '70022333', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'guetikila@gmail.com', '2020-04-28 14:15:02', 'guetikila@gmail.com', '2020-04-28 14:15:02', 1, 56, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `guard_name` varchar(255) NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_UN_name` (`name`),
  UNIQUE KEY `roles_UN_label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'Administrateur', 'web', '2020-02-15 20:51:05', '2020-02-15 20:51:05'),
(2, 'CSR', 'Chargé de Suivi des Recommandations', 'web', '2020-02-15 20:58:08', '2020-02-15 20:58:08'),
(3, 'CSS', 'Chargé de Suivi des Sessions', 'web', '2020-02-15 21:07:25', '2020-02-15 21:07:25'),
(4, 'ASR', 'Acteur de Suivi des Recommandations', 'web', '2020-03-31 14:21:04', '2020-03-31 14:21:04'),
(5, 'SUPERADMIN', 'Super administrateur', 'web', '2020-03-31 14:21:04', '2020-03-31 14:21:04');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(47, 1),
(48, 1),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(47, 2),
(48, 2),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(44, 3),
(45, 3),
(47, 3),
(48, 3),
(26, 4),
(27, 4),
(28, 4),
(29, 4),
(30, 4),
(31, 4),
(32, 4),
(33, 4),
(34, 4),
(35, 4),
(36, 4),
(37, 4),
(38, 4),
(39, 4),
(40, 4),
(41, 4),
(42, 4),
(43, 4),
(44, 4),
(45, 4),
(47, 4),
(48, 4),
(26, 5),
(27, 5),
(28, 5),
(29, 5),
(30, 5),
(31, 5),
(32, 5),
(33, 5),
(34, 5),
(35, 5),
(36, 5),
(37, 5),
(38, 5),
(39, 5),
(40, 5),
(41, 5),
(42, 5),
(43, 5),
(44, 5),
(45, 5),
(47, 5),
(48, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `ses_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ins_id` bigint(20) UNSIGNED NOT NULL,
  `ses_description` varchar(255) NOT NULL,
  `ses_annee` int(11) NOT NULL,
  `ses_statut` int(1) NOT NULL DEFAULT '0' COMMENT '1: EN FORMULATION; 2: EN PLANIFICATION; 3: EN REALISATION; 4: SUIVI VALIDER',
  `ses_date_debut` date NOT NULL,
  `ses_date_fin` date DEFAULT NULL,
  `ses_lieu` varchar(255) NOT NULL,
  `ses_id_precedante` bigint(20) UNSIGNED DEFAULT NULL,
  `ses_datefin_planification` date NOT NULL,
  `ses_datefin_realisation` date NOT NULL,
  `ses_liste_participants` varchar(200) DEFAULT NULL,
  `ses_is_planning_valide` smallint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ses_id`),
  UNIQUE KEY `ses_id` (`ses_id`),
  UNIQUE KEY `ses_desc_unique` (`ses_description`),
  KEY `ins_id` (`ins_id`),
  KEY `sessions_precedant_fk` (`ses_id_precedante`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`ses_id`, `ins_id`, `ses_description`, `ses_annee`, `ses_statut`, `ses_date_debut`, `ses_date_fin`, `ses_lieu`, `ses_id_precedante`, `ses_datefin_planification`, `ses_datefin_realisation`, `ses_liste_participants`, `ses_is_planning_valide`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, '1ère session CSD', 2020, 4, '2020-02-21', '2020-07-31', 'Salle de conférence MEA', NULL, '2020-03-31', '2020-04-18', NULL, 0, 'admin', '2020-02-23 07:02:33', 'Administrateur', '2020-03-20 17:10:06'),
(2, 1, '2è session CSD', 2021, 1, '2021-02-21', '2020-03-25', 'Salle de conférence MEA', NULL, '2020-02-23', '2020-04-15', 'CSD_21_02_2021.pdf', 0, 'admin', '2020-02-23 07:02:33', 'gaetan', '2020-04-16 12:56:03'),
(3, 2, '1ère session CASEM', 2020, 4, '2020-02-21', '2020-04-25', 'Salle de conférence MEA', NULL, '2020-02-23', '2020-02-23', NULL, 0, 'admin', '2020-02-23 07:02:33', 'gaetan@gmail.com', '2020-04-23 16:03:07'),
(4, 2, '2è session CASEM', 2021, 2, '2021-02-21', '2020-03-25', 'Salle de conférence MEA', NULL, '2020-02-23', '2020-02-23', NULL, 0, 'admin', '2020-02-23 07:02:33', 'gaetan@gmail.com', '2020-04-23 15:59:09'),
(5, 3, '1ère session CRP', 2020, 2, '2020-03-10', '2020-03-16', 'Salle de conférence MEA', NULL, '2020-04-25', '2020-04-21', 'CRP_10_03_2020.pdf', 0, 'admin', '2020-02-23 07:02:33', 'gaetan@gmail.com', '2020-04-24 15:07:26'),
(13, 1, '3è session CSD', 2022, 3, '2020-05-06', '2020-04-14', 'Salle de conférence MEA', 2, '2020-04-14', '2020-04-14', NULL, 0, 'Administrateur', '2020-04-02 11:53:20', 'Administrateur', '2020-04-14 12:28:22'),
(14, 4, '1ère session CP', 2020, 1, '2020-04-16', '2020-04-17', 'Salle de conférence MEA', NULL, '2020-04-18', '2020-05-19', NULL, 1, 'gaetan@gmail.com', '2020-04-20 10:12:11', 'gaetan@gmail.com', '2020-04-20 11:21:05'),
(15, 5, 'FE session de 2020', 2020, 4, '2020-04-27', '2020-04-27', 'Koudougou', NULL, '2020-04-27', '2020-04-27', 'FE_27_04_2020.pdf', 1, 'guetikila@gmail.com', '2020-04-28 11:36:54', 'guetikila@gmail.com', '2020-04-28 14:10:44'),
(16, 5, 'FE 2eme session de 2020', 2020, 2, '2020-04-26', '2020-04-27', 'Bobo-Dioulasso', 15, '2020-05-01', '2020-05-04', NULL, 0, 'guetikila@gmail.com', '2020-04-28 14:13:06', 'admin@admin.com', '2020-04-30 08:44:27');

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
CREATE TABLE IF NOT EXISTS `structures` (
  `str_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `str_sigle` varchar(20) NOT NULL,
  `str_nom_complet` varchar(255) NOT NULL,
  `str_categorie` bigint(20) UNSIGNED DEFAULT NULL COMMENT '1:Direction centrale; 2:DR; 3:AE; 4:PTF; 5:OSC; 6:Priv?; 7:Collectivit?; 8:Autres',
  `created_by` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`str_id`),
  UNIQUE KEY `str_id` (`str_id`),
  UNIQUE KEY `str_sigle` (`str_sigle`),
  UNIQUE KEY `str_nom_complet` (`str_nom_complet`),
  KEY `structures_categories_FK` (`str_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structures`
--

INSERT INTO `structures` (`str_id`, `str_sigle`, `str_nom_complet`, `str_categorie`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'SG', 'Secrétariat Général', 1, 'admin', '2019-10-08 23:28:59', NULL, NULL),
(2, 'DGESS', 'Direction Générale des Etudes et des Statistiques Sectorielles', 2, 'admin', '2019-10-08 23:28:59', NULL, NULL),
(3, 'DGEP', 'Direction Générale de l\'Eau Potable', 2, 'admin', '2019-10-08 23:28:59', NULL, NULL),
(4, 'DGA', 'Direction Générale de l\'Assainissement', 2, 'admin', '2019-10-08 23:28:59', NULL, NULL),
(5, 'DGIH', 'Direction Générale des Infrastructures Hydrauliques', 2, 'admin', '2019-10-08 23:28:59', NULL, NULL),
(6, 'HBS', 'Hauts-Bassins', 3, 'admin', '2018-10-12 08:42:58', NULL, '2018-10-12 08:42:58'),
(7, 'BMH', 'Boucle du Mouhoun', 3, 'admin', '2018-10-12 08:43:41', NULL, '2018-10-12 08:43:41'),
(8, 'SHL', 'Sahel', 3, 'admin', '2018-10-12 08:44:09', NULL, '2019-02-28 12:34:41'),
(9, 'EST', 'Est', 3, 'admin', '2018-10-12 08:44:28', NULL, '2018-10-12 08:44:28'),
(10, 'SUO', 'Sud-Ouest', 3, 'admin', '2018-10-12 08:44:50', NULL, '2019-02-28 12:34:33'),
(11, 'CNO', 'Centre-Nord', 3, 'admin', '2018-10-12 08:45:08', NULL, '2018-10-12 08:45:08'),
(12, 'COU', 'Centre-Ouest', 3, 'admin', '2018-10-12 08:45:27', NULL, '2018-10-12 08:45:27'),
(13, 'PCE', 'Plateau-Central', 3, 'admin', '2018-10-12 08:45:48', NULL, '2018-10-12 08:45:48'),
(14, 'NOR', 'Nord', 3, 'admin', '2018-10-12 08:46:03', NULL, '2018-10-12 08:46:03'),
(15, 'CES', 'Centre-Est', 3, 'admin', '2018-10-12 08:46:17', NULL, '2018-10-12 08:46:17'),
(16, 'CEN', 'Centre', 3, 'admin', '2018-10-12 08:46:35', NULL, '2018-10-12 08:46:35'),
(17, 'CAS', 'Cascades', 3, 'admin', '2018-10-12 08:46:58', NULL, '2018-10-12 08:46:58'),
(18, 'CSD', 'Centre-Sud', 3, 'admin', '2018-10-12 08:47:16', NULL, '2019-02-28 12:35:13'),
(19, 'ONEA', 'ONEA', 10, 'admin', '2018-10-15 07:20:14', NULL, '2019-01-18 09:22:59'),
(21, 'AEN', 'Agence de l\'Eau du Nakanbé', 4, 'admin', '2019-01-18 09:42:24', NULL, '2019-06-25 15:18:08'),
(22, 'AEC', 'Agence de l\'Eau des Cascades', 4, 'admin', '2019-01-18 09:42:49', NULL, '2019-01-18 09:42:49'),
(23, 'AEL', 'Agence de l\'Eau du Liptako', 4, 'admin', '2019-01-18 09:43:11', NULL, '2019-01-18 09:43:11'),
(24, 'AEG', 'Agence de l\'Eau du Gourma', 4, 'admin', '2019-01-18 09:43:32', NULL, '2019-01-18 09:43:32'),
(25, 'AEM', 'Agence de l\'Eau du Mouhoun', 4, 'admin', '2019-01-18 09:43:58', NULL, '2019-01-18 09:43:58'),
(26, 'DSI', 'Direction des Services Informatiques', 1, 'admin@admin.com', '2020-04-28 10:46:16', NULL, '2020-04-28 10:46:16');

-- --------------------------------------------------------

--
-- Table structure for table `templatemsgs`
--

DROP TABLE IF EXISTS `templatemsgs`;
CREATE TABLE IF NOT EXISTS `templatemsgs` (
  `tmsg_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tmsg_type` varchar(30) NOT NULL COMMENT 'PREPARATION;SUIVI;AUTRE',
  `tmsg_msg` text NOT NULL,
  `tmsg_periodicite` varchar(50) NOT NULL COMMENT 'MENSUELLE; TRIMESTRIELLE;SEMESTRIELLE;ANNUELLE',
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`tmsg_id`),
  UNIQUE KEY `tmsg_id` (`tmsg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templatemsgs`
--

INSERT INTO `templatemsgs` (`tmsg_id`, `tmsg_type`, `tmsg_msg`, `tmsg_periodicite`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'PLANIFICATION', 'Bonjour [USER_NOM] [USER_PRENOM]! Votre structure a des recommandations issues de la \"[DESCRITION_SESSION]\" qui a eu lieu du [DATE_DEBUT_SESSION] au [DATE_FIN_SESSION] à [LIEU_SESSION]. Vous êtes désignés pour la planification et la réalisation de ces recommandations.\r\nLa phase de planification prend fin au plus tard le [DATE_FIN_PLANIFICATION].\r\nCi-dessous la liste des recommandations en attentes de planification :\r\n [LISTE_RECO]\r\nMerci de prendre toutes les dispositions pour le respect de cette date qui sera de rigueure.', 'Hebdomadaire', '', '2020-04-29 16:08:53', NULL, '2020-04-29 16:29:39'),
(2, 'SUIVI', 'Bonjour  [USER_NOM_PRENOM]! Votre structure a des recommandations issues de la [DESCRITION_SESSION] qui a eu lieu du [DATE_DEBUT_SESSION] au  [DATE_FIN_SESSION] à  [LIEU_SESSION]. Vous êtes désignés pour la planification et la réalisation de ces recommandations.\r\nLa phase de réalisation prend fin au plus tard le [DATE_FIN_SUIVI].\r\nCi-dessous la liste des recommandations en attentes de réalisation :\r\n [LISTE_RECO]\r\n\r\nMerci de prendre toutes les dispositions pour le respect de cette date qui sera de rigueure.', 'Mensuelle', '', '2020-04-29 16:17:53', NULL, '2020-04-29 16:28:50'),
(3, 'AUTRE', 'Bonjour [USER_NOM] [USER_PRENOM]! Juste vous informer que  [DESCRITION_SESSION] est reporté(e) pour se tenir du [DATE_DEBUT_SESSION] au [DATE_FIN_SESSION] à  [LIEU_SESSION].\r\nTrès cordialement!', 'Aucune', '', '2020-04-29 16:33:25', NULL, '2020-04-29 16:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `str_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `firstconnect` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `sexe` varchar(10) NOT NULL DEFAULT 'M',
  `fonction` varchar(100) DEFAULT NULL,
  `matricule` varchar(10) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `lastconnect` timestamp NULL DEFAULT NULL,
  `fails_connect` int(11) DEFAULT '0',
  `is_blocked` tinyint(1) DEFAULT '0',
  `current_connect` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_UN_email` (`email`),
  UNIQUE KEY `matricule` (`matricule`),
  KEY `structures_fk1` (`id`),
  KEY `users_structures_FK` (`str_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `str_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `firstconnect`, `is_active`, `nom`, `prenom`, `sexe`, `fonction`, `matricule`, `telephone`, `lastconnect`, `fails_connect`, `is_blocked`, `current_connect`) VALUES
(1, 1, 'Administrateur', 'admin@admin.com', NULL, '$2y$10$NGQG6LJl9CeUDvuLeoEbbuhjaW6.BwqZZd0Oof1izyOrZ3Y6DqUJa', NULL, '2020-02-13 00:15:16', '2020-05-01 14:33:19', 0, 1, 'Super', 'Admin', 'M', NULL, NULL, NULL, '2020-05-01 11:27:11', 0, 0, '2020-05-01 14:33:19'),
(2, 2, 'isidore', 'isidore@gmail.com', '2020-02-13 00:00:00', '$2y$10$SI4XGec2iifb1744.v75V.4IV7478AabpgRHVC.O40VB0o5RDCxgK', NULL, '2020-02-15 13:42:32', '2020-04-28 10:29:31', 0, 1, 'isidore', 'isidore', 'M', NULL, NULL, NULL, '2020-04-28 11:11:16', 0, 0, '2020-04-28 10:29:31'),
(4, 4, 'souley', 'alassane@asso.com', NULL, '$2y$10$Hd4A9vhTzOq9aepXc3hAsuu/Pg5oV7vGf0RPcmEcHTRfaPQ9Pm8ye', NULL, '2020-02-15 14:20:00', '2020-02-29 13:34:45', 1, 1, 'souley', 'souley', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(5, 26, 'barbe', 'barbe@yahoo.fr', NULL, '$2y$10$.RTlp.3.E3rvREfwkjlteuUKj5s/CZAw0MPwY7krzzWvF9T6Md3He', NULL, '2020-02-15 14:43:46', '2020-04-28 13:50:12', 0, 1, 'barbe', 'barbe', 'M', 'IT', 'BB', '700000000', '2020-04-28 14:09:42', 0, 0, '2020-04-28 13:50:12'),
(37, 4, 'pelag', 'pelag@gmail.com', NULL, '$2y$10$ESYXBpHtaSMeId5HboNrTuR0xEB.pOQTkNB5g3x0NB9f9SKKtDhsC', NULL, '2020-02-28 23:48:57', '2020-04-28 09:34:29', 1, 1, 'SANKARA', 'Pelagie', 'M', NULL, NULL, NULL, '2020-04-27 20:39:53', 0, 0, '2020-04-27 20:38:44'),
(38, 7, 'sitadeni', 'sitadeni@gmail.com', NULL, '$2y$10$jpbGZZbiix59hU5ogiRu9.rq7Lhzc/MTqN2KfYAOmv3W7Yl9ND6QO', NULL, '2020-02-29 00:18:08', '2020-02-29 00:18:08', 1, 1, 'sitadeni', 'sitadeni', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(40, 8, 'imma', 'sigat@eaubf.net', NULL, '$2y$10$VpypaPcKotO6.v1BdlUcOOiWDAC/mYs/mRZ6hb.bPWSu7tDk59x0y', NULL, '2020-02-29 21:20:24', '2020-04-28 08:06:09', 1, 1, 'imma', 'imma', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(41, 9, 'yameogo', 'yameogo@yahoo.fr', NULL, '$2y$10$kw6vUjowQ3Gp1tFDkDhaIekPDzb.B6Hy9Brsuq.DQUWP6HW5/PI3.', NULL, '2020-02-29 21:43:30', '2020-02-29 22:56:54', 1, 1, 'yameogo', 'yameogo', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(42, 10, 'Rolande', 'rolande@rolande.com', NULL, '$2y$10$7KOFxOsVNV6UaRVQR69ZUedYgGzJaRsPKSM7DD8iNKTCi1BLfqoOW', NULL, '2020-02-29 21:47:15', '2020-03-04 16:42:34', 1, 1, 'SANOU', 'Rolande', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(43, 3, 'gaetan', 'kizitoouedraogo12@gmail.com', NULL, '$2y$10$GnU8D/Oyn.tCAylZal7APOsMafgpbKS0P2p1oX9jZQmZ795KGN/2q', NULL, '2020-03-04 11:15:35', '2020-04-30 20:19:02', 0, 1, 'BAMBARA', 'Gaetan', 'M', NULL, NULL, NULL, '2020-04-29 08:14:12', 0, 0, '2020-04-30 20:19:02'),
(44, 12, 'atata', 'atata@gmail.com', NULL, '$2y$10$UEM2cmCtlZ6fAT3L9PtZUO2EIrOE7fRKDKH3TzPV0TXLhyZY9ESNO', NULL, '2020-03-08 13:27:33', '2020-03-08 14:45:23', 1, 1, 'atata', 'atata', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(45, 13, 'ketanti', 'ketandi@gmail.com', NULL, '$2y$10$bSZVJ88YOAHjFPWw0qoL5.ToZFp8xHvCXd9FuZhRE61WvUQmTSD/K', NULL, '2020-03-08 16:44:12', '2020-03-08 16:44:12', 1, 1, 'ketanti', 'ketanti', 'M', NULL, NULL, NULL, NULL, 0, 0, NULL),
(46, 2, NULL, 'css@gmail.com', NULL, '$2y$10$jZOtzT0.UN1r2Frhqy.b5egfQQOTM4Aldg1TO/5uO9Ej/YDzclk2.', NULL, '2020-04-27 22:47:38', '2020-04-27 22:47:38', 1, 1, 'KONATE', 'Issouf', 'M', 'Professeur', NULL, NULL, NULL, 0, 0, NULL),
(47, 2, NULL, 'guetikila@gmail.com', NULL, '$2y$10$8h/igtb.Loa9puAJv.NMzOGLvRu4.gKseCntRPZrzPgsdT0JWxfpC', NULL, '2020-04-28 10:39:59', '2020-04-28 14:09:50', 0, 1, 'GUETIKILA', 'Daouda', 'M', 'Chargé de projet', 'DG12345', '70000000', '2020-04-28 16:59:26', 0, 0, '2020-04-28 14:09:50'),
(48, 26, NULL, 'brigitteouiya@gmail.com', NULL, '$2y$10$DgUBE8V0OFSuSlCOd6auY.woN9FNtdrnmvtTWlCqWs6s4.bkoFQzq', NULL, '2020-04-28 10:42:39', '2020-04-28 10:48:44', 1, 1, 'OUIYA', 'Brigitte', 'F', 'Chef de Service', '266141M', '71949748', NULL, 0, 0, NULL),
(49, 3, NULL, 'lessilassina@gmail.com', NULL, '$2y$10$7c74of79DSOe5cz9/6DY4OPiKYebR9L2i7BonTCfw/STMGGJTkIgy', NULL, '2020-04-28 10:44:42', '2020-04-28 13:40:32', 0, 1, 'COULIBALY', 'Lessi Lassina', 'M', 'IT', NULL, NULL, '2020-04-28 15:37:08', 0, 0, '2020-04-28 13:40:32'),
(50, 26, NULL, 'dgm@gmail.com', NULL, '$2y$10$TyJKRX6RHqha/VXJx6O8xuBGPJH65.M1U7vlFuq9JrUP2Gmif6Z8W', NULL, '2020-04-28 10:47:56', '2020-04-28 13:40:39', 1, 1, 'dieni', 'martin', 'M', 'IT', '4216', '75555544', NULL, 0, 0, NULL),
(51, 3, 'Claver', 'michaelclaver14@gmail.com', NULL, '$2y$10$vxqs2eVv0MrJgpkJCCM/nu0Vhx94Jcbh5BNkGVbY9718cTeTmFsci', NULL, '2020-04-28 10:48:27', '2020-04-28 13:19:48', 0, 1, 'KI', 'L. Michaël Claver', 'M', 'Informaticien', NULL, '+22671030490', '2020-04-28 11:19:11', 0, 0, '2020-04-28 13:19:48'),
(52, 26, NULL, 'compafab16@gmail.com', NULL, '$2y$10$64UCUCC/CEViM.nGv7vAWesfRtHy8wCys0INcock0i/NlfBK5.r9O', NULL, '2020-04-28 10:48:36', '2020-04-28 10:57:11', 1, 1, 'COMPAORE', 'Fabrice', 'M', 'Informaticien', 'CF123654', '0022676102516', NULL, 0, 0, NULL),
(53, 3, NULL, 'nikiemacamille2015@gmail.com', NULL, '$2y$10$.EWPlOLbModMXKv2xz7bGeoER2lafDIwsRBcv.gKWgA8kBIGezHgi', NULL, '2020-04-28 10:48:40', '2020-04-28 13:48:34', 0, 1, 'NIKIEMA', 'Teddy Camille', 'M', 'Informaticien-Développeur', 'DSI-1708', NULL, '2020-04-28 13:50:57', 0, 0, '2020-04-28 13:48:34'),
(54, 26, NULL, 'philippe.zabsonre@gmail.com', NULL, '$2y$10$WZBiCDqooKcw.Q6njVMscOQrLYCtN4o70tYS95xQsMty17VDRsN/.', NULL, '2020-04-28 11:01:18', '2020-04-28 11:04:33', 0, 1, 'ZABSONRE', 'Philippe', 'M', 'Informaticien', NULL, '+22670038230', '2020-04-28 11:03:31', 0, 0, '2020-04-28 11:04:33');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acteurs`
--
ALTER TABLE `acteurs`
  ADD CONSTRAINT `acteurs_instances_fk` FOREIGN KEY (`ins_id`) REFERENCES `instances` (`ins_id`),
  ADD CONSTRAINT `acteurs_structures_fk` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`);

--
-- Constraints for table `activites`
--
ALTER TABLE `activites`
  ADD CONSTRAINT `activites_ibfk_1` FOREIGN KEY (`rec_id`) REFERENCES `recommandations` (`rec_id`),
  ADD CONSTRAINT `activites_ibfk_2` FOREIGN KEY (`ptn_id`) REFERENCES `partenaires` (`ptn_id`);

--
-- Constraints for table `alertes`
--
ALTER TABLE `alertes`
  ADD CONSTRAINT `alertes_ibfk_1` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`),
  ADD CONSTRAINT `alertes_ibfk_2` FOREIGN KEY (`tmsg_id`) REFERENCES `templatemsgs` (`tmsg_id`);

--
-- Constraints for table `envoialertes`
--
ALTER TABLE `envoialertes`
  ADD CONSTRAINT `envoialertes_ibfk_2` FOREIGN KEY (`ale_id`) REFERENCES `alertes` (`ale_id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `partenaires`
--
ALTER TABLE `partenaires`
  ADD CONSTRAINT `partenaires_ibfk_1` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`),
  ADD CONSTRAINT `partenaires_ibfk_2` FOREIGN KEY (`rec_id`) REFERENCES `recommandations` (`rec_id`);

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`),
  ADD CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`);

--
-- Constraints for table `recommandations`
--
ALTER TABLE `recommandations`
  ADD CONSTRAINT `recommandations_ibfk_1` FOREIGN KEY (`ses_id`) REFERENCES `sessions` (`ses_id`),
  ADD CONSTRAINT `recommandations_reconduit_fk` FOREIGN KEY (`rec_id_ref`) REFERENCES `recommandations` (`rec_id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`ins_id`) REFERENCES `instances` (`ins_id`),
  ADD CONSTRAINT `sessions_precedant_fk` FOREIGN KEY (`ses_id_precedante`) REFERENCES `sessions` (`ses_id`);

--
-- Constraints for table `structures`
--
ALTER TABLE `structures`
  ADD CONSTRAINT `structures_categories_FK` FOREIGN KEY (`str_categorie`) REFERENCES `categories` (`cat_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_structures_FK` FOREIGN KEY (`str_id`) REFERENCES `structures` (`str_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
