php artisan infyom:api_scaffold Acteur --fromTable --tableName=ACTEURS --primary=act_id

php artisan infyom:api_scaffold Instance --fromTable --tableName=INSTANCES --primary=ins_id

php artisan infyom:api_scaffold Structure --fromTable --tableName=structures --primary=str_id

php artisan infyom:api_scaffold Session --fromTable --tableName=sessions --primary=ses_id

php artisan infyom:api_scaffold Templatemsgs --fromTable --tableName=templatemsgs --primary=tmsg_id

php artisan infyom:api_scaffold Alertes --fromTable --tableName=alertes --primary=ale_id

php artisan infyom:api_scaffold Participants --fromTable --tableName=participants --primary=par_id

php artisan infyom:api_scaffold Recommandations --fromTable --tableName=recommandations --primary=rec_id

php artisan infyom:api_scaffold Activites --fromTable --tableName=activites --primary=act_id

php artisan infyom:api_scaffold Partenaires --fromTable --tableName=partenaires --primary=ptn_id

php artisan infyom:api_scaffold Agents --fromTable --tableName=agents --primary=age_id

php artisan infyom:api_scaffold ListeAlertes --fromTable --tableName=liste_alertes --primary=lia_id

php artisan infyom:api_scaffold EnvoiAlertes --fromTable --tableName=envoialertes --primary=ena_id

php artisan infyom:api_scaffold Categories --fromTable --tableName=categories --primary=cat_id

php artisan infyom:api_scaffold ProgrammePbudgetaires --fromTable --tableName=programme_budgetaire --primary=bud_id
