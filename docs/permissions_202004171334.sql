﻿INSERT INTO permissions (id,name,label,guard_name,created_at,updated_at) VALUES 
(26,'GERER_STRUCTURE','Peut ajouter, modifier ou supprimer une structure','web',NULL,NULL)
,(27,'GERER_COMPTE','Peut créer un compte pour un utilisateur','web',NULL,NULL)
,(28,'GERER_PROFIL','Peut ajouter, modifier ou supprimer des profils d''utilisateurs','web',NULL,NULL)
,(29,'GERER_INSTANCE','Peut ajouter, modifier ou supprimer une instance','web',NULL,NULL)
,(30,'GERER_SESSION','Peut accéder aux sessions','web',NULL,NULL)
,(31,'GERER_RECOMMANDATION','Peut accéder aux recommandations','web',NULL,NULL)
,(32,'GERER_ALERTE','Peut gérer les alertes, notifications et modèles de message','web',NULL,NULL)
,(33,'GERER_PROGRAMME_BUDGETAIRE','Peut ajouter, modifier ou supprimer les programmes budgétaires','web',NULL,NULL)
,(34,'GERER_CATEGORIE_STRUCTURE','Peut gérer les différentes catégories de structure','web',NULL,NULL)
,(35,'GERER_ACTEUR_INSTANCE','Peut affecter des acteurs à une instance','web',NULL,NULL)
;
INSERT INTO permissions (id,name,label,guard_name,created_at,updated_at) VALUES 
(36,'GERER_ROLE_ACTEUR','Peut affecter ou retirer les rôles CSS ou CSR à un acteur d''une instance','web',NULL,NULL)
,(37,'VALIDER_FORMULATION','Peut valider la formulation des recommandations d''une session','web',NULL,NULL)
,(38,'VALIDER_PLANIFICATION','Peut valider la planification des recommandations d''une session','web',NULL,NULL)
,(39,'VALIDER_REALISATION','Peut valider le suivi des recommandations d''une session','web',NULL,NULL)
,(40,'RECONDUIRE_RECOMMANDATION','Peut réconduire les recommandations de la session précedente non encore réalisées, non abandonné ou permanent','web',NULL,NULL)
,(41,'ABANDONNER_RECOMMANDATION','Peut abandonner une recommandation','web',NULL,NULL)
,(42,'SAISIE_RECOMMANDATION','Peut ajouter, modifier ou supprimer une recommandation','web',NULL,NULL)
,(43,'PLANIFIER_RECOMMANDATION','Peut planifier les activités d''une recommandation','web',NULL,NULL)
,(44,'SUIVI_RECOMMANDATION','Peut assurer le suivi des activités d''une récommandation','web',NULL,NULL)
,(45,'CONSULTER_RAPPORT','Peut consulter les rapports de mise en oeuvre des recommandations','web',NULL,NULL)
;
INSERT INTO permissions (id,name,label,guard_name,created_at,updated_at) VALUES 
(47,'CONSULTER_STATS','Peut consulter les statistiques de mise en oeuvre des recommandations','web',NULL,NULL)
,(48,'VALIDER_PLANIFICATION_RECO','Peut valider la planification d''une recommandation','web',NULL,NULL)
;