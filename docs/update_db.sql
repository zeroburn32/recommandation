ALTER TABLE recommandations.users ADD current_connect TIMESTAMP NULL;

/**************28/02/2020*****************************/
ALTER TABLE recommandations.users MODIFY COLUMN `structure` BIGINT NULL;
ALTER TABLE recommandations.structures ENGINE=InnoDB;
ALTER TABLE recommandations.users ADD CONSTRAINT users_structures_FK FOREIGN KEY (`structure`) REFERENCES recommandations.structures(str_id);

-- ************************** 02/03/2020 *************************************
ALTER TABLE recommandations.acteurs ADD act_is_acteur_suivi INT DEFAULT 0 NOT NULL;
ALTER TABLE recommandations.acteurs ADD created_by varchar(100) NOT NULL;
ALTER TABLE recommandations.acteurs ADD created_at DATETIME NOT NULL;
ALTER TABLE recommandations.acteurs ADD updated_by varchar(100) NULL;
ALTER TABLE recommandations.acteurs ADD updated_at DATETIME NULL;
ALTER TABLE recommandations.acteurs ENGINE=InnoDB;
ALTER TABLE recommandations.structures ENGINE=InnoDB;
ALTER TABLE recommandations.instances ENGINE=InnoDB;
ALTER TABLE recommandations.acteurs ADD CONSTRAINT acteurs_structures_fk FOREIGN KEY (str_id) REFERENCES recommandations.structures(str_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE recommandations.acteurs ADD CONSTRAINT acteurs_instances_fk FOREIGN KEY (ins_id) REFERENCES recommandations.instances(ins_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE recommandations.sessions ADD ses_statut INT DEFAULT 0 NOT NULL COMMENT '0: EN PREPARATION; 1: EN COURS; 2: TERMINER';

-- ************************** 04/03/2020 *************************************
ALTER TABLE recommandations.activites ADD act_observation varchar(255) NULL;
ALTER TABLE recommandations.recommandations ADD rec_is_permanente SMALLINT DEFAULT 0 NOT NULL;

-- ************************** 06/03/2020 *************************************
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_statut smallint NOT NULL COMMENT '1:Proposition;2:Non réalisé;3:En cours; 4:Réalisé;5:Abandonné';
ALTER TABLE recommandations.recommandations DROP COLUMN rec_etat;

/***********************************07/03/2020********************************/
CREATE TABLE agents LIKE users;
INSERT agents SELECT * FROM users;
ALTER TABLE recommandations.agents MODIFY COLUMN name varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE recommandations.agents DROP COLUMN email_verified_at;
ALTER TABLE recommandations.agents DROP COLUMN password;
ALTER TABLE recommandations.agents DROP COLUMN remember_token;
ALTER TABLE recommandations.agents DROP COLUMN firstconnect;
ALTER TABLE recommandations.agents DROP COLUMN lastconnect;
ALTER TABLE recommandations.agents DROP COLUMN fails_connect;
ALTER TABLE recommandations.agents DROP COLUMN is_blocked;
ALTER TABLE recommandations.agents DROP COLUMN current_connect;
ALTER TABLE recommandations.agents DROP COLUMN is_active;
ALTER TABLE recommandations.agents ADD created_by varchar(100) NULL;
ALTER TABLE recommandations.agents ADD updated_by varchar(100) NULL;
ALTER TABLE recommandations.agents MODIFY COLUMN nom_prenom varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
ALTER TABLE recommandations.agents MODIFY COLUMN `structure` bigint(20) unsigned NOT NULL;
ALTER TABLE recommandations.agents ADD CONSTRAINT agents_structures_FK FOREIGN KEY (`structure`) REFERENCES recommandations.structures(str_id);
ALTER TABLE recommandations.agents CHANGE id age_id bigint(20) unsigned auto_increment NOT NULL;

/****************************08/03/2020*************************************/
ALTER TABLE recommandations.users MODIFY COLUMN name varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE recommandations.users MODIFY COLUMN id BIGINT UNSIGNED auto_increment NOT NULL;
ALTER TABLE `users` ADD `age_id` INT NOT NULL AFTER `id`;
ALTER TABLE recommandations.users MODIFY COLUMN age_id BIGINT UNSIGNED NOT NULL;
ALTER TABLE recommandations.users DROP COLUMN nom_prenom;
ALTER TABLE recommandations.users DROP COLUMN matricule;
ALTER TABLE recommandations.users DROP COLUMN telephone;
ALTER TABLE recommandations.users DROP FOREIGN KEY users_structures_FK;

ALTER TABLE recommandations.users DROP COLUMN `structure`;
ALTER TABLE recommandations.users ADD CONSTRAINT users_agents_FK FOREIGN KEY (age_id) REFERENCES recommandations.agents(age_id);

-- ************************************ 10/03/2020********************************
ALTER TABLE recommandations.sessions ADD ses_id_precedante BIGINT UNSIGNED NULL;
ALTER TABLE recommandations.sessions ADD CONSTRAINT sessions_precedant_fk FOREIGN KEY (ses_id_precedante) REFERENCES recommandations.sessions(ses_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE recommandations.recommandations ADD rec_id_ref BIGINT UNSIGNED NULL;
ALTER TABLE recommandations.recommandations ADD CONSTRAINT recommandations_reconduit_fk FOREIGN KEY (rec_id_ref) REFERENCES recommandations.recommandations(rec_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE recommandations.recommandations ADD rec_is_reconduit SMALLINT DEFAULT 0 NOT NULL;

-- ************************************ 20/03/2020********************************
CREATE TABLE recommandations.ligne_budgetaire
(
  bud_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  bud_code VARCHAR(10) NOT NULL,
  bud_intitule VARCHAR(150) NOT NULL,
  bud_annee SMALLINT
);
CREATE UNIQUE INDEX ligne_budgetaire_bud_code_uindex ON recommandations.ligne_budgetaire (bud_code);

ALTER TABLE recommandations.instances ADD bud_id BIGINT NOT NULL AFTER ins_id;

ALTER TABLE recommandations.sessions MODIFY COLUMN ses_statut int(1) DEFAULT 0 NOT NULL COMMENT '1: EN FORMULATION; 2: EN PLANIFICATION; 3: EN REALISATION; 4: SUIVI VALIDER';
ALTER TABLE recommandations.sessions CHANGE ses_statut ses_statut int(1) DEFAULT 0 NOT NULL COMMENT '1: EN FORMULATION; 2: EN PLANIFICATION; 3: EN REALISATION; 4: SUIVI VALIDER' AFTER ses_annee;
ALTER TABLE recommandations.sessions CHANGE ses_date_fin ses_date_fin date NULL AFTER ses_date_debut;
ALTER TABLE recommandations.sessions CHANGE ses_id_precedante ses_id_precedante bigint(20) unsigned NULL AFTER ses_lieu;
ALTER TABLE recommandations.sessions ADD ses_datefin_planification DATE NOT NULL;
ALTER TABLE recommandations.sessions CHANGE ses_datefin_planification ses_datefin_planification DATE NOT NULL AFTER ses_id_precedante;
ALTER TABLE recommandations.sessions ADD ses_datefin_realisation DATE NOT NULL;
ALTER TABLE recommandations.sessions CHANGE ses_datefin_realisation ses_datefin_realisation DATE NOT NULL AFTER ses_datefin_planification;
ALTER TABLE recommandations.sessions ADD ses_liste_participants varchar(200) NULL;
ALTER TABLE recommandations.sessions CHANGE ses_liste_participants ses_liste_participants varchar(200) NULL AFTER ses_datefin_realisation;

-- ************************************ 24/03/2020********************************
ALTER TABLE recommandations.instances ADD CONSTRAINT instances_unique_nom_complet UNIQUE KEY (ins_nom_complet);
ALTER TABLE recommandations.sessions ADD CONSTRAINT ses_desc_unique UNIQUE KEY (ses_description);

-- ************************************ 25/03/2020********************************
ALTER TABLE recommandations.acteurs ADD act_is_css SMALLINT(1) DEFAULT 0 NOT NULL AFTER str_id;
ALTER TABLE recommandations.acteurs ADD act_is_csr SMALLINT(1) DEFAULT 0 NOT NULL AFTER act_is_css;
ALTER TABLE recommandations.acteurs ADD act_is_asr SMALLINT(1) DEFAULT 0 NOT NULL AFTER act_is_csr;
ALTER TABLE recommandations.acteurs DROP COLUMN act_is_acteur_suivi;
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_statut smallint(1) DEFAULT 1 NOT NULL COMMENT '1:Préparation;2:Non réalisé;3:En cours; 4:Réalisé;5:Abandonné';
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_is_mode_standard smallint(6) NULL COMMENT '1: suivi par statut; 0: suivi par taux';
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_intitule varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
CREATE UNIQUE INDEX recommandations_rec_intitule_uindex ON recommandations.recommandations (rec_intitule);
ALTER TABLE recommandations.activites MODIFY COLUMN act_description VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE recommandations.activites ADD CONSTRAINT act_description_unique UNIQUE KEY (act_description);

-- ************************************ 26/03/2020********************************
ALTER TABLE recommandations.recommandations ADD rec_motif_abandon VARCHAR(255) NULL AFTER rec_taux_realise;

-- *********************************31/03/2020******************************************
ALTER TABLE recommandations.agents ADD CONSTRAINT agents_UN_nom_prenom UNIQUE KEY (nom_prenom);
ALTER TABLE recommandations.roles ADD CONSTRAINT roles_UN_name UNIQUE KEY (name);
ALTER TABLE recommandations.roles ADD CONSTRAINT roles_UN_label UNIQUE KEY (label);
ALTER TABLE recommandations.model_has_roles DROP FOREIGN KEY model_has_roles_role_id_foreign;
ALTER TABLE recommandations.model_has_roles ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES recommandations.roles(id) ON DELETE RESTRICT;
ALTER TABLE recommandations.permissions ADD CONSTRAINT permissions_UN_name UNIQUE KEY (name);

-- ************************************ 01/04/2020********************************
ALTER TABLE recommandations.recommandations ADD rec_is_planning_valide SMALLINT(1) DEFAULT 0 NOT NULL AFTER rec_is_mode_standard;
RENAME TABLE recommandations.ligne_budgetaire TO recommandations.programme_budgetaire;
ALTER TABLE recommandations.liste_alertes DROP FOREIGN KEY liste_alertes_users_FK;
ALTER TABLE recommandations.liste_alertes CHANGE use_id age_id bigint(20) unsigned NOT NULL;
ALTER TABLE recommandations.liste_alertes ADD CONSTRAINT liste_alertes_fk FOREIGN KEY (age_id) REFERENCES recommandations.agents(age_id);
ALTER TABLE recommandations.liste_alertes MODIFY COLUMN lia_sexe char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE recommandations.liste_alertes CHANGE lia_prenom lia_nom_prenom varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE recommandations.liste_alertes DROP COLUMN lia_nom;

-- ************************************ 02/04/2020********************************
ALTER TABLE recommandations.recommandations DROP KEY recommandations_rec_intitule_uindex;

ALTER TABLE recommandations.users DROP KEY users_email_unique;
ALTER TABLE recommandations.users ADD CONSTRAINT users_UN_email_agent UNIQUE KEY (age_id,email);
ALTER TABLE recommandations.agents DROP KEY agents_UN_nom_prenom;
ALTER TABLE recommandations.agents ADD CONSTRAINT agents_UN UNIQUE KEY (`structure`,nom_prenom);

ALTER TABLE `users` ADD `str_id` BIGINT(20) NULL AFTER `age_id`;
ALTER TABLE recommandations.users MODIFY COLUMN str_id BIGINT UNSIGNED NULL;
ALTER TABLE recommandations.users ADD CONSTRAINT users_structures_FK FOREIGN KEY (str_id) REFERENCES recommandations.structures(str_id);
ALTER TABLE recommandations.users MODIFY COLUMN str_id bigint(20) unsigned NOT NULL;

-- ************************************ 07/04/2020********************************
ALTER TABLE recommandations.recommandations ADD rec_dh_planning_valide DATETIME NULL AFTER rec_dh_abandon;

-- ************************************ 08/04/2020********************************
ALTER TABLE recommandations.envoialertes CHANGE ena_isl_u ena_is_lue smallint(6) DEFAULT 0 NOT NULL;

-- ************************************ 09/04/2020********************************
CREATE TABLE recommandations.notifications (
  `id` char(36) NOT NULL,
  `type` varchar(255) NOT NULL,
  `notifiable_type` varchar(255) NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB;

-- ************************************ 14/04/2020********************************
ALTER TABLE recommandations.activites DROP KEY act_id;

-- ************************************ 15/04/2020********************************
ALTER TABLE recommandations.categories ADD cat_is_ministere SMALLINT(1) DEFAULT 0 NOT null after cat_libelle;

-- ************************************ 16/04/2020********************************
UPDATE recommandations.recommandations SET rec_taux_realise = 0 WHERE rec_taux_realise=null;
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_taux_realise smallint(6) DEFAULT 0 NOT NULL;
ALTER TABLE recommandations.liste_alertes DROP FOREIGN KEY liste_alertes_fk;
ALTER TABLE recommandations.liste_alertes CHANGE age_id use_id bigint(20) unsigned NOT NULL;
ALTER TABLE recommandations.liste_alertes ADD CONSTRAINT liste_alertes_fk FOREIGN KEY (use_id) REFERENCES recommandations.users(id);

-- ************************************ 17/04/2020********************************
ALTER TABLE recommandations.envoialertes MODIFY COLUMN ena_msg TEXT;
ALTER TABLE recommandations.alertes MODIFY COLUMN ale_msg TEXT;
ALTER TABLE recommandations.programme_budgetaire ADD created_at DATETIME NULL;
ALTER TABLE recommandations.programme_budgetaire ADD updated_at DATETIME NULL;
ALTER TABLE recommandations.users ADD matricule VARCHAR(10) NULL AFTER fonction;
ALTER TABLE recommandations.users ADD telephone VARCHAR(20) NULL AFTER matricule;

-- ************************************ 17/04/2020********************************
ALTER TABLE recommandations.sessions ADD ses_is_planning_valide smallint(1) DEFAULT 0 NOT NULL AFTER ses_liste_participants;

-- ************************************ 21/04/2020********************************
INSERT INTO recommandations.alertes
(ale_id, ses_id, tmsg_id, ale_msg, ale_date_heure_prev, ale_etat, ale_tmsg_type, ale_date_heure_envoi, created_by, created_at, updated_by, updated_at)
VALUES(4, 2, 1, '[SESSION_DESCRITION] [SESSION_ANNEE] [SESSION_DATE_DEBUT] [LISTE_RECOMMANDATION]', '2018-09-06 17:27:20.0', 0, 'PREPARATION', '2018-06-06 17:27:20.0', '', '2020-04-18 17:27:20.0', NULL, '2020-04-20 15:33:22.0');

INSERT INTO recommandations.alertes
(ale_id, ses_id, tmsg_id, ale_msg, ale_date_heure_prev, ale_etat, ale_tmsg_type, ale_date_heure_envoi, created_by, created_at, updated_by, updated_at)
VALUES(8, 5, 2, '[SESSION_DESCRITION] ', '2020-04-30 16:42:53.0', 0, 'SUIVI', NULL, '', '2020-04-20 16:40:37.0', NULL, '2020-04-20 16:42:53.0');

INSERT INTO recommandations.liste_alertes
(lia_id, ses_id, use_id, str_id, lia_nom_prenom, lia_email, lia_sexe, lia_fonction, lia_matricule, created_by, created_at, updated_by, updated_at)
VALUES(19, 2, 44, 1, 'Felix Ouedraogo', 'kizitoouedraogo12@gmail.com', '1', NULL, NULL, 'Administrateur', '2020-04-14 12:28:22.0', 'Administrateur', '2020-04-14 12:28:22.0');

-- ************************************ 22/04/2020********************************
ALTER TABLE recommandations.instances ADD ins_ref_arrete VARCHAR(30) NULL AFTER ins_description;
ALTER TABLE recommandations.recommandations ADD bud_id BIGINT(20) NULL AFTER ses_id;
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_is_mode_standard smallint(6) DEFAULT 0 NULL COMMENT '1: suivi par statut; 0: suivi par taux';
ALTER TABLE recommandations.instances DROP COLUMN bud_id;
-- Après mise a jour de la colonne bud_id
ALTER TABLE recommandations.recommandations MODIFY COLUMN bud_id bigint(20) NOT NULL;
-- ALTER TABLE recommandations.recommandations ADD CONSTRAINT recommandations_bud_id_fk FOREIGN KEY (bud_id) REFERENCES recommandations.programme_budgetaire(bud_id);

-- ************************************ 24/04/2020********************************
ALTER TABLE recommandations.programme_budgetaire DROP COLUMN bud_annee;

-- ************************************ 27/04/2020********************************
ALTER TABLE recommandations.recommandations ADD rec_preuve_nonrealise varchar(255) NULL AFTER rec_motif_abandon;
ALTER TABLE recommandations.recommandations ADD rec_preuve_encours varchar(255) NULL AFTER rec_preuve_nonrealise;
ALTER TABLE recommandations.recommandations ADD rec_preuve_realise varchar(255) NULL AFTER rec_preuve_encours;
ALTER TABLE recommandations.recommandations ADD rec_dh_nonrealise DATETIME NULL AFTER rec_dh_abandon;

ALTER TABLE recommandations.recommandations ADD rec_obs_nonrealise varchar(1024) NULL AFTER rec_observation;
ALTER TABLE recommandations.recommandations ADD rec_obs_encours varchar(1024) NULL AFTER rec_obs_nonrealise;
ALTER TABLE recommandations.recommandations ADD rec_obs_realise varchar(1024) NULL AFTER rec_obs_encours;

ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_dh_en_cours datetime NULL COMMENT 'Date de passage au statut en cours';
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_dh_realise datetime NULL COMMENT 'Date de passage au statut réalisé';
ALTER TABLE recommandations.recommandations MODIFY COLUMN rec_dh_nonrealise datetime NULL COMMENT 'Date de passage au statut non réalisé';

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

-- ************************************ 29/04/2020********************************
ALTER TABLE recommandations.activites MODIFY COLUMN act_is_valide smallint(6) DEFAULT 0 NOT NULL;

-- ************************************ 30/04/2020********************************
DROP TABLE recommandations.envoialertes;
CREATE TABLE `envoialertes` (
  `ena_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `ale_id` bigint(20) unsigned NOT NULL,
  `ena_msg` text,
  `ale_mail_subject` varchar(255) DEFAULT NULL,
  `ale_date_heure_prev` datetime DEFAULT NULL,
  `ena_date_heure_recu` datetime NOT NULL,
  `ena_isenvoi` smallint(6) NOT NULL DEFAULT '0',
  `ena_is_lue` smallint(6) NOT NULL DEFAULT '0',
  `ena_read_at` datetime DEFAULT NULL,
  `ena_ses_description` varchar(255) NOT NULL,
  `ena_ses_annee` int(11) NOT NULL,
  `ena_ses_debut` date NOT NULL,
  `ena_ses_fin` date DEFAULT NULL,
  `ena_ses_lieu` varchar(255) NOT NULL,
  `user_nom` varchar(100) DEFAULT NULL,
  `user_prenom` varchar(100) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `str_id` bigint(20) DEFAULT NULL,
  `str_sigle` varchar(255) DEFAULT NULL,
  `str_nom_complet` varchar(255) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ena_id`),
  UNIQUE KEY `ena_id` (`ena_id`),
  KEY `ale_id` (`ale_id`),
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `envoialertes_ibfk_2` FOREIGN KEY (`ale_id`) REFERENCES `alertes` (`ale_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8

ALTER TABLE recommandations.alertes ADD ale_mail_subject VARCHAR(255) NOT NULL;
 ALTER TABLE recommandations.alertes ADD ale_destinataire TEXT;
 ALTER TABLE recommandations.alertes MODIFY ale_msg TEXT;