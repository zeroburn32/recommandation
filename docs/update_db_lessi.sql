
--*********************************31/03/2020******************************************
ALTER TABLE recommandations.agents ADD CONSTRAINT agents_UN_nom_prenom UNIQUE KEY (nom_prenom);
ALTER TABLE recommandations.roles ADD CONSTRAINT roles_UN_name UNIQUE KEY (name);
ALTER TABLE recommandations.roles ADD CONSTRAINT roles_UN_label UNIQUE KEY (label);

ALTER TABLE recommandations.model_has_roles DROP FOREIGN KEY model_has_roles_role_id_foreign;
ALTER TABLE recommandations.model_has_roles ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES recommandations.roles(id) ON DELETE RESTRICT;

ALTER TABLE recommandations.permissions ADD CONSTRAINT permissions_UN_name UNIQUE KEY (name);

-------*********02/04/2020***********************-------
ALTER TABLE recommandations.users DROP KEY users_email_unique;
ALTER TABLE recommandations.users ADD CONSTRAINT users_UN_email_agent UNIQUE KEY (age_id,email);
ALTER TABLE recommandations.agents DROP KEY agents_UN_nom_prenom;
ALTER TABLE recommandations.agents ADD CONSTRAINT agents_UN UNIQUE KEY (`structure`,nom_prenom);

ALTER TABLE `users` ADD `str_id` BIGINT(20) NULL AFTER `age_id`; 
ALTER TABLE recommandations.users MODIFY COLUMN str_id BIGINT UNSIGNED NULL;
ALTER TABLE recommandations.users ADD CONSTRAINT users_structures_FK FOREIGN KEY (str_id) REFERENCES recommandations.structures(str_id);
ALTER TABLE recommandations.users MODIFY COLUMN str_id bigint(20) unsigned NOT NULL;

-- ********************* 03/04/2020*********************   Not Apply----
ALTER TABLE `agents` ADD `nom` VARCHAR(40) NULL AFTER `name`, ADD `prenom` VARCHAR(100) NULL AFTER `nom`; 
ALTER TABLE `agents` ADD `sexe` VARCHAR(10) NULL AFTER `prenom`, ADD `fonction` VARCHAR(100) NULL AFTER `sexe`;

--******************16/04/2020***********************---
ALTER TABLE `users` ADD `nom` VARCHAR(50) NOT NULL AFTER `is_active`, ADD `prenom` VARCHAR(100) NOT NULL AFTER `nom`, ADD `sexe` VARCHAR(10) NOT NULL DEFAULT 'M' AFTER `prenom`, ADD `fonction` VARCHAR(100) NULL AFTER `sexe`; 

ALTER TABLE recommandations.users MODIFY COLUMN age_id bigint(20) unsigned NULL;
ALTER TABLE recommandations.users DROP FOREIGN KEY users_agents_FK;
ALTER TABLE recommandations.users DROP KEY users_UN_email_agent;
ALTER TABLE recommandations.users DROP KEY users_UN_email_agent;
ALTER TABLE recommandations.users ADD CONSTRAINT users_UN_email UNIQUE KEY (email);

ALTER TABLE recommandations.users DROP COLUMN age_id;

DROP TABLE recommandations.agents

-----------******************---------------------------------
ALTER TABLE `recommandations` ADD `rec_dh_en_cours` DATE NULL COMMENT 'Date de passage au statut en cours' AFTER `rec_dh_abandon`, ADD `rec_dh_realise` DATE NULL COMMENT 'Date de passage au statut réalisé' AFTER `rec_dh_en_cours`; 

ALTER TABLE recommandations.users CHANGE `fonction` `fonction` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE recommandations.users ADD matricule VARCHAR(10) NULL AFTER fonction;
ALTER TABLE recommandations.users ADD UNIQUE(`matricule`);

----------****************** 17/04/2020*************************----------------
CREATE TABLE `activity_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) unsigned DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) unsigned DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`),
  KEY `subject` (`subject_id`,`subject_type`),
  KEY `causer` (`causer_id`,`causer_type`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-----------******************18/04/2020*****************----------

ALTER TABLE `activity_log` ADD `email` VARCHAR(70) NOT NULL AFTER `causer_type`, ADD `adresse_ip` VARCHAR(100) NOT NULL AFTER `email`; 
ALTER TABLE `activity_log` ADD `user_agent` VARCHAR(254) NOT NULL AFTER `adresse_ip`; 

ALTER TABLE recommandations.categories ADD updated_by varchar(100) NULL;
ALTER TABLE recommandations.categories ADD created_by varchar(100) NULL;

ALTER TABLE `activity_log` CHANGE `log_name` `log_name` VARCHAR(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL; 
ALTER TABLE `activity_log` CHANGE `adresse_ip` `adresse_ip` VARCHAR(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL; 
ALTER TABLE `activity_log` CHANGE `causer_type` `causer_type` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL; 
ALTER TABLE `activity_log` CHANGE `properties` `properties` TEXT NULL DEFAULT NULL; 
ALTER TABLE `activity_log` CHANGE `id` `id` INT(15) UNSIGNED NOT NULL AUTO_INCREMENT; 

ALTER TABLE recommandations.categories ADD CONSTRAINT categories_UN UNIQUE KEY (cat_libelle);

---************ 20/04/2020 *********************************-----

ALTER TABLE recommandations.activites DROP KEY act_description_unique;




























