/**
 * Created by abdou on 19/02/2020.
 */

$('.selectSearch').select2({
    placeholder: "Choisir...",
    allowClear: true
});

(function ($) {
    // refreshCharts();
    // refreshDataCharts();
    refreshChartForUserByInstance();
    refreshChartForUserAllInstances();
    refreshChartByInstance();
    refreshChartAllInstances();
})(jQuery);

function refreshCharts() {
    var initChartInstanceStructure = new Highcharts.chart("chartForUserByInstance", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var initChartInstanceStructures = new Highcharts.chart("chartForUserAllInstances", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var initChartInstanceGlobal = new Highcharts.chart("chartByInstance", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var initChartInstanceGlobals = new Highcharts.chart("chartAllInstances", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });

    var dataInstanceStructure = [], dataInstanceStructures = [], dataInstanceGlobal = [], dataInstanceGlobals = [];
    var instanceStructure1 = 29;
    var instanceStructure2 = 71;
    var instanceStructures1 = 42;
    var instanceStructures2 = 58;
    var instanceGlobal1 = 29;
    var instanceGlobal2 = 71;
    var instanceGlobals1 = 42;
    var instanceGlobals2 = 58;

    dataInstanceStructure.push({
        name: "Réalisée",
        y: instanceStructure1
    });
    dataInstanceStructure.push({
        name: "Non réalisée",
        y: instanceStructure2
    });

    dataInstanceStructures.push({
        name: "Réalisée",
        y: instanceStructures1
    });
    dataInstanceStructures.push({
        name: "Non réalisée",
        y: instanceStructures2
    });

    dataInstanceGlobal.push({
        name: "Réalisée",
        y: instanceGlobal1
    });
    dataInstanceGlobal.push({
        name: "Non réalisée",
        y: instanceGlobal2
    });

    dataInstanceGlobals.push({
        name: "Réalisée",
        y: instanceGlobals1
    });
    dataInstanceGlobals.push({
        name: "Non réalisée",
        y: instanceGlobals2
    });

    if (initChartInstanceStructure.get('serieStructure')) {
        initChartInstanceStructure.get('serieStructure').remove();
    }
    if (initChartInstanceStructures.get('serieStructures')) {
        initChartInstanceStructures.get('serieStructures').remove();
    }
    if (initChartInstanceGlobal.get('serieGlobal')) {
        initChartInstanceGlobal.get('serieGlobal').remove();
    }
    if (initChartInstanceGlobals.get('serieGlobals')) {
        initChartInstanceGlobals.get('serieGlobals').remove();
    }

    $('#chartForUserByInstance').highcharts().addSeries({
        id: 'serieStructure',
        name: 'Valeur',
        type: 'pie',
        colorByPoint: true,
        data: dataInstanceStructure
    });
    $('#chartForUserAllInstances').highcharts().addSeries({
        id: 'serieStructures',
        name: 'Valeur',
        type: 'pie',
        colorByPoint: true,
        data: dataInstanceStructures
    });
    $('#chartByInstance').highcharts().addSeries({
        id: 'serieGlobal',
        name: 'Valeur',
        type: 'pie',
        colorByPoint: true,
        data: dataInstanceGlobal
    });
    $('#chartAllInstances').highcharts().addSeries({
        id: 'serieGlobals',
        name: 'Valeur',
        type: 'pie',
        colorByPoint: true,
        data: dataInstanceGlobals
    });
}

function refreshDataCharts() {
    var userIns_id = $('#instanceUser').val();
    var ins_id = $('#instance').val();
    var initChartInstanceStructure = new Highcharts.chart("chartForUserByInstance", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var initChartInstanceStructures = new Highcharts.chart("chartForUserAllInstances", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var initChartInstanceGlobal = new Highcharts.chart("chartByInstance", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var initChartInstanceGlobals = new Highcharts.chart("chartAllInstances", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#019b07', '#c8001b'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });

    var dataInstanceStructure = [], dataInstanceStructures = [], dataInstanceGlobal = [], dataInstanceGlobals = [];

    $.ajax({
        url: "/getDataByInstanceByStructure/" + userIns_id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            dataInstanceStructure.push({
                name: "Réalisée",
                y: data.countRealiseByInstanceForUser
            });
            dataInstanceStructure.push({
                name: "Non réalisée",
                y: data.countNonRealiseByInstanceForUser
            });
            if (initChartInstanceStructure.get('serieStructure')) {
                initChartInstanceStructure.get('serieStructure').remove();
            }
            $('#chartForUserByInstance').highcharts().addSeries({
                id: 'serieStructure',
                name: 'Valeur',
                type: 'pie',
                colorByPoint: true,
                data: dataInstanceStructure
            });
        }
    });

    $.ajax({
        url: "/getDataAllInstanceByStructure",
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            dataInstanceStructures.push({
                name: "Réalisée",
                y: data.countRealiseAllForUser
            });
            dataInstanceStructures.push({
                name: "Non réalisée",
                y: data.countNonRealiseAllForUser
            });
            if (initChartInstanceStructures.get('serieStructures')) {
                initChartInstanceStructures.get('serieStructures').remove();
            }
            $('#chartForUserAllInstances').highcharts().addSeries({
                id: 'serieStructures',
                name: 'Valeur',
                type: 'pie',
                colorByPoint: true,
                data: dataInstanceStructures
            });
        }
    });

    $.ajax({
        url: "/getDataByInstance/" + ins_id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            dataInstanceGlobal.push({
                name: "Réalisée",
                y: data.countByInstanceRealise
            });
            dataInstanceGlobal.push({
                name: "Non réalisée",
                y: data.countByInstanceNonRealise
            });
            if (initChartInstanceGlobal.get('serieGlobal')) {
                initChartInstanceGlobal.get('serieGlobal').remove();
            }
            $('#chartByInstance').highcharts().addSeries({
                id: 'serieGlobal',
                name: 'Valeur',
                type: 'pie',
                colorByPoint: true,
                data: dataInstanceGlobal
            });
        }
    });

    $.ajax({
        url: "/getDataAllInstance",
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            dataInstanceGlobals.push({
                name: "Réalisée",
                y: data.countAllRealise
            });
            dataInstanceGlobals.push({
                name: "Non réalisée",
                y: data.countAllNonRealise
            });
            if (initChartInstanceGlobals.get('serieGlobals')) {
                initChartInstanceGlobals.get('serieGlobals').remove();
            }
            $('#chartAllInstances').highcharts().addSeries({
                id: 'serieGlobals',
                name: 'Valeur',
                type: 'pie',
                colorByPoint: true,
                data: dataInstanceGlobals
            });
        }
    });
}

function refreshChartForUserByInstance() {
    var ins_id = $('#instanceUser').val();
    if (ins_id != null && ins_id > 0) {
        var initChartForUserByInstance = new Highcharts.chart("chartForUserByInstance", {
            lang: {
                decimalPoint: ",",
                downloadPNG: "Télécharger en image PNG",
                downloadJPEG: "Télécharger en image JPEG",
                downloadPDF: "Télécharger en document PDF",
                loading: "Chargement en cours...",
                thousandsSep: " ",
                printChart: "Imprimer le graphique",
                downloadCSV: "Exporter les données au format CSV",
                downloadXLS: "Exporter les données au format Excel",
                noData: "Pas de données pour ce filtre !"
            },
            noData: {
                style: {
                    fontSize: '18px',
                    fontWeight: 'bold'
                }
            },
            title: {
                text: ''
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: '',
                    skew3d: true
                }
            },
            tooltip: {
                headerFormat: '<b>{point.key}</b><br>',
                pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
            },
            plotOptions: {
                pie: {
                    colors: ['#00359B', '#c8001b', '#00889b', '#019b07', '#c8781f'],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            }
        });
        var datas = [];
        $.ajax({
            url: APP_URL + "/getDataByInstanceByStructure/" + ins_id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                datas.push({
                    name: "En préparation",
                    y: data.preparations
                });
                datas.push({
                    name: "Non réalisée",
                    y: data.nonRealises
                });
                datas.push({
                    name: "En cours",
                    y: data.enCours
                });
                datas.push({
                    name: "Réalisée",
                    y: data.realises
                });
                datas.push({
                    name: "Abandonnée",
                    y: data.abandonnes
                });
                if (data.preparations > 0 || data.nonRealises > 0 || data.enCours > 0 || data.realises > 0 || data.abandonnes > 0) {
                    if (initChartForUserByInstance.get('serieInstance')) {
                        initChartForUserByInstance.get('serieInstance').remove();
                    }
                    $('#chartForUserByInstance').highcharts().addSeries({
                        id: 'serieInstance',
                        name: 'Nombre de recommandations',
                        type: 'pie',
                        colorByPoint: true,
                        data: datas
                    });
                }
            }
        });
    }
}

function refreshChartForUserAllInstances() {
    var initChartForUserAllInstances = new Highcharts.chart("chartForUserAllInstances", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '18px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#00359B', '#c8001b', '#00889b', '#019b07', '#c8781f'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var datas = [];
    $.ajax({
        url: APP_URL + "/getDataAllInstanceByStructure",
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            datas.push({
                name: "En préparation",
                y: data.preparations
            });
            datas.push({
                name: "Non réalisée",
                y: data.nonRealises
            });
            datas.push({
                name: "En cours",
                y: data.enCours
            });
            datas.push({
                name: "Réalisée",
                y: data.realises
            });
            datas.push({
                name: "Abandonnée",
                y: data.abandonnes
            });
            if (data.preparations > 0 || data.nonRealises > 0 || data.enCours > 0 || data.realises > 0 || data.abandonnes > 0) {
                if (initChartForUserAllInstances.get('serieInstances')) {
                    initChartForUserAllInstances.get('serieInstances').remove();
                }
                $('#chartForUserAllInstances').highcharts().addSeries({
                    id: 'serieInstances',
                    name: 'Nombre de recommandations',
                    type: 'pie',
                    colorByPoint: true,
                    data: datas
                });
            }
        }
    });
}

function refreshChartByInstance() {
    var ins_id = $('#instance').val();
    if (ins_id != null && ins_id > 0) {
        var initChartByInstance = new Highcharts.chart("chartByInstance", {
            lang: {
                decimalPoint: ",",
                downloadPNG: "Télécharger en image PNG",
                downloadJPEG: "Télécharger en image JPEG",
                downloadPDF: "Télécharger en document PDF",
                loading: "Chargement en cours...",
                thousandsSep: " ",
                printChart: "Imprimer le graphique",
                downloadCSV: "Exporter les données au format CSV",
                downloadXLS: "Exporter les données au format Excel",
                noData: "Pas de données pour ce filtre !"
            },
            noData: {
                style: {
                    fontSize: '20px',
                    fontWeight: 'bold'
                }
            },
            title: {
                text: ''
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: '',
                    skew3d: true
                }
            },
            tooltip: {
                headerFormat: '<b>{point.key}</b><br>',
                pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
            },
            plotOptions: {
                pie: {
                    colors: ['#00359B', '#c8001b', '#00889b', '#019b07', '#c8781f'],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            }
        });
        var datas = [];
        $.ajax({
            url: APP_URL + "/getDataByInstance/" + ins_id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                datas.push({
                    name: "En préparation",
                    y: data.preparations
                });
                datas.push({
                    name: "Non réalisée",
                    y: data.nonRealises
                });
                datas.push({
                    name: "En cours",
                    y: data.enCours
                });
                datas.push({
                    name: "Réalisée",
                    y: data.realises
                });
                datas.push({
                    name: "Abandonnée",
                    y: data.abandonnes
                });
                if (data.preparations > 0 || data.nonRealises > 0 || data.enCours > 0 || data.realises > 0 || data.abandonnes > 0) {
                    if (initChartByInstance.get('serieGlobalInstance')) {
                        initChartByInstance.get('serieGlobalInstance').remove();
                    }
                    $('#chartByInstance').highcharts().addSeries({
                        id: 'serieGlobalInstance',
                        name: 'Nombre de recommandations',
                        type: 'pie',
                        colorByPoint: true,
                        data: datas
                    });
                }
            }
        });
    }
}

function refreshChartAllInstances() {
    var initChartAllInstances = new Highcharts.chart("chartAllInstances", {
        lang: {
            decimalPoint: ",",
            downloadPNG: "Télécharger en image PNG",
            downloadJPEG: "Télécharger en image JPEG",
            downloadPDF: "Télécharger en document PDF",
            loading: "Chargement en cours...",
            thousandsSep: " ",
            printChart: "Imprimer le graphique",
            downloadCSV: "Exporter les données au format CSV",
            downloadXLS: "Exporter les données au format Excel",
            noData: "Pas de données pour ce filtre !"
        },
        noData: {
            style: {
                fontSize: '20px',
                fontWeight: 'bold'
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '',
                skew3d: true
            }
        },
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                colors: ['#00359B', '#c8001b', '#00889b', '#019b07', '#c8781f'],
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });
    var datas = [];
    $.ajax({
        url: APP_URL + "/getDataAllInstance",
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            datas.push({
                name: "En préparation",
                y: data.preparations
            });
            datas.push({
                name: "Non réalisée",
                y: data.nonRealises
            });
            datas.push({
                name: "En cours",
                y: data.enCours
            });
            datas.push({
                name: "Réalisée",
                y: data.realises
            });
            datas.push({
                name: "Abandonnée",
                y: data.abandonnes
            });
            if (data.preparations > 0 || data.nonRealises > 0 || data.enCours > 0 || data.realises > 0 || data.abandonnes > 0) {
                if (initChartAllInstances.get('serieGlobalInstances')) {
                    initChartAllInstances.get('serieGlobalInstances').remove();
                }
                $('#chartAllInstances').highcharts().addSeries({
                    id: 'serieGlobalInstances',
                    name: 'Nombre de recommandations',
                    type: 'pie',
                    colorByPoint: true,
                    data: datas
                });
            }
        }
    });
}

function openModalDetailsReco(id) {
    $('#zoneDate').hide();
    $('#zoneMotif').hide();
    $('#historique').hide();
    $('#histNonRealise').hide();
    $('#histEnCours').hide();
    $('#histRealise').hide();
    $('.modal-title').text('Détails de la recommandation');
    $.ajax({
        url: APP_URL + "/getReco/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var statut, planning, mode, valideSuivi, permanente = '';
            var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
            var optionsWithTime = {year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"};

            /** rec_statut */
            if (data.rec_statut == 1) {
                statut = '<span class="badge badge-primary">Pr&eacute;aration</span>';
            }
            else if (data.rec_statut == 2) {
                statut = '<span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>';
            }
            else if (data.rec_statut == 3) {
                statut = '<span class="badge badge-info">En cours</span>';
            }
            else if (data.rec_statut == 4) {
                statut = '<span class="badge badge-success">R&eacute;alis&eacute;e</span>';
            }
            else if (data.rec_statut == 5) {
                statut = '<span class="badge badge-danger">Abandonn&eacute;e</span>';
            }
            else {
                mode = '<span>ND</span>';
            }
            /** rec_is_planning_valide */
            if (data.rec_is_planning_valide == 1) {
                planning = '<span class="badge badge-success">Oui</span>';
            }
            else if (data.rec_is_planning_valide == 0) {
                planning = '<span class="badge badge-danger">Non</span>';
            }
            else {
                mode = '<span>ND</span>';
            }
            /** rec_is_mode_standard */
            if (data.rec_is_mode_standard == 1) {
                mode = '<span class="badge badge-info">Par statut</span>';
            }
            else if (data.rec_is_mode_standard == 0) {
                mode = '<span class="badge badge-primary">Par taux</span>';
            }
            else {
                mode = '<span>ND</span>';
            }
            /** rec_is_suivi_valide */
            if (data.rec_is_suivi_valide == 1) {
                valideSuivi = '<span class="badge badge-success">Oui</span>';
            }
            else if (data.rec_is_suivi_valide == 0) {
                valideSuivi = '<span class="badge badge-danger">Non</span>';
            }
            else {
                valideSuivi = '<span></span>';
            }
            /** rec_is_permanente */
            if (data.rec_is_permanente == 1) {
                permanente = '<span class="badge badge-success">Oui</span>';
            }
            else if (data.rec_is_permanente == 0) {
                permanente = '<span class="badge badge-danger">Non</span>';
            }
            else {
                mode = '<span>ND</span>';
            }
            /** rec_dh_valide ou rec_dh_abandon */
            if (data.rec_is_suivi_valide == 1) {
                $('#zoneDate').show();
                $('#detDateLib').text("Validée le");
                var dtVal = new Date(data.rec_dh_valide);
                var datVal = dtVal.toLocaleDateString("fr-FR", optionsWithTime);
                $('#detDateValue').text(datVal);
//                        $('#detDateValue').text(dateFormat(data.rec_dh_valide, "dd/mm/yyyy à HH:mm:ss"));
            }
            if (data.rec_statut == 5) {
                $('#zoneDate').show();
                $('#detDateLib').text("Abandonnée le");
                var dtAb = new Date(data.rec_dh_abandon);
                var datAb = dtAb.toLocaleDateString("fr-FR", optionsWithTime);
                $('#detDateValue').text(datAb);
//                        $('#detDateValue').text(dateFormat(data.rec_dh_abandon, "dd/mm/yyyy à HH:mm:ss"));
            }

            if(data.rec_motif_abandon) {
                $('#zoneMotif').show();
                $('#detMotif').text(data.rec_motif_abandon);
            }

            $('#detIns').text(data.ins_nom_complet + ' (' + data.ins_sigle + ')');
            $('#detPB').text(data.bud_intitule);
            $('#detInti').text(data.rec_intitule);
            $('#detStatut').html(statut);
            $('#detPlan').html(planning);
            $('#detValid').html(valideSuivi);
            $('#detMode').html(mode);
            $('#detObs').text(data.rec_observation);
            $('#detPers').text(data.rec_personne_formule);
            $('#detPersTel').text(data.rec_personne_tel);
            $('#detTaux').text(data.rec_taux_realise + '%');
            $('#detPrt').html(permanente);

            var dtDeb = new Date(data.ses_date_debut);
            var datDeb = dtDeb.toLocaleDateString("fr-FR", options);
            $('#detSes').text(data.ses_description + ' du ' + datDeb);
            // $('#detDateEch').text(dateFormat(data.rec_date_echeance, "dd/mm/yyyy"));

            var dtEch = new Date(data.rec_date_echeance);
            var datEch = dtEch.toLocaleDateString("fr-FR", options);
            $('#detDateEch').html('<span class="badge badge-danger">' + datEch + '</span>');

            if (data.created_at != null && data.created_at != '') {
                // $('#detCreatedAt').text(dateFormat(data.created_at, "dd/mm/yyyy à HH:mm:ss"));
                var dtCree = new Date(data.created_at);
                var datCree = dtCree.toLocaleDateString("fr-FR", optionsWithTime);
                $('#detCreatedAt').text(datCree);
                $('#detCreatedBy').text(data.created_by);
            }
            if (data.updated_at != null && data.updated_at != '') {
                // $('#detUpdatedAt').text(dateFormat(data.updated_at, "dd/mm/yyyy à HH:mm:ss"));
                var dtMod = new Date(data.updated_at);
                var datMod = dtMod.toLocaleDateString("fr-FR", optionsWithTime);
                $('#detUpdatedAt').text(datMod);
                $('#detUpdatedBy').text(data.updated_by);
            }

            /** ***************************** HISTORIQUE ********************************* */
            var urlDown = "";
            if (data.rec_obs_nonrealise && data.rec_dh_nonrealise) {
                $('#historique').show();
                $('#histNonRealise').show();
                $('#obsNonRealise').text(data.rec_obs_nonrealise);
                var dtNonReal = new Date(data.rec_dh_nonrealise);
                var datNonReal = dtNonReal.toLocaleDateString("fr-FR", optionsWithTime);
                $('#dateNonRealise').text(datNonReal);
                if (data.rec_preuve_nonrealise) {
                    urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_nonrealise;
                    $('#downNonRealise').html('<a class="btn btn-primary text-white" ' +
                        'href='+ urlDown +'>' +
                        '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                        '</a>');
                }
            }

            if (data.rec_obs_encours && data.rec_dh_en_cours) {
                $('#historique').show();
                $('#histEnCours').show();
                $('#obsEnCours').text(data.rec_obs_encours);
                var dtEnCours = new Date(data.rec_dh_en_cours);
                var datEnCours = dtEnCours.toLocaleDateString("fr-FR", optionsWithTime);
                $('#dateEnCours').text(datEnCours);
                if (data.rec_preuve_encours) {
                    urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_encours;
                    $('#downEnCours').html('<a class="btn btn-primary text-white" ' +
                        'href='+ urlDown +'>' +
                        '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                        '</a>');
                }
            }

            if (data.rec_obs_realise && data.rec_dh_realise) {
                $('#historique').show();
                $('#histRealise').show();
                $('#obsRealise').text(data.rec_obs_realise);
                var dtRealise = new Date(data.rec_dh_realise);
                var datRealise = dtRealise.toLocaleDateString("fr-FR", optionsWithTime);
                $('#dateRealise').text(datRealise);
                if (data.rec_preuve_realise) {
                    urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_realise;
                    $('#downRealise').html('<a class="btn btn-primary text-white" ' +
                        'href='+ urlDown +'>' +
                        '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                        '</a>');
                }
            }
        },
        error: function () {
            alert("Erreur rencontrer lors de la recuperation des donnees!");
        }
    });
}
