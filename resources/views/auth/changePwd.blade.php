@extends('layouts.template')
@section('title')
Utlisateurs
@parent
@stop
@section('header_styles')
<link rel="stylesheet" href="{{ asset('backend/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/sweetalert2.min.css') }}" />
<link href="{{ asset('backend/css/tables.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<div class="container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Compte actif</a></li>
            <li class="breadcrumb-item active" aria-current="page">Changement de mot de passe</li>
        </ol>
    </nav>
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Changer mon mot de passe</div>
                        </div>
                        <div class="widget-content-right">
                            <button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                            </button>
                        </div>
                    </div>
                </div>

                <section class="content user_profile">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::model($user, ['route' => ['changeMyPassword'], 'method' => 'patch']) !!}
                            <div class="form-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="row justify-content-center">
                                        <label for="inputoldpassword" class="col-md-3 control-label">Ancien mot
                                            de
                                            passe
                                            <span class='require text-danger'>*</span>
                                        </label>
                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <span class="input-group-append"><span class="input-group-text">
                                                        <i class="fa fa-key"></i></span>
                                                </span>
                                                <input type="password" id="old_password"
                                                    placeholder="Ancien mot de passe" name="old_password"
                                                    class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row justify-content-center">
                                        <label for="inputpassword" class="col-md-3 control-label">Nouveau mot de
                                            passe
                                            <span class='require text-danger'>*</span>
                                        </label>
                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <span class="input-group-append"><span class="input-group-text">
                                                        <i class="fa fa-key"></i></span>
                                                </span>
                                                <input type="password" id="password" placeholder="Nouveau mot de passe"
                                                    name="password" class="form-control" minlength="8" />
                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <span style=""> <em>Complexité requise: 8 Caractères minimum; 1 majuscule, 1
                                                    minuscule et 1 chiffre </em></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row justify-content-center">
                                        <label for="inputnumber" class="col-md-3 control-label">Confirmation du
                                            nouveau mot de passe
                                            <span class='require text-danger'>*</span>
                                        </label>
                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <span class="input-group-append"><span class="input-group-text">
                                                        <i class="fa fa-key"></i></span>
                                                </span>
                                                <input type="password" id="password-confirm"
                                                    placeholder="Confirmation du nouveau mot de passe"
                                                    name="confirm_password" class="form-control" minlength="8" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-4 col-md-7 ml-auto">
                                    <input type="reset" class="btn btn-sm btn-danger" value="Annuler">&nbsp;
                                    <input type="submit" class="btn btn-sm btn-success" value="Changer">
                                </div>
                            </div>
                            {{--</form>--}}
                            {!! Form::close() !!}
                        </div>

                    </div>

                </section>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script type="text/javascript" src="{{ asset('js/backend/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript">
    function changerPassword() {
            var csrf_token = $('meta[name="_token"]').attr('content');
            swal({
                title: 'Confirmation?',
                text: "Confirmez-vous le changement de votre mot de passe ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non',
                cancelButtonColor: '#d33',
                reverseButtons: true
            }).then(function (e) {
                if (e.value == true) {
//                    $('input[name=_method]').val('PATCH');
                    $.ajax({
                        type: 'POST',
                        url: "{{url('/changerPassword')}}",
                        data: new FormData($("#formPwd form")[0]), '_method': 'PATCH', '_token': csrf_token,
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            console.log(response);
                            Swal({
                                title: "Changement de mot de passe !",
                                text: "Le changement de votre mot de passe a été effectuer avec succès !",
                                type: "success",
                                button: "OK"
                            })
                        },
                        error: function () {
                            swal({
                                title: "Changement de mot de passe !",
                                text: "Une erreur est survenue lors du changement de votre mot de passe",
                                type: "error",
                                timer: '3000'
                            })
//                            window.location.href = "admin";
                        }
                    });
                }
            });
        }
</script>
@stop