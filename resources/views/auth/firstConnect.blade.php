<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Connexion</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/css/bootstrap.min.css") }}">
    <link rel="stylesheet" type="text/css"
        href="{{ url("connexion/fonts/font-awesome-4.7.0/css/font-awesome.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/css/util.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/css/main.css") }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <style>
        .container-login100 {
            background-image: url('images/fonds/images_traite.jpeg');
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            min-height: 100%;
            padding-top: 6%;
            padding-bottom: 5%;
        }
    </style>
</head>

<body>
    @include('flashy::message')
    <div class="limiter">
        <div class="container-login100" style="padding-top:5px;">
            <div class="wrap-login100">
                {!! Form::model($user, ['route' => ['firstConnect', $user->id], 'method' => 'patch']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <span class="login100-form-title p-b-28">
                    <img src="{{asset('images/armoirie.png')}}" width="40%" height="15%" style="padding-top:0px;"
                        class="user-image" alt="logo MEA" />
                </span>
                <span class="login100-form-title p-b-20">
                    <h5 class="text-primary">Changement de mot de passe</h5>
                    <h6 class="text-center" style="color: #ff4f0e; font-size: 14px;border-radius: 2px;">Nous vous
                        prions de changer votre mot de passe par mésure de
                        sécurité !</h6>
                </span>

                <div class="wrap-input100 validate-input">
                    <input type="password" class="form-control" name="newPassword" min="8" required>
                    {{-- <input type="email" class="input100 form-control {{ $errors->has('email')?'is-invalid':'' }}"
                    name="email" value="{{ old('email') }}" placeholder="" required> --}}
                    <span class="focus-input100" data-placeholder="Nouveau mot de passe"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <span class="btn-show-pass">
                        <i class="zmdi zmdi-eye"></i>
                    </span>
                    <input type="password" class="form-control" name="confirmPassword" min="8" required>
                    {{-- <input type="password"
                            class="input100 form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder=""
                    name="password" required> --}}
                    <span class="focus-input100" data-placeholder="Confirmer le mot de passe"></span>

                </div>

                <div class="container-login100-form-btn" style="padding-top:0px;">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Se connecter
                        </button>
                    </div>
                </div>
                <span style="font-size:12px;"> <em>Complexité requise: 8 Caractères minimum; 1 majuscule, 1 minuscule et
                        1 chiffre </em></span>

                </form>
            </div>
        </div>
    </div>

    <div id="dropDownSelect1"></div>

    <script src="{{ url("connexion/js/jquery-3.2.1.min.js") }}"></script>
    @include('flashy::message')
    {{--<script src="{{ url("connexion/js/popper.js") }}"></script>
    <script src="{{ url("connexion/js/bootstrap.min.js") }}"></script>--}}
    <script src="{{ url("connexion/js/main.js") }}"></script>

</body>

</html>