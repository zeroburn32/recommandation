<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Connexion</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/css/bootstrap.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/fonts/font-awesome-4.7.0/css/font-awesome.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/css/util.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("connexion/css/main.css") }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <style>
        .container-login100 {
            background-image:url('images/fonds/bg2.jpg');
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            /*min-height:100%;
            padding-top:6%;
            padding-bottom :5%;*/
        }
    </style>
</head>
<body style="background-image: {{ url('images/armoirie.png') }}">
@include('flashy::message')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="{{ route('authenticate') }}">
                {{ csrf_field() }}
               
                <span class="login100-form-title p-b-48">
                    <img src="{{asset('images/armoirie.png')}}" width="40%" height="15%"
                         class="user-image" alt="logo MEA"/>
                </span>
                <span class="login100-form-title p-b-26">
                    Connexion
                </span>

                <div class="wrap-input100 validate-input">
                    <input type="email" class="input100 form-control {{ $errors->has('email')?'is-invalid':'' }}"
                           name="email" value="{{ old('email') }}" placeholder="" required>
                    <span class="focus-input100" data-placeholder="Votre Email"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <span class="btn-show-pass">
                        <i class="fa fa-eye" title="Afficher le mot de passe"></i>
                        {{--<i class="zmdi zmdi-eye"></i>--}}
                    </span>
                    <input type="password" class="input100 form-control {{ $errors->has('password')?'is-invalid':'' }}"
                           placeholder="" name="password" required>
                    <span class="focus-input100" data-placeholder="Votre mot de passe"></span>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Se connecter
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div id="dropDownSelect1"></div>

<script src="{{ url("connexion/js/jquery-3.2.1.min.js") }}"></script>
@include('flashy::message')
<script src="{{ url("connexion/js/main.js") }}"></script>

</body>
</html>