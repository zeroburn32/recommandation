@extends('layouts.template')
@section('title')
    Acteurs
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Instances</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des acteurs d'instance</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des acteurs de l'instance
                                    : <span class="text-danger">{{ $instance->ins_nom_complet.' ('.$instance->ins_sigle.')' }}</span></div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-warning"
                                onclick="window.location.href ='{{ url('instances') }}'">
                                    <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-body">
                        <form class="needs-validation" novalidate method="POST" action="{{ route('saveActeur') }}"
                              id="formAddActeurs">
                            <div class="row justify-content-center">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input id="ins_id" name="ins_id" @if ($instance) value="{{ $instance->ins_id }}"
                                       @endif type="hidden">
                                <input id="act_id" name="act_id" type="hidden">

                                <div class="form-group col-md-2">
                                    <label for="str_id">Choisir les acteurs</label>
                                </div>

                                <div class="form-group col-md-8">
                                    <select id="str_id" name="str_id[]"
                                            class="form-control form-control-sm selectSearch"
                                            multiple required>
                                        {{--<option value="null">Selectionner une structure</option>--}}
                                        @foreach($structures as $structure)
                                            <option value="{{ $structure->str_id }}">{{ $structure->str_sigle . ' - ' . $structure->str_nom_complet }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                {{--<div class="form-group col-md-4">
                                    <select id="act_is_acteur_suivi" name="act_is_acteur_suivi"
                                            class="form-control form-control-sm"
                                            required>
                                        <option value="null">Rapporteur ?</option>
                                        <option value="1">Oui</option>
                                        <option value="0">Non</option>
                                    </select>
                                </div>--}}

                                <div class="form-group col-md-2">
                                    <button type='submit' class="btn btn-sm btn-success" id="btnAdd"
                                            style="width: 100%;">
                                        <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Ajouter
                                    </button>
                                </div>
                            </div>
                        </form>
                        <hr/>
                        <div class="table-responsive" style="min-height: 300px;">
                            <table class="table table-bordered table-sm table-striped table-hover flip-content dt_example">
                                <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Sigle</th>
                                    <th>Nom complet</th>
                                    <th>R&ocirc;le</th>
                                    <th class="text-center no-sort">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($acteurs as $number=>$acteur)
                                    <tr>
                                        <td>{{ $number + 1 }}</td>
                                        <td>{{ $acteur->str_sigle }}</td>
                                        <td>{{ $acteur->str_nom_complet }}</td>
                                        <td>
                                            @if($acteur->act_is_css == 1)
                                                <span class="badge badge-info">C.S.S</span>&nbsp;/&nbsp;
                                            @endif
                                            @if($acteur->act_is_csr == 1)
                                                <span class="badge badge-info">C.S.R</span>&nbsp;/&nbsp;
                                                @endif
                                            @if($acteur->act_is_asr == 1)
                                                <span class="badge badge-info">A.S.R</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <div class='btn-group'>
                                                <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                                        data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Actions
                                                </button>
                                                <div class="dropdown-menu dropleft">
                                                    @can(['GERER_ROLE_ACTEUR'])
                                                    @if($acteur->act_is_css == 0)
                                                        <a class="dropdown-item"
                                                                onclick="setRole({{ $acteur->act_id }}, '{{addslashes($acteur->str_sigle)}}', '{{addslashes($instance->ins_sigle)}}', 1)">
                                                            <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Affecter r&ocirc;le C.S.S
                                                        </a>
                                                    @elseif($acteur->act_is_css == 1)
                                                        <button class="dropdown-item"
                                                                onclick="setRole({{ $acteur->act_id }}, '{{addslashes($acteur->str_sigle)}}', '{{addslashes($instance->ins_sigle)}}', 3)">
                                                            <i class="fa fa-minus-square fa-lg"></i>&nbsp;&nbsp;R&eacute;tirer r&ocirc;le C.S.S
                                                        </button>
                                                    @endif
                                                    @if($acteur->act_is_csr == 0)
                                                        <a class="dropdown-item"
                                                                onclick="setRole({{ $acteur->act_id }}, '{{addslashes($acteur->str_sigle)}}', '{{addslashes($instance->ins_sigle)}}', 2)">
                                                            <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Affecter r&ocirc;le C.S.R
                                                        </a>
                                                    @elseif($acteur->act_is_csr == 1)
                                                        <a class="dropdown-item"
                                                                onclick="setRole({{ $acteur->act_id }}, '{{addslashes($acteur->str_sigle)}}', '{{addslashes($instance->ins_sigle)}}', 4)">
                                                            <i class="fa fa-minus-square fa-lg"></i>&nbsp;&nbsp;R&eacute;tirer r&ocirc;le C.S.R
                                                        </a>
                                                    @endif
                                                    @endcan
                                                    <a class="dropdown-item" title="Supprimer l'acteur de l'instance"
                                                            onclick="deleteActeur({{ $acteur->act_id }}, '{{addslashes($acteur->str_sigle)}}', '{{addslashes($instance->ins_sigle)}}')">
                                                        <i class="fa fa-minus-square fa-lg"></i>&nbsp;&nbsp;Supprimer
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <span class="text-danger">NB : C.S.S = Chargé de suivi de sessions; C.S.R = Chargé de suivi des recommandations et A.S.R = Acteurs de suivi des recommandations</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $('.selectSearch').select2({
            placeholder: "Choisir...",
            allowClear: true
        });

        function setRole(id, libelle, instance, flag) {
            var txtConfirme, txtBtnConfirm, txtColorConfirme, txtErrorText, txtUrl = "";
            if (flag == 1) {
                txtConfirme = "Etes vous sûr de vouloir affecter le rôle " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>chargé de suivi de sessions (C.S.S)</span> à l'acteur : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + libelle + "</span> de l'instance : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + instance + "</span> ?";
                txtBtnConfirm = "Oui, définir!";
                txtColorConfirme = '#008d03';
                txtUrl = "{{url('/setCss')}}/" + id;
                txtErrorText = "Une erreure est survenue lors de l'affectation du rôle <span class='text-info' style='font-size: 18px; font-weight: bold;'>chargé de suivi de sessions (C.S.S)</span> à l'acteur!\nVeuillez reessayer!";
            } else if (flag == 2) {
                txtConfirme = "Etes vous sûr de vouloir affecter le rôle " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>chargé de suivi des recommandations (C.S.R)</span> à l'acteur : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + libelle + "</span> de l'instance : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + instance + "</span> ?";
                txtBtnConfirm = "Oui, définir!";
                txtColorConfirme = '#008d03';
                txtUrl = "{{url('/setCsr')}}/" + id;
                txtErrorText = "Une erreure est survenue lors de l'affectation du rôle <span class='text-info' style='font-size: 18px; font-weight: bold;'>chargé de suivi des recommandations (C.S.R)</span> à l'acteur!\nVeuillez reessayer!";
            } else if (flag == 3) {
                txtConfirme = "Etes vous sûr de vouloir rétirer le rôle " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>chargé de suivi de sessions (C.S.S)</span> à l'acteur : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + libelle + "</span> de l'instance : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + instance + "</span> ?";
                txtBtnConfirm = "Oui, Rétirer!";
                txtColorConfirme = '#d33';
                txtUrl = "{{url('/setCss')}}/" + id;
                txtErrorText = "Une erreure est survenue lors du rétrait du <span class='text-info' style='font-size: 18px; font-weight: bold;'>rôle chargé de suivi de sessions (C.S.S)</span> à l'acteur!\nVeuillez reessayer!";
            } else if (flag == 4) {
                txtConfirme = "Etes vous sûr de vouloir rétirer le rôle " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>chargé de suivi des recommandations (C.S.R)</span> à l'acteur : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + libelle + "</span> de l'instance : " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>" + instance + "</span> ?";
                txtBtnConfirm = "Oui, Rétirer!";
                txtColorConfirme = '#d33';
                txtUrl = "{{url('/setCsr')}}/" + id;
                txtErrorText = "Une erreure est survenue lors du rétrait du <span class='text-info' style='font-size: 18px; font-weight: bold;'>rôle chargé de suivi des recommandations (C.S.R)</span> à l'acteur!\nVeuillez reessayer!";
            }
            swal({
                title: 'CONFIRMATION',
                html: txtConfirme,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: txtColorConfirme,
                cancelButtonColor: '#3085d6',
                confirmButtonText: txtBtnConfirm,
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: txtUrl,
                            type: "get",
                            data: {act_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: "Succes!",
                                        text: "Opération effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                } else {
                                    swal({
                                        title: 'Erreur',
                                        html: txtErrorText,
                                        type: 'error',
                                        button: "OK"
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    html: txtErrorText,
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function deleteActeur(id, libelle, instance) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir rétirer définitivement l'acteur : " + libelle + " de l'instance : " + instance + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, rétirer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteActeur')}}/" + id,
                            type: "get",
                            data: {act_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de rétirer ce acteur, il est rattaché à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Rétrait de l'acteur effectué avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors du rétrait de l'acteur!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
@endsection
