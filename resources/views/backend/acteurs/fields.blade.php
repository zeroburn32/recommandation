<!-- Ins Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ins_id', 'Ins Id:') !!}
    {!! Form::number('ins_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Str Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('str_id', 'Str Id:') !!}
    {!! Form::number('str_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('acteurs.index') }}" class="btn btn-secondary">Cancel</a>
</div>
