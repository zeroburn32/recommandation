<div class="table-responsive-sm">
    <table class="table table-striped" id="acteurs-table">
        <thead>
            <th>Ins Id</th>
        <th>Str Id</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($acteurs as $acteur)
            <tr>
                <td>{{ $acteur->ins_id }}</td>
            <td>{{ $acteur->str_id }}</td>
                <td>
                    {!! Form::open(['route' => ['acteurs.destroy', $acteur->act_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('acteurs.show', [$acteur->act_id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('acteurs.edit', [$acteur->act_id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>