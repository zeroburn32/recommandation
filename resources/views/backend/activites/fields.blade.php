<!-- Rec Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_id', 'Rec Id:') !!}
    {!! Form::number('rec_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ptn Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ptn_id', 'Ptn Id:') !!}
    {!! Form::number('ptn_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Act Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('act_description', 'Act Description:') !!}
    {!! Form::textarea('act_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Act Is Realise Field -->
<div class="form-group col-sm-6">
    {!! Form::label('act_is_realise', 'Act Is Realise:') !!}
    {!! Form::number('act_is_realise', null, ['class' => 'form-control']) !!}
</div>

<!-- Act Is Valide Field -->
<div class="form-group col-sm-6">
    {!! Form::label('act_is_valide', 'Act Is Valide:') !!}
    {!! Form::number('act_is_valide', null, ['class' => 'form-control']) !!}
</div>

<!-- Act Poids Field -->
<div class="form-group col-sm-6">
    {!! Form::label('act_poids', 'Act Poids:') !!}
    {!! Form::number('act_poids', null, ['class' => 'form-control']) !!}
</div>

<!-- Act Date Prevue Field -->
<div class="form-group col-sm-6">
    {!! Form::label('act_date_prevue', 'Act Date Prevue:') !!}
    {!! Form::text('act_date_prevue', null, ['class' => 'form-control','id'=>'act_date_prevue']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#act_date_prevue').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Act Date Realise Field -->
<div class="form-group col-sm-6">
    {!! Form::label('act_date_realise', 'Act Date Realise:') !!}
    {!! Form::text('act_date_realise', null, ['class' => 'form-control','id'=>'act_date_realise']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#act_date_realise').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Act Preuve Field -->
<div class="form-group col-sm-6">
    {!! Form::label('act_preuve', 'Act Preuve:') !!}
    {!! Form::text('act_preuve', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('activites.index') }}" class="btn btn-secondary">Cancel</a>
</div>
