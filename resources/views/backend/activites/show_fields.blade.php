<!-- Rec Id Field -->
<div class="form-group">
    {!! Form::label('rec_id', 'Rec Id:') !!}
    <p>{{ $activites->rec_id }}</p>
</div>

<!-- Ptn Id Field -->
<div class="form-group">
    {!! Form::label('ptn_id', 'Ptn Id:') !!}
    <p>{{ $activites->ptn_id }}</p>
</div>

<!-- Act Description Field -->
<div class="form-group">
    {!! Form::label('act_description', 'Act Description:') !!}
    <p>{{ $activites->act_description }}</p>
</div>

<!-- Act Is Realise Field -->
<div class="form-group">
    {!! Form::label('act_is_realise', 'Act Is Realise:') !!}
    <p>{{ $activites->act_is_realise }}</p>
</div>

<!-- Act Is Valide Field -->
<div class="form-group">
    {!! Form::label('act_is_valide', 'Act Is Valide:') !!}
    <p>{{ $activites->act_is_valide }}</p>
</div>

<!-- Act Poids Field -->
<div class="form-group">
    {!! Form::label('act_poids', 'Act Poids:') !!}
    <p>{{ $activites->act_poids }}</p>
</div>

<!-- Act Date Prevue Field -->
<div class="form-group">
    {!! Form::label('act_date_prevue', 'Act Date Prevue:') !!}
    <p>{{ $activites->act_date_prevue }}</p>
</div>

<!-- Act Date Realise Field -->
<div class="form-group">
    {!! Form::label('act_date_realise', 'Act Date Realise:') !!}
    <p>{{ $activites->act_date_realise }}</p>
</div>

<!-- Act Preuve Field -->
<div class="form-group">
    {!! Form::label('act_preuve', 'Act Preuve:') !!}
    <p>{{ $activites->act_preuve }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $activites->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $activites->updated_by }}</p>
</div>

