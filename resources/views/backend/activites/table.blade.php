<div class="table-responsive-sm">
    <table class="table table-striped" id="activites-table">
        <thead>
            <th>Rec Id</th>
        <th>Ptn Id</th>
        <th>Act Description</th>
        <th>Act Is Realise</th>
        <th>Act Is Valide</th>
        <th>Act Poids</th>
        <th>Act Date Prevue</th>
        <th>Act Date Realise</th>
        <th>Act Preuve</th>
        <th>Created By</th>
        <th>Updated By</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($activites as $activites)
            <tr>
                <td>{{ $activites->rec_id }}</td>
            <td>{{ $activites->ptn_id }}</td>
            <td>{{ $activites->act_description }}</td>
            <td>{{ $activites->act_is_realise }}</td>
            <td>{{ $activites->act_is_valide }}</td>
            <td>{{ $activites->act_poids }}</td>
            <td>{{ $activites->act_date_prevue }}</td>
            <td>{{ $activites->act_date_realise }}</td>
            <td>{{ $activites->act_preuve }}</td>
            <td>{{ $activites->created_by }}</td>
            <td>{{ $activites->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['activites.destroy', $activites->act_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('activites.show', [$activites->act_id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('activites.edit', [$activites->act_id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>