@extends('layouts.template')
@section('title')
    Historique
    @parent
@stop
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}" />
@stop
@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
            <li class="breadcrumb-item"> </li><a href="{{route('users.index')}}"><i class="cib-co-op"></i>Administration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Historique</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="card-title">Liste des actions effectuées sur le système</div>
                                </div>
                                <div class="widget-content-right">
                                    <button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                        <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                                    </button>
                                </div>
                            </div>
                    </div>
                    <div class="card-body">
                       <form method="POST" action="{{ route('activityLogSearch')}}" id="formFilter">
                        <div class="row justify-content-center">
                            @csrf
                            <div class="from-group col-lg-3 col-xl-3">
                            <input type="hidden" id="id" name="id" value="{{ $id}}"/>
                                <label for="startDate">Date début </label>
                                 <input type="date" id="startDate" name="startDate" class="form-control form-control-sm" />
                                </div>
                            <div class="from-group col-lg-3 col-xl-3">
                                <label for="endDate">Date fin </label>
                                 <input type="date" id="endDate" name="endDate" class="form-control form-control-sm" />
                            </div>
                            <div class="from-group col-lg-3 col-xl-3">
                                <label for="btnSearch">&nbsp;</label>
                                <button type="submit" class="btn btn-sm btn-primary" style="width: 100%" id="btnSearch">
                                    <i class="fa fa-search"></i>&nbsp;Rechercher
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive" style="min-height: 45vh;">
                                    @include('backend.activity_log.table')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('modal')

@endsection
@section('scripts')
   <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
    <script>
        $('.selectSearch').select2({
            placeholder: "Choisir...",
            allowClear: true
        });

        $("#startDate").datetimepicker({
            ignoreReadonly: true,
            showClear: true,
            viewMode: 'years',
            format: 'YYYY',
            maxDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                clear: "fa fa-trash"
            }
        }).val('').parent().css("position :relative");

    </script>
@endsection

