<div class="table-responsive-sm ">
    <table class="table table-striped table-sm dt_example" id="users-table">
        <thead class="thead-dark">
            <th>#</th>
            <th>Objet</th>
            <th>Action</th>
            <th>E-mail</th>
            <th>Date</th>
            <th>IP</th>
        </thead>
        <tbody>
            @foreach($activities as $i=>$item)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ $item->objet }}</td>
                <td>{{ $item->action }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->ip }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>