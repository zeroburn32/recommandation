@extends('layouts.template')

@section('title')
    Alert
    @parent
@stop

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Alerte</a></li>
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Notifications</a></li>
				<li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Boîte d'envois</a></li>
                <li class="breadcrumb-item active" aria-current="page">Détails</li>
            </ol>
        </nav>

        <div class="row mb-3">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title"><strong> Détail de la notification </strong><span style="color:#0073e6">{{$envoi->ale_mail_subject}}&nbsp;&nbsp;-&nbsp;&nbsp;</span><span style="color:#ffcc00">reçu le {{ $envoi->ena_date_heure_recu->format('d/m/Y à H:i:s')}}</span></div>
                            </div>
                            <div class="widget-content-right">
                                <button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                    <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="content">
                            <div class="portlet box default" style="margin-top: 10px">
                                <div class="portlet-body flip-scroll">
                                    <div class="row justify-content-center">
                                        <div class="col-md-12">
											<div class="row">
													<div class="col-md-6"><strong>Session concernée ({{$envoi->ena_ses_annee}})</strong>&nbsp;:&nbsp;{{$envoi->ena_ses_description}} - du {{$envoi->ena_ses_debut->format('d/m/Y')}} au {{$envoi->ena_ses_fin->format('d/m/Y')}} à {{$envoi->ena_ses_lieu}}<hr></div>
													
													<div class="col-md-6"><strong>Destinataire</strong>&nbsp;:&nbsp;{{$envoi->str_sigle}} > {{$envoi->user_nom}} {{$envoi->user_prenom}} < {{$envoi->user_email}} ><hr></div>
											</div>
											<div class="row">
												<div class="col-md-12"><strong>Contenu de la notification :</strong></div>
											</div>
											<div class="row">
												<div class="col-md-12">{!!$envoi->ena_msg  !!}</div> 
											</div>
											
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
