@extends('layouts.template')

@section('title')
    Alertes
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('sessions') }}">Sessions</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des alertes</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des Alertes de le session : CASEM Juin 2019</div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddAlerte(null)"
                                        data-toggle="modal" data-target="#modalAddAlerte">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle alerte
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row my-3">
                    <?php for ($i = 1; $i < 4; $i++) { ?>
                    <div class="col-lg-6 col-xl-4 mb-3">
                        <div class="card">
                            <div class="card-header widget-content bg-info" style="color: white !important;">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        Alert {{ $i }}
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="btn-group dropdown nav-item">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                               class="nav-link">
                                                <div style="font-size: 24px; font-weight: bold;">&vellip;</div>
                                            </a>
                                            <div tabindex="-1" role="menu" aria-hidden="true"
                                                 class="dropdown-menu dropdown-menu-right">
                                                <a tabindex="0" class="dropdown-item"
                                                   onclick="openModalAddAlerte(5)"
                                                   data-toggle="modal" data-target="#modalAddAlerte">
                                                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                                </a>
                                                <a tabindex="0" class="dropdown-item"
                                                   onclick="deleteAlerte(5, '{{addslashes(" Modèle " . $i)}}')">
                                                    <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div><span>Date & heure pr&eacute;vu : jj/mm/aaaa à HH:mm</span></div>
                                <div><span>Date & heure d'envoi : jj/mm/aaaa à HH:mm</span></div>
                                <div><span>Etat : </span></div>
                                <div><span>Type : </span></div>
                                <div><span>Contenu..........................</span></div>
                            </div>
                            <div class="card-footer">
                                <button type="button" class="btn btn-secondary" style="width: 100%;"
                                        {{--onclick="window.location.href='getPageAgentSuivi/3'"--}}>
                                    Modèle
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $(".form_datetime").datetimepicker({
            format: "dd MM yyyy - hh:ii",
            autoclose: true,
            todayBtn: true,
            widgetParent: $("#modalAddAlerte"),
            pickerPosition: "bottom-left"
        });

        $("#ale_date_heure_prev").datetimepicker({
            ignoreReadonly: true,
//            viewMode: 'years',
            format: 'dd/mm/yyyy hh:ii',
            maxDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right"

            }
        }).parent().css("position :relative");

        function openModalAddAlerte(id) {
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAddAlerte"),
                placeholder: "Choisir...",
                allowClear: true
            });
//            $('#formAddInstance').attr('action', 'saveModele');
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle alerte');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#ale_id').val(null);
                $('#ses_id').val(null);
                $('#tmsg_id').val(null);
                $('#ale_msg').val(null);
                $('#ale_date_heure_prev').val(null);
                $('#ale_etat').val(null);
                $('#ale_tmsg_type').val(null);
                $('#ale_date_heure_envoi').val(null);
            }
            else {
                /*$.ajax({
                 url: "/getInstance/" + id,
                 type: "GET",
                 dataType: "JSON",
                 success: function (data) {*/
                $('.modal-title').text('Modification de l\'alerte');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                $('#tmsg_id').val(id);
                $('#tmsg_type').val('Type');
                $('#tmsg_msg').val('Contenu.............................................');
                $('#tmsg_periodicite').val('Trimestrielle');
                $('#ins_periodicite').val('');
                $('#ins_description').val('Description');
                /*},
                 error: function () {
                 alert("Erreur rencontrer lors de la recuperation des donnees!");
                 }
                 });*/
            }
        }

        function deleteAlerte(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement l'alerte : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        /*$.ajax({
                         url: "{{url('/deleteModele')}}/" + id,
                         type: "get",
                         data: {ale_id: id, '_token': '{{csrf_token()}}'},
                         success: function (data) {
                         if (data == 0) {
                         swal({
                         title: 'Attention',
                         text: "Impossible de supprimer cette alerte, elle est rattachée à certaines données !",
                         type: 'info',
                         button: "OK"
                         });
                         } else {
                         swal({
                         title: "Succes!",
                         text: "Suppression de l'alerte effectuée avec succès!",
                         type: "success",
                         button: "OK"
                         });
                         location.reload();
                         }
                         },
                         error: function (data) {
                         swal({
                         title: 'Erreur',
                         text: "Une erreure est survenue lors de la suppression de l'alerte!\nVeuillez reessayer!",
                         type: 'error',
                         button: "OK"
                         });
                         }
                         });*/
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddAlerte">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="ale_id" name="tmsg_id" type="hidden">
                            <input id="ses_id" name="ses_id" type="hidden">

                            <div class="form-group col-md-4">
                                <label for="tmsg_id">Mod&egrave;le <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="tmsg_id" name="tmsg_id"
                                        required>
                                    <option value="null">Modèle...</option>
                                    <option value="Préparation">Modèle 1</option>
                                    <option value="Suivi">Modèle 2</option>
                                    <option value="Autre">Modèle 3</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="ale_tmsg_type">Type <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="ale_tmsg_type"
                                        name="ale_tmsg_type" required>
                                    <option value="null">Type...</option>
                                    <option value="Préparation">Préparation</option>
                                    <option value="Suivi">Suivi</option>
                                    <option value="Autre">Autre</option>
                                </select>
                            </div>

                            {{--<div class="input-append date form_datetime col-md-4">
                                <input class="form-control form-control-sm" size="16" type="text" value="" readonly>
                                <span class="add-on"><i class="fa fa-calendar-day"></i></span>
                            </div>--}}

                            <div class="form-group col-md-4">
                                <label for="ale_date_heure_prev">Date pr&eacute;vue <span
                                            class="requit">*</span></label>
                                <input type="date" class="form-control form-control-sm" id="ale_date_heure_prev"
                                       name="ale_date_heure_prev" required {{--readonly--}}/>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="ale_msg">Contenu <span class="requit">*</span></label>
                                <textarea class="form-control" name="ale_msg" id="ale_msg" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
