<!-- Ses Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_id', 'Ses Id:') !!}
    {!! Form::number('ses_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tmsg Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tmsg_id', 'Tmsg Id:') !!}
    {!! Form::number('tmsg_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ale Msg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ale_msg', 'Ale Msg:') !!}
    {!! Form::text('ale_msg', null, ['class' => 'form-control']) !!}
</div>

<!-- Ale Date Heure Prev Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ale_date_heure_prev', 'Ale Date Heure Prev:') !!}
    {!! Form::text('ale_date_heure_prev', null, ['class' => 'form-control','id'=>'ale_date_heure_prev']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#ale_date_heure_prev').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Ale Etat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ale_etat', 'Ale Etat:') !!}
    {!! Form::number('ale_etat', null, ['class' => 'form-control']) !!}
</div>

<!-- Ale Tmsg Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ale_tmsg_type', 'Ale Tmsg Type:') !!}
    {!! Form::text('ale_tmsg_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Ale Date Heure Envoi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ale_date_heure_envoi', 'Ale Date Heure Envoi:') !!}
    {!! Form::text('ale_date_heure_envoi', null, ['class' => 'form-control','id'=>'ale_date_heure_envoi']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#ale_date_heure_envoi').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('alertes.index') }}" class="btn btn-secondary">Cancel</a>
</div>
