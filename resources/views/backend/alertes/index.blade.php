@extends('layouts.template')
@section('title')
    Alertes
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui-timepicker-addon.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Alerte</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des alertes <span style="color:red">(selon le filtre)</span></div>
                            </div>
                            <div class="widget-content-right">

                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddAlerte(null)"
                                        data-toggle="modal" data-target="#modalAddAlerte">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle alerte
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-title">Filtre</div>
                        <form class="needs-validation" novalidate method="GET" action="{{route('alertSearch')}}"
                              id="formFilter">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <input type="hidden" name="instance_id" id="instance_id" value="{{$instance_id}}">
                                <input type="hidden" name="session_id" id="session_id" value="{{$session_id}}">
                                <div class="from-group col-lg-3 col-xl-3">
                                    <select class="form-control form-control-sm selectSearch" id="instance_f"
                                            name="instance_f">
                                        <option value=null>Instances...</option>
                                    </select>
                                </div>
                                <div class="from-group col-lg-3 col-xl-3">
                                    <select class="form-control form-control-sm selectSearch" id="session_f"
                                            name="session_f">
                                        <option value=null>Sessions...</option>

                                    </select>
                                </div>
                            {{--{{  dd(intval($annee)) }}--}}
                                <div class="from-group col-lg-3 col-xl-3">
                                    <button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
                                        <i class="fa fa-search"></i>&nbsp;Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive" style="min-height: 45vh;">
                                    <table class="table table-bordered table-sm table-striped table-hover dt_example">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>#</th>
                                            <th width="45%">Contenu de la notification</th>
                                            <th>Date et heure prévue</th>
                                            <th>Etat</th>
                                            <th>Modèle</th>
                                            <th>Date et heure dernier envoi</th>
                                            <th class="text-center no-sort">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($alertes))
                                            @foreach($alertes as $number=>$alertes)
                                                <tr>
                                                    <td>{{ $number +1 }}</td>
                                                    <td>{{ $alertes->ale_msg }}</td>
                                                    <td>{{ $alertes->ale_date_heure_prev->format("d/m/Y à H:i:s") }}</td>
                                                    @if($alertes->ale_etat>0)
                                                     <td>
														Envoyé <span class="badge badge-pill badge-success">			{{$alertes->ale_etat}}
														</span>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <span class="badge badge-pill badge-danger">Non envoyé</span>
                                                        </td>
                                                    @endif

                                                    <td>{{ $alertes->ale_tmsg_type }}</td>
                                                    @if($alertes->ale_date_heure_envoi==null)
                                                        <td>-</td>
                                                    @else
                                                        <td>{{$alertes->ale_date_heure_envoi->format("d/m/Y à H:i:s")}}</td>
                                                    @endif

                                                    <td>
                                                        <div class='btn-group'>

                                                            <button type="button"
                                                                    class="btn btn-xs btn-primary dropdown-toggle"
                                                                    data-toggle="dropdown"
                                                                    aria-haspopup="true" aria-expanded="false">
                                                                Actions
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                @if($alertes->ale_etat <= 0)
                                                                    <button class='dropdown-item'
                                                                            onclick="openModalModAlerte({{ $alertes->ale_id }})"

                                                                            data-placement="top"
                                                                            title="Modifier l'alerte"
                                                                            data-toggle="modal"
                                                                            data-target="#modalModAlerte">
                                                                        <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                                                    </button>
                                                                    <button class='dropdown-item'
                                                                            onclick="deleteAlerte({{$alertes->ale_id}},'{{$alertes->ale_id}}')"
                                                                            data-placement="top"
                                                                            title="Supprimer l'alerte">

                                                                        <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                                                    </button>
                                                                @else
                                                                    <a href="{{route('listMessage',['id'=>$alertes->ale_id])}}"
                                                                       class='dropdown-item btn'
                                                                       data-placement="top"
                                                                       title="Consulter les messages">
                                                                        <i class="fa fa-envelope"></i>&nbsp;&nbsp;Liste
                                                                        d'envois
                                                                    </a>
                                                                @endif
                                                                @if(($alertes->ale_etat>0))
                                                                    @if( App\Http\Controllers\MethodesStaticController::verifyEmailNonEnvoyeByAlerte($alertes->ale_id))
                                                                        <button class='dropdown-item'
                                                                                onclick="renvoiAlerte({{$alertes->ale_id}})"
                                                                                data-placement="top"
                                                                                title="Renvoyer les alertes dont l'envoi par email à échoué">
                                                                            <i class="fa fa-forward "></i>&nbsp;&nbsp;Renvoyer
                                                                            <span class="badge badge-pill badge-warning">{{App\Http\Controllers\MethodesStaticController::verifyEmailNonEnvoyeByAlerte($alertes->ale_id)}}</span>
                                                                        </button>
                                                                    @endif
                                                                @endif
                                                                @if(\Carbon\Carbon::parse($alertes->ale_date_heure_prev)->lte(\Carbon\Carbon::now()))

                                                                    <button class='dropdown-item'
                                                                            onclick="envoiAlerte({{$alertes->ale_id}})"
                                                                            data-placement="top"
                                                                            title="Envoyer l'alerte">
                                                                        <i class="fa fa-forward"></i>&nbsp;&nbsp;Envoyer
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date-french.js') }}" type="text/javascript"></script>


    <script src="{{ url('backend/js/jquery.maxlength.min.js') }}" type="text/javascript"></script>

    <script>


        $(document).ready(function () {
            var instance_id = $('#instance_id').val();
            var session_id = $('#session_id').val();

            $.get("{{ url("/getAllInstances") }}", function (data) {
                $.each(data, function (index, d) {
                    $('#instance').append('<option value="' + d.ins_id + '">' + d.ins_sigle + '-' + d.ins_nom_complet + '</option>');
                    $('#instance_m').append('<option value="' + d.ins_id + '">' + d.ins_sigle + '-' + d.ins_nom_complet + '</option>');
                    $('#instance_f').append('<option value="' + d.ins_id + '">' + d.ins_sigle + '-' + d.ins_nom_complet + '</option>');
                });
                // $('#instance_f').val(instance_id);
            });

            $('#session').empty();
            $('#instance').on('change', function () {
                $('#session').empty();
                var val = this.value;
                $.get("{{ url("/instance/getSession") }}/" + val, function (data) {
                    $.each(data, function (index, d) {
                        $('#session').append('<option value="' + d.ses_id + '">' + d.ses_description + '</option>');
                    })
                });
            });

            $('#instance_f').on('change', function () {
                // $('#session_f').empty();
                var val = this.value;
                $.get("{{ url("/instance/getSession") }}/" + val, function (data) {
                    $.each(data, function (index, d) {
                        $('#session_f').append('<option value="' + d.ses_id + '">' + d.ses_description + '</option>');
                    });
                    $('#session_f').val(session_id);
                });
            });


            //$('#fil_session').empty();

            $.get("{{ url("/getSession") }}", function (data) {
                console.log(data);
                $.each(data, function (index, d) {

                    $('#fil_session').append('<option value="' + d.ses_description + '">' + d.ses_description + '</option>');

                })
            });

            // $('#ale_tmsg_type').empty();
            $.get("{{ url("/getModele") }}", function (data) {
                console.log(data);
                $.each(data, function (index, d) {

                    $('#ale_tmsg_type').append('<option value="' + d.tmsg_id + '|' + d.tmsg_type + '">' + d.tmsg_type + '</option>');

                })
            });
            //modifier alerte


            $('#instance_m').on('change', function () {
                $('#session_m').empty();
                var val = this.value;
                $.get("{{ url("/instance/getSession") }}/" + val, function (data) {

                    console.log(data);
                    $.each(data, function (index, d) {

                        $('#session_m').append('<option value="' + d.ses_id + '">' + d.ses_description + '</option>');

                    })
                });

            });


            // $('#ale_tmsg_type').empty();
            $.get("{{ url("/getModele") }}", function (data) {
                console.log(data);
                $.each(data, function (index, d) {

                    $('#ale_tmsg_type_m').append('<option value="' + d.tmsg_id + '|' + d.tmsg_type + '">' + d.tmsg_type + '</option>');

                })
            });


            //fin modifier alerte


        })
    </script>
    <script>

        $(document).ready(function () {
            $('#ale_tmsg_type').on('change', function () {
                var id = this.value;

                $.get("{{ url("/getMsg") }}/" + id, function (data) {
                    $('#ale_msg').val(data.tmsg_msg);
                });

                var tab = $('#ale_tmsg_type').val().split('|');
                var ins_id = $('#instance').val();
                if (tab[1] == 'AUTRE') {
                    $("#ale_destinataire_field").show();
                    $.get("{{ url("/getStructuresByInstance") }}/" + ins_id, function (data) {
                        $.each(data, function (index, d) {
                            $('#ale_destinataire').append('<option value="' + d.str_id + '">' + d.str_sigle + ' - ' + d.str_nom_complet + '</option>');
                        })
                    });
                }
                else {
                    $("#ale_destinataire_field").hide();
                    $("#ale_destinataire").empty();
                }

            });
        });


        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            viewMode: 'years',
            format: 'YYYY',
//            maxDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right"

            }
        }).parent().css("position :relative");

        function openModalAddAlerte(id) {
            $('#session').select2({
                placeholder: "Choisir...",
                allowClear: true,
                dropdownParent: $('#modalAddAlerte'),
                width: '100%'
            });
            $('#instance').select2({
                placeholder: "Choisir...",
                allowClear: true,
                dropdownParent: $('#modalAddAlerte'),
                width: '100%'
            });
            $("#ale_destinataire_field").hide();
            //$('#ale_msg').val('boom');

            $("#ale_date_heure_prev").datepicker({
                minDate: new Date(), dateFormat: 'dd/mm/yy',
                showSecond: true,
                timeFormat: 'hh:mm:ss',

            });

            $.datepicker.regional["fr"];

            $('#formAddAlerte').attr('action', 'alertes/save');
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle alerte');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');

            }
        }

        /*
        $('#formAddAlerte').on('submint', function () {
            checkFormData();
        });

        function checkFormData() {

            if ($("#ale_date_heure_prev").val()==null) {
                swal({
                    title: 'Attention',
                    text: "Veuillez définir la date d'envoi prévue pour le message SVP!",
                    type: 'info',
                    button: "OK"
                });
                return false;
            }
        }
        */

        function openModalModAlerte(id) {

            $('.selectSearch').select2({
                placeholder: "Choisir...",
                allowClear: true,
                dropdownParent: $('#modalModAlerte'),
                width: '100%'
            });
            $("#ale_destinataire_field_m").hide();


            $("#ale_date_heure_prev_m").datetimepicker({
                minDate: new Date(), dateFormat: 'dd/mm/yy',
            });

            $.datepicker.regional["fr"];

            $('#formModAlerte').attr('action', 'alerte/updating/' + id);
            $.ajax({
                url: "{{ url("/getAlerte") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('.modal-title').text('Modification de l\'alerte');
                    $('#btnSave_m').empty();
                    $('#btnSave_m').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                    $('#ale_tmsg_type_m').val(data.p).trigger('change');
                    $('#ale_msg_m').val(data.ale_msg);
                    $('#ale_date_heure_prev_m').val(data.ale_date_heure_prev);
                    $('#mail_subject_m').val(data.mail_subject_m);
                    $('#instance_m').val(data.ins_id).trigger('change');

                    var tab = $('#ale_tmsg_type_m').val().split('|');
                    var ins_id = $('#instance_m').val();
                    if (tab[1] == 'AUTRE') {
                        $("#ale_destinataire_field_m").show();
                        $.get("{{ url("/getStructuresByInstance") }}/" + ins_id, function (data) {
                            $("#ale_destinataire_m").empty();
                            $.each(data, function (index, d) {
                                $('#ale_destinataire_m').append('<option value="' + d.str_id + '">' + d.str_sigle + ' - ' + d.str_nom_complet + '</option>');
                            });
                        });
                    }
                    else {
                        $("#ale_destinataire_field_m").hide();
                        $("#ale_destinataire_m").empty();
                        //console.log($("#ale_destinataire_m").val());
                    }
                    setTimeout(function() {
                        $('#ale_destinataire_m').val([]).change();
                        $('#ale_destinataire_m').val(data.ale_destinataire).trigger('change');
                    }, 2000);

                    console.log(data.ale_destinataire);
                    /*
                    var tab = $('#ale_tmsg_type_m').val().split('|');
                    if (tab[1]=='AUTRE')
                    {
                        $('#ale_destinataire_m').val(data.ale_destinataire);
                    }*/
                }
            });

            $('#rechargez').on('click', function () {
                $.ajax({
                    url: "{{ url("/getAlerte") }}/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {

                        //$('#session').val(data.ses_id).trigger('change');
                        $('#ale_msg_m').val(data.ale_msg);
                        $('#ale_tmsg_type_m').val(data.p).trigger('change');
                    }
                });
            });

            $('#ale_tmsg_type_m').on('change', function () {
                var id = this.value;

                $.get("{{ url("/getMsg") }}/" + id, function (data) {
                    $('#ale_msg_m').val(data.tmsg_msg);
                });

                var tab = $('#ale_tmsg_type_m').val().split('|');
                var ins_id = $('#instance_m').val();
                if (tab[1] == 'AUTRE') {
                    $("#ale_destinataire_field_m").show();
                    $.get("{{ url("/getStructuresByInstance") }}/" + ins_id, function (data) {
                        $.each(data, function (index, d) {
                            $('#ale_destinataire_m').append('<option value="' + d.str_id + '">' + d.str_sigle + ' - ' + d.str_nom_complet + '</option>');
                        })
                    });
                }
                else {
                    $("#ale_destinataire_field_m").hide();
                    $("#ale_destinataire_m").empty();
                    console.log($("#ale_destinataire_m").val());
                }

            });

        }


        function deleteAlerte(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer l'alerte définitivement",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteAlerte')}}/" + id,
                            type: "get",
                            data: {ale_id: id, '_token': '{{csrf_token()}}'},
                            dataType: "JSON",
                            success: function (data) {

                                if (data.message == 'error') {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cette alerte, l'alerte est deja envoyée !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression de l'alerte effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    });
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression de l'alerte!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
        function renvoiAlerte(id) {

            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir renvoyer l'alerte?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui!',
                cancelButtonText: 'Non',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                //url:

                //allowOutsideClick: false
            }).then(function () {
                window.location.href = "{{ url("/resendAlerte") }}/" + id;
            })
            ;
        }

        function envoiAlerte(id) {

            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir envoyer l'alerte?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui!',
                cancelButtonText: 'Non',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                //url:

                //allowOutsideClick: false
            }).then(function () {
                window.location.href = "{{ url("/sendAlerte") }}/" + id;
            })
            ;
        }

        function openModalDetailAlerte(id) {
            $.ajax({
                url: "{{url('/getAlerte')}}/" + id,
                type: "get",
                success: function (data) {
                    $('.modal-title').text("Détails de l'alerte : " + data.ale_tmsg_type);
                    $('#message').text(data.ale_msg);
                    $('#heureP').text(data.ale_date_heure_prev);
                    if (data.ale_date_heure_envoi == null) {
                        $('#heureE').text('-');
                    }
                    else {
                        $('#heureE').text(data.ale_date_heure_envoi);
                    }

                    if (data.ale_etat == 0) {
                        $('#etat').html('<span class="badge badge-pill badge-danger">Non envoyé</span>');
                    }
                    else {
                        $('#etat').html('<span class="badge badge-pill badge-success">Envoyé</span>');
                    }


                    $('#type').text(data.ale_tmsg_type);
                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données du modele!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }

            });
        }
        function openModalDetailMessage(id) {
            $.ajax({
                url: "{{url('/ConsulterMessage/')}}/" + id,
                type: "get",
                success: function (data) {
                    $.each(data, function (index, d) {
                        $('.modal-title').text("Détails de l'alerte : " + data.type);
                        $('#ena_isenvoi').text(data.ena_isenvoi);
                        $('#ena_ses_description').text(data.ena_ses_description);
                        $('#ena_is_lu').text(data.ena_is_lu);

                        $('#ena_ses_annee').text(data.ena_ses_annee);
                        $('#ena_date_heure_recu').text(data.ena_date_heure_recu);

                        $('#ena_ses_lieu').text(data.ena_ses_lieu);
                        $('#ena_msg').text(data.ena_msg);
                    })

                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données du modele!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }

            });
        }

    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation"  method="POST" action="#" id="formAddAlerte">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!--<input id="ale_id" name="tmsg_id" type="hidden">
                            <input id="ses_id" name="ses_id" type="hidden"> -->
                            <div class="form-group col-md-3">
                                <label for="instance">Instance <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="instance" name="instance"
                                        required>
                                    <option value="null">Choisir...</option>

                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="session">Session <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="session" name="session"
                                        required>
                                    <option value="null">Session...</option>

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="ale_tmsg_type">Modèle <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="ale_tmsg_type"
                                        name="ale_tmsg_type" required>
                                    <option value="null">Choisir...</option>

                                </select>
                            </div>

                            {{--<div class="input-append date form_datetime col-md-4">
                                <input class="form-control form-control-sm" size="16" type="text" value="" readonly>
                                <span class="add-on"><i class="fa fa-calendar-day"></i></span>
                            </div>--}}

                            <div class="form-group col-md-3">
                                <label for="ale_date_heure_prev">Première date d'envoi pr&eacute;vue <span
                                            class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="ale_date_heure_prev"
                                       name="ale_date_heure_prev" readonly="readonly" required="true"/>
                            </div>

                            <div class="form-group col-md-12" id="ale_destinataire_field">
                                <label for="ale_destinataire">Choisir les structures des destinataires</label>
                                <select id="ale_destinataire" name="ale_destinataire[]"
                                        class="form-control form-control-sm selectSearch"
                                        multiple>

                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="ale_msg">Objet du message <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="mail_subject"
                                       name="mail_subject" required {{--readonly--}}/>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="ale_msg">Contenu du message <span class="requit">*</span></label>
                                <textarea class="form-control" name="ale_msg" id="ale_msg" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--modal modify-->
    <div class="modal fade my-5" id="modalModAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formModAlerte">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!--<input id="ale_id" name="tmsg_id" type="hidden">
                            <input id="ses_id" name="ses_id" type="hidden"> -->
                            <div class="form-group col-md-3">
                                <label for="session">Instance <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="instance_m"
                                        name="instance_m"
                                        required>


                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="session">Session <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="session_m"
                                        name="session_m"
                                        required>
                                    <option value="null">Session...</option>

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="ale_tmsg_type">Modèle <span class="requit">*</span></label>
                                <select size="1" class="form-control form-control-sm selectSearch" id="ale_tmsg_type_m"
                                        name="ale_tmsg_type_m" required>
                                    <option value="null" disabled>Type...</option>

                                </select>
                            </div>
                            {{--<div class="input-append date form_datetime col-md-4">
                                <input class="form-control form-control-sm" size="16" type="text" value="" readonly>
                                <span class="add-on"><i class="fa fa-calendar-day"></i></span>
                            </div>--}}

                            <div class="form-group col-md-3">
                                <label for="ale_date_heure_prev">Date-Heure pr&eacute;vue d'envoi <span
                                            class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="ale_date_heure_prev_m"
                                       name="ale_date_heure_prev_m" readonly="readonly" required {{--readonly--}}/>
                            </div>

                            <div class="form-group col-md-12" id="ale_destinataire_field_m">
                                <label for="ale_destinataire">Choisir les structures des destinataires</label>
                                <select id="ale_destinataire_m" name="ale_destinataire_m[]"
                                        class="form-control form-control-sm selectSearch"
                                        multiple>

                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="ale_msg">Objet du message <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="mail_subject_m"
                                       name="mail_subject_m" required {{--readonly--}}/>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="ale_msg">Contenu du message<span class="requit">*</span></label>
                                <textarea class="form-control" name="ale_msg_m" id="ale_msg_m" rows="4"
                                          required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button class="btn btn-warning" type="button" id="rechargez">Réinitialiser</button>
                        <button type='submit' class="btn btn-sm btn-success mp" id="btnSave_m"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!--Modele detail-->
    <div class="modal fade my-5" id="modalDetailAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <tbody id="contentActeurs">
                        <tr>
                            <th>Contenu du message</th>
                            <td id="message"></td>
                        </tr>

                        <tr>
                            <th>Heure prevue de l'envoi</th>
                            <td id="heureP"></td>
                        </tr>
                        <tr>
                            <th>Heure d'envoi du message</th>
                            <td id="heureE"></td>
                        </tr>
                        <tr>
                            <th>Etat du message</th>
                            <td id="etat"></td>

                        <tr>
                            <th>Type du modèle de message</th>
                            <td id="type"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!--modal de message-->

    <div class="modal fade my-5" id="modalDetailMessage" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <tbody id="contentActeurs">
                        <tr>
                            <th>date reception</th>
                            <td id='ena_date_heure_recu'></td>
                        </tr>
                        <tr>
                            <th>Message Lu</th>
                            <td id="ena_is_lu"></td>
                        </tr>
                        <tr>
                            <th>Description de la session</th>
                            <td id="ena_ses_description"></td>
                        <tr>
                            <th>Debut de la session</th>
                            <td id="ena_ses_debut"></td>
                        </tr>
                        <tr>
                            <th>Année de la session</th>
                            <td id="ena_ses_annee"></td>
                        </tr>
                        <tr>
                            <th>Lieu de la session</th>
                            <td id="ena_ses_lieu"></td>
                        </tr>
                        <tr>
                            <th>Contenu du message</th>
                            <td id="ena_msg"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                </div>
            </div>
        </div>
    </div>

@endsection
