<!-- Ses Id Field -->
<div class="form-group">
    {!! Form::label('ses_id', 'Ses Id:') !!}
    <p>{{ $alertes->ses_id }}</p>
</div>

<!-- Tmsg Id Field -->
<div class="form-group">
    {!! Form::label('tmsg_id', 'Tmsg Id:') !!}
    <p>{{ $alertes->tmsg_id }}</p>
</div>

<!-- Ale Msg Field -->
<div class="form-group">
    {!! Form::label('ale_msg', 'Ale Msg:') !!}
    <p>{{ $alertes->ale_msg }}</p>
</div>

<!-- Ale Date Heure Prev Field -->
<div class="form-group">
    {!! Form::label('ale_date_heure_prev', 'Ale Date Heure Prev:') !!}
    <p>{{ $alertes->ale_date_heure_prev }}</p>
</div>

<!-- Ale Etat Field -->
<div class="form-group">
    {!! Form::label('ale_etat', 'Ale Etat:') !!}
    <p>{{ $alertes->ale_etat }}</p>
</div>

<!-- Ale Tmsg Type Field -->
<div class="form-group">
    {!! Form::label('ale_tmsg_type', 'Ale Tmsg Type:') !!}
    <p>{{ $alertes->ale_tmsg_type }}</p>
</div>

<!-- Ale Date Heure Envoi Field -->
<div class="form-group">
    {!! Form::label('ale_date_heure_envoi', 'Ale Date Heure Envoi:') !!}
    <p>{{ $alertes->ale_date_heure_envoi }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $alertes->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $alertes->updated_by }}</p>
</div>

