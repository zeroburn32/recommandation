<div class="table-responsive-sm">
    <table class="table table-striped dt_exemple" id="alertes-table">
        <thead>
            <th>#</th>

        <th>Message</th>
        <th>Heure Prevue</th>
        <th>Etat</th>
        <th>Type</th>
        <th>Heure d'envoie</th>

            <th>Action</th>

        </thead>
        <tbody>
        @php($i=0)
        @foreach($alertes as $alertes)

            @php($i=$i+1)
            <tr>
                <td>{{ $i}}</td>
            <td>{{ $alertes->ale_msg }}</td>
            <td>{{ $alertes->ale_date_heure_prev }}</td>
            <td><span class="badge badge-pill badge-warning">{{$alertes->etat}}</span></td>
            <td>{{ $alertes->ale_tmsg_type }}</td>
            <td>{{ $alertes->ale_date_heure_envoi }}</td>

                <td>

                    <div class='btn-group'>

                            <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>

                            <div class="dropdown-menu">
                                <button class='dropdown-item'
                                        data-toggle="modal" data-target="#modalDetailAlerte"  data-placement="top" title="Voir détail" onclick="openModalDetailAlerte({{ $alertes->ale_id }})">
                                    <i class="fa fa-eye"></i>&nbsp;&nbsp;Voir détails
                                </button>

                                <button class='dropdown-item' onclick="openModalAddAlerte({{ $alertes->ale_id }})"

                                        data-placement="top" title="modifier l'alerte"    data-toggle="modal" data-target="#modalAddAlerte">
                                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                </button>
                                <button class='dropdown-item' onclick="deleteAlerte({{$alertes->ale_id}},'{{$alertes->ale_id}}')"
                                        data-placement="top" title="Supprimer l'alerte">

                                    <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                </button>
                                <button class='dropdown-item' onclick="deleteModele()"
                                        data-toggle="tooltip" data-placement="top" title="Envoyer l'alerte">

                                    <i class="fa fa-forward"></i>&nbsp;&nbsp;Envoyer
                                </button>
                            </div>
                        </div>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>