<!-- Cat Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cat_libelle', 'Cat Libelle:') !!}
    {!! Form::text('cat_libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Cat Is Ministere Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cat_is_ministere', 'Cat Is Ministere:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('cat_is_ministere', 0) !!}
        {!! Form::checkbox('cat_is_ministere', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categories.index') }}" class="btn btn-secondary">Cancel</a>
</div>
