@extends('layouts.template')

@section('title')
Catégories de structure
@parent
@stop
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/select2.min.css') }}" />
@stop

@section('content')
<div class="container-fluid">
    <div class="card mb-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('structures.index') }}">Structures</a></li>
                <li class="breadcrumb-item active" aria-current="page">Catégories</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Liste des catégories de structure</div>
                        </div>
                        <div class="widget-content-right">
                            @can('GERER_STRUCTURE')
                            <button type="button" class="btn btn-sm btn-primary" onclick="openModalAddStructure(null)" data-toggle="modal"
                                data-target="#modalAddStructure">
                                <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle Catégorie
                            </button>
                            @endcan
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row my-2">
                <div class="col-lg-12 col-xl-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 45vh;">
                                @include('backend.categories.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
<script>
    $('.selectSearch').select2({
                width: '100%',
                placeholder: "Choisir...",
                allowClear: true
            });

        function openModalAddStructure(id) {
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAddStructure"),
                placeholder: "Choisir...",
                allowClear: true
            });
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle catégorie');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#cat_id').val(null);
                }
            else {
                $.ajax({
                url: APP_URL+"/getCategorie/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                $('.modal-title').html("Modification de la catégorie <span style='color:maroon;'>"+data.str_sigle+ "</span>");
                $('#btnSave').empty();
                $('#btnSave').text('Modifier');
                $('#cat_id').val(data.cat_id);
                $('#cat_libelle').val(data.cat_libelle);
                $('#cat_is_ministere').val(data.cat_is_ministere).trigger('change');
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
            }
        }
        function deleteStructure(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement la catégorie : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                        url: "{{url('/deleteCategorie')}}/" + id,
                        type: "get",
                        data: {cat_id: id, '_token': '{{csrf_token()}}'},
                        success: function (data) {
                        if (data == 0) {
                            swal({
                            title: 'Attention',
                            text: "Impossible de supprimer cette catégorie, elle est rattachée à certaines données !",
                            type: 'info',
                            button: "OK"
                            }).then((result) => {
                               
                                });
                        } else {
                            swal({
                            title: "Succes!",
                            text: "Suppression de la catégorie effectuée avec succès!",
                            type: "success",
                            button: "OK"
                            }).then((result) => {
                            location.reload();
                            });
                            
                        }
                        },
                        error: function (data) {
                            swal({
                            title: 'Erreur',
                            text: "Une erreure est survenue lors de la suppression de la catégorie!\nVeuillez reessayer!",
                            type: 'error',
                            button: "OK"
                            }).then((result) => {
                            //location.reload();
                            });;
                        }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
</script>
@endsection

@section('modal')
{{--MODAL AJOUT & EDIT structure--}}
<div class="modal fade my-5" id="modalAddStructure" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" action="{{route('categories.store')}}"
                id="formAddStructure">
                <div class="modal-body" style="overflow:hidden;">
                    <div class="row justify-content-center">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="cat_id" name="cat_id" type="hidden">
                        <div class="form-group col-md-4">
                            <label for="str_sigle">Libellé catégorie <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="cat_libelle" name="cat_libelle"
                                maxlength="50" required />
                        </div>

                        <div class="form-group col-md-4" title="La catégorie concerne-t-elle des structures du ministères ?">
                            <label for="str_categorie"  >Catégorie ministère ?<span class="requit">*</span></label>
                            <select class="form-control form-control-sm selectSearch" id="cat_is_ministere"
                                name="cat_is_ministere" required>
                                <option value="1">Oui</option>
                                <option value="0">Non</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i
                            class="fa fa-ban"></i>&nbsp;Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

