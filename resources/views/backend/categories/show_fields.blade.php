<!-- Cat Libelle Field -->
<div class="form-group">
    {!! Form::label('cat_libelle', 'Cat Libelle:') !!}
    <p>{{ $categories->cat_libelle }}</p>
</div>

<!-- Cat Is Ministere Field -->
<div class="form-group">
    {!! Form::label('cat_is_ministere', 'Cat Is Ministere:') !!}
    <p>{{ $categories->cat_is_ministere }}</p>
</div>

