<div class="table-responsive-sm">
    <table class="table table-striped table-sm dt_example" id="categories-table">
        <thead>
            <tr>
                <th>Libelle</th>
                <th>Est Ministère ?</th>
                @can('GERER_STRUCTURE')
                <th>Action</th>
                @endcan
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $categories)
            <tr>
                <td>{{ $categories->cat_libelle }}</td>
            <td>@if($categories->cat_is_ministere == 1) Oui 
                @else Non @endif
            </td>
            @can('GERER_STRUCTURE')
            <td class="text-center">
                <a href="#" onclick="openModalAddStructure({{$categories->cat_id}})" data-toggle="modal" data-target="#modalAddStructure" class='btn btn-sm btn-primary'>
                    <i class="fa fa-edit"></i>
                </a>
                <button onclick="deleteStructure({{$categories->cat_id}}, '{{addslashes(" Libellé " . $categories->cat_libelle)}}')" class='btn btn-sm btn-danger'>
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            @endcan
            </tr>
        @endforeach
        </tbody>
    </table>
</div>