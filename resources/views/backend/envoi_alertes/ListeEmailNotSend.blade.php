@extends('layouts.template')
@section('title')
    Alertes
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui-timepicker-addon.css') }}"/>

@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Alertes</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des alertes</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des emails non envoyés</div>
                            </div>
                            <div class="widget-content-right">

                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddAlerte(null)"
                                        data-toggle="modal" data-target="#modalAddAlerte">
                                    <i class="fa fa-forward"></i>&nbsp;&nbsp;Tout renvoyer
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">


                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive" style="min-height: 45vh;">
                                    <table class="table table-bordered table-sm table-striped table-hover dt_example">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>#</th>
                                            <th>Contenu du message</th>
                                            <th>Objet du message</th>
                                            <th>Recepteur</th>
                                            <th class="text-center no-sort">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($alertes))
                                            @foreach($alertes as $number=>$alertes)
                                                <tr>
                                                    <td>{{ $number +1 }}</td>
                                                    <td>{{ $alertes->ale_msg }}</td>
                                                    <td>{{ $alertes->mail_subject }}</td>
                                                    <td>{{ $alertes->lia_email }}</td>
                                                    <td>
                                                        <div class='btn-group'>

                                                            <button type="button"
                                                                    class="btn btn-xs btn-primary dropdown-toggle"
                                                                    data-toggle="dropdown"
                                                                    aria-haspopup="true" aria-expanded="false">
                                                                Actions
                                                            </button>
                                                            <div class="dropdown-menu">
                                                            <!--<button class='dropdown-item'
                                                                    data-toggle="modal" data-target="#modalDetailAlerte"  data-placement="top" title="Voir détail" onclick="openModalDetailAlerte({{ $alertes->ale_id }})">
                                                                <i class="fa fa-eye"></i>&nbsp;&nbsp;Voir détails
                                                            </button> -->

                                                                    <button class='dropdown-item'
                                                                            onclick="openModalModAlerte({{ $alertes->ale_id }})"

                                                                            data-placement="top"
                                                                            title="modifier l'alerte"
                                                                            data-toggle="modal"
                                                                            data-target="#modalModAlerte">
                                                                        <i class="fa fa-edit"></i>&nbsp;&nbsp;Renvoyer l'email
                                                                    </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/lightpick.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date-french.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/jquery-ui-timepicker-addon.js') }}" type="text/javascript"></script>

    <script>

    </script>
    <script>


    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddAlerte">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!--<input id="ale_id" name="tmsg_id" type="hidden">
                            <input id="ses_id" name="ses_id" type="hidden"> -->
                            <div class="form-group col-md-3">
                                <label for="session">Instance <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="instance" name="instance"
                                        required>
                                    <option value="null">Choisir...</option>

                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="session">Session <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="session" name="session"
                                        required>
                                    <option value="null">Session...</option>

                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="ale_tmsg_type">Modèle <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="ale_tmsg_type"
                                        name="ale_tmsg_type" required>
                                    <option value="null">Choisir...</option>

                                </select>
                            </div>

                            {{--<div class="input-append date form_datetime col-md-4">
                                <input class="form-control form-control-sm" size="16" type="text" value="" readonly>
                                <span class="add-on"><i class="fa fa-calendar-day"></i></span>
                            </div>--}}

                            <div class="form-group col-md-3">
                                <label for="ale_date_heure_prev">Date pr&eacute;vue d'envoi <span
                                            class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="ale_date_heure_prev"
                                       name="ale_date_heure_prev" readonly="readonly" required {{--readonly--}}/>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ale_msg">Objet du message <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="mail_subject"
                                       name="mail_subject"  required {{--readonly--}}/>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="ale_msg">Contenu du message <span class="requit">*</span></label>
                                <textarea class="form-control" name="ale_msg" id="ale_msg" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection
