<!-- Lia Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_id', 'Lia Id:') !!}
    {!! Form::number('lia_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ale Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ale_id', 'Ale Id:') !!}
    {!! Form::number('ale_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ena Msg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_msg', 'Ena Msg:') !!}
    {!! Form::text('ena_msg', null, ['class' => 'form-control']) !!}
</div>

<!-- Ena Date Heure Recu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_date_heure_recu', 'Ena Date Heure Recu:') !!}
    {!! Form::text('ena_date_heure_recu', null, ['class' => 'form-control','id'=>'ena_date_heure_recu']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#ena_date_heure_recu').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Ena Isenvoi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_isenvoi', 'Ena Isenvoi:') !!}
    {!! Form::number('ena_isenvoi', null, ['class' => 'form-control']) !!}
</div>

<!-- Ena Is Lue Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_is_lue', 'Ena Is Lue:') !!}
    {!! Form::number('ena_is_lue', null, ['class' => 'form-control']) !!}
</div>

<!-- Ena Ses Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_ses_description', 'Ena Ses Description:') !!}
    {!! Form::text('ena_ses_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Ena Ses Annee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_ses_annee', 'Ena Ses Annee:') !!}
    {!! Form::number('ena_ses_annee', null, ['class' => 'form-control']) !!}
</div>

<!-- Ena Ses Debut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_ses_debut', 'Ena Ses Debut:') !!}
    {!! Form::text('ena_ses_debut', null, ['class' => 'form-control','id'=>'ena_ses_debut']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#ena_ses_debut').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Ena Ses Lieu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ena_ses_lieu', 'Ena Ses Lieu:') !!}
    {!! Form::text('ena_ses_lieu', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('envoiAlertes.index') }}" class="btn btn-secondary">Cancel</a>
</div>
