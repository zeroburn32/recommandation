@extends('layouts.template')
@section('title')
    Alertes
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui.min.css') }}"/>

@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Alerte</a></li>
                    <li class="breadcrumb-item">Notifications</li>
					<li class="breadcrumb-item active" aria-current="page">Boîte d'envois</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des messages</div>
                            </div>
                            <div class="widget-content-right">
								<button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                    <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive" style="min-height: 45vh;">
                                    <table class="table table-bordered table-sm table-striped table-hover dt_example">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>#</th>
                                            <th>Recepteur</th>
                                            <th>Structure</th>
                                            <th>Date et heure d'envoi</th>
                                            <th>Etat envoi email</th>
                                            <th colspan="2">Etat lecture</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($msg_alertes))
                                            @php($i=0)
                                            @foreach($msg_alertes as $msg_alerte)
                                                @php($i=$i+1)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{ $msg_alerte->user_nom}} {{ $msg_alerte->user_prenom}} < {{$msg_alerte->user_email}} ></td>
                                                    <td>{{ $msg_alerte->str_sigle}}</td>
                                                    <td>{{ $msg_alerte->ena_date_heure_recu->format("d/m/Y à H:i:s")}}</td>
                                                    <td>
                                                        @if($msg_alerte->ena_isenvoi==1)
                                                            <span class="badge badge-pill badge-success">Envoyé</span>
                                                        @else
                                                            <span class="badge badge-pill badge-warning">Non envoyé</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($msg_alerte->ena_is_lue==1)
                                                            <span class="badge badge-pill badge-success">Lu</span>
                                                        @else
                                                            <span class="badge badge-pill badge-warning">Non lu</span>
                                                        @endif
                                                    </td>
                                                    <td>
														<a href="{{route('ConsulterMessage',['id'=>$msg_alerte->ena_id])}}" class='btn btn-primary'
														   data-placement="top" title="Voir les details du message">
															<i class="fa fa-eye"></i>&nbsp;&nbsp;Details
														</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/lightpick.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date-french.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('#session').empty();
            $.get('/getSession', function (data) {
                console.log(data);
                $.each(data, function (index, d) {

                    $('#session').append('<option value="' + d.ses_id + '">' + d.ses_description+ '</option>');

                })
            });


            //$('#fil_session').empty();
            $.get('/getSession', function (data) {
                console.log(data);
                $.each(data, function (index, d) {

                    $('#fil_session').append('<option value="' + d.ses_description + '">' + d.ses_description+ '</option>');

                })
            });

            $('#ale_tmsg_type').empty();
            $.get('/getModele', function (data) {
                console.log(data);
                $.each(data, function (index, d) {

                    $('#ale_tmsg_type').append('<option value="'+ d.tmsg_id +'|'+ d.tmsg_type +'">' + d.tmsg_type+ '</option>');

                })
            });

        })
    </script>
    <script>
        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            viewMode: 'years',
            format: 'YYYY',
//            maxDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right"

            }
        }).parent().css("position :relative");



        function openModalAddAlerte(id) {
            $("#ale_date_heure_prev").datepicker({
            });
            $.datepicker.regional[ "fr"];

            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAddAlerte"),
                placeholder: "Choisir...",
                allowClear: true
            });
            $('#formAddAlerte').attr('action', 'alertes/save');
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle alerte');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');

            }
            else {
                $('#formAddAlerte').attr('action', 'alerte/updating/' + id);
                $.ajax({
                    url: "/getAlerte/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $('.modal-title').text('Modification de l\'alerte');
                        $('#btnSave').empty();
                        $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                        //$('#session').val();
                        //  var p=data.tmsg_id+'|'+data.ale_tmsg_type;
                        $('#ale_tmsg_type').val(data.p).trigger('change');
                        $('#session').val(data.ses_id).trigger('change');
                        $('#ale_msg').val(data.ale_msg);
                        $('#ale_date_heure_prev').val(data.ale_date_heure_prev);
                        /*},
                         error: function () {
                         alert("Erreur rencontrer lors de la recuperation des donnees!");
                         }
                         });*/
                    }
                })


            }}
        function deleteAlerte(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer l'alerte définitivement",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteAlerte')}}/" + id,
                            type: "get",
                            data: {ale_id: id, '_token': '{{csrf_token()}}'},
                            dataType: "JSON",
                            success: function (data) {

                                if (data.message == 'error') {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cette alerte, l'alerte est deja envoyée !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression de l'alerte effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    });
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression de l'alerte!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
        function envoiAlerte(id, libelle) {

            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de renvoyer l'alerte?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, renvoyer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({

                        });
                        $.ajax({

                            url: "sendAlerte/" + id,
                            type: "get",
                            dataType: "JSON",
                            success: function (data) {

                                if (data.message == 'error') {
                                    swal({
                                        title: 'Attention',
                                        text: "Cette alerte ne peut pas être envoyée!",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Alerte envoyée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    });
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de l'envoi!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
        function openModalDetailAlerte(id) {
            $.ajax({
                url: "{{url('/getAlerte/')}}/" + id,
                type: "get",
                success: function (data) {
                    $('.modal-title').text("Détails de l'alerte : " + data.type);
                    $('#message').text(data.ale_msg);
                    $('#heureP').text(data.ale_date_heure_prev);
                    if(data.ale_date_heure_envoi==null)
                    {
                        $('#heureE').html('<span class="badge badge-pill badge-warning">Non envoyé</span>');
                    }
                    else {
                        $('#heureE').text(data.ale_date_heure_envoi);
                    }


                    $('#etat').text(data.ale_etat);

                    $('#type').text(data.ale_tmsg_type);
                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données du modele!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }

            });
        }
        function openModalDetailMessage(id) {
            $.ajax({
                url: "{{url('/ConsulterMessage/')}}/" + id,
                type: "get",
                success: function (data) {
                    $.each(data, function (index, d) {
                        $('.modal-title').text("Détails de l'alerte : " + data.type);
                        $('#ena_isenvoi').text(data.ena_isenvoi);
                        $('#ena_ses_description').text(data.ena_ses_description);
                        $('#ena_is_lu').text(data.ena_is_lu);

                        $('#ena_ses_annee').text(data.ena_ses_annee);
                        $('#ena_date_heure_recu').text(data.ena_date_heure_recu);

                        $('#ena_ses_lieu').text(data.ena_ses_lieu);
                        $('#ena_msg').text(data.ena_msg);
                    })

                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données du modele!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }

            });
        }
        function openModalDetailAlerteMessage(id) {

            $.ajax({

                url: "{{url('/ConsulterMessage')}}/" + id,
                type: "get",
                success: function (data) {

                        $('.modal-title').text("Détails de l'alerte : " );
                        $('#ena_isenvoi').text(data.ena_isenvoi);

                        $('#ena_ses_description').text(data.ena_ses_description);
                        $('#ena_is_lu').text(data.ena_is_lu);

                        $('#ena_ses_annee').text(data.ena_ses_annee);
                        $('#ena_date_heure_recu').text(data.ena_date_heure_recu);

                        $('#ena_ses_lieu').text(data.ena_ses_lieu);



                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données du modele!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }

            });
        }


    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddAlerte">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!--<input id="ale_id" name="tmsg_id" type="hidden">
                            <input id="ses_id" name="ses_id" type="hidden"> -->

                            <div class="form-group col-md-4">
                                <label for="session">Session <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="session" name="session"
                                        required>
                                    <option value="null">Session...</option>

                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="ale_tmsg_type">Type modèle <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="ale_tmsg_type"
                                        name="ale_tmsg_type" required>
                                    <option value="null">Type...</option>

                                </select>
                            </div>

                            {{--<div class="input-append date form_datetime col-md-4">
                                <input class="form-control form-control-sm" size="16" type="text" value="" readonly>
                                <span class="add-on"><i class="fa fa-calendar-day"></i></span>
                            </div>--}}

                            <div class="form-group col-md-4">
                                <label for="ale_date_heure_prev">Date pr&eacute;vue <span
                                            class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="ale_date_heure_prev"
                                       name="ale_date_heure_prev" required {{--readonly--}}/>
                            </div>


                            <div class="form-group col-md-12">
                                <label for="ale_msg">Contenu <span class="requit">*</span></label>
                                <textarea class="form-control" name="ale_msg" id="ale_msg" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!--Modele detail-->
    <div class="modal fade my-5" id="modalDetailAlerte" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <tbody id="contentActeurs">
                        <tr>
                            <th>Contenu du message</th>
                            <td id="message"></td>
                        </tr>
                        <tr>
                            <th>Heure prevue de l'envoi</th>
                            <td id="heureP"></td>
                        </tr>
                        <tr>
                            <th>Heure d'envoi du message</th>
                            <td id="heureE"></td>
                        </tr>
                        <tr>
                            <th>Etat du message</th>
                            <td id="etat"></td>

                        <tr>
                            <th>Type du modèle de message</th>
                            <td id="type"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!--modal de message-->


    <div class="modal fade my-5" id="modalDetailAlertMessage" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <tbody id="contentActeurs">
                        <tr>
                            <th>date reception</th>
                            <td id='ena_date_heure_recu'></td>
                        </tr>
                        <tr>
                            <th>Message Lu</th>
                            <td id="ena_is_lu"></td>
                        </tr>
                        <tr>
                            <th>Description de la session</th>
                            <td id="ena_ses_description"></td>
                        <tr>
                            <th>Debut de la session</th>
                            <td id="ena_ses_debut"></td>
                        </tr>
                        <tr>
                            <th>Année de la session</th>
                            <td id="ena_ses_annee"></td>
                        </tr>
                        <tr>
                            <th>Lieu de la session</th>
                            <td id="ena_ses_lieu"></td>
                        </tr>
                        <tr>
                            <th>Contenu du message</th>
                            <td id="ena_msg"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                </div>
            </div>
        </div>
    </div>

@endsection
