<!-- Lia Id Field -->
<div class="form-group">
    {!! Form::label('lia_id', 'Lia Id:') !!}
    <p>{{ $envoiAlertes->lia_id }}</p>
</div>

<!-- Ale Id Field -->
<div class="form-group">
    {!! Form::label('ale_id', 'Ale Id:') !!}
    <p>{{ $envoiAlertes->ale_id }}</p>
</div>

<!-- Ena Msg Field -->
<div class="form-group">
    {!! Form::label('ena_msg', 'Ena Msg:') !!}
    <p>{{ $envoiAlertes->ena_msg }}</p>
</div>

<!-- Ena Date Heure Recu Field -->
<div class="form-group">
    {!! Form::label('ena_date_heure_recu', 'Ena Date Heure Recu:') !!}
    <p>{{ $envoiAlertes->ena_date_heure_recu }}</p>
</div>

<!-- Ena Isenvoi Field -->
<div class="form-group">
    {!! Form::label('ena_isenvoi', 'Ena Isenvoi:') !!}
    <p>{{ $envoiAlertes->ena_isenvoi }}</p>
</div>

<!-- Ena Is Lue Field -->
<div class="form-group">
    {!! Form::label('ena_is_lue', 'Ena Is Lue:') !!}
    <p>{{ $envoiAlertes->ena_is_lue }}</p>
</div>

<!-- Ena Ses Description Field -->
<div class="form-group">
    {!! Form::label('ena_ses_description', 'Ena Ses Description:') !!}
    <p>{{ $envoiAlertes->ena_ses_description }}</p>
</div>

<!-- Ena Ses Annee Field -->
<div class="form-group">
    {!! Form::label('ena_ses_annee', 'Ena Ses Annee:') !!}
    <p>{{ $envoiAlertes->ena_ses_annee }}</p>
</div>

<!-- Ena Ses Debut Field -->
<div class="form-group">
    {!! Form::label('ena_ses_debut', 'Ena Ses Debut:') !!}
    <p>{{ $envoiAlertes->ena_ses_debut }}</p>
</div>

<!-- Ena Ses Lieu Field -->
<div class="form-group">
    {!! Form::label('ena_ses_lieu', 'Ena Ses Lieu:') !!}
    <p>{{ $envoiAlertes->ena_ses_lieu }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $envoiAlertes->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $envoiAlertes->updated_by }}</p>
</div>

