<div class="table-responsive-sm">
    <table class="table table-striped" id="envoiAlertes-table">
        <thead>
            <th>Lia Id</th>
        <th>Ale Id</th>
        <th>Ena Msg</th>
        <th>Ena Date Heure Recu</th>
        <th>Ena Isenvoi</th>
        <th>Ena Is Lue</th>
        <th>Ena Ses Description</th>
        <th>Ena Ses Annee</th>
        <th>Ena Ses Debut</th>
        <th>Ena Ses Lieu</th>
        <th>Created By</th>
        <th>Updated By</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($envoiAlertes as $envoiAlertes)
            <tr>
                <td>{{ $envoiAlertes->lia_id }}</td>
            <td>{{ $envoiAlertes->ale_id }}</td>
            <td>{{ $envoiAlertes->ena_msg }}</td>
            <td>{{ $envoiAlertes->ena_date_heure_recu }}</td>
            <td>{{ $envoiAlertes->ena_isenvoi }}</td>
            <td>{{ $envoiAlertes->ena_is_lue }}</td>
            <td>{{ $envoiAlertes->ena_ses_description }}</td>
            <td>{{ $envoiAlertes->ena_ses_annee }}</td>
            <td>{{ $envoiAlertes->ena_ses_debut }}</td>
            <td>{{ $envoiAlertes->ena_ses_lieu }}</td>
            <td>{{ $envoiAlertes->created_by }}</td>
            <td>{{ $envoiAlertes->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['envoiAlertes.destroy', $envoiAlertes->ena_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('envoiAlertes.show', [$envoiAlertes->ena_id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('envoiAlertes.edit', [$envoiAlertes->ena_id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>