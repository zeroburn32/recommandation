@extends('layouts.template')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('instances.index') !!}">Instance</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             {{-- @include('coreui-templates::common.errors') --}}
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Instance</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($instance, ['route' => ['instances.update', $instance->ins_id], 'method' => 'patch']) !!}

                              @include('backend.instances.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection