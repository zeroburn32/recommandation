<!-- Ins Sigle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ins_sigle', 'Ins Sigle:') !!}
    {!! Form::text('ins_sigle', null, ['class' => 'form-control']) !!}
</div>

<!-- Ins Nom Complet Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ins_nom_complet', 'Ins Nom Complet:') !!}
    {!! Form::text('ins_nom_complet', null, ['class' => 'form-control']) !!}
</div>

<!-- Ins Nbr Session Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ins_nbr_session', 'Ins Nbr Session:') !!}
    {!! Form::number('ins_nbr_session', null, ['class' => 'form-control']) !!}
</div>

<!-- Ins Periodicite Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ins_periodicite', 'Ins Periodicite:') !!}
    {!! Form::text('ins_periodicite', null, ['class' => 'form-control']) !!}
</div>

<!-- Ins Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ins_description', 'Ins Description:') !!}
    {!! Form::textarea('ins_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('instances.index') }}" class="btn btn-secondary">Cancel</a>
</div>
