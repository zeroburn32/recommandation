@extends('layouts.template')
@section('title')
    Instances
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des instances</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des instances</div>
                            </div>
                            <div class="widget-content-right">
                                {{--<button type="button" class="btn btn-sm btn-primary"
                                        onclick="window.location.href='{{ url('programmeBudgetaires') }}'">
                                    <i class="fa fa-wrench fa-lg"></i>&nbsp;&nbsp;G&eacute;rer programme budg&eacute;taire
                                </button>--}}
                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddInstance(null)"
                                        data-toggle="modal" data-target="#modalAddInstance">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle instance
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                @include('backend.instances.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
    <script>
        function openModalAddInstance(id) {
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#formAddInstance"),
                placeholder: "Choisir...",
                allowClear: true
            });
            $('#formAddInstance').attr('action', 'saveInstance');
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle instance');
                $('#btnSave').text('Enregistrer');
                $('#ins_id').val(null);
                $('#ins_sigle').val(null);
                $('#ins_nom_complet').val(null);
                $('#ins_nbr_session').val(null);
                $('#ins_periodicite').val('null');
                $('#ins_description').val(null);
                $('#ins_ref_arrete').val(null);
            }
            else {
                $.ajax({
                    url: "{{ url("/getInstance") }}/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        console.log(data);
                        $('.modal-title').text('Modification d\'une instance');
                        $('#btnSave').text('Modifier');
                        if (data.ins_id != null && data.ins_id != undefined) {
                            $('#ins_id').val(id);
                            $('#bud_id').val(data.bud_id);
                            $('#ins_sigle').val(data.ins_sigle);
                            $('#ins_nom_complet').val(data.ins_nom_complet);
                            $('#ins_nbr_session').val(data.ins_nbr_session);
                            $('#ins_periodicite').val(data.ins_periodicite);
                            $('#ins_description').val(data.ins_description);
                            $('#ins_ref_arrete').val(data.ins_ref_arrete);
                        }
                    },
                    error: function () {
                        alert("Erreur rencontrer lors de la recuperation des donnees!");
                    }
                });
            }
        }

        function openModalDetail(id) {
            $.ajax({
                url: "{{url('/getInstance')}}/" + id,
                type: "get",
                success: function (data) {
                    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
                    var optionsWithTime = {year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"};

                    if (data.ins_id != null && data.ins_id > 0) {
                        $('.modal-title').text("Détails de l'instance : " + data.ins_sigle);
                        $('#libSigle').text(data.ins_sigle);
                        $('#libNom').text(data.ins_nom_complet);
                        $('#libNbSession').text(data.ins_nbr_session);
                        $('#libPeriodicite').html('<span class="badge badge-info">' + data.ins_periodicite + '</span>');
                        $('#libArr').text(data.ins_ref_arrete);
                        $('#libPbg').text(data.bud_intitule);
                        $('#libDesc').text(data.ins_description);
                        if (data.created_at != null && data.created_at != '') {
                            // $('#libCreatedAt').text(dateFormat(data.created_at, "dd/mm/yyyy à HH:mm:ss"));
                            var dtCree = new Date(data.created_at);
                            var datCree = dtCree.toLocaleDateString("fr-FR", optionsWithTime);
                            $('#libCreatedAt').text(datCree);
                            $('#libCreatedBy').text(data.created_by);
                        }
                        if (data.updated_at != null && data.updated_at != '') {
                            // $('#libUpdatedAt').text(dateFormat(data.updated_at, "dd/mm/yyyy à HH:mm:ss"));
                            var dtMod = new Date(data.updated_at);
                            var datMod = dtMod.toLocaleDateString("fr-FR", optionsWithTime);
                            $('#libUpdatedAt').text(datMod);
                            $('#libUpdatedBy').text(data.updated_by);
                        }
                    }
                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données de l'instance!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }
            });
        }

        function deleteInstance(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement l'instance : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteInstance')}}/" + id,
                            type: "get",
                            data: {ins_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cette instance, elle est rattachée à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression de l'instance effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression de l'instance!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        /*function reload() {
            {{--var url = "{{ route('instances.reload') }}";--}}
            $.ajax({
                url: url,
                type: "get",
                datatype: "html"
            }).done(function (data) {
                // append liste of directions to the table directions-table
                $('#tb_instances').empty().html(data);
                $('#tb_instances').DataTable().clear();
                $('#tb_instances').DataTable().destroy();
                var tab = $('#tb_instances').DataTable({
                    dom: 'lBfrtip',
                    responsive: true,
                    processing: true,
                    select: true,
                    "language": {
                        "url": "{{ asset('lang/datatables.french.json')}}"
                    },
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ],
                    "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                        "searchable": false
                    }]
                });
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                var prom = ezBSAlert({
                    messageText: "No response from server on load Error : " + thrownError,
                    alertType: "danger"
                });
            });
        }*/
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT instance--}}
    @include('backend.instances.modalAddInstance')

    {{--MODAL MAJ Details--}}
    @include('backend.instances.modalDetail')
@endsection