<div class="modal fade my-5" id="modalAddInstance" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" action="#" id="formAddInstance">
                <div class="modal-body">
                    <div class="row {{--justify-content-center--}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="ins_id" name="ins_id" type="hidden">

                        <div class="form-group col-md-6">
                            <label for="ins_sigle">Sigle <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="ins_sigle" name="ins_sigle"
                                   maxlength="20" placeholder="Sigle de l'instance" required/>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="ins_nom_complet">Nom complet <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="ins_nom_complet"
                                   name="ins_nom_complet" maxlength="255" placeholder="Nom complet de l'instance" required/>
                        </div>

                        {{--<div class="form-group col-md-4">
                            <label for="bud_id">Programme budg&eacute;taire <span class="requit">*</span></label>
                            <select  class="form-control form-control-sm" id="bud_id" name="bud_id" required>
                                <option value="null">Choisir...</option>
                                @foreach($budges as $budge)
                                    <option value="{{ $budge->bud_id }}">{{ $budge->bud_intitule }}</option>
                                @endforeach
                            </select>
                        </div>--}}

                        <div class="form-group col-md-6">
                            <label for="ins_ref_arrete">Ref. arr&ecirc;t&eacute; / d&eacute;cret <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="ins_ref_arrete"
                                   name="ins_ref_arrete" maxlength="30" placeholder="Ref. arr&ecirc;t&eacute; ou d&eacute;cret de l'instance" required/>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="ins_nbr_session">Nombre de sessions <span class="requit">*</span></label>
                            <input type="number" class="form-control form-control-sm" id="ins_nbr_session"
                                   name="ins_nbr_session" min="0" max="10" required/>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="ins_periodicite">P&eacute;riodicit&eacute; <span class="requit">*</span></label>
                            <select class="form-control form-control-sm {{--selectSearch--}}" id="ins_periodicite"
                                    name="ins_periodicite" required>
                                <option value="null">Choisir...</option>
                                @foreach($periodicites as $periodicite)
                                    <option value="{{ $periodicite }}">{{ $periodicite }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="ins_description">Description <span class="requit">*</span></label>
                            <textarea class="form-control" id="ins_description" name="ins_description"
                                      rows="3" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                </div>
            </form>
        </div>
    </div>
</div>