<div class="modal fade my-5" id="modalDetail" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-sm table-striped table-hover">
                    <tbody id="contentActeurs">
                    <tr>
                        <th>Sigle</th>
                        <td id="libSigle"></td>
                        <th>Nom complet</th>
                        <td id="libNom"></td>
                    </tr>
                    <tr>
                        <th>Nombre de session</th>
                        <td id="libNbSession"></td>
                        <th>P&eacute;riodicit&eacute;</th>
                        <td id="libPeriodicite"></td>
                    </tr>
                    <tr>
                        <th>Ref. arr&ecirc;t&eacute; / d&eacute;cret</th>
                        <td id="libArr" colspan="3"></td>
                    </tr>
                    {{--<tr>
                        <th>Programme budg&eacute;taire</th>
                        <td id="libPbg" colspan="3"></td>
                    </tr>--}}
                    <tr>
                        <th>Description</th>
                        <td id="libDesc" colspan="3"></td>
                    </tr>
                    <tr>
                        <th>Crée le</th>
                        <td id="libCreatedAt"></td>
                        <th>Par</th>
                        <td id="libCreatedBy"></td>
                    </tr>
                    <tr>
                        <th>Modifié le</th>
                        <td id="libUpdatedAt"></td>
                        <th>Par</th>
                        <td id="libUpdatedBy"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer" style="cursor: pointer;">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
            </div>
        </div>
    </div>
</div>