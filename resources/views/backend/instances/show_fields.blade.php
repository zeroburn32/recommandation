<!-- Ins Sigle Field -->
<div class="form-group">
    {!! Form::label('ins_sigle', 'Ins Sigle:') !!}
    <p>{{ $instance->ins_sigle }}</p>
</div>

<!-- Ins Nom Complet Field -->
<div class="form-group">
    {!! Form::label('ins_nom_complet', 'Ins Nom Complet:') !!}
    <p>{{ $instance->ins_nom_complet }}</p>
</div>

<!-- Ins Nbr Session Field -->
<div class="form-group">
    {!! Form::label('ins_nbr_session', 'Ins Nbr Session:') !!}
    <p>{{ $instance->ins_nbr_session }}</p>
</div>

<!-- Ins Periodicite Field -->
<div class="form-group">
    {!! Form::label('ins_periodicite', 'Ins Periodicite:') !!}
    <p>{{ $instance->ins_periodicite }}</p>
</div>

<!-- Ins Description Field -->
<div class="form-group">
    {!! Form::label('ins_description', 'Ins Description:') !!}
    <p>{{ $instance->ins_description }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $instance->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $instance->updated_by }}</p>
</div>

