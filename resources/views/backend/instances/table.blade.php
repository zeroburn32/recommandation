<div class="table-responsive">
    <table class="table table-bordered table-sm table-striped table-hover dt_example" id="tb_instances">
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Sigle</th>
            <th>Nom complet</th>
            <th>P&eacute;riodicit&eacute;</th>
            <th>Nombre de sessions</th>
            {{--<th>Programme budg&eacute;taire</th>--}}
            <th class="text-center no-sort">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($instances as $number=>$instance)
            <tr>
                <td>{{ $number + 1 }}</td>
                <td>{{ $instance->ins_sigle }}</td>
                <td>{{ $instance->ins_nom_complet }}</td>
                <td>{{ $instance->ins_periodicite }}</td>
                <td class="text-center">{{ $instance->ins_nbr_session }}</td>
                {{--<td>{{ $instance->bud_intitule }}</td>--}}
                <td class="text-center">
                    <div class='btn-group'>
                        <button type="button"
                                class="btn btn-xs btn-primary dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu">
                            <a type="button" class="dropdown-item"
                               onclick="openModalDetail({{ $instance->ins_id }})"
                               data-toggle="modal" data-target="#modalDetail">
                                <i class="fa fa-eye"></i>&nbsp;&nbsp;D&eacute;tails
                            </a>
                            <a type="button" class="dropdown-item"
                               onclick="openModalAddInstance({{ $instance->ins_id }})"
                               data-toggle="modal" data-target="#modalAddInstance">
                                <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                            </a>
                            <a type="button" class="dropdown-item"
                               onclick="deleteInstance({{ $instance->ins_id }}, '{{ $instance->ins_sigle }}')">
                                <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                            </a>
                            @can(['GERER_ACTEUR_INSTANCE'])
                            <hr/>
                            <a type="button" class="dropdown-item" style="width: 100%;"
                               onclick="window.location.href='{{ url('getPageActeursInstance'). '/' . $instance->ins_id }}'">
                                <i class="fa fa-users"></i>&nbsp;&nbsp;Acteurs&nbsp;
                                <span class="badge badge-pill badge-warning">
                                    {{ App\Http\Controllers\MethodesStaticController::nbreActeursByInstance($instance->ins_id) }}
                                </span>
                            </a>
                            @endcan
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
