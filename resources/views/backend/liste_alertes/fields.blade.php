<!-- Ses Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_id', 'Ses Id:') !!}
    {!! Form::number('ses_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Use Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('use_id', 'Use Id:') !!}
    {!! Form::number('use_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Str Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('str_id', 'Str Id:') !!}
    {!! Form::number('str_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Lia Nom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_nom', 'Lia Nom:') !!}
    {!! Form::text('lia_nom', null, ['class' => 'form-control']) !!}
</div>

<!-- Lia Prenom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_prenom', 'Lia Prenom:') !!}
    {!! Form::text('lia_prenom', null, ['class' => 'form-control']) !!}
</div>

<!-- Lia Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_email', 'Lia Email:') !!}
    {!! Form::text('lia_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Lia Sexe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_sexe', 'Lia Sexe:') !!}
    {!! Form::text('lia_sexe', null, ['class' => 'form-control']) !!}
</div>

<!-- Lia Fonction Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_fonction', 'Lia Fonction:') !!}
    {!! Form::text('lia_fonction', null, ['class' => 'form-control']) !!}
</div>

<!-- Lia Matricule Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lia_matricule', 'Lia Matricule:') !!}
    {!! Form::text('lia_matricule', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('listeAlertes.index') }}" class="btn btn-secondary">Cancel</a>
</div>
