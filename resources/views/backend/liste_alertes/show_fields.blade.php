<!-- Ses Id Field -->
<div class="form-group">
    {!! Form::label('ses_id', 'Ses Id:') !!}
    <p>{{ $listeAlertes->ses_id }}</p>
</div>

<!-- Use Id Field -->
<div class="form-group">
    {!! Form::label('use_id', 'Use Id:') !!}
    <p>{{ $listeAlertes->use_id }}</p>
</div>

<!-- Str Id Field -->
<div class="form-group">
    {!! Form::label('str_id', 'Str Id:') !!}
    <p>{{ $listeAlertes->str_id }}</p>
</div>

<!-- Lia Nom Field -->
<div class="form-group">
    {!! Form::label('lia_nom', 'Lia Nom:') !!}
    <p>{{ $listeAlertes->lia_nom }}</p>
</div>

<!-- Lia Prenom Field -->
<div class="form-group">
    {!! Form::label('lia_prenom', 'Lia Prenom:') !!}
    <p>{{ $listeAlertes->lia_prenom }}</p>
</div>

<!-- Lia Email Field -->
<div class="form-group">
    {!! Form::label('lia_email', 'Lia Email:') !!}
    <p>{{ $listeAlertes->lia_email }}</p>
</div>

<!-- Lia Sexe Field -->
<div class="form-group">
    {!! Form::label('lia_sexe', 'Lia Sexe:') !!}
    <p>{{ $listeAlertes->lia_sexe }}</p>
</div>

<!-- Lia Fonction Field -->
<div class="form-group">
    {!! Form::label('lia_fonction', 'Lia Fonction:') !!}
    <p>{{ $listeAlertes->lia_fonction }}</p>
</div>

<!-- Lia Matricule Field -->
<div class="form-group">
    {!! Form::label('lia_matricule', 'Lia Matricule:') !!}
    <p>{{ $listeAlertes->lia_matricule }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $listeAlertes->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $listeAlertes->updated_by }}</p>
</div>

