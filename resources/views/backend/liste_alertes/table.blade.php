<div class="table-responsive-sm">
    <table class="table table-striped" id="listeAlertes-table">
        <thead>
            <th>Ses Id</th>
        <th>Use Id</th>
        <th>Str Id</th>
        <th>Lia Nom</th>
        <th>Lia Prenom</th>
        <th>Lia Email</th>
        <th>Lia Sexe</th>
        <th>Lia Fonction</th>
        <th>Lia Matricule</th>
        <th>Created By</th>
        <th>Updated By</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($listeAlertes as $listeAlertes)
            <tr>
                <td>{{ $listeAlertes->ses_id }}</td>
            <td>{{ $listeAlertes->use_id }}</td>
            <td>{{ $listeAlertes->str_id }}</td>
            <td>{{ $listeAlertes->lia_nom }}</td>
            <td>{{ $listeAlertes->lia_prenom }}</td>
            <td>{{ $listeAlertes->lia_email }}</td>
            <td>{{ $listeAlertes->lia_sexe }}</td>
            <td>{{ $listeAlertes->lia_fonction }}</td>
            <td>{{ $listeAlertes->lia_matricule }}</td>
            <td>{{ $listeAlertes->created_by }}</td>
            <td>{{ $listeAlertes->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['listeAlertes.destroy', $listeAlertes->lia_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('listeAlertes.show', [$listeAlertes->lia_id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('listeAlertes.edit', [$listeAlertes->lia_id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>