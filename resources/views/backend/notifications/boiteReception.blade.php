@extends('layouts.template')

@section('title')
    Alerte
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery-ui.min.css') }}"/>
@stop
@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/alertes') }}"><i class="cib-co-op"></i>Alerte</a></li>
                <li class="breadcrumb-item active" aria-current="page">Boite de reception</li>

            </ol>
        </nav>

        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form class="needs-validation" novalidate method="get" action="{{route('filtreAlerte')}}"
                              id="formFilter">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="from-group col-lg-3 col-xl-3">
                                    <div class="card-title">BOITE DE RECEPTION <span style="color:red">(SELON LE FILTRE)</span>&nbsp;&nbsp;-&nbsp;&nbsp;FILTRE:</div>
                                </div>
                                <div class="from-group col-lg-2 col-xl-2">
                                    <select class="form-control form-control-sm selectSearch" id="fil_statut"
                                            name="fil_statut">
                                        <option value=null>statut...</option>
                                        @foreach($etats as $etat)
                                            <option value="{{ $etat }}"
                                                    @if($etat ==$et ) selected @endif>
                                                {{ $etat }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="from-group col-lg-2 col-xl-2">
                                    <button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
                                        <i class="fa fa-search"></i>&nbsp;Rechercher
                                    </button>
                                </div>
								<div class="from-group col-lg-5 col-xl-5">
									<div class="widget-content-wrapper">
										<div class="widget-content-right">
											<button onclick="goBack()" class="float-right btn btn-sm btn-warning">
												<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
											</button>
										</div>
									</div>
								</div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card my-2">
                    
                    <div class="card-body">
                        
						@if(count($alerte))
						<div class="table-responsive" style="min-height: 45vh;">
                                    <table class="table table-bordered table-sm table-striped table-hover dt_example">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>#</th>
                                            <th>Objet de la notification</th>
                                            <th>Etat lecture</th>
                                            <th colspan="2">Date et heure de réception</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                            @php($i=0)
                                            @foreach($alerte as $notif)
                                                @php($i=$i+1)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>
														<a href="{{route('alertInfo',['id'=>$notif->ena_id])}}"
														   style="color: black"
														   class="m-r-10">
														   @if($notif->ena_is_lue==0)
																<strong>{{$notif->ale_mail_subject}}</strong>
														   @else
															   {{$notif->ale_mail_subject}}
														   @endif
														</a>
													</td>
                                                    <td>
														@if($notif->ena_is_lue==0)
															<span class="badge bg-warning">Non lu</span>
														@else
															<span class="badge bg-success">lu</span>
														@endif
													</td>
                                                    <td>{{$notif->ena_date_heure_recu->format("d/m/Y à H:i:s")}}</td>
                                                    <td>
														@if($notif->ena_is_lue==0)
														<a href="{{route('alertInfo',['id'=>$notif->ena_id])}}" class="btn btn-warning float-right"
														   data-placement="top" title="Voir les details de la notification">
															<i class="fa fa-eye"></i>&nbsp;&nbsp;Details
														</a>
														@else
														<a href="{{route('alertInfo',['id'=>$notif->ena_id])}}" class="btn btn-primary float-right"
														   data-placement="top" title="Voir les details de la notification">
															<i class="fa fa-eye"></i>&nbsp;&nbsp;Details
														</a>
														@endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
						@else
							<div class="alert alert-info text-center">Aucune notification reçue, boîte de reception vide...</div>
                        @endif
						
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/lightpick.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date-french.js') }}" type="text/javascript"></script>
    <script>

        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            viewMode: 'years',
            format: 'YYYY',
//            maxDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right"

            }
        }).parent().css("position :relative");

        $.get('/getSession', function (data) {
            console.log(data);
            $.each(data, function (index, d) {

                $('#fil_session').append('<option value="' + d.ses_description + '">' + d.ses_description + '</option>');

            })
        });
    </script>
@endsection
