<!-- Str Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('str_id', 'Str Id:') !!}
    {!! Form::number('str_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_id', 'Rec Id:') !!}
    {!! Form::number('rec_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ptn Is Responsable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ptn_is_responsable', 'Ptn Is Responsable:') !!}
    {!! Form::number('ptn_is_responsable', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('partenaires.index') }}" class="btn btn-secondary">Cancel</a>
</div>
