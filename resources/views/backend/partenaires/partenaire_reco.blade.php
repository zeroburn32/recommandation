@extends('layouts.template')
@section('title')
    Partenaires
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Recommandations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des Partenaires</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des partenairesde de mise en oeuvre de la recommandation :
                                    <span class="text-danger">{{ $reco->rec_intitule }}</span>
                                </div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-warning" onclick="goBack()">
                                    <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-body">
                        @if($reco->rec_statut == 1 || Auth::user()->hasRole(['SUPERADMIN']))
                            <form class="needs-validation" novalidate method="POST" action="{{ route('savePartenaire') }}"
                              id="formAddActeurs">
                                <div class="row justify-content-center">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input id="rec_id" name="rec_id" @if ($reco) value="{{ $reco->rec_id }}" @endif type="hidden">
                                    <input id="ptn_id" name="ptn_id" type="hidden">

                                    <div class="form-group col-md-2">
                                        <label for="str_id">Choisir les partenaires</label>
                                    </div>

                                    <div class="form-group col-md-8">
                                        <select id="str_id" name="str_id[]" class="form-control form-control-sm selectSearch"
                                                multiple required>
                                            {{--<option value="null">Selectionner une structure</option>--}}
                                            @foreach($structures as $structure)
                                                <option value="{{ $structure->str_id }}">{{ $structure->str_sigle . ' - ' . $structure->str_nom_complet }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    {{--<div class="form-group col-md-4">
                                        <select id="ptn_is_responsable" name="ptn_is_responsable"
                                                class="form-control form-control-sm"
                                                required>
                                            <option value="null">Responsable ?</option>
                                            <option value="1">Oui</option>
                                            <option value="0">Non</option>
                                        </select>
                                    </div>--}}

                                    <div class="form-group col-md-2">
                                        <button type='submit' class="btn btn-sm btn-success" id="btnAdd"
                                                style="width: 100%;">
                                            <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Ajouter
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <hr/>
                        @endif
                        <div class="table-responsive" style="min-height: 45vh;">
                            <table class="table table-bordered table-sm table-striped table-hover">
                                <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Sigle</th>
                                    <th>Nom complet</th>
                                    <th>Responsable ?</th>
                                    @if($reco->rec_statut == 1)
                                        <th class="text-center no-sort">Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($partenaires as $number=>$partenaire)
                                    <tr>
                                        <td>{{ $number + 1 }}</td>
                                        <td>{{ $partenaire->str_sigle }}</td>
                                        <td>{{ $partenaire->str_nom_complet }}</td>
                                        <td>
                                            @if($partenaire->ptn_is_responsable == 0)
                                                <span class="badge badge-danger">Non</span>
                                            @elseif($partenaire->ptn_is_responsable == 1)
                                                <span class="badge badge-success">Oui</span>
                                            @else
                                                <span></span>
                                            @endif
                                        </td>
                                        @if($reco->rec_statut == 1 || Auth::user()->hasRole(['SUPERADMIN']))
                                            <td class="text-center">
                                                <div class='btn-group'>
                                                    <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                                            data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        Actions
                                                    </button>
                                                    <div class="dropdown-menu dropleft">
                                                        @if($partenaire->ptn_is_responsable == 0)
                                                            <a class="dropdown-item"
                                                               onclick="setResponsable({{ $partenaire->ptn_id }}, '{{addslashes($partenaire->str_sigle)}}', '{{addslashes($reco->rec_intitule)}}', 1)">
                                                                <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;D&eacute;finir comme responsable
                                                            </a>
                                                        @endif
                                                        <a class="dropdown-item" title="Supprimer le partenaire à la recommandation"
                                                           onclick="deletePartenaire({{ $partenaire->ptn_id }}, '{{addslashes($partenaire->str_sigle)}}', '{{addslashes($reco->rec_intitule)}}')">
                                                            <i class="fa fa-minus-square fa-lg"></i>&nbsp;&nbsp;Supprimer
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $('.selectSearch').select2({
            placeholder: "Choisir...",
            allowClear: true
        });

        function setResponsable(id, libelle, reco, flag) {
            var txtConfirme, txtBtnConfirm, txtColorConfirme, txtErrorText = "";
            if (flag == 1) {
                txtConfirme = "Etes-vous sûr de vouloir définir " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>"+libelle+"</span> comme " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>responsable de la recommandation</span> ?";
                txtBtnConfirm = "Oui, définir!";
                txtColorConfirme = '#008d03';
                txtErrorText = "Une erreure est survenue lors définition du " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>rôle responsable de recommandation</span>" +
                    " au partenaire!\nVeuillez reessayer!";
            } else if (flag == 0) {
                txtConfirme = "Etes vous sûr de vouloir rétirer le " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>rôle responsable de la recommandation</span>" +
                    " à <span class='text-info' style='font-size: 18px; font-weight: bold;'>"+libelle+"</span> ?";
                txtBtnConfirm = "Oui, Rétirer!";
                txtColorConfirme = '#d33';
                txtErrorText = "Une erreure est survenue lors du rétrait du " +
                    "<span class='text-info' style='font-size: 18px; font-weight: bold;'>rôle responsable de recommandation</span>" +
                    " au partenaire!\nVeuillez reessayer!";
            }
            swal({
                title: 'CONFIRMATION',
                html: txtConfirme,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: txtColorConfirme,
                cancelButtonColor: '#3085d6',
                confirmButtonText: txtBtnConfirm,
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/setResponsable')}}/" + id,
                            type: "get",
                            data: {act_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: "Succes!",
                                        text: "Opération effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                } else {
                                    swal({
                                        title: 'Erreur',
                                        text: txtErrorText,
                                        type: 'error',
                                        button: "OK"
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: txtErrorText,
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function deletePartenaire(id, libelle, reco) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir rétirer définitivement le partenaire : " + libelle + " de la recommandation : " + reco + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, rétirer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deletePartenaire')}}/" + id,
                            type: "get",
                            data: {act_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de rétirer ce partenaire, il est rattaché à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Rétrait du partenaire effectué avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors du rétrait du partenaire!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
@endsection