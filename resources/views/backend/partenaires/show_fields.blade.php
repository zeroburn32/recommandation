<!-- Str Id Field -->
<div class="form-group">
    {!! Form::label('str_id', 'Str Id:') !!}
    <p>{{ $partenaires->str_id }}</p>
</div>

<!-- Rec Id Field -->
<div class="form-group">
    {!! Form::label('rec_id', 'Rec Id:') !!}
    <p>{{ $partenaires->rec_id }}</p>
</div>

<!-- Ptn Is Responsable Field -->
<div class="form-group">
    {!! Form::label('ptn_is_responsable', 'Ptn Is Responsable:') !!}
    <p>{{ $partenaires->ptn_is_responsable }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $partenaires->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $partenaires->updated_by }}</p>
</div>

