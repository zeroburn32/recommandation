<div class="table-responsive-sm">
    <table class="table table-striped" id="partenaires-table">
        <thead>
            <th>Str Id</th>
        <th>Rec Id</th>
        <th>Ptn Is Responsable</th>
        <th>Created By</th>
        <th>Updated By</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($partenaires as $partenaires)
            <tr>
                <td>{{ $partenaires->str_id }}</td>
            <td>{{ $partenaires->rec_id }}</td>
            <td>{{ $partenaires->ptn_is_responsable }}</td>
            <td>{{ $partenaires->created_by }}</td>
            <td>{{ $partenaires->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['partenaires.destroy', $partenaires->ptn_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('partenaires.show', [$partenaires->ptn_id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('partenaires.edit', [$partenaires->ptn_id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>