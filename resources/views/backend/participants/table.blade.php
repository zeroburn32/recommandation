<div class="table-responsive-sm">
    <table class="table table-striped" id="participants-table">
        <thead>
            <th>Str Id</th>
        <th>Ses Id</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($participants as $participants)
            <tr>
                <td>{{ $participants->str_id }}</td>
            <td>{{ $participants->ses_id }}</td>
                <td>
                    {!! Form::open(['route' => ['participants.destroy', $participants->par_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('participants.show', [$participants->par_id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('participants.edit', [$participants->par_id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>