<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nom:') !!}
        {!! Form::text('name', null, ['class' => 'form-control form-control-sm', 'readonly'=>'readonly']) !!}
    </div>

    <!-- Description Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('label', 'Description:') !!}
        {!! Form::text('label', null, ['class' => 'form-control form-control-sm']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Enregistrer', ['class' => 'btn btn-sm btn-primary']) !!}
        <a href="{{ route('permissions.index') }}" class="btn btn-sm btn-danger">Annuler</a>
    </div>
</div>