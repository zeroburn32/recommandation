@extends('layouts.template')
@section('title')
    Droits
    @parent
@stop
@section('content')

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Gestion des Droits</li>
            </ol>
        </nav>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des droits</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('backend.permissions.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

