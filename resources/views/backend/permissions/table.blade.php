<div class="table-responsive ">
    <table class="table table-striped table-sm table-hover dt_example" id="permissions-table">
        <thead class="thead-dark">
        <th>Nom</th>
        <th>Description</th>
        @can('GERER_COMPTE')
        <th class="text-center no-sort">Action</th>
        @endcan
        </thead>
        <tbody>
        @foreach($permissions as $permissions)
            <tr>
                <td>{{ $permissions->name }}</td>
                <td>{{ $permissions->label }}</td>
                @can('GERER_COMPTE')
                <td class="text-center">
                        <a href="{{ route('permissions.edit', [$permissions->id]) }}" class='btn btn-sm btn-primary'>
                            <i class="fa fa-edit"></i>
                        </a>
                </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
    </table>
</div>