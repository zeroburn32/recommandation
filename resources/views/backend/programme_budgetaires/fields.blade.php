<!-- Bud Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bud_code', 'Bud Code:') !!}
    {!! Form::text('bud_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bud Intitule Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bud_intitule', 'Bud Intitule:') !!}
    {!! Form::text('bud_intitule', null, ['class' => 'form-control']) !!}
</div>

<!-- Bud Annee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bud_annee', 'Bud Annee:') !!}
    {!! Form::number('bud_annee', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('programmeBudgetaires.index') }}" class="btn btn-secondary">Cancel</a>
</div>
