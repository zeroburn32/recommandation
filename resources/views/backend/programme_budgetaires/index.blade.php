@extends('layouts.template')
@section('title')
    Programmes Budg&eacute;taires
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des programmes budg&eacute;taires</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des programmes budg&eacute;taires</div>
                            </div>
                            <div class="widget-content-right">
                                @can(['GERER_PROGRAMME_BUDGETAIRE'])
                                    <button type="button" class="btn btn-sm btn-primary"
                                            onclick="openModalAddProBudge(null)"
                                            data-toggle="modal" data-target="#modalAddProBudge">
                                        <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouveau programme budg&eacute;taire
                                    </button>
                                @endcan
                                {{--<button type="button" class="btn btn-sm btn-warning"
                                        onclick="window.location.href ='{{ url('instances') }}'">
                                    <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                                </button>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                @include('backend.programme_budgetaires.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script>
        function openModalAddProBudge(id) {
            $('#formAddProBudge').attr('action', 'saveProgBug');
            if (id == null) {
                $('.modal-title').text('Ajout d\'un nouveau programme budgétaire');
                $('#btnSave').text('Enregistrer');
                $('#bud_id').val(null);
                $('#bud_code').val(null);
                $('#bud_intitule').val(null);
                $('#bud_annee').val(null);
            }
            else {
                $.ajax({
                    url: "{{ url("/getProgBug") }}/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        console.log(data);
                        $('.modal-title').text('Modification d\'un programme budgétaire');
                        $('#btnSave').text('Modifier');
                        if (data.bud_id != null && data.bud_id != undefined) {
                            $('#bud_id').val(data.bud_id);
                            $('#bud_code').val(data.bud_code);
                            $('#bud_intitule').val(data.bud_intitule);
                            $('#bud_annee').val(data.bud_annee);
                        }
                    },
                    error: function () {
                        alert("Erreur rencontrer lors de la recuperation des donnees!");
                    }
                });
            }
        }

        function deleteProBudge(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement le programme budgétaire : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteProgBug')}}/" + id,
                            type: "get",
                            data: {ins_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cet programme budgétaire, il est rattaché à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression du programme budgétaire effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
//                                        reload();
                                        location.reload();
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression du programme budgétaire!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function reload() {
            var url = "{{ route('progBudgetaires.reload') }}";
            $.ajax({
                url: url,
                type: "get",
                datatype: "html"
            }).done(function (data) {
                // append liste of directions to the table directions-table
//                $('.table-responsive-sm').removeAttr("style");
                $('#tb_progBudgetaires').empty().html(data);
                $('#tb_progBudgetaires').DataTable().clear();
                $('#tb_progBudgetaires').DataTable().destroy();
                $('.table-responsive-sm').css('min-height', '15vh');
                var tab = $('#tb_progBudgetaires').DataTable({
                    dom: 'lBfrtip',
                    responsive: true,
                    processing: true,
                    select: true,
                    "language": {
                        "url": "{{ asset('lang/datatables.french.json')}}"
                    },
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ],
                    "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                        "searchable": false
                    }]
                });
                if (tab) {
                    $('.table-responsive-sm').css('min-height', '45vh');
                }
                /*tab.on('order.dt search.dt', function () {
                    tab.column(0, {
                        search: 'applied',
                        order: 'applied'
                    }).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                    });
                }).draw();*/

            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                var prom = ezBSAlert({
                    messageText: "No response from server on load Error : " + thrownError,
                    alertType: "danger"
                });
            });
        }
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT instance--}}
    @include('backend.programme_budgetaires.modalAddProBudge')
@endsection
