<div class="modal fade my-5" id="modalAddProBudge" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" action="#" id="formAddProBudge">
                {{--                {!! Form::model($programmeBudgetaire, ['route' => 'saveProgBug', 'method' => 'post',--}}
                {{--'class' => 'needs-validation', 'novalidate' => true]) !!}--}}
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="bud_id" name="bud_id" type="hidden">

                        <div class="form-group col-md-6">
                            <label for="bud_code">Code <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="bud_code" name="bud_code"
                                   maxlength="10" required/>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="bud_intitule">Intitul&eacute; <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="bud_intitule"
                                   name="bud_intitule" maxlength="150" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                </div>
            </form>
            {{--                {!! Form::close() !!}--}}
        </div>
    </div>
</div>