<!-- Bud Code Field -->
<div class="form-group">
    {!! Form::label('bud_code', 'Bud Code:') !!}
    <p>{{ $programmeBudgetaires->bud_code }}</p>
</div>

<!-- Bud Intitule Field -->
<div class="form-group">
    {!! Form::label('bud_intitule', 'Bud Intitule:') !!}
    <p>{{ $programmeBudgetaires->bud_intitule }}</p>
</div>

<!-- Bud Annee Field -->
<div class="form-group">
    {!! Form::label('bud_annee', 'Bud Annee:') !!}
    <p>{{ $programmeBudgetaires->bud_annee }}</p>
</div>

