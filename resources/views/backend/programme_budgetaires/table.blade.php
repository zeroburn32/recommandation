<div class="table-responsive-sm">
    <table class="table table-bordered table-sm table-striped table-hover dt_example" id="tb_progBudgetaires">
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Intitul&eacute;</th>
            <th class="text-center no-sort">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($programmeBudgetaires as $number=>$programmeBudgetaire)
            <tr>
                <td>{{ $number + 1 }}</td>
                <td>{{ $programmeBudgetaire->bud_code }}</td>
                <td>{{ $programmeBudgetaire->bud_intitule }}</td>
                <td class="text-center">
                    <div class='btn-group'>
                        <button type="button"
                                class="btn btn-xs btn-primary dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu">
                            <a type="button" class="dropdown-item"
                               onclick="openModalAddProBudge({{ $programmeBudgetaire->bud_id }})"
                               data-toggle="modal" data-target="#modalAddProBudge">
                                <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                            </a>
                            <a type="button" class="dropdown-item"
                               onclick="deleteProBudge({{ $programmeBudgetaire->bud_id }}, '{{ $programmeBudgetaire->bud_intitule }}')">
                                <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>