@extends('layouts.template')

@section('title')
Rapport
@parent
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/select2.min.css') }}" />

@stop

@section('content')
<div class="container-fluid">
    <div class="card mb-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">Etats</li>
                <li class="breadcrumb-item active" aria-current="page">Rapport</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Critères de recherche ( <em
                                    style="text-transform:none; color:mediumslateblue"> Remplissez les critères
                                    souhaitées </em>) </div>
                        </div>
                        <div class="widget-content-right">
                            {{-- <button type="button" class="btn btn-sm btn-primary" onclick="openModalAddStructure(null)"
                                data-toggle="modal" data-target="#modalAddStructure">
                                <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;
                            </button> --}}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('rapportSearch')}}" id="formFilter">
                        <div class="row justify-content-center">
                            @csrf
                            <div class="from-group col-lg-3 col-xl-2">
                                <label for="fil_instance">Instance</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_instance"
                                    name="fil_instance" onchange="onChangeInstance()">
                                    <option value="">Choisir l'instance...</option>
                                    @foreach($instances as $key => $value)
                                    <option value="{{ $key }}" @if($instance==$key) selected @endif>{{ $value }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <label for="fil_annee">Annee</label>
                                <input type="number" class="form-control form-control-sm" id="fil_annee"
                                    name="fil_annee" readonly />
                                {{-- <div class="input-group-append">
                                    <span class="input-group-text form-control form-control-sm">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div> --}}
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <label for="fil_session">Session de l'instance</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_session"
                                    name="fil_session">
                                    <option value="">Choisir la session...</option>
                                    @foreach($sessions as $key => $value)
                                    <option value="{{ $key }}" @if($session==$key) selected @endif>{{ $value }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <label for="fil_structure">Structure responsable</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_structure"
                                    name="fil_structure">
                                    <option value="">Choisir l'acteur responsable</option>
                                    @foreach($structures as $key => $value)
                                    <option value="{{ $key }}" @if($acteur==$key) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-xl-2">
                                <label for="fil_status">Statut</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_status"
                                    name="fil_status">
                                    <option value="">Choisir le statut...</option>
                                    @foreach($statuts as $key => $value)
                                    <option value="{{ $key }}" @if($statut==$key) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="from-group col-lg-2 col-xl-2    ">
                                <label for="btnSearch">&nbsp;</label>
                                <button type="submit" class="btn btn-sm btn-primary" style="width: 100%" id="btnSearch">
                                    <i class="fa fa-search"></i>&nbsp;Rechercher
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if (empty($recos))
            <div class="row my-3">
                <div class="col-lg-12 col-xl-12 mb-3">
                    {{-- <div class="card">
                        <div class="card-body">
                        </div>
                    </div> --}}
                </div>
            </div>

            @else
            <div class="row my-3">
                <div class="col-lg-12 col-xl-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 45vh;">
                                <table class="table table-striped table-sm dt_example" id="etat_table">
                                    <thead class="thead-dark">
                                        <th>#</th>
                                        <th>Instance</th>
                                        <th>Session</th>
                                        <th>Recommandations</th>
                                        <th>Echéance</th>
                                        <th>Statut</th>
                                        <th>Délais</th>
                                        <th title="Taux de mise en oeuvre">Taux</th>
                                        <th title="Responsable de mise en oeuvre">Resp.</th>
                                        <th class="no-sort">Détail</th>
                                    </thead>
                                    <tbody>
                                        @foreach($recos as $i=>$reco)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td title="{{$reco->ins_nom_complet}}">{{$reco->ins_sigle ?? ''}}</td>
                                            <td>{{$reco->ses_description ?? ''}}</td>
                                            <td>{{ $reco->rec_intitule }}
                                                @if( ! is_null($reco->rec_id_ref))
                                                <span class="badge badge-info">Réconduit</span>
                                                @endif
                                            </td>
                                            <td>{{ Carbon\Carbon::parse($reco->rec_date_echeance)->format('d-m-Y') }}
                                            </td>
                                            <td>
                                                @if($reco->rec_statut == 2)
                                                <span class="badge badge-danger">Non R&eacute;alis&eacute;e</span>
                                                @elseif($reco->rec_statut == 3)
                                                <span class="badge badge-primary">En cours</span>
                                                @elseif($reco->rec_statut == 4)
                                                <span class="badge badge-success">R&eacute;alis&eacute;e</span>
                                                @elseif($reco->rec_statut == 5)
                                                <span class="badge badge-warning">Abandonn&eacute;e</span>
                                                @else
                                                <span class="badge badge-secondary">Préparation</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{--  @if($reco->rec_statut == 2 || $reco->rec_statut == 3)
                                                @if($reco->rec_date_echeance > Carbon\Carbon::now() ) 
                                                    <span class="badge badge-pill badge-warning">Dans les délais</span>
                                                @elseif($reco->rec_date_echeance  < Carbon\Carbon::now())
                                                    <span class="badge badge-pill badge-danger">En retard</span>
                                                @endif --}}

                                                @if($reco->rec_statut == 4)
                                                @if($reco->rec_dh_realise > $reco->rec_date_echeance) )
                                                <span class="badge badge-pill badge-info">Réalisé hors délais</span>
                                                @elseif($reco->rec_dh_realise < $reco->rec_date_echeance)
                                                    <span class="badge badge-pill badge-success">Réalisé à temps</span>
                                                    @endif
                                                    @else
                                                    <span class="badge badge-pill badge-secondary">N/A</span>
                                                    @endif
                                            </td>
                                            <td>{{ $reco->rec_taux_realise }}% </td>
                                            <td>{{ $reco->str_sigle }} </td>
                                            <td class="text-left">
                                                <a href="" class=""
                                                    onclick="openModalListPartenaire({{ $reco->rec_id }})"
                                                    data-toggle="modal" data-target="#modalListPart"
                                                    title="Consulter la liste des partenaires">
                                                    <i class="fa fa-list"></i>
                                                </a>
                                                @if($reco->rec_is_mode_standard == 0)
                                                <a href="" class="" onclick="openModalListActivite({{ $reco->rec_id }})"
                                                    data-toggle="modal" data-target="#modalListAct"
                                                    title="Consulter la liste des activités">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @else
                                                <a href="" class="" onclick="openModalDetailsReco({{ $reco->rec_id }})"
                                                    data-toggle="modal" data-target="#modalHistorique"
                                                    title="Voir l'historique du suivi">
                                                    <i class="fa fa-calendar"></i>
                                                </a>
                                                @endif


                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
<script>
    $('#fil_instance').select2({placeholder: "Choisir l'instance...",allowClear: true });
        $('#fil_session').select2({placeholder: "Choisir la session de l'instance...",allowClear: true});
        $('#fil_structure').select2({placeholder: "Choisir l'acteur responsable...",allowClear: true});
        $('#fil_status').select2({placeholder: "Choisir le statut de la recommandation...",allowClear: true});

        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            showClear: true,
            viewMode: 'years',
            format: 'YYYY',
            maxDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                clear: "fa fa-trash"
            }
        }).val('{{$annee}}').parent().css("position :relative");

// Chargement de la session en fonction de l'année
            $('#fil_annee').on('dp.update', function(e){ 
                if($('#fil_instance').val()) {
                    onChangeInstance();
                }
            });

// Chargement des sessions de l'instance
        function onChangeInstance() {
            var instance = $('#fil_instance').val();
            if(!instance) {
                instance = -1;
            }
            var annee = $('#fil_annee').val();
            $.ajax({
                url: APP_URL+"/getSessionsByInstance/"+instance+"/"+annee,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#fil_session').empty();
                    $('#fil_session').append($('<option>', {
                        value: '',
                        text: 'Choisir la session...'
                    }));
                    $.each(data, function (index, session) {
                        if (instance == session.ins_id) {
                            $('#fil_session').append($('<option>', {
                                value: session.ses_id,
                                text: session.ses_description
                            }));
                        }
                    });
                    //$('#fil_session').attr('required', true);
                }
            });
            $.ajax({
                url: APP_URL+"/getActeursByInstance/"+instance,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#fil_structure').empty();
                    $('#fil_structure').append($('<option>', {
                        value: '',
                        text: 'Choisir un responsable...'
                    }));
                    if (instance ==-1) {
                        $.each(data, function (index, acteur) {
                            $('#fil_structure').append($('<option>', {
                                value: acteur.str_id,
                                text: acteur.str_sigle+' - '+acteur.str_nom_complet
                            }));
                    });
                    } else {
                        $.each(data, function (index, acteur) {
                        if (instance == acteur.ins_id) {
                            $('#fil_structure').append($('<option>', {
                                value: acteur.str_id,
                                text: acteur.str_sigle+' - '+acteur.str_nom_complet
                            }));
                        }
                    });
                    }
                    //$('#fil_structure').attr('required', true);
                }
            });
        }        

        // Liste des partenaires de la recommandation
        function openModalListPartenaire(id) {
            $.ajax({
                url: "{{ url('/getListPartenaires') }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var contentBody = '';
                    if (data.length > 0) {
                        $('#msgError').hide();
                        $('#tabPart').show();
                        $('.modal-title').html('Liste des partenaires de la recommandation : <em style="color:blue;">' + data[0].rec_intitule+'</em>');
                       // $('.modal-title').text('Liste des partenaires de la recommandation : ' + data[0].rec_intitule);
                        $.each(data, function (index, part) {
                            var resp = '';
                            if (part.ptn_is_responsable == 0) {
                                resp = '<span class="badge badge-danger">Non</span>';
                            } else if (part.ptn_is_responsable == 1) {
                                resp = '<span class="badge badge-success">Oui</span>';
                            } else {
                                resp = '';
                            }
                            contentBody += '<tr>' +
                                '<td>' + (index + 1) + '</td>' +
                                '<td>' + part.str_sigle + '</td>' +
                                '<td>' + part.str_nom_complet + '</td>' +
                                '<td>' + resp + '</td>' +
                                '</tr>';
                        });
                        $('#listPart').html(contentBody);
                    } else {
                        $('.modal-title').text('Liste des partenaires de la recommandation');
                        $('#tabPart').hide();
                        $('#msgError').show();
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        // Liste des partenaires de la recommandation
        function openModalListActivite(id) {
            $.ajax({
                url: "{{ url('/getListActivites') }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var contentBody = '';
                    if (data.activites.length > 0) {
                        $('#msgErrorAct').hide();
                        $('#tabAct').show();
                        $('.modal-title').html('Liste des partenaires de la recommandation : <em style="color:blue;">' + data.reco.rec_intitule+'</em>');   
                        //$('.modal-title').html('Liste des partenaires de la recommandation : ' + data.reco.rec_intitule);
                        $.each(data.activites, function (index, act) {
                            var statut = '';
                            var etat = '';
                            var observation = '';
                            var lien = '';
                            var td_lien = '';
                            if (act.act_is_valide == 0) {
                                etat = '<span class="badge badge-danger">Non validée</span>';
                            } else if (act.act_is_valide == 1) {
                                etat = '<span class="badge badge-success">Validée</span>';
                            } else {
                                etat = '';
                            }
                            if (act.act_observation == null) {
                                observation = ''
                            } else {
                                observation = act.act_observation
                            }

                            if (act.act_is_realise == 0) {
                                statut = '<span class="badge badge-danger">Non validée</span>';
                            } else if (act.act_is_realise == 1) {
                                statut = '<span class="badge badge-success">Validée</span>';
                            } else {
                                statut = '';
                            }
                            if(act.act_preuve) {
                                lien = APP_URL+'/pj_activites/'+act.act_preuve;
                                td_lien = '<td title="Télécharger le fichier '+act.act_preuve+'"><a type="button" class="btn btn-sm btn-info" href="'+lien+'" target="_blank"><i class="fa fa-download fa-sm"></i></a></td>'
                            } else {
                                lien= '#';
                                td_lien='<td></td>'
                            }
                            contentBody += '<tr>' +
                                '<td>' + (index + 1) + '</td>' +
                                '<td>' + act.act_description + '</td>' +
                                '<td>' + act.act_date_prevue + '</td>' +
                                '<td>' + act.act_poids + '</td>' +
                                '<td>' + etat + '</td>' +
                                '<td>' + statut + '</td>' +
                                '<td title="'+act.str_nom_complet+'" >' + act.str_sigle + '</td>' +
                                '<td >' +  observation + '</td>' +
                                td_lien
                                '</tr>';
                        });
                        $('#listAct').html(contentBody);
                    } else {
                        $('.modal-title').text('Liste des activités de la recommandation');
                        $('#tabAct').hide();
                        $('#msgErrorAct').show();
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }
    
// Modal details recos
function openModalDetailsReco(id) {
            $('#zoneDate').hide();
            $('#zoneMotif').hide();
            $('#historique').hide();
            $('#histNonRealise').hide();
            $('#histEnCours').hide();
            $('#histRealise').hide();
            $('.modal-title').text('Détails de la recommandation');
            $.ajax({
                url: "{{ url("/getReco") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('.modal-title').html('Historique de suivi de la recommandation : <em style="color:blue;">' + data.rec_intitule+'</em>');
                    var statut, planning, mode, valideSuivi, permanente = '';
                    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
                    var optionsWithTime = {year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"};

                    /** ***************************** HISTORIQUE ********************************* */
                    var urlDown = "";
                    if (data.rec_obs_nonrealise && data.rec_dh_nonrealise) {
                        $('#historique').show();
                        $('#histNonRealise').show();
                        $('#obsNonRealise').text(data.rec_obs_nonrealise);
                        var dtNonReal = new Date(data.rec_dh_nonrealise);
                        var datNonReal = dtNonReal.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateNonRealise').text(datNonReal);
                        if (data.rec_preuve_nonrealise) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_nonrealise;
                            $('#downNonRealise').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +'>' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }

                    if (data.rec_obs_encours && data.rec_dh_en_cours) {
                        $('#historique').show();
                        $('#histEnCours').show();
                        $('#obsEnCours').text(data.rec_obs_encours);
                        var dtEnCours = new Date(data.rec_dh_en_cours);
                        var datEnCours = dtEnCours.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateEnCours').text(datEnCours);
                        if (data.rec_preuve_encours) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_encours;
                            $('#downEnCours').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +'>' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }

                    if (data.rec_obs_realise && data.rec_dh_realise) {
                        $('#historique').show();
                        $('#histRealise').show();
                        $('#obsRealise').text(data.rec_obs_realise);
                        var dtRealise = new Date(data.rec_dh_realise);
                        var datRealise = dtRealise.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateRealise').text(datRealise);
                        if (data.rec_preuve_realise) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_realise;
                            $('#downRealise').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +'>' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

</script>
@endsection

@section('modal')
{{--MODAL Liste partenaire--}}
<div class="modal fade my-5" id="modalListPart" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-striped table-hover" id="tabPart">
                        <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Sigle</th>
                                <th>Nom complet</th>
                                <th>Responsable ?</th>

                            </tr>
                        </thead>
                        <tbody id="listPart"></tbody>
                    </table>
                    <div id="msgError" class="badge badge-danger">
                        Pour l'instant il n'y a pas de partenaire pour cette recommandation !
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                    <i class="fa fa-ban"></i>&nbsp;Fermer
                </button>
            </div>
        </div>
    </div>
</div>

{{--MODAL Liste activités--}}
<div class="modal fade my-5" id="modalListAct" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-striped table-hover" id="tabAct">
                        <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Activité</th>
                                <th>Echeance</th>
                                <th>Poids </th>
                                <th>Statut </th>
                                <th> Etat</th>
                                <th> Part. </th>
                                <th>Observations</th>
                                <th>Pièce</th>
                            </tr>
                        </thead>
                        <tbody id="listAct"></tbody>
                    </table>
                    <div id="msgErrorAct" class="badge badge-danger">
                        Aucune activités trouvée pour cette recommandation !
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                    <i class="fa fa-ban"></i>&nbsp;Fermer
                </button>
            </div>
        </div>
    </div>
</div>

{{-- Modal histatorique --}}
<div class="modal fade" id="modalHistorique" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive" id="historique">
                    {{-- <h5>Historique du suivi</h5> --}}
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>Statut</th>
                                <th>Observation</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="histNonRealise">
                                <th><span class="badge badge-danger">Non r&eacute;alis&eacute;e</span></th>
                                <td id="obsNonRealise"></td>
                                <td id="dateNonRealise"></td>
                                <td class="text-right" id="downNonRealise"></td>
                            </tr>

                            <tr id="histEnCours">
                                <th><span class="badge badge-info">En cours</span></th>
                                <td id="obsEnCours"></td>
                                <td id="dateEnCours"></td>
                                <td class="text-right" id="downEnCours"></td>
                            </tr>

                            <tr id="histRealise">
                                <th><span class="badge badge-success">R&eacute;alis&eacute;e</span></th>
                                <td id="obsRealise"></td>
                                <td id="dateRealise"></td>
                                <td class="text-right" id="downRealise"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                    <i class="fa fa-ban"></i>&nbsp;Fermer
                </button>
            </div>
        </div>
    </div>
</div>
@endsection