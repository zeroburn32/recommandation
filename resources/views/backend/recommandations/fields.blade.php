<!-- Ses Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_id', 'Ses Id:') !!}
    {!! Form::number('ses_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Intitule Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rec_intitule', 'Rec Intitule:') !!}
    {!! Form::textarea('rec_intitule', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Date Echeance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_date_echeance', 'Rec Date Echeance:') !!}
    {!! Form::text('rec_date_echeance', null, ['class' => 'form-control','id'=>'rec_date_echeance']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#rec_date_echeance').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Rec Statut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_statut', 'Rec Statut:') !!}
    {!! Form::number('rec_statut', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Etat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_etat', 'Rec Etat:') !!}
    {!! Form::number('rec_etat', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Is Suivi Valide Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_is_suivi_valide', 'Rec Is Suivi Valide:') !!}
    {!! Form::number('rec_is_suivi_valide', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Is Mode Standard Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_is_mode_standard', 'Rec Is Mode Standard:') !!}
    {!! Form::number('rec_is_mode_standard', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Observation Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rec_observation', 'Rec Observation:') !!}
    {!! Form::textarea('rec_observation', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Personne Formule Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_personne_formule', 'Rec Personne Formule:') !!}
    {!! Form::text('rec_personne_formule', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Personne Tel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_personne_tel', 'Rec Personne Tel:') !!}
    {!! Form::text('rec_personne_tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Rec Dh Valide Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_dh_valide', 'Rec Dh Valide:') !!}
    {!! Form::text('rec_dh_valide', null, ['class' => 'form-control','id'=>'rec_dh_valide']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#rec_dh_valide').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Rec Dh Abandon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_dh_abandon', 'Rec Dh Abandon:') !!}
    {!! Form::text('rec_dh_abandon', null, ['class' => 'form-control','id'=>'rec_dh_abandon']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#rec_dh_abandon').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Rec Taux Realise Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rec_taux_realise', 'Rec Taux Realise:') !!}
    {!! Form::number('rec_taux_realise', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('recommandations.index') }}" class="btn btn-secondary">Cancel</a>
</div>
