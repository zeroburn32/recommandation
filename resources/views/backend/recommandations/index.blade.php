@extends('layouts.template')

@section('title')
    Recommandations
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    @if($mode == 'SAISIE')
                        <li class="breadcrumb-item active" aria-current="page">Gestion des saisies de recommandation</li>
                    @elseif($mode == 'PLAN')
                        <li class="breadcrumb-item active" aria-current="page">Gestion des planifications des recommandations</li>
                    @elseif($mode == 'SUIVI')
                        <li class="breadcrumb-item active" aria-current="page">Gestion des suivis des recommandations</li>
                    @else
                        <li class="breadcrumb-item active" aria-current="page">Gestion des recommandations</li>
                    @endif
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des recommandations
                                    @if(isset($_GET['_token']))
                                        <span class="requit">(selon le filtre)</span>
                                    @else
                                        <span class="requit">(en pr&eacute;paration & en cours)</span>
                                    @endif
                                </div>
                            </div>
                            <div class="widget-content-right">
                                @if($mode == 'SAISIE')
                                    {{--@if (isset($_GET['log']))
                                        @if ($_GET['log'])--}}
                                            <a type="button" class="btn btn-sm btn-warning text-lowercase my-1" style="text-decoration:none; !important;"
                                               href="{{ url('errors/erreurs_importation_recos.log') }}" target="_blank">
                                                <i class="fa fa-eye fa-lg"></i>&nbsp;&nbsp;Consulter log du dernier import
                                            </a>
                                        {{--@endif
                                    @endif--}}
                                    <button type="button" class="btn btn-sm btn-info"
                                            onclick="openModalImport()"
                                            data-toggle="modal" data-target="#modalImport">
                                        <i class="fa fa-upload fa-lg"></i>&nbsp;&nbsp;Importer des recommandations
                                    </button>
                                    <button type="button" class="btn btn-sm btn-primary"
                                            onclick="openModalAddReco(null)"
                                            data-toggle="modal" data-target="#modalAddReco">
                                        <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle recommandation
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="GET"
                              @if($mode == 'SAISIE') action="{{ route('recosSearch') }}"
                              @elseif($mode == 'PLAN' || $mode == 'SUIVI') action="{{ route('recosSearchForUserConnect') }}"
                              @endif id="formFilter">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="mode" value="{{ $mode }}">
                            <div class="row justify-content-center">
                                <div class="form-group col-lg-1 col-xl-1">
                                    <div class="card-title">Filtre</div>
                                </div>
                                <div class="form-group col-lg-2 col-xl-2">
                                    <select class="form-control form-control-sm" id="fil_status" name="fil_status">
                                        <option value="">Choisir le statut...</option>
                                        @foreach($statuts as $key => $value)
                                            <option value="{{ $key }}" @if($statut == $key) selected @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="from-group col-lg-3 col-xl-3">
                                    <select class="form-control form-control-sm" id="fil_instance" name="fil_instance"
                                            onchange="onChangeInstance()">
                                        <option value="">Choisir l'instance...</option>
                                        @foreach($instances as $instance)
                                            <option value="{{ $instance->ins_id }}"
                                                    @if($instance->ins_id == $instance_id) selected @endif>
                                                {{ $instance->ins_nom_complet . ' (' . $instance->ins_sigle . ')' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="from-group col-lg-2 col-xl-2">
                                    <select class="form-control form-control-sm" id="fil_session" name="fil_session">
                                        <option value="">Choisir session de l'instance...</option>
                                        @if(count($sessions) > 0)
                                            @foreach($sessions as $session)
                                                <option value="{{ $session->ses_id }}"
                                                        @if($session->ses_id == $session_id) selected @endif>
                                                    {{ $session->ses_description }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="input-group col-lg-2 col-xl-2">
                                    <input type="number" class="form-control form-control-sm" id="fil_annee"
                                           name="fil_annee" value="{{ $annee }}" readonly/>
                                    <div class="input-group-append">
                                        <span class="input-group-text form-control form-control-sm">
                                            {{--<i class="pe-7s-date"></i>--}}
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="from-group col-lg-2 col-xl-2">
                                    <button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
                                        <i class="fa fa-search"></i>&nbsp;Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row my-3">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                @include('backend.recommandations.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
    <script>
        $('#fil_status').select2({placeholder: "Choisir le statut...", allowClear: true});
        $('#fil_instance').select2({placeholder: "Choisir l'instance...",  allowClear: true});
        $('#fil_session').select2({placeholder: "Choisir session de l'instance...", allowClear: true});

        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            viewMode: 'years',
            format: 'YYYY',
            minDate: '2015',
            showClose: true,
            showClear: true,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        }).parent().css("position :relative");

        function onChangeStatus() {
            var recId = $('#rec_id').val();
            var status = $('input:radio[name="rec_statut"]:checked').val();
            $.ajax({
                url: "{{ url("/getReco") }}/" + recId,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    if (status == 2) {
                        $('#observation').val(data.rec_obs_nonrealise);
                    }
                    else if (status == 3) {
                        $('#observation').val(data.rec_obs_encours);
                    }
                    else if (status == 4) {
                        $('#observation').val(data.rec_obs_realise);
                    } else {
                        $('#observation').val(null);
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });

        }

        function onChangeInstance() {
            var instance = $('#fil_instance').val();
            $.ajax({
                url: "{{ url('/getAllSessions') }}",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    console.log(data);
                    $('#fil_session').empty();
                    $('#fil_session').append($('<option>', {
                        value: "",
                        text: 'Choisir session de l\'instance...'
                    }));
                    $.each(data, function (index, session) {
                        if (instance == session.ins_id) {
                            $('#fil_session').append($('<option>', {
                                value: session.ses_id,
                                text: session.ses_description
                            }));
                        }
                    });
//                    $('#fil_session').attr('required', true);
                }
            });
        }

        function onChangeInstanceMod() {
            var instance = $('#ins_id').val();
            $.ajax({
                url: "{{ url('/getAllSessionsEnFormulation') }}",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#ses_id').empty();
                    $('#ses_id').append($('<option>', {
                        value: null,
                        text: "Choisir session de l'instance..."
                    }));
                    $.each(data, function (index, session) {
                        if (instance == session.ins_id) {
                            $('#ses_id').append($('<option>', {
                                value: session.ses_id,
                                text: session.ses_description
                            }));
                        }
                    });
                }
            });
        }

        function openModalAddReco(id) {
            $('#ins_id').select2({width: '100%', dropdownParent: $("#modalAddReco"),  placeholder: "Choisir instance...", allowClear: true});
            $('#ses_id').select2({width: '100%', dropdownParent: $("#modalAddReco"),  placeholder: "Choisir session de l'instance...", allowClear: true});
            $('#bud_id').select2({width: '100%', dropdownParent: $("#modalAddReco"),  placeholder: "Choisir programme budgétaire...", allowClear: true});

            $('#formAddReco').attr('action', "{{ route('saveReco') }}");
            $.ajax({
                {{--url: "{{ url('/getAllInstances') }}",--}}
                url: "{{ url('/getAllInstancesCSS') }}",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#ins_id').empty();
                    $('#ins_id').append($('<option>', {
                        value: null,
                        text: "Choisir instance..."
                    }));
                    $('#ses_id').empty();
                    $('#ses_id').append($('<option>', {
                        value: null,
                        text: "Choisir session de l'instance..."
                    }));
                    $.each(data, function (index, instance) {
                        $('#ins_id').append($('<option>', {
                            value: instance.ins_id,
                            text: instance.ins_sigle
                        }));
                    });

                    if (id == null) {
                        $.ajax({
                            url: "{{ url('/getAllProgBugs') }}",
                            type: "GET",
                            dataType: "JSON",
                            success: function (responses) {
                                $('#bud_id').empty();
                                $('#bud_id').append($('<option>', {
                                    value: null,
                                    text: "Choisir programme budgétaire..."
                                }));
                                $.each(responses, function (index, budge) {
                                    $('#bud_id').append($('<option>', {
                                        value: budge.bud_id,
                                        text: budge.bud_intitule
                                    }));
                                });
                                $('#bud_id').val(data.bud_id).trigger('change');
                            }
                        });

                        $('.modal-title').text('Ajout d\'une nouvelle recommandation');
                        $('#btnSaveReco').empty();
                        $('#btnSaveReco').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                        $('#rec_id').val(null);
                        $('#rec_intitule').val(null);
                        $('#rec_date_echeance').val(null);
                        $('#rec_personne_formule').val(null);
                        $('#rec_personne_tel').val(null);
                        $('#rec_observation').val(null);
                        $('input:radio[name="rec_is_permanente"][value="0"]').prop('checked', true);
                    }
                    else {
                        $.ajax({
                            url: "{{ url('/getReco') }}/" + id,
                            type: "GET",
                            dataType: "JSON",
                            success: function (data) {
                                console.log(data);
                                $('.modal-title').text('Modification de la recommandation');
                                $('#btnSaveReco').empty();
                                $('#btnSaveReco').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                                $('#rec_id').val(data.rec_id);
                                $('#ins_id').val(data.ins_id).trigger('change');
                                $('#ses_id').val(data.ses_id).trigger('change');
                                $('#rec_intitule').val(data.rec_intitule);
                                $('#rec_date_echeance').val(data.rec_date_echeance);
                                $('#rec_personne_formule').val(data.rec_personne_formule);
                                $('#rec_personne_tel').val(data.rec_personne_tel);
                                $('#rec_observation').val(data.rec_observation);
                                if (data.rec_is_permanente == 0) {
                                    $('input:radio[name="rec_is_permanente"][value="0"]').prop('checked', true);
                                } else {
                                    $('input:radio[name="rec_is_permanente"][value="1"]').prop('checked', true);
                                }

                                $.ajax({
                                    url: "{{ url('/getAllProgBugs') }}",
                                    type: "GET",
                                    dataType: "JSON",
                                    success: function (responses) {
                                        $('#bud_id').empty();
                                        $('#bud_id').append($('<option>', {
                                            value: null,
                                            text: "Choisir programme budgétaire..."
                                        }));
                                        $.each(responses, function (index, budge) {
                                            $('#bud_id').append($('<option>', {
                                                value: budge.bud_id,
                                                text: budge.bud_intitule
                                            }));
                                        });
                                        $('#bud_id').val(data.bud_id).trigger('change');
                                    }
                                });
                            },
                            error: function () {
                                alert("Erreur rencontrer lors de la recuperation des donnees!");
                            }
                        });
                    }
                }
            });
        }

        function openModalAddSuivi(id) {
            $.ajax({
                url: "{{ url("/getReco") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#formAddSuivi').attr('action', '{{ url("/doSuiviReco") }}/' + data.rec_id);
                    $('.modal-title').text('Suivi de la recommandation');
                    $('#btnSaveSuivi').empty();
                    $('#btnSaveSuivi').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                    $('#rec_id').val(id);
                    if (data.rec_statut == 2) {
                        $('input:radio[name="rec_statut"][value="2"]').prop('checked', true);
                        $('input:radio[name="rec_statut"][value="3"]').prop('checked', false);
                        $('input:radio[name="rec_statut"][value="4"]').prop('checked', false);
                        $('#observation').val(data.rec_obs_nonrealise);
                    }
                    else if (data.rec_statut == 3) {
                        $('input:radio[name="rec_statut"][value="2"]').prop('checked', false);
                        $('input:radio[name="rec_statut"][value="3"]').prop('checked', true);
                        $('input:radio[name="rec_statut"][value="4"]').prop('checked', false);
                        $('#observation').val(data.rec_obs_encours);
                    }
                    else if (data.rec_statut == 4) {
                        $('input:radio[name="rec_statut"][value="2"]').prop('checked', false);
                        $('input:radio[name="rec_statut"][value="3"]').prop('checked', false);
                        $('input:radio[name="rec_statut"][value="4"]').prop('checked', true);
                        $('#observation').val(data.rec_obs_realise);
                    } else {
                        $('input:radio[name="rec_statut"][value="2"]').prop('checked', false);
                        $('input:radio[name="rec_statut"][value="3"]').prop('checked', false);
                        $('input:radio[name="rec_statut"][value="4"]').prop('checked', false);
                        $('#observation').val(null);
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        function openModalMajObs(id) {
            $.ajax({
                url: "{{ url("/getReco") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#formMajObservation').attr('action', '{{ url("/modifierObservationReco") }}');
                    $('.modal-title').html('Modification de l’observation de la recommandation <br/><span class="text-danger text-bold">"' + data.rec_intitule + '"</span>');
                    $('#btnMajObservation').empty();
                    $('#btnMajObservation').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                    $('#reco_id').val(id);
                    $('#reco_observation').val(data.rec_observation);
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        function openModalDetailsReco(id) {
            $('#zoneDate').hide();
            $('#zoneMotif').hide();
            $('#historique').hide();
            $('#histNonRealise').hide();
            $('#histEnCours').hide();
            $('#histRealise').hide();
            $('.modal-title').text('Détails de la recommandation');
            $.ajax({
                url: "{{ url("/getReco") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var statut, planning, mode, valideSuivi, permanente = '';
                    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
                    var optionsWithTime = {year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"};

                    /** rec_statut */
                    if (data.rec_statut == 1) {
                        statut = '<span class="badge badge-primary">Pr&eacute;aration</span>';
                    }
                    else if (data.rec_statut == 2) {
                        statut = '<span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>';
                    }
                    else if (data.rec_statut == 3) {
                        statut = '<span class="badge badge-info">En cours</span>';
                    }
                    else if (data.rec_statut == 4) {
                        statut = '<span class="badge badge-success">R&eacute;alis&eacute;e</span>';
                    }
                    else if (data.rec_statut == 5) {
                        statut = '<span class="badge badge-danger">Abandonn&eacute;e</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_is_planning_valide */
                    if (data.rec_is_planning_valide == 1) {
                        planning = '<span class="badge badge-success">Oui</span>';
                    }
                    else if (data.rec_is_planning_valide == 0) {
                        planning = '<span class="badge badge-danger">Non</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_is_mode_standard */
                    if (data.rec_is_mode_standard == 1) {
                        mode = '<span class="badge badge-info">Par statut</span>';
                    }
                    else if (data.rec_is_mode_standard == 0) {
                        mode = '<span class="badge badge-primary">Par taux</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_is_suivi_valide */
                    if (data.rec_is_suivi_valide == 1) {
                        valideSuivi = '<span class="badge badge-success">Oui</span>';
                    }
                    else if (data.rec_is_suivi_valide == 0) {
                        valideSuivi = '<span class="badge badge-danger">Non</span>';
                    }
                    else {
                        valideSuivi = '<span></span>';
                    }
                    /** rec_is_permanente */
                    if (data.rec_is_permanente == 1) {
                        permanente = '<span class="badge badge-success">Oui</span>';
                    }
                    else if (data.rec_is_permanente == 0) {
                        permanente = '<span class="badge badge-danger">Non</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_dh_valide ou rec_dh_abandon */
                    if (data.rec_is_suivi_valide == 1) {
                        $('#zoneDate').show();
                        $('#detDateLib').text("Validée le");
                        var dtVal = new Date(data.rec_dh_valide);
                        var datVal = dtVal.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detDateValue').text(datVal);
//                        $('#detDateValue').text(dateFormat(data.rec_dh_valide, "dd/mm/yyyy à HH:mm:ss"));
                    }
                    if (data.rec_statut == 5) {
                        $('#zoneDate').show();
                        $('#detDateLib').text("Abandonnée le");
                        var dtAb = new Date(data.rec_dh_abandon);
                        var datAb = dtAb.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detDateValue').text(datAb);
//                        $('#detDateValue').text(dateFormat(data.rec_dh_abandon, "dd/mm/yyyy à HH:mm:ss"));
                    }

                    if(data.rec_motif_abandon) {
                        $('#zoneMotif').show();
                        $('#detMotif').text(data.rec_motif_abandon);
                    }

                    $('#detIns').text(data.ins_nom_complet + ' (' + data.ins_sigle + ')');
                    $('#detPB').text(data.bud_intitule);
                    $('#detInti').text(data.rec_intitule);
                    $('#detStatut').html(statut);
                    $('#detPlan').html(planning);
                    $('#detValid').html(valideSuivi);
                    $('#detMode').html(mode);
                    $('#detObs').text(data.rec_observation);
                    $('#detPers').text(data.rec_personne_formule);
                    $('#detPersTel').text(data.rec_personne_tel);
                    $('#detTaux').text(data.rec_taux_realise + '%');
                    $('#detPrt').html(permanente);

                    var dtDeb = new Date(data.ses_date_debut);
                    var datDeb = dtDeb.toLocaleDateString("fr-FR", options);
                    $('#detSes').text(data.ses_description + ' du ' + datDeb);
                    // $('#detDateEch').text(dateFormat(data.rec_date_echeance, "dd/mm/yyyy"));

                    var dtEch = new Date(data.rec_date_echeance);
                    var datEch = dtEch.toLocaleDateString("fr-FR", options);
                    $('#detDateEch').html('<span class="badge badge-danger">' + datEch + '</span>');

                    if (data.created_at != null && data.created_at != '') {
                        // $('#detCreatedAt').text(dateFormat(data.created_at, "dd/mm/yyyy à HH:mm:ss"));
                        var dtCree = new Date(data.created_at);
                        var datCree = dtCree.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detCreatedAt').text(datCree);
                        $('#detCreatedBy').text(data.created_by);
                    }
                    if (data.updated_at != null && data.updated_at != '') {
                        // $('#detUpdatedAt').text(dateFormat(data.updated_at, "dd/mm/yyyy à HH:mm:ss"));
                        var dtMod = new Date(data.updated_at);
                        var datMod = dtMod.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detUpdatedAt').text(datMod);
                        $('#detUpdatedBy').text(data.updated_by);
                    }

                    /** ***************************** HISTORIQUE ********************************* */
                    var urlDown = "";
                    if (data.rec_obs_nonrealise && data.rec_dh_nonrealise) {
                        $('#historique').show();
                        $('#histNonRealise').show();
                        $('#obsNonRealise').text(data.rec_obs_nonrealise);
                        var dtNonReal = new Date(data.rec_dh_nonrealise);
                        var datNonReal = dtNonReal.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateNonRealise').text(datNonReal);
                        if (data.rec_preuve_nonrealise) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_nonrealise;
                            $('#downNonRealise').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +' target="_blank">' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }

                    if (data.rec_obs_encours && data.rec_dh_en_cours) {
                        $('#historique').show();
                        $('#histEnCours').show();
                        $('#obsEnCours').text(data.rec_obs_encours);
                        var dtEnCours = new Date(data.rec_dh_en_cours);
                        var datEnCours = dtEnCours.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateEnCours').text(datEnCours);
                        if (data.rec_preuve_encours) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_encours;
                            $('#downEnCours').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +' target="_blank">' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }

                    if (data.rec_obs_realise && data.rec_dh_realise) {
                        $('#historique').show();
                        $('#histRealise').show();
                        $('#obsRealise').text(data.rec_obs_realise);
                        var dtRealise = new Date(data.rec_dh_realise);
                        var datRealise = dtRealise.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateRealise').text(datRealise);
                        if (data.rec_preuve_realise) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_realise;
                            $('#downRealise').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +' target="_blank">' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        function validerPlanning(id, libelle) {
            var txtTest, txtConfirme, colorConfirm, txtSuccess, txtError = "";
            txtTest = "Etes vous sûr de vouloir valider la planification de cette recommandation ?";
//            txtTest = "Etes vous sûr de vouloir valider la planification de la recommandation : " + libelle + " ?";
            txtConfirme = "Oui, valider";
            colorConfirm = '#14671c';
            txtSuccess = "Validation de la planification de la recommandation effectuée avec succès!";
            txtError = "Une erreure est survenue lors de la validation de la planification de la recommandation!\nVeuillez reessayer!";
            swal({
                title: 'CONFIRMATION',
                text: txtTest,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: colorConfirm,
                cancelButtonColor: '#3085d6',
                confirmButtonText: txtConfirme,
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{ url('/validerPlanningReco') }}/" + id,
                            type: "post",
                            data: {rec_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data.status == "success") {
                                    title = "Succes";
                                    type = "success";
                                    text = txtSuccess;
                                }
                                else if (data.status == "moins_cent") {
                                    title = "Opération échouée";
                                    type = "error";
                                    text = "Le cumul des pondérations des activités ne vaut pas 100";
                                }
                                else if (data.status == "not_found") {
                                    title = "Opération échouée";
                                    type = "error";
                                    text = "La recommandation est introuvable";
                                }
                                swal({
                                    title: title,
                                    type: type,
                                    text: text,
                                    button: "OK !"
                                }).then(function () {
                                    location.reload();
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: txtError,
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function validerSuivi(id, libelle) {
            var txtTest, txtConfirme, colorConfirm, txtSuccess, txtError = "";
//            txtTest = "Etes vous sûr de vouloir valider le suivi de la recommandation : " + libelle + " ?";
            txtTest = "Etes vous sûr de vouloir valider le suivi de cette recommandation ?";
            txtConfirme = "Oui, valider";
            colorConfirm = '#14671c';
            txtSuccess = "Validation du suivi de la recommandation effectuée avec succès!";
            txtError = "Une erreure est survenue lors de la validation du suivi de la recommandation!\nVeuillez reessayer!";
            swal({
                title: 'CONFIRMATION',
                text: txtTest,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: colorConfirm,
                cancelButtonColor: '#3085d6',
                confirmButtonText: txtConfirme,
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{ url('/validerSuiviReco') }}/" + id,
                            type: "post",
                            data: {rec_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                swal({
                                    title: "Succes",
                                    text: txtSuccess,
                                    type: "success",
                                    button: "OK"
                                }).then(function () {
                                    location.reload();
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: txtError,
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function abandonner(id, libelle) {
            swal({
                title: 'CONFIRMATION',
//                text: "Etes vous sûr de vouloir abandonner cette recommandation : " + libelle + " ?",
                text: "Etes vous sûr de vouloir abandonner cette recommandation ?",
                type: 'warning',
                input: 'textarea',
                inputPlaceholder: "Motif d'abandon",
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: "Oui, abandonner",
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                animation: "slide-from-top",
                preConfirm: function (value) {
                    return new Promise(function (resolve) {
                        if (value != '' && value != null) {
//                            swal("success", 'Motif saisi : ' + value);
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                }
                            });
                            $.ajax({
                                url: "{{ url('/abandonnerReco') }}/" + id + "/" + value,
                                type: "post",
                                data: {rec_id: id, '_token': '{{csrf_token()}}'},
                                success: function (data) {
                                    swal({
                                        title: "Succes",
                                        text: "Abandon de la recommandation effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                },
                                error: function (data) {
                                    swal({
                                        title: 'Erreur',
                                        text: "Une erreure est survenue lors de l'Abandon de la recommandation!\nVeuillez reessayer!",
                                        type: 'error',
                                        button: "OK"
                                    });
                                }
                            });
                        } else {
                             swal("Erreur", 'SVP, indiquer le motif');
                            // swal.showInputError('SVP, indiquer le motif');
                        }
                    });
                },
                allowOutsideClick: false
            });
        }

        function changerModeSuivi(id) {
            /* inputOptions can be an object or Promise */
            const inputOptions = new Promise(function (resolve) {
                setTimeout(function () {
                    resolve({
                        '1': 'Suivre les recommandations par statut (En cours, non réalisé et réalisé)',
                        '0': 'Suivre les recommandations par taux (séquencé par activité)'
                    })
                }, 1000)
            });
            swal({
                title: 'Choisir le mode de suivi',
                input: 'radio',
                inputOptions: inputOptions,
                inputValidator: function(value) {
                    if (!value) {
                        return 'SVP, veuillez choisir un mode !'
                    }
                    else if (value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{ url('/changerModeSuivi') }}/" + id + "/" + value,
                            type: "post",
                            data: {rec_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                swal({
                                    title: "Succes",
                                    text: "Modification du mode de suivi effectuée avec succès!",
                                    type: "success",
                                    button: "OK"
                                }).then(function () {
                                    location.reload();
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la Modification!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    }
                },
                allowOutsideClick: true
            });
        }

        function openModalListPartenaire(id) {
            $.ajax({
                url: "{{ url('/getListPartenairesByReco') }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var contentBody = '';
                    if (data.length > 0) {
                        $('#msgError').hide();
                        $('#tabPart').show();
                        $('.modal-title').text('Liste des partenaires de la recommandation : ' + data[0].rec_intitule);
                        $.each(data, function (index, part) {
                            var resp = '';
                            if (part.ptn_is_responsable == 0) {
                                resp = '<span class="badge badge-danger">Non</span>';
                            } else if (part.ptn_is_responsable == 1) {
                                resp = '<span class="badge badge-success">Oui</span>';
                            } else {
                                resp = '';
                            }
                            contentBody += '<tr>' +
                                '<td>' + (index + 1) + '</td>' +
                                '<td>' + part.str_sigle + '</td>' +
                                '<td>' + part.str_nom_complet + '</td>' +
                                '<td>' + resp + '</td>' +
                                '</tr>';
                        });
                        $('#listPart').html(contentBody);
                    } else {
                        $('.modal-title').text('Liste des partenaires de la recommandation');
                        $('#tabPart').hide();
                        $('#msgError').show();
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        function deleteReco(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement cette recommandation ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                /*showClass: {
                    popup: 'animated fadeInDown faster'
                },
                hideClass: {
                    popup: 'animated fadeOutUp faster'
                },*/
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{ url('/deleteReco') }}/" + id,
                            type: "get",
                            data: {rec_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cette recommandation, elle est rattachée à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression de la recommandation effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression de la recommandation!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function openModalImport() {
            $('#modImportTitre').text("Importation des recommandations en format Excel");
        }

        function importSave() {
            var tailleMax = parseInt($('#taille_max').val());
            var myFile = document.getElementById("fichier");
            var fsize = myFile.files[0].size,
                fname = myFile.files[0].name,
                fextension = fname.substring(fname.lastIndexOf('.') + 1);
            if (fextension.toLowerCase() == "xlsx") {
                if (fsize <= tailleMax) {
                    swal({
                        title: 'CONFIRMATION',
                        text: "Voullez-vous importer ces données ?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#115e1e',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Oui, importer!',
                        cancelButtonText: 'Non, annuler',
                        reverseButtons: true,
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                    }
                                });
                                $.ajax({
                                    url: "{{ url('/importRecos') }}",
                                    type: "POST",
                                    data: new FormData($("#formImport")[0]),
                                    processData: false,
                                    contentType: false,
                                    success: function (data) {
                                        var title = "";
                                        var type = "";
                                        var text = "";
                                        if (data.status == "no-open") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "Echec d'ouverture du fichier";
                                        }
                                        else if (data.status == "no-file") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "Fichier de données non valide";
                                        }
                                        else if (data.status == "success-with-error") {
                                            title = "Opération reussie";
                                            type = "warning";
                                            text = "Importation partielle des données effectuée avec succès.\n Veuillez consulter le fichier log pour plus d'information";
                                        }
                                        else if (data.status == "success") {
                                            title = "Opération reussie";
                                            type = "success";
                                            text = "Importation des données effectuée avec succès. Merci de consulter les log pour plus de détails!";
                                        }
                                        else if (data.status == "ext-no-valide") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "L'extension du fichier n'est pas valide, veuillez selectionner un fichier .xlsx";
                                        }
                                        else if (data.status == "taille-no-valide") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "La taille du fichier ne doit pas dépassé " + tailleMax / (1024*1024) + "Mo !"
                                        }
                                        swal({
                                            title: title,
                                            type: type,
                                            text: text,
                                            button: "OK !"
                                        }).then(function () {
                                            window.location = "getPageSaisieRecos?log=true";
                                        });
                                    },
                                    error: function (data) {
                                        swal({
                                            title: 'Erreur',
                                            type: "error",
                                            text: "Une erreure est survenue lors de l'importation des données! Veuillez reessayer!",
                                            button: 'Réessayer !'
                                        });
                                    }
                                });
                            });
                        },
                        allowOutsideClick: false
                    });
                } else {
                    swal({
                        title: 'Erreur',
                        type: "error",
                        text: "La taille du fichier ne doit pas dépassé " + tailleMax / (1024*1024) + "Mo !"
                    });
                }
            } else {
                swal({
                    title: 'Erreur',
                    type: "error",
                    text: "L'extension du fichier n'est pas valide, veuillez selectionner un fichier .xlsx"
                });
            }
        }

        /*function reload() {
            {{--var url = "{{ route('recos.reload') }}";--}}
            $.ajax({
                url: url,
                type: "get",
                datatype: "html"
            }).done(function (data) {
                // append liste of directions to the table directions-table
                $('#tb_recos').empty().html(data);
                $('#tb_recos').DataTable().clear();
                $('#tb_recos').DataTable().destroy();
                var tab = $('#tb_recos').DataTable({
                    dom: 'lBfrtip',
                    responsive: true,
                    processing: true,
                    select: true,
                    "language": {
                        "url": "{{ asset('lang/datatables.french.json')}}"
                    },
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ],
                    "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                        "searchable": false
                    }]
                });
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                var prom = ezBSAlert({
                    messageText: "No response from server on load Error : " + thrownError,
                    alertType: "danger"
                });
            });
        }*/
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT recommandation--}}
    @include('backend.recommandations.modalAddReco')

    {{--MODAL Details recommandation--}}
    @include('backend.recommandations.modalDetails')

    {{--MODAL Maj suivi--}}
    @include('backend.recommandations.modalAddSuivi')

    {{--MODAL Liste partenaire--}}
    @include('backend.recommandations.modalListPart')

    {{-- Modal import fichier --}}
    @include('backend.recommandations.modalImport')

    {{--MODAL Maj observation reco--}}
    @include('backend.recommandations.modalMajObservation')
@endsection
