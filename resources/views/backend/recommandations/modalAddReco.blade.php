<div class="modal fade my-5" id="modalAddReco" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" {{--novalidate--}} method="POST" action="#" id="formAddReco">
                <div class="modal-body">
                    <div class="row {{--justify-content-center--}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="rec_id" name="rec_id" type="hidden">
                        {{--<input id="ses_id" name="ses_id" @if($session) value="{{ $session->ses_id }}"
                               @endif type="hidden">--}}

                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="ins_id">Instance <span class="requit">*</span></label>
                                    <select class="form-control form-control-sm selectSearchModal" id="ins_id" name="ins_id"
                                            onchange="onChangeInstanceMod()" required>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="bud_id">Programme budgétaire <span class="requit">*</span></label>
                                    <select class="form-control form-control-sm" id="bud_id" name="bud_id" required></select>
                                </div>

                                <div class="form-group col-md-8">
                                    <label for="rec_personne_formule">Formul&eacute;e par</label>
                                    <input type="text" class="form-control form-control-sm" id="rec_personne_formule"
                                        name="rec_personne_formule" maxlength="255" placeholder="Nom & pr&eacute;nom"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="rec_personne_tel">Son contact</label>
                                    <input type="text" class="form-control form-control-sm" id="rec_personne_tel"
                                        name="rec_personne_tel" maxlength="100"/>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="rec_date_echeance">Date d'&eacute;cheance <span
                                                class="requit">*</span></label>
                                    <input type="date" class="form-control form-control-sm" id="rec_date_echeance"
                                        name="rec_date_echeance" required/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="ses_id">Session <span class="requit">*</span></label>
                                    <select class="form-control form-control-sm selectSearchModal" id="ses_id" name="ses_id" required>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="rec_intitule">Intitul&eacute; <span class="requit">*</span></label>
                                    <textarea class="form-control" id="rec_intitule" name="rec_intitule"
                                    rows="4" maxlength="255" required></textarea>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="rec_is_mode_standard">Permanente
                                        ? {{--<span class="requit">*</span>--}}</label><br/>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="rec_is_permanente1" name="rec_is_permanente"
                                               class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="rec_is_permanente1">Oui</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="rec_is_permanente2" name="rec_is_permanente"
                                               class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="rec_is_permanente2">Non</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                        <i class="fa fa-ban"></i>&nbsp;Fermer
                    </button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSaveReco"></button>
                </div>
            </form>
        </div>
    </div>
</div>
