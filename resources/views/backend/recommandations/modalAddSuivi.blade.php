<div class="modal fade my-5" id="modalAddSuivi" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" enctype="multipart/form-data" action="#" id="formAddSuivi">
                <div class="modal-body">
                    <div class="row {{--justify-content-center--}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="rec_id" name="rec_id" type="hidden">

                        <div class="form-group col-md-3">
                            <label>Etat de mise en &oelig;uvre <span class="requit">*</span> :</label>
                        </div>
                        <div class="form-group col-md-9">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="rec_statut1" name="rec_statut"
                                       class="custom-control-input" value="2" onclick="onChangeStatus()">
                                <label class="custom-control-label" for="rec_statut1">Non
                                    r&eacute;alis&eacute;e</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="rec_statut2" name="rec_statut"
                                       class="custom-control-input" value="3" onclick="onChangeStatus()">
                                <label class="custom-control-label" for="rec_statut2">En cours</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="rec_statut3" name="rec_statut"
                                       class="custom-control-input" value="4" onclick="onChangeStatus()">
                                <label class="custom-control-label" for="rec_statut3">R&eacute;alis&eacute;e</label>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="observation">Observations <span class="requit">*</span></label>
                            <textarea class="form-control" name="observation" id="observation"
                                      rows="4" required></textarea>
                        </div>

                        <div class="input-group col-md-12">
                            <label for="preuve">Pi&egrave;ce jointe{{-- <span class="requit">*</span>--}}</label>
                            <input type="file" id="preuve" name="preuve" class="form-control form-control-sm" {{--required--}}>
                            <div class="input-group-append">
                                <span class="input-group-text form-control form-control-sm text-danger"
                                      style="font-weight: bold;">Max {{ intval(config('app.taille_max_upload')) / (1024 * 1024) }}Mo
                                </span>
                            </div>
                        </div>

                        <div class="form-group col-md-12 text-center">
                            <span class="text-danger">Extensions valides : pdf, png, jpg, jpeg, doc, docx, xls ou xlsx</span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                        <i class="fa fa-ban"></i>&nbsp;Fermer
                    </button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSaveSuivi"></button>
                </div>
            </form>
        </div>
    </div>
</div>