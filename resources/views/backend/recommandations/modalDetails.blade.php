<div class="modal fade" id="modalDetails" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <tbody>
                        <tr>
                            <th>Instance</th>
                            <td id="detIns" colspan="3"></td>
                        </tr>

                        <tr>
                            <th>Session</th>
                            <td id="detSes" colspan="3"></td>
                        </tr>

                        <tr>
                            <th>Intitul&eacute;</th>
                            <td id="detInti" colspan="3"></td>
                        </tr>

                        <tr>
                            <th>Programme budgétaire</th>
                            <td id="detPB"></td>
                            <th>Date d'&eacute;cheance</th>
                            <td id="detDateEch"></td>
                        </tr>

                        <tr>
                            <th>Statut</th>
                            <td id="detStatut"></td>
                            <th>Planification valid&eacute;e ?</th>
                            <td id="detPlan"></td>
                        </tr>

                        <tr>
                            <th>Permanente ?</th>
                            <td id="detPrt"></td>
                            <th>Suivi valid&eacute; ?</th>
                            <td id="detValid"></td>
                        </tr>

                        <tr>
                            <th>Mode de suivi</th>
                            <td id="detMode"></td>
                            <th>Taux de r&eacute;alisation</th>
                            <td id="detTaux"></td>
                        </tr>

                        <tr>
                            <th>Formul&eacute;e par</th>
                            <td id="detPers"></td>
                            <th>Son contact</th>
                            <td id="detPersTel"></td>
                        </tr>

                        <tr id="zoneDate">
                            <th id="detDateLib"></th>
                            <td id="detDateValue"></td>
                            <td colspan="2"></td>
                        </tr>

                        <tr id="zoneMotif">
                            <th>Motif d'abandon</th>
                            <td id="detMotif" colspan="3"></td>
                        </tr>

                        <tr>
                            <th>Cr&eacute;e le</th>
                            <td id="detCreatedAt"></td>
                            <th>Par</th>
                            <td id="detCreatedBy"></td>
                        </tr>

                        <tr>
                            <th>Modifi&eacute; le</th>
                            <td id="detUpdatedAt"></td>
                            <th>Par</th>
                            <td id="detUpdatedBy"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive" id="historique">
                    <h5>Historique du suivi</h5>
                    <table class="table table-bordered table-sm table-striped table-hover">
                     <thead class="thead-dark">
                        <tr>
                            <th>Statut</th>
                            <th>Observation</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                     </thead>
                        <tbody>
                        <tr id="histNonRealise">
                            <th><span class="badge badge-danger">Non r&eacute;alis&eacute;e</span></th>
                            <td id="obsNonRealise"></td>
                            <td id="dateNonRealise"></td>
                            <td class="text-right" id="downNonRealise"></td>
                        </tr>

                        <tr id="histEnCours">
                            <th><span class="badge badge-info">En cours</span></th>
                            <td id="obsEnCours"></td>
                            <td id="dateEnCours"></td>
                            <td class="text-right" id="downEnCours"></td>
                        </tr>

                        <tr id="histRealise">
                            <th><span class="badge badge-success">R&eacute;alis&eacute;e</span></th>
                            <td id="obsRealise"></td>
                            <td id="dateRealise"></td>
                            <td class="text-right" id="downRealise"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                    <i class="fa fa-ban"></i>&nbsp;Fermer
                </button>
            </div>
        </div>
    </div>
</div>