<div class="modal fade my-5" id="modalImport" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form class="needs-validation" novalidate action="#" method="POST" enctype="multipart/form-data" id="formImport">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modImportTitre"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input id="taille_max" name="taille_max" value="{{ config('app.taille_max_import_recos') }}" type="hidden">
                    <div class="row justify-content-center">
                        <div class="form-group col-md-12 col-lg-12 mb-3">
                            <label for="fichier" class="control-label">Fichier Excel <span
                                        class="requit">(xlsx) *</span></label>
                            <div class="input-group">
                                <input id="fichier" type="file" class="form-control form-control-sm" name="fichier"
                                       required
                                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                                <div class="input-group-append">
                                    <span class="input-group-text form-control form-control-sm text-danger"
                                          style="font-weight: bold;">Max {{ config('app.taille_max_import_recos') / (1024*1024) }}Mo</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Fermer</button>
                    <a href="{{ route('downloadTemplateImport') }}" class="btn btn-sm btn-info text-white">T&eacute;l&eacute;charger le canevas</a>
                    <a onclick="importSave()" class="btn btn-sm btn-success text-white">Importer les données</a>
                </div>
            </div>
        </form>
    </div>
</div>