<div class="modal fade my-5" id="modalListPart" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-striped table-hover" id="tabPart">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Sigle</th>
                            <th>Nom complet</th>
                            <th>Responsable ?</th>
                        </tr>
                        </thead>
                        <tbody id="listPart"></tbody>
                    </table>
                    <div id="msgError" class="badge badge-danger">
                        Pour l'instant il n'y a pas de partenaire pour cette recommandation !
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                    <i class="fa fa-ban"></i>&nbsp;Fermer
                </button>
            </div>
        </div>
    </div>
</div>