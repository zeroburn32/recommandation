<div class="modal fade my-5" id="modalMajObservation" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" action="#" id="formMajObservation">
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="reco_id" name="rec_id" type="hidden">
                        <div class="form-group col-md-12">
                            <label for="reco_observation">Observations <span class="requit">*</span></label>
                            <textarea class="form-control" name="rec_observation" id="reco_observation"
                                      rows="5" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                        <i class="fa fa-ban"></i>&nbsp;Fermer
                    </button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnMajObservation"></button>
                </div>
            </form>
        </div>
    </div>
</div>