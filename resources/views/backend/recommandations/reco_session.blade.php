@extends('layouts.template')

@section('title')
    Recommandations
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Sessions</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des recommandations</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des recommandations de la session du :
                                    <span class="text-danger">
                                    {{ \Carbon\Carbon::parse($session->ses_date_debut)->format('d/m/Y') }}</span>
                                    de l'instance <span class="text-danger">{{ $session->ins_nom_complet }}</span>
                                </div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-warning"
                                        onclick="goBack()">
                                    <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                                </button>
                                {{--<a type="button" class="btn btn-sm btn-primary"
                                   href="{{ url('getPageRecos') }}">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Gestion des recommandations
                                </a>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row my-3">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive" style="min-height: 45vh;">
                                    <table class="table table-bordered table-sm table-striped table-hover dt_example">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>#</th>
                                            <th>Session</th>
                                            <th>Ann&eacute;e</th>
                                            <th>Intitul&eacute;</th>
                                            <th>Date d'&eacute;cheance</th>
                                            <th>Statut</th>
                                            <th>Mode de suivi</th>
                                            <th>Taux de r&eacute;alisation</th>
                                            <th class="text-center no-sort">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($recos as $number=>$reco)
                                            <tr>
                                                <td>{{ $number + 1 }}</td>
                                                <td>{{ $reco->ses_description }}</td>
                                                <td>{{ $reco->ses_annee }}</td>
                                                <td>{{ $reco->rec_intitule }}</td>
                                                <td>{{ \Carbon\Carbon::parse($reco->rec_date_echeance)->format('d/m/Y') }}</td>
                                                <td>
                                                    @if($reco->rec_statut == 1)
                                                        <span class="badge badge-primary">Pr&eacute;paration</span>
                                                    @elseif($reco->rec_statut == 2)
                                                        <span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>
                                                    @elseif($reco->rec_statut == 3)
                                                        <span class="badge badge-info">En cours</span>
                                                    @elseif($reco->rec_statut == 4)
                                                        <span class="badge badge-success">R&eacute;alis&eacute;e</span>
                                                    @elseif($reco->rec_statut == 5)
                                                        <span class="badge badge-danger">Abandonn&eacute;e</span>
                                                    @else
                                                        <span></span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reco->rec_is_mode_standard == 0)
                                                        <span class="badge badge-primary">Par taux</span>
                                                    @elseif($reco->rec_is_mode_standard == 1)
                                                        <span class="badge badge-info">Par statut</span>
                                                    @else
                                                        <span class="badge badge-warning">ND</span>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ $reco->rec_taux_realise }}%</td>
                                                <td class="text-center">
                                                    <button class="btn btn-info btn-sm"
                                                            onclick="openModalDetailsReco({{ $reco->rec_id }})"
                                                            data-toggle="modal" data-target="#modalDetails">
                                                        <i class="fa fa-eye"></i>&nbsp;&nbsp;D&eacute;tails
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
    <script>
        function openModalDetailsReco(id) {
            $('#zoneDate').hide();
            $('#zoneMotif').hide();
            $('#historique').hide();
            $('#histNonRealise').hide();
            $('#histEnCours').hide();
            $('#histRealise').hide();
            $('.modal-title').text('Détails de la recommandation');
            $.ajax({
                url: "{{ url("/getReco") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var statut, planning, mode, valideSuivi, permanente = '';
                    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
                    var optionsWithTime = {year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"};

                    /** rec_statut */
                    if (data.rec_statut == 1) {
                        statut = '<span class="badge badge-primary">Pr&eacute;aration</span>';
                    }
                    else if (data.rec_statut == 2) {
                        statut = '<span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>';
                    }
                    else if (data.rec_statut == 3) {
                        statut = '<span class="badge badge-info">En cours</span>';
                    }
                    else if (data.rec_statut == 4) {
                        statut = '<span class="badge badge-success">R&eacute;alis&eacute;e</span>';
                    }
                    else if (data.rec_statut == 5) {
                        statut = '<span class="badge badge-danger">Abandonn&eacute;e</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_is_planning_valide */
                    if (data.rec_is_planning_valide == 1) {
                        planning = '<span class="badge badge-success">Oui</span>';
                    }
                    else if (data.rec_is_planning_valide == 0) {
                        planning = '<span class="badge badge-danger">Non</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_is_mode_standard */
                    if (data.rec_is_mode_standard == 1) {
                        mode = '<span class="badge badge-info">Par statut</span>';
                    }
                    else if (data.rec_is_mode_standard == 0) {
                        mode = '<span class="badge badge-primary">Par taux</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_is_suivi_valide */
                    if (data.rec_is_suivi_valide == 1) {
                        valideSuivi = '<span class="badge badge-success">Oui</span>';
                    }
                    else if (data.rec_is_suivi_valide == 0) {
                        valideSuivi = '<span class="badge badge-danger">Non</span>';
                    }
                    else {
                        valideSuivi = '<span></span>';
                    }
                    /** rec_is_permanente */
                    if (data.rec_is_permanente == 1) {
                        permanente = '<span class="badge badge-success">Oui</span>';
                    }
                    else if (data.rec_is_permanente == 0) {
                        permanente = '<span class="badge badge-danger">Non</span>';
                    }
                    else {
                        mode = '<span>ND</span>';
                    }
                    /** rec_dh_valide ou rec_dh_abandon */
                    if (data.rec_is_suivi_valide == 1) {
                        $('#zoneDate').show();
                        $('#detDateLib').text("Validée le");
                        var dtVal = new Date(data.rec_dh_valide);
                        var datVal = dtVal.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detDateValue').text(datVal);
//                        $('#detDateValue').text(dateFormat(data.rec_dh_valide, "dd/mm/yyyy à HH:mm:ss"));
                    }
                    if (data.rec_statut == 5) {
                        $('#zoneDate').show();
                        $('#detDateLib').text("Abandonnée le");
                        var dtAb = new Date(data.rec_dh_abandon);
                        var datAb = dtAb.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detDateValue').text(datAb);
//                        $('#detDateValue').text(dateFormat(data.rec_dh_abandon, "dd/mm/yyyy à HH:mm:ss"));
                    }

                    if(data.rec_motif_abandon) {
                        $('#zoneMotif').show();
                        $('#detMotif').text(data.rec_motif_abandon);
                    }

                    $('#detIns').text(data.ins_nom_complet + ' (' + data.ins_sigle + ')');
                    $('#detPB').text(data.bud_intitule);
                    $('#detInti').text(data.rec_intitule);
                    $('#detStatut').html(statut);
                    $('#detPlan').html(planning);
                    $('#detValid').html(valideSuivi);
                    $('#detMode').html(mode);
                    $('#detObs').text(data.rec_observation);
                    $('#detPers').text(data.rec_personne_formule);
                    $('#detPersTel').text(data.rec_personne_tel);
                    $('#detTaux').text(data.rec_taux_realise + '%');
                    $('#detPrt').html(permanente);

                    var dtDeb = new Date(data.ses_date_debut);
                    var datDeb = dtDeb.toLocaleDateString("fr-FR", options);
                    $('#detSes').text(data.ses_description + ' du ' + datDeb);
                    // $('#detDateEch').text(dateFormat(data.rec_date_echeance, "dd/mm/yyyy"));

                    var dtEch = new Date(data.rec_date_echeance);
                    var datEch = dtEch.toLocaleDateString("fr-FR", options);
                    $('#detDateEch').html('<span class="badge badge-danger">' + datEch + '</span>');

                    if (data.created_at != null && data.created_at != '') {
                        // $('#detCreatedAt').text(dateFormat(data.created_at, "dd/mm/yyyy à HH:mm:ss"));
                        var dtCree = new Date(data.created_at);
                        var datCree = dtCree.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detCreatedAt').text(datCree);
                        $('#detCreatedBy').text(data.created_by);
                    }
                    if (data.updated_at != null && data.updated_at != '') {
                        // $('#detUpdatedAt').text(dateFormat(data.updated_at, "dd/mm/yyyy à HH:mm:ss"));
                        var dtMod = new Date(data.updated_at);
                        var datMod = dtMod.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#detUpdatedAt').text(datMod);
                        $('#detUpdatedBy').text(data.updated_by);
                    }

                    /** ***************************** HISTORIQUE ********************************* */
                    var urlDown = "";
                    if (data.rec_obs_nonrealise && data.rec_dh_nonrealise) {
                        $('#historique').show();
                        $('#histNonRealise').show();
                        $('#obsNonRealise').text(data.rec_obs_nonrealise);
                        var dtNonReal = new Date(data.rec_dh_nonrealise);
                        var datNonReal = dtNonReal.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateNonRealise').text(datNonReal);
                        if (data.rec_preuve_nonrealise) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_nonrealise;
                            $('#downNonRealise').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +' target="_blank">' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }

                    if (data.rec_obs_encours && data.rec_dh_en_cours) {
                        $('#historique').show();
                        $('#histEnCours').show();
                        $('#obsEnCours').text(data.rec_obs_encours);
                        var dtEnCours = new Date(data.rec_dh_en_cours);
                        var datEnCours = dtEnCours.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateEnCours').text(datEnCours);
                        if (data.rec_preuve_encours) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_encours;
                            $('#downEnCours').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +' target="_blank">' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }

                    if (data.rec_obs_realise && data.rec_dh_realise) {
                        $('#historique').show();
                        $('#histRealise').show();
                        $('#obsRealise').text(data.rec_obs_realise);
                        var dtRealise = new Date(data.rec_dh_realise);
                        var datRealise = dtRealise.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dateRealise').text(datRealise);
                        if (data.rec_preuve_realise) {
                            urlDown = APP_URL + "/pj_recos/" + data.rec_preuve_realise;
                            $('#downRealise').html('<a class="btn btn-primary text-white" ' +
                                'href='+ urlDown +' target="_blank">' +
                                '<i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la preuve' +
                                '</a>');
                        }
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }
    </script>
@endsection

@section('modal')
    {{--MODAL Details recommandation--}}
    @include('backend.recommandations.modalDetails')
@endsection
