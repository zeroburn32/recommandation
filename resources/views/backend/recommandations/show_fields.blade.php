<!-- Ses Id Field -->
<div class="form-group">
    {!! Form::label('ses_id', 'Ses Id:') !!}
    <p>{{ $recommandations->ses_id }}</p>
</div>

<!-- Rec Intitule Field -->
<div class="form-group">
    {!! Form::label('rec_intitule', 'Rec Intitule:') !!}
    <p>{{ $recommandations->rec_intitule }}</p>
</div>

<!-- Rec Date Echeance Field -->
<div class="form-group">
    {!! Form::label('rec_date_echeance', 'Rec Date Echeance:') !!}
    <p>{{ $recommandations->rec_date_echeance }}</p>
</div>

<!-- Rec Statut Field -->
<div class="form-group">
    {!! Form::label('rec_statut', 'Rec Statut:') !!}
    <p>{{ $recommandations->rec_statut }}</p>
</div>

<!-- Rec Etat Field -->
<div class="form-group">
    {!! Form::label('rec_etat', 'Rec Etat:') !!}
    <p>{{ $recommandations->rec_etat }}</p>
</div>

<!-- Rec Is Suivi Valide Field -->
<div class="form-group">
    {!! Form::label('rec_is_suivi_valide', 'Rec Is Suivi Valide:') !!}
    <p>{{ $recommandations->rec_is_suivi_valide }}</p>
</div>

<!-- Rec Is Mode Standard Field -->
<div class="form-group">
    {!! Form::label('rec_is_mode_standard', 'Rec Is Mode Standard:') !!}
    <p>{{ $recommandations->rec_is_mode_standard }}</p>
</div>

<!-- Rec Observation Field -->
<div class="form-group">
    {!! Form::label('rec_observation', 'Rec Observation:') !!}
    <p>{{ $recommandations->rec_observation }}</p>
</div>

<!-- Rec Personne Formule Field -->
<div class="form-group">
    {!! Form::label('rec_personne_formule', 'Rec Personne Formule:') !!}
    <p>{{ $recommandations->rec_personne_formule }}</p>
</div>

<!-- Rec Personne Tel Field -->
<div class="form-group">
    {!! Form::label('rec_personne_tel', 'Rec Personne Tel:') !!}
    <p>{{ $recommandations->rec_personne_tel }}</p>
</div>

<!-- Rec Dh Valide Field -->
<div class="form-group">
    {!! Form::label('rec_dh_valide', 'Rec Dh Valide:') !!}
    <p>{{ $recommandations->rec_dh_valide }}</p>
</div>

<!-- Rec Dh Abandon Field -->
<div class="form-group">
    {!! Form::label('rec_dh_abandon', 'Rec Dh Abandon:') !!}
    <p>{{ $recommandations->rec_dh_abandon }}</p>
</div>

<!-- Rec Taux Realise Field -->
<div class="form-group">
    {!! Form::label('rec_taux_realise', 'Rec Taux Realise:') !!}
    <p>{{ $recommandations->rec_taux_realise }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $recommandations->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $recommandations->updated_by }}</p>
</div>

