@extends('layouts.template')
@section('title')
    Activit&eacute;s
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Recommandations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des activit&eacute;s</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des activit&eacute;s de la recommandation
                                    : <span class="text-danger">{{ $reco->rec_intitule }}</span></div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-warning"
                                        onclick="goBack()">
                                    <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-body">
                        @if(Auth::user()->hasRole(['SUPERADMIN']))
                            <div class="pull-right mb-3">
                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddActivite(null)"
                                        data-toggle="modal" data-target="#modalAddActivite">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle activit&eacute;
                                </button>
                            </div>
                        @else
                            @if($reco->ses_statut == 2 && (!is_null($reco->ses_datefin_planification)
                            && $reco->ses_datefin_planification > \Carbon\Carbon::now()->format('Y-m-d'))
                            && $reco->rec_is_planning_valide == 0)
                                <div class="pull-right mb-3">
                                    <button type="button" class="btn btn-sm btn-primary"
                                            onclick="openModalAddActivite(null)"
                                            data-toggle="modal" data-target="#modalAddActivite">
                                        <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle activit&eacute;
                                    </button>
                                </div>
                            @endif
                        @endif
                        <div class="table-responsive" style="min-height: 450px;">
                            <table class="table table-bordered table-sm table-striped table-hover dt_example">
                                <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Activit&eacute;</th>
                                    <th>Partenaire</th>
                                    <th>Echeance</th>
                                    <th>Poids</th>
                                    <th>Cumul</th>
                                    <th>Statut</th>
                                    <th>Etat</th>
                                    <th class="text-center no-sort">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $cumul = 0 ?>
                                @foreach($activites as $number=>$activite)
                                    <?php $cumul = $cumul + $activite->act_poids ?>
                                    <tr>
                                        <td>{{ $number + 1 }}</td>
                                        <td>{{ $activite->act_description }}</td>
                                        <td>{{ $activite->str_sigle }}</td>
                                        <td>{{ \Carbon\Carbon::parse($activite->act_date_prevue)->format('d/m/Y') }}</td>
                                        <td>{{ $activite->act_poids }}</td>
                                        <td>{{ $cumul }}</td>
                                        <td>
                                            @if($activite->act_is_valide == 1)
                                                <span class="badge badge-success">Valid&eacute;e</span>
                                            @elseif($activite->act_is_valide == 0)
                                                <span class="badge badge-danger">Non valid&eacute;e</span>
                                            @else
                                                <span class="badge badge-warning">N/D</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($activite->act_is_realise == 0)
                                                <span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>
                                            @elseif($activite->act_is_realise == 1)
                                                <span class="badge badge-success">R&eacute;alis&eacute;e</span>
                                            @else
                                                <span class="badge badge-warning">N/D</span>
                                        @endif
                                        <td class="text-center">
                                            <div class='btn-group'>
                                                <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                                        data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Actions
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class='dropdown-item' data-toggle="modal"
                                                       data-target="#modalDetailActivite" title="Voir détail"
                                                       onclick="openModalDetailsAcitivite({{ $activite->act_id }})">
                                                        <i class="fa fa-eye fa-lg"></i>&nbsp;&nbsp;D&eacute;tails
                                                    </a>
                                                    @if(isset($_GET['plan']))
                                                        @if($_GET['plan'] == 'true')
                                                            @if(Auth::user()->hasRole(['SUPERADMIN']))
                                                                <a class='dropdown-item' data-toggle="modal"
                                                                   data-target="#modalAddActivite" title="Modifier"
                                                                   onclick="openModalAddActivite({{ $activite->act_id }})">
                                                                    <i class="fa fa-edit fa-lg"></i>&nbsp;&nbsp;Modifier
                                                                </a>
                                                                <a class='dropdown-item'
                                                                   title="Supprimer l'activit&eacute;"
                                                                   onclick="deleteActivite({{ $activite->act_id }}, '{{addslashes($activite->act_description)}}')">
                                                                    <i class="fa fa-trash fa-lg"></i>&nbsp;&nbsp;Supprimer
                                                                </a>
                                                            @else
                                                                {{--En cours de planification--}}
                                                                @if($reco->ses_statut == 2 && (!is_null($reco->ses_datefin_planification)
                                                                && $reco->ses_datefin_planification > \Carbon\Carbon::now()->format('Y-m-d'))
                                                                && ($activite->act_is_valide == 0 || $reco->rec_is_planning_valide == 0))
                                                                    <a class='dropdown-item' data-toggle="modal"
                                                                       data-target="#modalAddActivite" title="Modifier"
                                                                       onclick="openModalAddActivite({{ $activite->act_id }})">
                                                                        <i class="fa fa-edit fa-lg"></i>&nbsp;&nbsp;Modifier
                                                                    </a>
                                                                    <a class='dropdown-item'
                                                                       title="Supprimer l'activit&eacute;"
                                                                       onclick="deleteActivite({{ $activite->act_id }}, '{{addslashes($activite->act_description)}}')">
                                                                        <i class="fa fa-trash fa-lg"></i>&nbsp;&nbsp;Supprimer
                                                                    </a>
                                                                @endif
                                                            @endif
                                                            {{--En cours de realisation--}}
                                                        @elseif($_GET['plan'] == 'false')
                                                            @if(Auth::user()->hasRole(['SUPERADMIN']))
                                                                <a class='dropdown-item' data-toggle="modal"
                                                                   data-target="#modalAddSuivi"
                                                                   title="Mise &agrave; jour du suivi"
                                                                   onclick="openModalAddSuivi({{ $activite->act_id }})">
                                                                    <i class="fa fa-clipboard-list fa-lg"></i>&nbsp;&nbsp;Saisir
                                                                    données suivi
                                                                </a>
                                                            @else
                                                                @if($reco->ses_statut == 3 && (!is_null($reco->ses_datefin_realisation)
                                                                && $reco->ses_datefin_realisation > \Carbon\Carbon::now()->format('Y-m-d')))
                                                                    <a class='dropdown-item' data-toggle="modal"
                                                                       data-target="#modalAddSuivi"
                                                                       title="Mise &agrave; jour du suivi"
                                                                       onclick="openModalAddSuivi({{ $activite->act_id }})">
                                                                        <i class="fa fa-clipboard-list fa-lg"></i>&nbsp;&nbsp;Saisir
                                                                        données suivi
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endif
                                                    @if(!is_null($activite->act_preuve))
                                                        <a class="dropdown-item"
                                                           href="{{ url('/pj_activites/' . $activite->act_preuve) }}" target="_blank">
                                                            <i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger
                                                            la pi&egrave;ce jointe
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
    <script>
        function openModalAddActivite(id) {
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAddActivite"),
                placeholder: "Choisir...",
                allowClear: true
            });
            $('#formAddReco').attr('action', '{{ route('saveActivite') }}');
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle activité');
                $('#btnSaveReco').empty();
                $('#btnSaveReco').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#ptn_id').val(null);
                $('#act_description').val(null);
                $('#act_poids').val(null);
                $('#act_date_prevue').val(null);
//                $('#act_preuve').val(null);
            }
            else {
                $.ajax({
                    url: "{{ url("/getActivite") }}/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $('.modal-title').text('Modification de l\'activité');
                        $('#btnSaveReco').empty();
                        $('#btnSaveReco').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                        $('#act_id').val(data.act_id);
                        $('#rec_id').val(data.rec_id);
                        $('#ptn_id').val(data.ptn_id);
                        $('#act_description').val(data.act_description);
                        $('#act_poids').val(data.act_poids);
                        $('#act_date_prevue').val(data.act_date_prevue);
//                        $('#act_preuve').val(data.act_preuve);
                    },
                    error: function () {
                        alert("Erreur rencontrer lors de la recuperation des donnees!");
                    }
                });
            }
        }

        function openModalAddSuivi(id) {
            $.ajax({
                url: "{{ url("/getActivite") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#formAddSuivi').attr('action', '{{ url("/doSuiviActivite") }}/' + data.act_id);
                    $('.modal-title').text('Suivi de l\'activité');
                    $('#btnSaveSuivi').empty();
                    $('#btnSaveSuivi').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                    $('#act_id').val(id);
                    $('#act_observation').val(data.act_observation);
                    $('#act_preuve').val(null);
                    if (data.act_is_realise == -1) {
                        $('input:radio[name="act_is_realise"][value="-1"]').prop('checked', true);
                    }
                    else if (data.act_is_realise == 0) {
                        $('input:radio[name="act_is_realise"][value="0"]').prop('checked', true);
                    }
                    else if (data.act_is_realise == 1) {
                        $('input:radio[name="act_is_realise"][value="1"]').prop('checked', true);
                    } else {
                        $('#act_is_realise').val(null);
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        function openModalDetailsAcitivite(id) {
            $.ajax({
                url: "{{ url("/getActivite") }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
                    var optionsWithTime = {
                        year: "numeric",
                        month: "long",
                        day: "numeric",
                        hour: "numeric",
                        minute: "numeric",
                        second: "numeric"
                    };

                    $('.modal-title').text('Détails de l\'activité');
                    if (data.act_is_valide == 1) {
                        valide = '<span class="badge badge-success">Valid&eacute;e</span>';
                    } else if (data.act_is_valide == 0) {
                        valide = '<span class="badge badge-danger">Non valid&eacute;e</span>';
                    } else {
                        valide = '<span class="badge badge-warning">N/D</span>';
                    }

                    if (data.act_is_realise == 0) {
                        realise = '<span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>';
                    }
                    else if (data.act_is_realise == 1) {
                        realise = '<span class="badge badge-success">R&eacute;alis&eacute;e</span>';
                    }
                    else {
                        realise = '<span class="badge badge-warning">N/D</span>';
                    }
                    $('#dtRec').text(data.rec_intitule);
                    $('#dtPart').text(data.str_sigle);
                    $('#dtDesc').text(data.act_description);
                    $('#dtActPre').text(data.act_preuve);
                    $('#dtPoid').text(data.act_poids);
                    $('#dtStatut').html(valide);
                    $('#dtEtat').html(realise);
                    if (data.act_date_prevue != null && data.act_date_prevue != '') {
                        // $('#dtDatePre').text(dateFormat(data.act_date_prevue, "dd/mm/yyyy"));
                        var dtPre = new Date(data.act_date_prevue);
                        var datPre = dtPre.toLocaleDateString("fr-FR", options);
                        $('#dtDatePre').text(datPre);
                    }
                    if (data.act_date_realise != null && data.act_date_realise != '') {
                        // $('#dtDateReal').text(dateFormat(data.act_date_realise, "dd/mm/yyyy"));
                        var dtReal = new Date(data.act_date_realise);
                        var datReal = dtReal.toLocaleDateString("fr-FR", options);
                        $('#dtDateReal').text(datReal);
                    }
                    if (data.created_at != null && data.created_at != '') {
                        // $('#dtCreatedAt').text(dateFormat(data.created_at, "dd/mm/yyyy à HH:mm:ss"));
                        var dtCree = new Date(data.created_at);
                        var datCree = dtCree.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dtCreatedAt').text(datCree);
                        $('#dtCreatedBy').text(data.created_by);
                    }
                    if (data.updated_at != null && data.updated_at != '') {
                        // $('#dtUpdatedAt').text(dateFormat(data.updated_at, "dd/mm/yyyy à HH:mm:ss"));
                        var dtMod = new Date(data.updated_at);
                        var datMod = dtMod.toLocaleDateString("fr-FR", optionsWithTime);
                        $('#dtUpdatedAt').text(datMod);
                        $('#dtUpdatedBy').text(data.updated_by);
                    }
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
        }

        function deleteActivite(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement l'activité : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteActivite')}}/" + id,
                            type: "get",
                            data: {act_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cette activité, elle est rattachée à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression de l'activité effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    });
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression de l'activité!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT activite--}}
    <div class="modal fade my-5" id="modalAddActivite" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddReco">
                    <div class="modal-body">
                        <div class="row {{--justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="act_id" name="act_id" type="hidden">
                            <input id="rec_id" name="rec_id" @if ($reco) value="{{ $reco->rec_id }}"
                                   @endif type="hidden">

                            <div class="form-group col-md-4">
                                <label for="ptn_id">Partenaire responsable <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="ptn_id" name="ptn_id">
                                    <option value="null">Partenaire...</option>
                                    @foreach($partenaires as $partenaire)
                                        <option value="{{ $partenaire->ptn_id }}">{{ $partenaire->str_sigle }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="rec_personne_formule">Poids <span
                                            class="requit">(restant : {{ 100 - $cumul }}%) *</span></label>
                                <input type="number" class="form-control form-control-sm" id="act_poids"
                                       name="act_poids" min="0" maxlength="500" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="act_date_prevue">Date d'&eacute;cheance pr&eacute;vu <span
                                            class="requit">*</span></label>
                                <input type="date" class="form-control form-control-sm" id="act_date_prevue"
                                       name="act_date_prevue" required/>
                            </div>

                            {{--<div class="form-group col-md-12">
                                <label for="act_preuve">Actions pr&eacute;vu <span class="requit">*</span></label>
                                <textarea class="form-control" name="act_preuve" id="act_preuve" rows="2"
                                          maxlength="255"></textarea>
                            </div>--}}

                            <div class="form-group col-md-12">
                                <label for="act_description">Description <span class="requit">*</span></label>
                                <textarea class="form-control" name="act_description" id="act_description"
                                          rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSaveReco"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--MODAL Maj suivi--}}
    <div class="modal fade my-5" id="modalAddSuivi" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" enctype="multipart/form-data" action="#"
                      id="formAddSuivi">
                    <div class="modal-body">
                        <div class="row {{--justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="act_id" name="act_id" type="hidden">

                            <div class="form-group col-md-3">
                                <label>Etat de mise en &oelig;uvre <span class="requit">*</span> :</label>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="act_is_realise1" name="act_is_realise"
                                           class="custom-control-input" value="0">
                                    <label class="custom-control-label" for="act_is_realise1">Non
                                        r&eacute;alis&eacute;e</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="act_is_realise3" name="act_is_realise"
                                           class="custom-control-input" value="1">
                                    <label class="custom-control-label"
                                           for="act_is_realise3">R&eacute;alis&eacute;e</label>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="act_observation">Observations <span class="requit">*</span></label>
                                <textarea class="form-control" name="act_observation" id="act_observation"
                                          maxlength="255" rows="4" required>
                                </textarea>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="act_preuve">Pi&egrave;ce
                                    jointe {{--<span class="requit">*</span>--}}</label>
                            </div>
                            <div class="input-group col-md-9">
                                <input type="file" id="act_preuve" name="act_preuve"
                                       class="form-control form-control-sm">
                                <div class="input-group-append">
                                    <span class="input-group-text form-control form-control-sm text-danger"
                                          style="font-weight: bold;">Max {{ intval(config('app.taille_max_upload')) / (1024 * 1024) }}
                                        Mo</span>
                                </div>
                            </div>

                            <div class="form-group col-md-12 text-center">
                                <span class="text-danger">Extensions valides : pdf, png, jpg, jpeg, doc, docx, xls ou xlsx</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                            <i class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSaveSuivi"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--MODAL Maj suivi--}}
    <div class="modal fade my-5" id="modalDetailActivite" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-striped table-hover">
                            <tbody>
                            <tr>
                                <th>Recommandation</th>
                                <td id="dtRec" colspan="3"></td>
                            </tr>

                            <tr>
                                <th>Description</th>
                                <td id="dtDesc" colspan="3"></td>
                            </tr>

                            {{--<tr>
                                <th>Actions pr&eacute;vu</th>
                                <td id="dtActPre" colspan="3"></td>
                            </tr>--}}

                            <tr>
                                <th>Partenaire</th>
                                <td id="dtPart"></td>
                                <th>Poids</th>
                                <td id="dtPoid"></td>
                            </tr>

                            <tr>
                                <th>Statut</th>
                                <td id="dtStatut"></td>
                                <th>Etat</th>
                                <td id="dtEtat"></td>
                            </tr>

                            <tr>
                                <th>Date pr&eacute;vu</th>
                                <td id="dtDatePre"></td>
                                <th>Date de r&eacute;alisation</th>
                                <td id="dtDateReal"></td>
                            </tr>

                            <tr>
                                <th>Cr&eacute;e le</th>
                                <td id="dtCreatedAt"></td>
                                <th>Par</th>
                                <td id="dtCreatedBy"></td>
                            </tr>

                            <tr>
                                <th>Modifi&eacute; le</th>
                                <td id="dtUpdatedAt"></td>
                                <th>Par</th>
                                <td id="dtUpdatedBy"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                        <i class="fa fa-ban"></i>&nbsp;Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection