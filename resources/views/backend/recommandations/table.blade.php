<div class="table-responsive">
    <table class="table table-bordered table-sm table-striped table-hover dt_example" id="tb_recos">
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Session</th>
            <th>Ann&eacute;e</th>
            <th>Intitul&eacute;</th>
            <th>Date d'&eacute;cheance</th>
            <th>Statut</th>
            <th>Mode de suivi</th>
            <th>Taux de r&eacute;alisation</th>
            <th class="text-center no-sort">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($recos as $number=>$reco)
            <tr>
                <td>{{ $number + 1 }}</td>
                <td>{{ $reco->ses_description }}</td>
                <td>{{ $reco->ses_annee }}</td>
                <td>{{ $reco->rec_intitule }}
                    @if(!is_null($reco->rec_id_ref))
                        <span class="badge badge-info">R&eacute;conduite</span>
                    @endif
                </td>
                <td>{{ \Carbon\Carbon::parse($reco->rec_date_echeance)->format('d/m/Y') }}</td>
                <td>
                    @if($reco->rec_statut == 1)
                        <span class="badge badge-primary">Pr&eacute;paration</span>
                    @elseif($reco->rec_statut == 2)
                        <span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>
                    @elseif($reco->rec_statut == 3)
                        <span class="badge badge-info">En cours</span>
                    @elseif($reco->rec_statut == 4)
                        <span class="badge badge-success">R&eacute;alis&eacute;e</span>
                    @elseif($reco->rec_statut == 5)
                        <span class="badge badge-danger">Abandonn&eacute;e</span>
                    @else
                        <span></span>
                    @endif
                </td>
                <td>
                    @if(is_null($reco->rec_is_mode_standard))
                        <span class="badge badge-warning">N/D</span>
                    @elseif($reco->rec_is_mode_standard == 0)
                        <span class="badge badge-primary">Par taux</span>
                    @elseif($reco->rec_is_mode_standard == 1)
                        <span class="badge badge-info">Par statut</span>
                    @else
                        <span class="badge badge-warning">N/D</span>
                    @endif
                </td>
                <td class="text-right">{{ $reco->rec_taux_realise }}%</td>
                <td class="text-center">
                    <div class='btn-group'>
                        <button type="button"
                                class="btn btn-xs btn-primary dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu">
                            <a tabindex="0" class="dropdown-item"
                               onclick="openModalDetailsReco({{ $reco->rec_id }})"
                               data-toggle="modal" data-target="#modalDetails">
                                <i class="fa fa-eye"></i>&nbsp;&nbsp;D&eacute;tails
                            </a>
                            @if(Auth::user()->hasRole(['SUPERADMIN']))
                                @if($mode == 'SAISIE')
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="openModalAddReco({{ $reco->rec_id }})"
                                       data-toggle="modal" data-target="#modalAddReco">
                                        <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                    </a>
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="deleteReco({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                        <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                    </a>
                                    <a tabindex="0" class="dropdown-item" title="Abandon de la recommandation"
                                       onclick="abandonner({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                        <i class="fa fa-times"></i>&nbsp;&nbsp;Abandonner
                                    </a>
                                    <hr/>
                                    <a class="dropdown-item" title="Partenaires"
                                       href="{{ url('getPagePartenaireReco/'.$reco->rec_id) }}">
                                        <i class="fa fa-users"></i>&nbsp;&nbsp;Partenaires&nbsp;
                                        <span class="badge badge-pill badge-warning">
                                        {{ App\Http\Controllers\MethodesStaticController::nbrePartenairesByReco($reco->rec_id) }}
                                    </span>
                                    </a>
                                @elseif($mode == 'PLAN')
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="changerModeSuivi({{ $reco->rec_id }})"
                                       title="Changer le mode de suivi">
                                        <i class="fa fa-edit"></i>&nbsp;&nbsp;Changer le mode
                                    </a>
                                    <a class="dropdown-item" title="Activit&eacute;s"
                                       title="G&eacute;rer les activit&eacute;s"
                                       href="{{ url('getPageActiviteReco/'.$reco->rec_id.'?plan=true') }}">
                                        <i class="fa fa-wrench"></i>&nbsp;&nbsp;Planifier les activités&nbsp;
                                        <span class="badge badge-pill badge-warning">
                                            {{ App\Http\Controllers\MethodesStaticController::nbreActivitesByReco($reco->rec_id) }}
                                        </span>
                                    </a>
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="openModalListPartenaire({{ $reco->rec_id }})"
                                       data-toggle="modal" data-target="#modalListPart"
                                       title="Consulter la liste des partenaires">
                                        <i class="fa fa-list"></i>&nbsp;&nbsp;Les partenaires
                                    </a>
                                    @if($reco->rec_is_planning_valide = 0)
                                        <a tabindex="0" class="dropdown-item"
                                           title="Valider la planification de la recommandation"
                                           onclick="validerPlanning({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                            <i class="fa fa-check"></i>&nbsp;&nbsp;Valider planification
                                        </a>
                                    @endif
                                @elseif($mode == 'SUIVI')
                                    @if($reco->rec_is_mode_standard = 1)
                                        <a class="dropdown-item" title="Saisir les donn&eacute;es de suivi"
                                           data-toggle="modal" data-target="#modalAddSuivi"
                                           onclick="openModalAddSuivi({{ $reco->rec_id }})">
                                            <i class="fa fa-list"></i>&nbsp;&nbsp;Saisir données suivi
                                        </a>
                                    @elseif($reco->rec_is_mode_standard = 0)
                                        <a class="dropdown-item" title="G&eacute;rer les activit&eacute;s"
                                           href="{{ url('getPageActiviteReco/'.$reco->rec_id.'?plan=false') }}">
                                            <i class="fa fa-wrench"></i>&nbsp;&nbsp;Saisir données suivi (par activit&eacute;s)&nbsp;
                                            <span class="badge badge-pill badge-warning">
                                            {{ App\Http\Controllers\MethodesStaticController::nbreActivitesByReco($reco->rec_id) }}
                                        </span>
                                        </a>
                                    @endif
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="openModalListPartenaire({{ $reco->rec_id }})"
                                       data-toggle="modal" data-target="#modalListPart"
                                       title="Consulter la liste des partenaires">
                                        <i class="fa fa-list"></i>&nbsp;&nbsp;Les partenaires
                                    </a>
                                @endif
                            @else
                                @if($mode == 'SAISIE')
                                    {{--seul en preparation--}}
                                    @if($reco->rec_statut == 1)
                                        <a tabindex="0" class="dropdown-item"
                                           onclick="openModalAddReco({{ $reco->rec_id }})"
                                           data-toggle="modal" data-target="#modalAddReco">
                                            <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                        </a>
                                        <a tabindex="0" class="dropdown-item"
                                           onclick="deleteReco({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                            <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                        </a>
                                        <hr/>
                                        {{--different du statut en preparation--}}
                                    @elseif($reco->rec_statut != 1)
                                    @can(['ABANDONNER_RECOMMANDATION'])
                                        <a tabindex="0" class="dropdown-item" title="Abandon de la recommandation"
                                           onclick="abandonner({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                            <i class="fa fa-times"></i>&nbsp;&nbsp;Abandonner
                                        </a>
                                    @endcan
                                    @endif
                                    <a class="dropdown-item" title="Partenaires"
                                       href="{{ url('getPagePartenaireReco/'.$reco->rec_id) }}">
                                        <i class="fa fa-users"></i>&nbsp;&nbsp;Partenaires&nbsp;
                                        <span class="badge badge-pill badge-warning">
                                            {{ App\Http\Controllers\MethodesStaticController::nbrePartenairesByReco($reco->rec_id) }}
                                        </span>
                                    </a>
                                @elseif($mode == 'PLAN')
                                    {{--session en formulation ou en planification et avant la date fin de planification--}}
                                    @if(($reco->ses_statut == 1 || $reco->ses_statut == 2)
                                    && $reco->rec_is_mode_standard == 0
                                    && (!is_null($reco->ses_datefin_planification)
                                    && $reco->ses_datefin_planification >= \Carbon\Carbon::now()->format('Y-m-d')))
                                        <a tabindex="0" class="dropdown-item"
                                           onclick="changerModeSuivi({{ $reco->rec_id }})"
                                           title="Changer le mode de suivi">
                                            <i class="fa fa-edit"></i>&nbsp;&nbsp;Changer le mode
                                        </a>
                                    @endif
                                    {{--suivi par taux et statut de la session en planification--}}
                                    @if($reco->rec_is_mode_standard == 0 && $reco->ses_statut == 2)
                                        <a class="dropdown-item" title="G&eacute;rer les activit&eacute;s"
                                           href="{{ url('getPageActiviteReco/'.$reco->rec_id.'?plan=true') }}">
                                            <i class="fa fa-wrench"></i>&nbsp;&nbsp;Planifier les activités&nbsp;
                                            <span class="badge badge-pill badge-warning">
                                                {{ App\Http\Controllers\MethodesStaticController::nbreActivitesByReco($reco->rec_id) }}
                                            </span>
                                        </a>
                                    @endif
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="openModalListPartenaire({{ $reco->rec_id }})"
                                       data-toggle="modal" data-target="#modalListPart"
                                       title="Consulter la liste des partenaires">
                                        <i class="fa fa-list"></i>&nbsp;&nbsp;Les partenaires
                                    </a>
                                    {{--Reco non valide et mode de suivi different de null--}}
                                    @can(['VALIDER_PLANIFICATION_RECO'])
                                    @if(!is_null($reco->rec_is_mode_standard)
                                    && $reco->rec_is_planning_valide == 0 && $reco->ses_statut == 2)
                                        <a tabindex="0" class="dropdown-item"
                                           title="Valider la planification de la recommandation"
                                           onclick="validerPlanning({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                            <i class="fa fa-check"></i>&nbsp;&nbsp;Valider planification
                                        </a>
                                    @endif
                                    @endcan
                                @elseif($mode == 'SUIVI')
                                    {{--Suivi reco non valider et date debut arrivee ou depassee (prochaine session)--}}
                                    {{--@if(($reco->rec_is_suivi_valide == 0)
                                    && (!is_null($reco->ses_date_debut)
                                    && $reco->ses_date_debut < \Carbon\Carbon::now()->format('Y-m-d')))
                                        <a tabindex="0" class="dropdown-item"
                                           onclick="abandonner({{ $reco->rec_id }}, '{{addslashes($reco->rec_intitule)}}')">
                                            <i class="fa fa-times"></i>&nbsp;&nbsp;Abandonner
                                        </a>
                                    @endif--}}
                                    {{--Suivi valide, suivi par statut et date fin de realisation depassee--}}
                                    @if($reco->rec_is_planning_valide == 1 && $reco->rec_is_mode_standard == 1 && $reco->ses_statut == 3
                                    && (!is_null($reco->ses_datefin_realisation) && \Carbon\Carbon::now()->format('Y-m-d') < $reco->ses_datefin_realisation))
                                        <a class="dropdown-item" title="Saisir les donn&eacute;es de suivi"
                                           data-toggle="modal" data-target="#modalAddSuivi"
                                           onclick="openModalAddSuivi({{ $reco->rec_id }})">
                                            <i class="fa fa-list"></i>&nbsp;&nbsp;Saisir données suivi
                                        </a>
                                        {{--Suivi valide, suivi par taux et date fin de realisation depassee--}}
                                    @elseif($reco->rec_is_planning_valide == 1 && $reco->rec_is_mode_standard == 0 && $reco->ses_statut == 3
                                    && (!is_null($reco->ses_datefin_realisation) && \Carbon\Carbon::now()->format('Y-m-d') < $reco->ses_datefin_realisation))
                                        <a class="dropdown-item" title="G&eacute;rer les activit&eacute;s"
                                           href="{{ url('getPageActiviteReco/'.$reco->rec_id.'?plan=false') }}">
                                            <i class="fa fa-wrench"></i>&nbsp;&nbsp;Saisir données suivi (par
                                            activit&eacute;s)&nbsp;
                                            <span class="badge badge-pill badge-warning">
                                                {{ App\Http\Controllers\MethodesStaticController::nbreActivitesByReco($reco->rec_id) }}
                                            </span>
                                        </a>
                                    @endif
                                    {{--date debut arrivee ou depassee (prochaine session)--}}
                                    {{--@if(!is_null($reco->ses_date_debut)
                                    && $reco->ses_date_debut <= \Carbon\Carbon::now()->format('Y-m-d'))
                                        <a tabindex="0" class="dropdown-item"
                                           onclick="openModalMajObs({{ $reco->rec_id }})"
                                           data-toggle="modal" data-target="#modalMajObservation"
                                           title="Modifier l'observation d'une recommandation">
                                            <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier observation
                                        </a>
                                    @endif--}}
                                    <a tabindex="0" class="dropdown-item"
                                       onclick="openModalListPartenaire({{ $reco->rec_id }})"
                                       data-toggle="modal" data-target="#modalListPart"
                                       title="Consulter la liste des partenaires">
                                        <i class="fa fa-list"></i>&nbsp;&nbsp;Les partenaires
                                    </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
