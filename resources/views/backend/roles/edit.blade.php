@extends('layouts.template')
@section('title')
    Profils
    @parent
@stop
@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
                <li class="breadcrumb-item"><a href="{!! route('roles.index') !!}"><i class="cib-co-op"></i>Gestion des
                        profils</a></li>
                <li class="breadcrumb-item active" aria-current="page">Modification du profil</li>
            </ol>
        </nav>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Modifier le profil</div>
                            </div>
                            <div class="widget-content-right">
                                <button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                    <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}
                        @include('backend.roles.fields', ['formMode' => 'edit'])
                        {!! Form::close() !!} 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection