<div class="row justify-content-center">
    <!-- Name Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('name', 'Profil ', ['class' => 'control-label']) !!}<span class="requit">*</span>
    @if( in_array($role->name ?? '',['SUPERADMIN','ADMIN','ASR','CSS','CSR'] ))    
        {!! Form::text('name', null, ['class' => 'form-control form-control-sm', 'readonly'=>'readonly']) !!}
    @else 
        {!! Form::text('name', null, ['class' => 'form-control form-control-sm']) !!}
    @endif
    </div>
 
    <!-- Description Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('label', 'Description:', ['class' => 'control-label']) !!}
        {!! Form::text('label', null, ['class' => 'form-control form-control-sm']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::label('label', 'Selectionnez les Droits: ', ['class' => 'control-label']) !!}
    </div>
    <div class="form-group col-sm-8" style="max-height: 50vh; overflow: auto;">

        <table class="table table-bordered table-sm table-striped table-hover">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Code</th>
                <th>Libelle</th>
                <th class="no-sort text-right"></th>
            </tr>
            </thead>
            <tbody>

            @foreach($permissions as $number=>$permission)
                <tr>
                    <td> {!! $number + 1 !!} </td>
                    <td> {!! $permission->name !!} </td>
                    <td> {!! $permission->label !!} </td>
                    <td class="text-center">
                        <input type="checkbox" class="checkboxes" value="{{ $permission->name }}"
                               @if($formMode==='edit' )
                               {{ ($permission->id_role == $role->id) ? 'checked' : '' }} @endif name="permissions[]">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12 offset-4">
        {!! Form::submit('Enregistrer', ['class' => 'btn btn-sm btn-primary']) !!}
        <a href="{{ route('roles.index') }}" class="btn btn-sm btn-danger">Annuler</a>
    </div>
</div>
