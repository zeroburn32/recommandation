@extends('layouts.template')
@section('title')
    Profils
    @parent
@stop
@section('content')

    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Gestion des profils</li>
            </ol>
        </nav>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">LISTE DES PROFILS</div>
                            </div>
                            <div class="widget-content-right">
                                @can('GERER_COMPTE')
                                <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary" style="text-transform:none">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Ajouter un profil
                                </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('backend.roles.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
<script>
function supprimerProfil(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir supprimer le profil " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Supprimer',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        /* var url = '{{ route("roles.destroy", ":id") }}';
                        url = url.replace(':id',id); */
                        $.ajax({
                            url: APP_URL+'/supprimerProfil/'+id,
                            type: 'GET',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                               if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer le profil, il est rattachée à certains comptes !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Suppression !",
                                        text: "Profil supprimé avec succès !",
                                        type: "success",
                                        button: "OK"
                                    }).then((result) => {
                                    location.reload();
                                    });
                                //window.location.href = "roles";
                                }                          
                            },
                            error: function () {
                                swal({
                                    title: "Suppression !",
                                    text: "Une erreur est survenue lors de la suppression !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
</script>
@endsection