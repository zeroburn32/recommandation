<div class="table-responsive">
    <table class="table table-striped table-sm dt_example" id="roles-table">
        <thead class="thead-dark">
        <th>#</th>
        <th>Profil</th>
        <th>Description</th>
        @can('GERER_COMPTE')
        <th class="no-sort text-center">Action</th>
        @endcan
        </thead>
        <tbody>
        @foreach($roles as $i=>$roles)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ $roles->name }}</td>
                <td>{{ $roles->label }}</td>
                @can('GERER_COMPTE')
                <td class="text-center">
                    <a href="{{ route('roles.edit', [$roles->id]) }}" class='btn btn-sm btn-primary'>
                        <i class="fa fa-edit"></i>
                    </a>
                    <button onclick="supprimerProfil({{ $roles->id }}, '{{ $roles->name }}')" class='btn btn-sm btn-danger'>
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
    </table>
</div>