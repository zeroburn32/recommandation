<!-- Ins Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ins_id', 'Ins Id:') !!}
    {!! Form::number('ins_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ses Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_description', 'Ses Description:') !!}
    {!! Form::text('ses_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Ses Annee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_annee', 'Ses Annee:') !!}
    {!! Form::number('ses_annee', null, ['class' => 'form-control']) !!}
</div>

<!-- Ses Date Debut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_date_debut', 'Ses Date Debut:') !!}
    {!! Form::text('ses_date_debut', null, ['class' => 'form-control','id'=>'ses_date_debut']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#ses_date_debut').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Ses Lieu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_lieu', 'Ses Lieu:') !!}
    {!! Form::text('ses_lieu', null, ['class' => 'form-control']) !!}
</div>

<!-- Ses Date Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ses_date_fin', 'Ses Date Fin:') !!}
    {!! Form::text('ses_date_fin', null, ['class' => 'form-control','id'=>'ses_date_fin']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#ses_date_fin').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sessions.index') }}" class="btn btn-secondary">Cancel</a>
</div>
