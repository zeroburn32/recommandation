@extends('layouts.template')
@section('title')
    Sessions
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des sessions</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des sessions
                                    @if(isset($_GET['_token']))
                                        <span class="requit">(selon le filtre)</span>
                                    @else
                                        <span class="requit">(en fromulation & en planification)</span>
                                    @endif
                                </div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddSession(null)"
                                        data-toggle="modal" data-target="#modalAddSession">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle session
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate method="GET" action="{{ route('sessionsSearch') }}"
                              id="formFilter">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="from-group col-lg-2 col-xl-2">
                                    <div class="card-title">Filtre</div>
                                </div>
                                <div class="from-group col-lg-3 col-xl-3">
                                    <select class="form-control form-control-sm selectSearch" id="fil_instance"
                                            name="fil_instance">
                                        <option value="">Choisir l'instance...</option>
                                        @foreach($instances as $instance)
                                            <option value="{{ $instance->ins_id }}"
                                                    @if(intVal($instance->ins_id) == $instance_id) selected @endif>
                                                {{ $instance->ins_sigle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <select class="form-control form-control-sm selectSearch" id="fil_status"
                                            name="fil_status">
                                        <option value="">Choisir le statut...</option>
                                        @foreach($statutsSessions as $key => $value)
                                            <option value="{{ $key }}" @if($statut == $key) selected @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group col-lg-2 col-xl-2">
                                    <input type="number" class="form-control form-control-sm" id="fil_annee"
                                           name="fil_annee" value="{{ $annee }}" readonly/>
                                    <div class="input-group-append">
                                        <span class="input-group-text form-control form-control-sm">
                                            <i class="pe-7s-date"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="from-group col-lg-2 col-xl-2">
                                    <button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
                                        <i class="fa fa-search"></i>&nbsp;Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="card">
                            <div class="card-body">
                                @include('backend.sessions.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/lightpick.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
    <script>
        $(function () {
//            jQuery.datetimepicker.setLocale('fr');
            $('#ses_date_debut').datetimepicker({
                ignoreReadonly: true,
//                locale: 'fr',
//                format: 'DD/MM/YYYY',
                format: 'YYYY-MM-DD',
                showClose: true,
                showClear: true,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            });
            $('#ses_date_fin').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD',
                showClose: true,
                showClear: true,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            });
            $('#ses_datefin_planification').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD',
                showClose: true,
                showClear: true,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            });
            $('#ses_datefin_realisation').datetimepicker({
                ignoreReadonly: true,
                format: 'YYYY-MM-DD',
                showClose: true,
                showClear: true,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            });

            $("#ses_annee").datetimepicker({
                ignoreReadonly: true,
                viewMode: 'years',
                format: 'YYYY',
                minDate: '2015',
                showClose: true,
                showClear: true,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            })
        });

        $('.selectSearch').select2({
            placeholder: "Choisir...",
            allowClear: true
        });

        $('#fil_status').select2({placeholder: "Choisir le statut...", allowClear: true});
        $('#fil_instance').select2({placeholder: "Choisir l'instance...",  allowClear: true});

        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            viewMode: 'years',
            format: 'YYYY',
            minDate: '2015',
            showClose: true,
            showClear: true,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        }).parent().css("position :relative");

        function openModalAddSession(id) {
            $('.selectSearchModal').select2({
                width: '100%',
                dropdownParent: $("#modalAddSession"),
                placeholder: "Choisir...",
                allowClear: true
            });

            $('#formAddSession').attr('action', 'saveSession');
            $.ajax({
                url: "{{ url("/getAllInstances") }}",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#ins_id').empty();
                    $.each(data, function (index, instance) {
                        $('#ins_id').append($('<option>', {
                            value: instance.ins_id,
                            text: instance.ins_sigle
                        }));
                    });

                    if (id == null) {
                        $('.modal-title').text('Ajout d\'une nouvelle session');
                        $('#btnSave').text('Enregistrer');
                        $('#ses_id').val(null);
                        $('#ins_id').val(null);
                        $('#ses_description').val(null);
                        $('#ses_annee').val(null);
                        $('#ses_date_debut').val(null);
                        $('#ses_lieu').val(null);
                        $('#ses_date_fin').val(null);
                        $('#ses_datefin_planification').val(null);
                        $('#ses_datefin_realisation').val(null);
                    }
                    else {
                        var $option = $('<option selected>Loading...</option>').val(null);
                        $.ajax({
                            url: "{{ url("/getSession") }}/" + id,
                            type: "GET",
                            dataType: "JSON",
                            success: function (data) {
                                $('.modal-title').text('Modification de le session');
                                $('#btnSave').text('Modifier');
                                $('#ses_id').val(id);
                                $('#ses_description').val(data.ses_description);
                                $('#ses_annee').val(data.ses_annee);
                                $('#ses_date_debut').val(data.ses_date_debut);
                                $('#ses_lieu').val(data.ses_lieu);
                                $('#ses_date_fin').val(data.ses_date_fin);
                                $('#ses_datefin_planification').val(data.ses_datefin_planification);
                                $('#ses_datefin_realisation').val(data.ses_datefin_realisation);
                                $('#ins_id').val(data.ins_id).trigger('change');
                            },
                            error: function () {
                                alert("Erreur rencontrer lors de la recuperation des donnees!");
                            }
                        });
                    }
                }
            });
        }

        function openModalJoindreList(id) {
            $('#formJoindrePart').attr('action', 'joindreListParticipant');
            $('.modal-title').text('Joindre la liste des participants de le session');
            $('#btnJoindre').text('Joindre');
            $('#session_id').val(id);
        }

        function openModalDetail(id) {
            $.ajax({
                url: "{{url('/getSession')}}/" + id,
                type: "get",
                success: function (data) {
                    console.log(data);
                    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
                    var optionsWithTime = {year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"};

                    if (data.ses_id != null && data.ses_id > 0) {
                        $('.modal-title').text("Détails de la session");
                        $('#libInstance').text(data.ins_nom_complet + ' (' + data.ins_sigle + ')');
                        $('#libDesc').text(data.ses_description);
                        $('#libLieu').text(data.ses_lieu);
                        $('#libAnnee').text(data.ses_annee);
                        var statut = "";
                        if(data.ses_statut == 1) {
                            statut = '<span class="badge badge-info">En formulation</span>';
                        }
                        else if(data.ses_statut == 2) {
                            statut = '<span class="badge badge-warning">En planification</span>';
                        }
                        else if(data.ses_statut == 3) {
                            statut = '<span class="badge badge-primary">En r&eacute;alisation</span>';
                        }
                        else if(data.ses_statut == 4) {
                            statut = '<span class="badge badge-success">Suivi validé</span>';
                        }
                        else {
                            statut = '<span></span>';
                        }
                        $('#libStatut').html(statut);

                        /*$('#libDateDebut').text(dateFormat(data.ses_date_debut, "dd/mm/yyyy"));
                        $('#libDateFin').text(dateFormat(data.ses_date_fin, "dd/mm/yyyy"));
                        $('#libDateFinPlan').text(dateFormat(data.ses_datefin_planification, "dd/mm/yyyy"));
                        $('#libDateFinReal').text(dateFormat(data.ses_datefin_realisation, "dd/mm/yyyy"));*/
                        if (data.ses_date_debut != null && data.ses_date_debut != '') {
                            $('#libDateDebut').empty();
                            var dtDeb = new Date(data.ses_date_debut);
                            var datDeb = dtDeb.toLocaleDateString("fr-FR", options);
                            $('#libDateDebut').text(datDeb);
                        }
                        if (data.ses_date_fin != null && data.ses_date_fin != '') {
                            $('#libDateFin').empty();
                            var dtFin = new Date(data.ses_date_fin);
                            var datFin = dtFin.toLocaleDateString("fr-FR", options);
                            $('#libDateFin').text(datFin);
                        }
                        if (data.ses_datefin_planification != null && data.ses_datefin_planification != '') {
                            $('#libDateFinPlan').empty();
                            var dtFinPlan = new Date(data.ses_datefin_planification);
                            var datFinPlan = dtFinPlan.toLocaleDateString("fr-FR", options);
                            $('#libDateFinPlan').text(datFinPlan);
                        }
                        if (data.ses_datefin_realisation != null && data.ses_datefin_realisation != '') {
                            $('#libDateFinReal').empty();
                            var dtFinReal = new Date(data.ses_datefin_realisation);
                            var datFinReal = dtFinReal.toLocaleDateString("fr-FR", options);
                            $('#libDateFinReal').text(datFinReal);
                        }
                        if (data.created_at != null && data.created_at != '') {
                            // $('#libCreatedAt').text(dateFormat(data.created_at, "dd/mm/yyyy à HH:mm:ss"));
                            var dtCree = new Date(data.created_at);
                            var datCree = dtCree.toLocaleDateString("fr-FR", optionsWithTime);
                            $('#libCreatedAt').text(datCree);
                            $('#libCreatedBy').text(data.created_by);
                        }

                        if (data.updated_at != null && data.updated_at != '') {
                            // $('#libUpdatedAt').text(dateFormat(data.updated_at, "dd/mm/yyyy à HH:mm:ss"));
                            var dtMod = new Date(data.updated_at);
                            var datMod = dtMod.toLocaleDateString("fr-FR", optionsWithTime);
                            $('#libUpdatedAt').text(datMod);
                            $('#libUpdatedBy').text(data.updated_by);
                        }
                    }
                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données de l'instance!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }
            });
        }

        function openModalValidePlaning(id) {
            $.ajax({
                url: "{{url('/getSession')}}/" + id,
                type: "get",
                success: function (session) {
                    if (session.ses_id != null && session.ses_id > 0) {
                        $.ajax({
                            url: "{{url('/structuresWhereRecoNotPlan')}}/" + session.ses_id,
                            type: "get",
                            success: function (data) {
                                console.log(data);
                                if (data != null && data != '') {
                                    var tableContent = '';
                                    $('.modal-title').text("Lister des structures n’ayant pas encore validé la planification de toutes leurs recommandations");
                                    $.each(data, function (index, structure) {
                                        tableContent += '<tr>' +
                                            '<td>' + (index + 1) + '</td>' +
                                            '<td>' + structure.str_sigle + '</td>' +
                                            '<td>' + structure.str_nom_complet + '</td>' +
                                            '</tr>';
                                    });
                                    $('#structPlanNotValide').html(tableContent);
                                    $('#btnValidePlan').attr('onclick', 'changeStatus(' + session.ses_id + ', "' + session.ins_sigle + ' ' + session.ses_annee + '", 3)');
                                } else {
                                    $('#modalValidation').hide();
                                    $('.modal-backdrop').remove();
                                    changeStatus(session.ses_id, '' + session.ins_sigle + ' ' + session.ses_annee + '', 3);
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la récupération des données de l'instance!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    }
                }
            });
        }

        function openModalReconduireReco(id) {
            $.ajax({
                url: "{{url('/getSession')}}/" + id,
                type: "get",
                success: function (session) {
                    if (session.ses_id != null && session.ses_id > 0) {
                        $('#session').val(session.ses_id);
                        $.ajax({
                            url: "{{url('/getRecosAreconduire')}}/" + session.ses_id_precedante,
                            type: "get",
                            success: function (data) {
                                if (data != null && data != '') {
                                    var tableContent, statut, permanente = '';
                                    $('.modal-title').text("Réconduire certains recommandations de la session précédante");
                                    $.each(data, function (index, reco) {
                                        var myId = "check"+ (index+1);
                                        /** rec_statut */
                                        if (reco.rec_statut == 1) {
                                            statut = '<span class="badge badge-primary">Pr&eacute;aration</span>';
                                        }
                                        else if (reco.rec_statut == 2) {
                                            statut = '<span class="badge badge-danger">Non r&eacute;alis&eacute;e</span>';
                                        }
                                        else if (reco.rec_statut == 3) {
                                            statut = '<span class="badge badge-info">En cours</span>';
                                        }
                                        else if (reco.rec_statut == 4) {
                                            statut = '<span class="badge badge-success">R&eacute;alis&eacute;e</span>';
                                        }
                                        else if (reco.rec_statut == 5) {
                                            statut = '<span class="badge badge-danger">Abandonn&eacute;e</span>';
                                        }
                                        else {
                                            statut = '<span>ND</span>';
                                        }
                                        /** rec_is_permanente */
                                        if (reco.rec_is_permanente == 1) {
                                            permanente = '<span class="badge badge-success">Oui</span>';
                                        }
                                        else if (reco.rec_is_permanente == 0) {
                                            permanente = '<span class="badge badge-danger">Non</span>';
                                        }
                                        else {
                                            permanente = '<span>ND</span>';
                                        }
                                        tableContent += '<tr>' +
                                            '<td>' +
                                            '<div class="custom-control custom-checkbox">' +
                                            '<input type="checkbox" name="recos[]" value="'+reco.rec_id+'" class="custom-control-input" id='+myId+' />' +
                                            '<label class="custom-control-label" for='+myId+'></label>' +
                                            '</div>' +
                                            '</td>' +
                                            '<td>' + reco.rec_intitule + '</td>' +
                                            '<td>' + statut + '</td>' +
                                            '<td class="text-right">' + reco.rec_taux_realise + '%</td>' +
                                            '<td class="text-center">' + permanente + '</td>' +
                                            '</tr>';
                                    });
                                    $('#recosAreconduire').html(tableContent);
                                } else {
                                    $('#modalReconduire').hide();
                                    $('.modal-backdrop').remove();
                                    swal({
                                        title: 'Attention',
                                        text: "Il n'y a pas de recommandation à reconduire.",
                                        type: 'warning',
                                        button: "OK"
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la récupération des données de l'instance!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    }
                }
            });
        }

        function deleteSession(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement la session : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/deleteSession')}}/" + id,
                            type: "get",
                            data: {ses_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer cette session, elle est rattachée à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression de la session effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression de la session!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function reconduireRecos_old(ses_id, ses_id_precedant) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir réconduire les recommandations de la session pécédante ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#019b07',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, réconduire!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/reconduireRecos')}}/" + ses_id + "/" + ses_id_precedant,
                            type: "get",
                            data: {ses_id: ses_id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                        title: "Succes!",
                                        text: "Réconduction des recommandations de la session précédante effectuée avec succès!",
                                        type: "success",
                                        button: "OK"
                                    }).then(function () {
                                        location.reload();
                                    });
                                } else if (data == 1) {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de réconduire les recommandations de la session précédante, Veuillez reessayer plutard ou contacter l'administrateur !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible d'effectuer cette opération, la session n'a pas de précédante ou pas de recommandation(s) à réconduire !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la réconduction des recommandations de la session précédante !\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function reconduireRecos() {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir réconduire les recommandations de la session pécédante ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#019b07',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, réconduire!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{ url("/reconduireRecos") }}",
                            type: "POST",
                            data: new FormData($("#formReconduire")[0]),
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                swal({
                                    title: "Succes!",
                                    type: "success",
                                    text: "Réconduction des recommandations de la session précédante effectuée avec succès!",
                                    button: "OK !"
                                }).then(function () {
                                    window.location = "sessions";
//                                            window.history.back();
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    type: "error",
                                    text: "Impossible de réconduire les recommandations de la session précédante, Veuillez reessayer plutard ou contacter l'administrateur !",
                                    button: 'Réessayer !'
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function changeStatus(id, libelle, status) {
            var txtTest, txtConfirme, txtSuccess, txtError = "";
            if (status == 2) {
                txtTest = "Etes vous sûr de vouloir valider la formulation de la session : " + libelle + " ?";
                txtSuccess = "Validation de la formulation effectuée avec succès!";
                txtError = "Une erreure est survenue lors de la validation de la formulation!\nVeuillez reessayer!";
            } else if (status == 3) {
                txtTest = "Etes vous sûr de vouloir valider la planification de la session : " + libelle + " ?";
                txtSuccess = "Validation de la planification effectuée avec succès!";
                txtError = "Une erreure est survenue lors de la validation de la planification!\nVeuillez reessayer!";
            } else if (status == 4) {
                txtTest = "Etes vous sûr de vouloir valider la réalisation de la session : " + libelle + " ?";
                txtSuccess = "Validation de la réalisation effectuée avec succès!";
                txtError = "Une erreure est survenue lors de la validation de la réalisation!\nVeuillez reessayer!";
            }
            swal({
                title: 'CONFIRMATION',
                text: txtTest,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#008f32',
                cancelButtonColor: '#3085d6',
                confirmButtonText: "Oui, valider",
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/changeStatus')}}/" + id + "/" + status,
                            type: "post",
                            data: {ses_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                swal({
                                    title: "Succes",
                                    text: txtSuccess,
                                    type: "success",
                                    button: "OK"
                                }).then(function () {
                                    location.reload();
                                    // document.location.reload();
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: txtError,
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        /*function reload() {
            {{--var url = "{{ route('sessions.reload') }}";--}}
            $.ajax({
                url: url,
                type: "get",
                datatype: "html"
            }).done(function (data) {
                // append liste of directions to the table directions-table
                $('#tb_sessions').empty().html(data);
                $('#tb_sessions').DataTable().clear();
                $('#tb_sessions').DataTable().destroy();
                var tab = $('#tb_sessions').DataTable({
                    dom: 'lBfrtip',
                    responsive: true,
                    processing: true,
                    select: true,
                    "language": {
                        "url": "{{ asset('lang/datatables.french.json')}}"
                    },
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ],
                    "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                        "searchable": false
                    }]
                });
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                var prom = ezBSAlert({
                    messageText: "No response from server on load Error : " + thrownError,
                    alertType: "danger"
                });
            });
        }*/
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT Session--}}
    @include('backend.sessions.modalAddSession')

    {{--MODAL MAJ liste des particiapants--}}
    @include('backend.sessions.modalJoindrePart')

    {{--MODAL MAJ Details--}}
    @include('backend.sessions.modalDetail')

    {{--MODAL validation planification--}}
    @include('backend.sessions.modalValidation')

    {{--MODAL reconduction de recommandation--}}
    @include('backend.sessions.modalReconduire')
@endsection