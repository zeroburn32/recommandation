<div class="modal fade my-5" id="modalAddSession" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" {{--novalidate--}} method="POST" action="#" id="formAddSession">
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="ses_id" name="ses_id" type="hidden">

                        <div class="form-group col-md-4">
                            <label for="ins_id">Instance session <span class="requit">*</span></label>
                            <select class="form-control form-control-sm selectSearchModal" id="ins_id" name="ins_id" required>
                                {{--<option>Instance...</option>--}}
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_lieu">Lieu tenu session <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="ses_lieu"
                                   name="ses_lieu" maxlength="255" required/>
                        </div>

                        <div class="form-group col-md-4">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_annee">Ann&eacute;e session <span class="requit">*</span></label>
                            {{--<input type="number" class="form-control form-control-sm" id="ses_annee"
                                   name="ses_annee" min="2000" max="2199" maxlength="4" required/>--}}
                            <input type="text" class="form-control form-control-sm datetimepicker-input"
                                   id="ses_annee" name="ses_annee" data-toggle="datetimepicker"
                                   data-target="#ses_annee" required readonly/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_date_debut">Date d&eacute;but session <span class="requit">*</span></label>
                            {{--<input type="date" class="form-control form-control-sm" id="ses_date_debut"
                                   name="ses_date_debut" required/>--}}
                            <input type="text" class="form-control form-control-sm datetimepicker-input"
                                   id="ses_date_debut" name="ses_date_debut" data-toggle="datetimepicker"
                                   data-target="#ses_date_debut" required readonly/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_date_fin">Date fin session <span class="requit">*</span></label>
                            {{--<input type="date" class="form-control form-control-sm" id="ses_date_fin"
                                   name="ses_date_fin" required/>--}}
                            <input type="text" class="form-control form-control-sm datetimepicker-input"
                                   id="ses_date_fin" name="ses_date_fin" data-toggle="datetimepicker"
                                   data-target="#ses_date_fin" required readonly/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_description">Intitulé session <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="ses_description"
                                   name="ses_description" placeholder="Ex : 1ère session CASEM" required/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_datefin_planification">Date fin planification des reco. <span class="requit">*</span></label>
                            {{--<input type="date" class="form-control form-control-sm" id="ses_datefin_planification"
                                   name="ses_datefin_planification" required/>--}}
                            <input type="text" class="form-control form-control-sm datetimepicker-input"
                                   id="ses_datefin_planification" name="ses_datefin_planification" data-toggle="datetimepicker"
                                   data-target="#ses_datefin_planification" required readonly/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ses_datefin_realisation">Date fin suivi des reco. <span class="requit">*</span></label>
                            {{--<input type="date" class="form-control form-control-sm" id="ses_datefin_realisation"
                                   name="ses_datefin_realisation" required/>--}}
                            <input type="text" class="form-control form-control-sm datetimepicker-input"
                                   id="ses_datefin_realisation" name="ses_datefin_realisation" data-toggle="datetimepicker"
                                   data-target="#ses_datefin_realisation" required readonly/>
                        </div>

                        <div class="form-group col-md-12">
                                <span class="text-danger text-gras">NB : La date début de session doit être inférieur à
                                    la date fin session, la date fin session inférieur à la date fin planification
                                    et la date fin planification inférieur à la date fin réalisation</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                </div>
            </form>
        </div>
    </div>
</div>