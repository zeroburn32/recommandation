<div class="modal fade my-5" id="modalDetail" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-sm table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>Instance</th>
                        <td id="libInstance" colspan="3"></td>
                    </tr>
                    <tr>
                        <th>Intitul&eacute;</th>
                        <td id="libDesc"></td>
                        <th>Lieu</th>
                        <td id="libLieu"></td>
                    </tr>
                    <tr>
                        <th>Ann&eacute;e</th>
                        <td id="libAnnee"></td>
                        <th>Statut</th>
                        <td id="libStatut"></td>
                    </tr>
                    <tr>
                        <th>Date d&eacute;but</th>
                        <td id="libDateDebut"></td>
                        <th>Date fin</th>
                        <td id="libDateFin"></td>
                    </tr>
                    <tr>
                        <th>Date fin planification</th>
                        <td id="libDateFinPlan"></td>
                        <th>Date fin r&eacute;alisation</th>
                        <td id="libDateFinReal"></td>
                    </tr>
                    <tr>
                        <th>Crée le</th>
                        <td id="libCreatedAt"></td>
                        <th>Par</th>
                        <td id="libCreatedBy"></td>
                    </tr>
                    <tr>
                        <th>Modifié le</th>
                        <td id="libUpdatedAt"></td>
                        <th>Par</th>
                        <td id="libUpdatedBy"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer" style="cursor: pointer;">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
            </div>
        </div>
    </div>
</div>