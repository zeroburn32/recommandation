<div class="modal fade my-5" id="modalJoindrePart" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="needs-validation" novalidate method="POST" action="#" id="formJoindrePart" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-center">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="session_id" name="session_id" type="hidden">

                        <div class="form-group col-md-4 col-lg-4">
                            <label for="ses_liste_participants">Joindre la liste</label>
                        </div>
                        <div class="input-group col-md-8 col-lg-8 mb-3">
                            <input type="file" id="ses_liste_participants" name="ses_liste_participants" class="form-control form-control-sm"
                                   required {{--accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"--}}>
                            <div class="input-group-append">
                                <span class="input-group-text form-control form-control-sm text-danger"
                                      style="font-weight: bold;">Max {{ intval(config('app.taille_max_upload')) / (1024 * 1024) }}Mo</span>
                            </div>
                        </div>

                        <div class="form-group col-md-8 col-lg-8 text-center">
                            <span class="text-danger">Extensions valides : pdf, png, jpg, jpeg, doc, docx, xls ou xlsx</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnJoindre"></button>
                </div>
            </form>
        </div>
    </div>
</div>