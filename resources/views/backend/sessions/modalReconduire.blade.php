<div class="modal fade my-5" id="modalReconduire" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="needs-validation" novalidate method="POST" action="#" id="formReconduire">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="session" id="session" />
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th></th>
                            <th>Intitul&eacute;</th>
                            <th>Statut</th>
                            <th>Taux de r&eacute;alisation</th>
                            <th>Permanente ?</th>
                        </tr>
                        </thead>
                        <tbody id="recosAreconduire"></tbody>
                    </table>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                    <button type="button" class="btn btn-sm btn-success" id="btnReconduire" onclick="reconduireRecos()"
                            title="R&eacute;conduire les recommandations selectionner">Reconduire</button>
                </div>
            </form>
        </div>
    </div>
</div>