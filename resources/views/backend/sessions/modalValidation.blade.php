<div class="modal fade my-5" id="modalValidation" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-sm table-striped table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Sigle</th>
                        <th>Nom complet</th>
                    </tr>
                    </thead>
                    <tbody id="structPlanNotValide">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer" style="cursor: pointer;">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                <button type="button" class="btn btn-sm btn-success" id="btnValidePlan">Valider planification</button>
            </div>
        </div>
    </div>
</div>