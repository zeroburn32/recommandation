@extends('layouts.template')

@section('title')
    Structure
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des sessions</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des structures participantes à la session 
                                <strong style="color:maroon;">{{$session->ses_description}} </strong>
                                de l'année <strong style="color:maroon;">{{$session->ses_annee}}</strong>
                                de l'instance <strong style="color:maroon;">{{$session->ins_sigle}}</strong>
                                </div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddStructure(null)"
                                        data-toggle="modal" data-target="#modalAddStructure">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle structure
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row my-3">
                    @foreach ($participants as $item)                        
                    <div class="col-lg-4 col-xl-3 mb-3">
                        <div class="card">
                            <div class="card-header widget-content bg-info" style="color: white !important;">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        {{ $item->str_id }} - {{$item->str_sigle}} 
                                    </div> 
                                    <div class="widget-content-right">
                                        <div class="btn-group dropdown nav-item">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                               class="nav-link">
                                                <div style="font-size: 24px; font-weight: bold;">&vellip;</div>
                                            </a>
                                            <div tabindex="-1" role="menu" aria-hidden="true"
                                                 class="dropdown-menu dropdown-menu-right">
                                                <a tabindex="0" class="dropdown-item"
                                                   onclick="openModalAddStructure(5)"
                                                   data-toggle="modal" data-target="#modalAddStructure">
                                                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                                </a>
                                                <a tabindex="0" class="dropdown-item"
                                                   onclick="deleteStructure(5, '{{addslashes("Sigle Structure " . $item->str_id)}}')">
                                                    <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div><span>{{$item->str_nom_complet}}</span></div>
                            </div>
                            <div class="card-footer">
                                <button type="button" class="btn btn-secondary" style="width: 100%;"
                                        onclick="window.location.href='{{url('getPageAgentSuivi/'.$item->str_id)}}'">
                                    Agents de suivi&nbsp;<span class="badge badge-pill badge-light">{{ $item->nb_agents }}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script>
        function openModalAddStructure(id) {
//            $('#formAddInstance').attr('action', 'saveInstance');
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle structure');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#ins_id').val(null);
                $('#ins_sigle').val(null);
                $('#ins_nom_complet').val(null);
                $('#ins_nbr_session').val(null);
                $('#ins_periodicite').val(null);
                $('#ins_description').val(null);
            }
            else {
                /*$.ajax({
                 url: "/getInstance/" + id,
                 type: "GET",
                 dataType: "JSON",
                 success: function (data) {*/
                $('.modal-title').text('Modification de la structure');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                $('#ins_id').val(id);
                $('#ins_sigle').val('Sigle');
                $('#ins_nom_complet').val('Nom complet');
                $('#ins_nbr_session').val(4);
                $('#ins_periodicite').val('Trimestrielle');
                $('#ins_description').val('Description');
                /*},
                 error: function () {
                 alert("Erreur rencontrer lors de la recuperation des donnees!");
                 }
                 });*/
            }
        }

        function openModalAgent(id) {
            $('.modal-title').text("Liste des agents de la structure : DAF");
            $('#btnAdd').empty();
            $('#btnAdd').html('<i class="fa fa-save"></i>&nbsp;Ajouter');
            $('#ins_id').val(null);
            $('#ins_sigle').val(null);
        }

        function openModalAddAgent(id) {
            if (id == null) {
                $('.modal-title').text("Ajout d'un nouveau agent de suivi");
                $('#btnAddAgent').empty();
                $('#btnAddAgent').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#id').val(null);
                $('#name').val(null);
                $('#email').val(null);
                $('#structure').val(null);
                $('#matricule').val(null);
                $('#telephone').val(null);
            }
            else {
                $('.modal-title').text('Modification d\'agent de suivi');
                $('#btnAddAgent').empty();
                $('#btnAddAgent').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                $('#id').val(id);
                $('#name').val('Nom1');
                $('#email').val('email@mail.bf');
                $('#structure').val(id);
                $('#matricule').val('MAT001');
                $('#telephone').val('0000000'+id);
            }
        }

        function openModalDetailAgent(id) {
            /*$.ajax({
                url: "/getUser/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    console.log(data);*/
                    $('.modal-title').text('Détails du compte utilisateur');
                    /*$('#lbNom').text(data.user.user_nomprenom);
                    $('#lbLogin').text(data.user.user_login);
                    $('#lbEmail').text(data.user.user_email);
                    $('#lbTel').text(data.user.user_tel);
                    $('#lbStruc').text(data.user.user_structure);
                    $('#lbProf').text(data.profil.prof_libelle);
                    $('#libStatut').text(data.user.user_isactive);
                    $('#lbLastconnect').text(data.user.user_lastconnect);
                    $('#lbCreate').text(data.user.created_at);*/
                    $('#lbNom').text('Nom');
                    $('#lbLogin').text('Login');
                    $('#lbEmail').text("email1@mail.bf");
                    $('#lbTel').text('00000001');
                    $('#lbStruc').text('Structure');
                    $('#lbProf').text('User');
                    $('#libStatut').text('Actif');
                    $('#lbLastconnect').text('Il y a 3 jours');
                    $('#lbCreate').text('Il y a 7 jours');
                /*},
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });*/
        }

        function deleteStructure(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement la structure : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        /*$.ajax({
                         url: "{{url('/deleteStructure')}}/" + id,
                         type: "get",
                         data: {ins_id: id, '_token': '{{csrf_token()}}'},
                         success: function (data) {
                         if (data == 0) {
                         swal({
                         title: 'Attention',
                         text: "Impossible de supprimer cette structure, elle est rattachée à certaines données !",
                         type: 'info',
                         button: "OK"
                         });
                         } else {
                         swal({
                         title: "Succes!",
                         text: "Suppression de la structure effectuée avec succès!",
                         type: "success",
                         button: "OK"
                         });
                         location.reload();
                         }
                         },
                         error: function (data) {
                         swal({
                         title: 'Erreur',
                         text: "Une erreure est survenue lors de la suppression de la structure!\nVeuillez reessayer!",
                         type: 'error',
                         button: "OK"
                         });
                         }
                         });*/
                    });
                },
                allowOutsideClick: false
            });
        }

        function deleteAgent(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement l'agent : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        /*$.ajax({
                         url: "{{url('/deleteAgent')}}/" + id,
                         type: "get",
                         data: {ins_id: id, '_token': '{{csrf_token()}}'},
                         success: function (data) {
                         if (data == 0) {
                         swal({
                         title: 'Attention',
                         text: "Impossible de supprimer ce agent, elle est rattachée à certaines données !",
                         type: 'info',
                         button: "OK"
                         });
                         } else {
                         swal({
                         title: "Succes!",
                         text: "Suppression de l'agent effectuée avec succès!",
                         type: "success",
                         button: "OK"
                         });
                         location.reload();
                         }
                         },
                         error: function (data) {
                         swal({
                         title: 'Erreur',
                         text: "Une erreure est survenue lors de la suppression de l'agent!\nVeuillez reessayer!",
                         type: 'error',
                         button: "OK"
                         });
                         }
                         });*/
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddStructure" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddInstance">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="str_id" name="ins_id" type="hidden">

                            <div class="form-group col-md-4">
                                <label for="str_sigle">Sigle <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="str_sigle" name="str_sigle"
                                       maxlength="20" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="str_nom_complet">Nom complet <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="str_nom_complet"
                                       name="str_nom_complet" maxlength="255" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="str_categorie">Cat&eacute;gorie <span class="requit">*</span></label>
                                <select id="str_categorie" name="str_categorie"
                                        class="form-control form-control-sm selectSearch"
                                        required>
                                    <option value="null">Selectionner la cat&eacute;gorie...</option>
                                    <option value="1">CAT 1</option>
                                    <option value="2">CAT 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>&nbsp;Fermer</button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--MODAL MAJ Agents--}}
    <div class="modal fade my-5" id="modalMajAgent" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="pull-right mb-3">
                        <button type="button" class="btn btn-sm btn-primary"
                                onclick="openModalAddAgent(null)"
                                data-toggle="modal" data-target="#modalAddAgent">
                            <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouveau agent de suivi
                        </button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nom</th>
                                <th>Pr&eacute;nom</th>
                                <th>Email</th>
                                <th>T&eacute;l&eacute;phone</th>
                                <th class="text-center no-sort">Actions</th>
                            </tr>
                            </thead>
                            <tbody id="contentAgents">
                            <tr>
                                <td>1</td>
                                <td>Nom1</td>
                                <td>Prénom1</td>
                                <td>email1@mail.bf</td>
                                <td>00000001</td>
                                <td class="text-center">
                                    <div class='btn-group'>
                                        <button type="button" class="btn btn-sm btn-info" title="D&eacute;tails"
                                                onclick="openModalDetailAgent(null)"
                                                data-toggle="modal" data-target="#modalDetailAgent">
                                            <i class="pe-7s-look"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-primary" title="Modifier"
                                                onclick="openModalAddAgent(1)"
                                                data-toggle="modal" data-target="#modalAddAgent">
                                            <i class="pe-7s-pen"></i></button>
                                        <button type="button" class="btn btn-sm btn-danger" title="Supprimer"
                                                onclick="deleteAgent(1, '{{addslashes("Nom1")}}')">
                                            <i class="pe-7s-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Nom2</td>
                                <td>Prénom2</td>
                                <td>email2@mail.bf</td>
                                <td>00000002</td>
                                <td class="text-center">
                                    <div class='btn-group'>
                                        <button type="button" class="btn btn-sm btn-info" title="D&eacute;tails"
                                                onclick="openModalDetailAgent(null)"
                                                data-toggle="modal" data-target="#modalDetailAgent">
                                            <i class="pe-7s-look"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-primary" title="Modifier"
                                                onclick="openModalAddAgent(2)"
                                                data-toggle="modal" data-target="#modalAddAgent">
                                            <i class="pe-7s-pen"></i></button>
                                        <button type="button" class="btn btn-sm btn-danger" title="Supprimer"
                                                onclick="deleteAgent(2, '{{addslashes("Nom2")}}')">
                                            <i class="pe-7s-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">
                        <i class="fa fa-ban"></i>&nbsp;Fermer</button>
                </div>
            </div>
        </div>
    </div>

    {{--MODAL AJOUT & EDIT Agent--}}
    <div class="modal fade my-5" id="modalAddAgent" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddInstance">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="id" name="id" type="hidden">

                            <div class="form-group col-md-4">
                                <label for="name">Nom & Pr&eacute;nom <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="name" name="name"
                                       maxlength="255" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="matricule">Matricule <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="matricule"
                                       name="matricule" maxlength="255" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="telephone">T&eacute;l&eacute;phones <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="telephone"
                                       name="telephone" maxlength="255" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="email">Email <span class="requit">*</span></label>
                                <input type="email" class="form-control form-control-sm" id="email"
                                       name="email" maxlength="255" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="structure">Structure <span class="requit">*</span></label>
                                <select id="structure" name="structure"
                                        class="form-control form-control-sm selectSearch"
                                        required>
                                    <option value="null">Selectionner la structure...</option>
                                    <option value="1">Struc 1</option>
                                    <option value="2">Struc 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>&nbsp;Fermer</button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnAddAgent"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--MODAL DETAIL Agent--}}
    <div class="modal fade my-5" id="modalDetailAgent" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-12">
                            <div class="row justify-content-center">
                                <div class="col-md-11">
                                    <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                                        <table class="table table-sm table-bordered table-striped"
                                               id="users">
                                            <tr>
                                                <th>Nom & Prénom</th>
                                                <td id="lbNom"></td>
                                            </tr>
                                            <tr>
                                                <th>Nom d'utilisateur</th>
                                                <td id="lbLogin"></td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td id="lbEmail"></td>
                                            </tr>
                                            <tr>
                                                <th>Téléphone(s)</th>
                                                <td id="lbTel"></td>
                                            </tr>
                                            <tr>
                                                <th>Structure</th>
                                                <td id="lbStruc"></td>
                                            </tr>
                                            <tr>
                                                <th>Profil</th>
                                                <td id="lbProf"></td>
                                            </tr>
                                            <tr>
                                                <th>Statut</th>
                                                <td id="libStatut">
                                                    {{--@if ($user->user_isactive == false)
                                                        <span class="badge badge-danger">Désactivé</span>
                                                    @else <span class="badge badge-success">Activé</span>
                                                    @endif--}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Dernière connexion il y a</th>
                                                <td id="lbLastconnect"></td>
                                            </tr>
                                            <tr>
                                                <th>Compte crée il y a</th>
                                                <td id="lbCreate"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>&nbsp;Fermer</button>
                </div>

            </div>
        </div>
    </div>

@endsection