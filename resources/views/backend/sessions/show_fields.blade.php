<!-- Ins Id Field -->
<div class="form-group">
    {!! Form::label('ins_id', 'Ins Id:') !!}
    <p>{{ $session->ins_id }}</p>
</div>

<!-- Ses Description Field -->
<div class="form-group">
    {!! Form::label('ses_description', 'Ses Description:') !!}
    <p>{{ $session->ses_description }}</p>
</div>

<!-- Ses Annee Field -->
<div class="form-group">
    {!! Form::label('ses_annee', 'Ses Annee:') !!}
    <p>{{ $session->ses_annee }}</p>
</div>

<!-- Ses Date Debut Field -->
<div class="form-group">
    {!! Form::label('ses_date_debut', 'Ses Date Debut:') !!}
    <p>{{ $session->ses_date_debut }}</p>
</div>

<!-- Ses Lieu Field -->
<div class="form-group">
    {!! Form::label('ses_lieu', 'Ses Lieu:') !!}
    <p>{{ $session->ses_lieu }}</p>
</div>

<!-- Ses Date Fin Field -->
<div class="form-group">
    {!! Form::label('ses_date_fin', 'Ses Date Fin:') !!}
    <p>{{ $session->ses_date_fin }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $session->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $session->updated_by }}</p>
</div>

