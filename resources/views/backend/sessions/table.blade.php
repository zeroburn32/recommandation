<div class="table-responsive">
    <table class="table table-bordered table-sm table-striped table-hover dt_example" id="tb_sessions">
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Intitul&eacute;</th>
            <th>Date d&eacute;but</th>
            <th>Lieu</th>
            <th>Statut</th>
            <th class="text-center no-sort">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sessions as $number=>$session)
            <tr>
                <td>{{ $number + 1 }}</td>
                <td>{{ $session->ses_description }}</td>
                <td>{{ \Carbon\Carbon::parse($session->ses_date_debut)->format('d/m/Y') }}</td>
                <td>{{ $session->ses_lieu }}</td>
                <td>
                    @if($session->ses_statut == 1)
                        <span class="badge badge-info">En formulation</span>
                    @elseif($session->ses_statut == 2)
                        <span class="badge badge-warning">En planification</span>
                    @elseif($session->ses_statut == 3)
                        <span class="badge badge-primary">En r&eacute;alisation</span>
                    @elseif($session->ses_statut == 4)
                        <span class="badge badge-success">Suivi validé</span>
                    @else
                        <span></span>
                    @endif
                </td>
                <td class="text-center">
                    <div class='btn-group'>
                        <button type="button"
                                class="btn btn-xs btn-primary dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item"
                               onclick="openModalDetail({{ $session->ses_id }})"
                               data-toggle="modal" data-target="#modalDetail">
                                <i class="fa fa-eye"></i>&nbsp;&nbsp;D&eacute;tails
                            </a>
                            @if(Auth::user()->hasRole(['SUPERADMIN']))
                                <a class="dropdown-item"
                                   onclick="openModalAddSession({{ $session->ses_id }})"
                                   data-toggle="modal" data-target="#modalAddSession">
                                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                </a>
                                <a class="dropdown-item"
                                   title="Supprimer la session"
                                   onclick="deleteSession({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}')">
                                    <i class="fa fa-trash"></i>&nbsp;&nbsp;Annuler
                                </a>
                                <a class="dropdown-item"
                                   title="Joindre la liste des participants scannée"
                                   onclick="openModalJoindreList({{ $session->ses_id }})"
                                   data-toggle="modal" data-target="#modalJoindrePart">
                                    <i class="fa fa-upload"></i>&nbsp;&nbsp;MAJ de la liste des participants
                                </a>
                                {{--Si la liste des participants est uploader--}}
                                @if(!is_null($session->ses_liste_participants))
                                    <a class="dropdown-item"
                                       href="{{ url('/list_participants/' . $session->ses_liste_participants) }}" target="_blank">
                                        <i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la liste des
                                        participants
                                    </a>
                                @endif
                                @if($session->ses_statut == 1)
                                    <a class="dropdown-item"
                                       title="Valider la formulation"
                                       onclick="changeStatus({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}', 2)">
                                        <i class="fa fa-check"></i>&nbsp;&nbsp;Valider la formulation
                                    </a>
                                @endif
                                @if($session->ses_statut == 2)
                                    <a class="dropdown-item" title="Valider la planification"
                                       data-toggle="modal" data-target="#modalValidation"
                                       onclick="openModalValidePlaning({{ $session->ses_id }})"
                                            {{--"changeStatus({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}', 3)"--}}>
                                        <i class="fa fa-check"></i>&nbsp;&nbsp;Valider la planification
                                    </a>
                                @endif
                                @if($session->ses_statut == 3)
                                    <a class="dropdown-item"
                                       title="Valider la r&eacute;alisation"
                                       onclick="changeStatus({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}', 4)">
                                        <i class="fa fa-check"></i>&nbsp;&nbsp;Valider la r&eacute;alisation
                                    </a>
                                @endif
                                <hr/>
                                <a class="dropdown-item"
                                   title="Recommandations"
                                   href="{{ url('getPageRecosBySession').'/'.$session->ses_id }}">
                                    <i class="fa fa-list"></i>&nbsp;&nbsp;Recommandation(s)&nbsp;
                                    <span class="badge badge-pill badge-warning">
                                    {{ App\Http\Controllers\MethodesStaticController::nbreRecosBySession($session->ses_id) }}
                                </span>
                                </a>
                                <a class="dropdown-item"
                                   title="R&eacute;conduire les recommandations de la session pr&eacute;c&eacute;dante"
                                   onclick="openModalReconduireReco({{ $session->ses_id }})"
                                   data-toggle="modal" data-target="#modalReconduire">
                                    {{--onclick="reconduireRecos({{ $session->ses_id }}, {{ $session->ses_id_precedante }})">--}}
                                    <i class="fa fa-upload"></i>&nbsp;&nbsp;R&eacute;conduire&nbsp;
                                    <span class="badge badge-pill badge-warning">
                                        {{ App\Http\Controllers\MethodesStaticController::nbreRecosOfReconductBySession($session->ses_id_precedante) }}
                                    </span>
                                </a>
                            @else
                                {{--En formulation & en planification--}}
                                @if($session->ses_statut == 1 || $session->ses_statut == 2)
                                    <a class="dropdown-item"
                                       onclick="openModalAddSession({{ $session->ses_id }})"
                                       data-toggle="modal" data-target="#modalAddSession">
                                        <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                    </a>
                                @endif
                                <a class="dropdown-item"
                                   title="Joindre la liste des participants scannée"
                                   onclick="openModalJoindreList({{ $session->ses_id }})"
                                   data-toggle="modal" data-target="#modalJoindrePart">
                                    <i class="fa fa-upload"></i>&nbsp;&nbsp;MAJ de la liste des participants
                                </a>
                                {{--Si la liste des participants est uploader--}}
                                @if(!is_null($session->ses_liste_participants))
                                    <a class="dropdown-item"
                                       href="{{ url('/list_participants/' . $session->ses_liste_participants) }}" target="_blank">
                                        <i class="fa fa-download"></i>&nbsp;&nbsp;T&eacute;l&eacute;charger la liste des
                                        participants
                                    </a>
                                @endif
                                @if($session->ses_statut == 1)
                                    {{-- La date fin de session doit etre depassee --}}
                                    @can(['VALIDER_FORMULATION'])
                                    @if(!is_null($session->ses_date_fin) && $session->ses_date_fin < \Carbon\Carbon::now()->format('Y-m-d'))
                                        <a class="dropdown-item"
                                           title="Valider la formulation"
                                           onclick="changeStatus({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}', 2)">
                                            <i class="fa fa-check"></i>&nbsp;&nbsp;Valider la formulation
                                        </a>
                                    @endif
                                    @endcan
                                    <a class="dropdown-item"
                                       title="Supprimer la session"
                                       onclick="deleteSession({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}')">
                                        <i class="fa fa-trash"></i>&nbsp;&nbsp;Annuler
                                    </a>
                                @elseif($session->ses_statut == 2)
                                    {{-- La date fin de planification doit etre depassee --}}
                                    @can(['VALIDER_PLANIFICATION'])
                                    @if(!is_null($session->ses_datefin_planification) && $session->ses_datefin_planification < \Carbon\Carbon::now()->format('Y-m-d'))
                                        <a class="dropdown-item" title="Valider la planification"
                                           data-toggle="modal" data-target="#modalValidation"
                                           onclick="openModalValidePlaning({{ $session->ses_id }})"
                                                {{--"changeStatus({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}', 3)"--}}>
                                            <i class="fa fa-check"></i>&nbsp;&nbsp;Valider la planification
                                        </a>
                                    @endif
                                    @endcan
                                @elseif($session->ses_statut == 3)
                                    {{-- La date fin de realisation (suivi) doit etre depassee --}}
                                    @can(['VALIDER_REALISATION'])
                                    @if(!is_null($session->ses_datefin_realisation) && $session->ses_datefin_realisation < \Carbon\Carbon::now()->format('Y-m-d'))
                                        <a class="dropdown-item"
                                           title="Valider la r&eacute;alisation"
                                           onclick="changeStatus({{ $session->ses_id }}, '{{addslashes($session->ins_sigle .' '. $session->ses_annee)}}', 4)">
                                            <i class="fa fa-check"></i>&nbsp;&nbsp;Valider la r&eacute;alisation
                                        </a>
                                    @endif
                                    @endcan
                                @endif
                                <hr/>
                                <a class="dropdown-item"
                                   title="Recommandations"
                                   href="{{ url('getPageRecosBySession').'/'.$session->ses_id }}">
                                    <i class="fa fa-list"></i>&nbsp;&nbsp;Recommandation(s)&nbsp;
                                    <span class="badge badge-pill badge-warning">
                                    {{ App\Http\Controllers\MethodesStaticController::nbreRecosBySession($session->ses_id) }}
                                </span>
                                </a>
                                {{-- En formulation --}}
                                @can(['RECONDUIRE_RECOMMANDATION'])
                                @if($session->ses_statut == 1)
                                    <a class="dropdown-item"
                                       title="R&eacute;conduire les recommandations de la session pr&eacute;c&eacute;dante"
                                       onclick="openModalReconduireReco({{ $session->ses_id }})"
                                       data-toggle="modal" data-target="#modalReconduire">
                                        {{--onclick="reconduireRecos({{ $session->ses_id }}, {{ $session->ses_id_precedante }})">--}}
                                        <i class="fa fa-upload"></i>&nbsp;&nbsp;R&eacute;conduire&nbsp;
                                        <span class="badge badge-pill badge-warning">
                                        {{ App\Http\Controllers\MethodesStaticController::nbreRecosOfReconductBySession($session->ses_id_precedante) }}
                                    </span>
                                    </a>
                                @endif
                                @endcan
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
