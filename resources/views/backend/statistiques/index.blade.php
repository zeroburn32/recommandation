@extends('layouts.template')

@section('title')
Statistiques
@parent
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/select2.min.css') }}" />
<style>
    .mycard {
        height: 230px;
        overflow: auto;
    }
</style>
@stop

@section('content')
<div class="container-fluid">
    <div class="card mb-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Etats</li>
                <li class="breadcrumb-item active" aria-current="page">Statistiques</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Critères de statistiques ( <em
                                    style="text-transform:none; color:mediumslateblue"> Remplissez les critères
                                    souhaitées </em>)</div>
                        </div>
                        {{-- <label for="fil_type" class="col-sm-3 offset-sm-3 col-form-label">Choisir le type de statistique &nbsp;&nbsp;&nbsp;</label>
                        <div class="widget-content-center" style="padding-right:20%">
                            <select class="form-control form-control-sm selectSearch" id="fil_type"
                                    name="fil_type" onchange="onChangeInstance()">
                                    <option value="0">Choisir la statistique...</option>
                                    <option value="1">Nombre total de recommandations suivant l'instance, la session, l'année, le responsable, l'état...</option>
                                    <option value="2">Taux de mise en oeuvre des recommandations suivant l'instance, la session, l'année, le responsable, l'état...</option>
                                </select>
                        </div> --}}
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('statistiqueSearch')}}" id="formFilter">
                        <div class="row justify-content-center">
                            @csrf
                            <div class="from-group col-lg-3 col-xl-2">
                                <label for="fil_instance"> Instance </label>
                                <select class="form-control form-control-sm selectSearch" id="fil_instance"
                                    name="fil_instance" onchange="onChangeInstance()">
                                    <option value="">Choisir l'instance...</option>
                                    @foreach($instances as $key => $value)
                                    <option value="{{ $key }}" @if($instance==$key) selected @endif>{{ $value }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <label for="fil_annee">Annee</label>
                                <input type="number" class="form-control form-control-sm" id="fil_annee"
                                    name="fil_annee" value="{{$annee}}" readonly />
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <label for="fil_session">Session de l'instance</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_session"
                                    name="fil_session">
                                    <option value="">Choisir la session...</option>
                                    @foreach($sessions as $key => $value)
                                    <option value="{{ $key }}" @if($session==$key) selected @endif>{{ $value }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <label for="fil_structure">Structure responsable</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_structure"
                                    name="fil_structure">
                                    <option value="">Choisir l'acteur responsable</option>
                                    @foreach($structures as $key => $value)
                                    <option value="{{ $key }}" @if($acteur==$key) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-xl-2">
                                <label for="fil_status">Statut</label>
                                <select class="form-control form-control-sm selectSearch" id="fil_status"
                                    name="fil_status">
                                    <option value="">Choisir le statut...</option>
                                    @foreach($statuts as $key => $value)
                                    <option value="{{ $key }}" @if($statut==$key) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="from-group col-lg-2 col-xl-2    ">
                                <label for="btnSearch">&nbsp;</label>
                                <button type="submit" class="btn btn-sm btn-primary" style="width: 100%" id="btnSearch">
                                    <i class="fa fa-search"></i>&nbsp;Rechercher
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if (empty($nb_recos))
            <div class="row my-3">
                <div class="col-lg-12 col-xl-12 mb-3">
                    {{-- <div class="card">
                                <div class="card-body">
                                </div>
                            </div> --}}
                </div>
            </div>
            @else

            <div class="row my-3">
                <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="widget-heading">{{$title_table_nombre}}</div>
                                        </div>
                                        <div class="card-body mycard">
                                            @if ($nb_recos->isEmpty())
                                            Aucun résultat disponible
                                            @else
                                            <table class="table table-striped table-sm dt_example0" id="nombre_table">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th>{{$title_col}}</th>
                                                        <th>Nombre</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="mytbody">
                                                    @foreach($nb_recos as $i=>$reco)
                                                    <tr>
                                                        <th>{{$reco->ins_sigle ?? ''}}</th>
                                                        <td>{{$reco->valeur ?? ''}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="widget-heading">{{$title_table_taux}}</div>
                                        </div>
                                        <div class="card-body mycard">
                                            @if ($tx_recos->isEmpty())
                                            Aucun résultat disponible
                                            @else
                                            <table class="table table-striped table-sm dt_example0" id="taux_table">
                                                <thead class="thead-dark">
                                                    <th>{{$title_col}}</th>
                                                    <th>Taux(%)</th>

                                                </thead>
                                                <tbody class="mytbody">
                                                    @foreach($tx_recos as $i=>$reco)
                                                    <tr>
                                                        <td>{{$reco->ins_sigle ?? ''}}</td>
                                                        <td>{{number_format($reco->valeur ?? '',2)}} </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-2">
                @if ($nb_recos->isEmpty())
                @else
                <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                                    <div class="card">
                                        {{-- <div class="card-header">
                                            <div class="widget-heading">{{$title_table_nombre}}</div>
                                </div> --}}
                                <div class="card-body">
                                    <div id="chartNombre" style="width:100%; height: 250px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if ($tx_recos->isEmpty())
        @else
        <div class="col-sm-6 col-md-6 col-lg-6 col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                            <div class="card">
                                {{-- <div class="card-header">
                                            <div class="widget-heading">{{$title_table_taux}}</div>
                        </div> --}}
                        <div class="card-body">
                            <div id="chartTaux" style="width:100%; height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
</div>

<div class="row my-2">
    <div class="col-sm-12 col-md-12 col-lg-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="widget-heading">{{$title_table_statut}}</div>
                            </div>
                            <div class="card-body mycard">
                                @if ($statut_recos->isEmpty())
                                Aucun résultat disponible
                                @else
                                <table class="table table-striped table-sm dt_example0" id="statut_table">
                                    <thead class="thead-dark">
                                        <th></th>
                                        <th>Non réalisé</th>
                                        <th>En cours</th>
                                        <th>Réalisé</th>
                                        <th>Abandonnée</th>
                                    </thead>
                                    <tbody class="mytbody">
                                        @foreach($statut_recos as $i=>$reco)
                                        <tr>
                                            <td>{{$reco->ins_sigle }}</td>
                                            <td>
                                                @if(!empty($reco->non_realise[0]))
                                                {{$reco->non_realise[0]->valeur1 ?? '' }}
                                                @else 0 @endif
                                            </td>
                                            <td>
                                                @if(!empty($reco->en_cours[0]))
                                                {{$reco->en_cours[0]->valeur1 ?? ''}}
                                                @else 0 @endif
                                            </td>
                                            <td>
                                                @if(!empty($reco->realise[0]))
                                                {{$reco->realise[0]->valeur1  ?? '' }}
                                                @else 0 @endif
                                            </td>
                                            <td>
                                                @if(!empty($reco->abandonne[0]))
                                                {{$reco->abandonne[0]->valeur1 ?? '' }}
                                                @else 0 @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($statut_recos->isEmpty())
@else
<div class="row my-2">
    <div class="col-sm-12 col-md-12 col-lg-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                        <div class="card">

                            <div class="card-body">
                                <div id="chartStatut" style="width:100%; height: 270px;"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

</div>
@endif
</div>

</div>
{{-- </div>
</div> --}}

@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('backend/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/daterangepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('backend/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript">
</script>
{{-- <script src="{{ url('backend/js/clockface.js') }}" type="text/javascript"></script> --}}
<script type="text/javascript" src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/date.format.js') }}" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $('#fil_instance').select2({placeholder: "Choisir l'instance...",allowClear: true });
        $('#fil_session').select2({placeholder: "Choisir la session de l'instance...",allowClear: true});
        $('#fil_structure').select2({placeholder: "Choisir l'acteur responsable...",allowClear: true});
        $('#fil_status').select2({placeholder: "Choisir le statut de la recommandation...",allowClear: true});

        $("#fil_annee").datetimepicker({
            ignoreReadonly: true,
            viewMode: 'years',
            format: 'YYYY',
            maxDate: new Date(),
            showClear: true,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                clear: "fa fa-trash"
            }
        }).val('{{$annee}}').parent().css("position :relative");

        // Chargement de la session en fonction de l'année
            $('#fil_annee').on('dp.update', function(e){ 
                if($('#fil_instance').val()) {
                    onChangeInstance();
                }
            });

        function onChangeInstance() {
            var instance = $('#fil_instance').val();
            if(!instance) {
                instance = -1;
            }
            var annee = $('#fil_annee').val();
            $.ajax({
                url: APP_URL+"/getSessionsByInstance/"+instance+"/"+annee,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#fil_session').empty();
                    $('#fil_session').append($('<option>', {
                        value: '',
                        text: 'Choisir la session...'
                    }));
                    $.each(data, function (index, session) {
                        if (instance == session.ins_id) {
                            $('#fil_session').append($('<option>', {
                                value: session.ses_id,
                                text: session.ses_description
                            }));
                        }
                    });
                    //$('#fil_session').attr('required', true);
                }
            });
           
            $.ajax({
                url: APP_URL+"/getActeursByInstance/"+instance,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#fil_structure').empty();
                    $('#fil_structure').append($('<option>', {
                        value: '',
                        text: 'Choisir un responsable...'
                    }));
                    if (instance ==-1) {
                        $.each(data, function (index, acteur) {
                            $('#fil_structure').append($('<option>', {
                                value: acteur.str_id,
                                text: acteur.str_sigle+' - '+acteur.str_nom_complet
                            }));
                    });
                    } else {
                        $.each(data, function (index, acteur) {
                        if (instance == acteur.ins_id) {
                            $('#fil_structure').append($('<option>', {
                                value: acteur.str_id,
                                text: acteur.str_sigle+' - '+acteur.str_nom_complet
                            }));
                        }
                    });
                    }
                    
                    //$('#fil_structure').attr('required', true);
                }
            });
        }


// Graphiques 
Highcharts.chart('chartNombre', {
    data: {
        table: 'nombre_table'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: "{{$title_table_nombre}}"
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Nombre'
        }
    },
    tooltip: {
        pointFormat: '{series.name} <b> {point.y} </b>'
        /* formatter: function () {
            return '<b> {series.name} </b><br/>{point.name}.toLowerCase() {point.y}' ;
        } */
    },
    lang : {
            downloadCSV : 'Télécharger CSVs',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                 contextButton: {
                                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ],
                                //symbol:"<i class='fa fa-home'></i>"
                },
                exportButton: {
                    enabled: false
                }   
            }
        }
});

Highcharts.chart('chartTaux', {
    data: {
        table: 'taux_table'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: "{{$title_table_taux}}"
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Taux'
        }
    },
    tooltip: {
        pointFormat: '{series.name} <b> {point.y}% </b>'
        /* formatter: function () {
            return '<b> {series.name} </b><br/>{point.name}.toLowerCase() {point.y}' ;
        } */
    },
    plotOptions: {
            column: {
                colorByPoint: true
            }
        },
    colors: [
        '#db9f37',
    ],
    lang : {
            downloadCSV : 'Télécharger CSVs',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                 contextButton: {
                                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ],
                                //symbol:"<i class='fa fa-home'></i>"
                },
                exportButton: {
                    enabled: false
                }   
            }
        }
});
Highcharts.chart('chartStatut', {
    data: {
        table: 'statut_table',
    },
    chart: {
        type: 'column'
    },
    title: {
        text: "{{ $title_table_statut }}"
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Nombre'
        }
    },
    tooltip: {
        pointFormat: '{series.name} <b> {point.y}% </b>'
        /* formatter: function () {
            return '<b> {series.name} </b><br/>{point.name}.toLowerCase() {point.y}' ;
        } */
    },
    lang : {
            downloadCSV : 'Télécharger CSVs',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                 contextButton: {
                                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ],
                                //symbol:"<i class='fa fa-home'></i>"
                },
                exportButton: {
                    enabled: false
                }   
            }
        }
});

</script>
@endsection

@section('modal')

@endsection