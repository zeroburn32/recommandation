@extends('layouts.template')
@section('title')
    Agents de suivi
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('structures') }}">Structures</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des agents de suivi</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des agents de suivi de la structure : {{$structure->str_nom_complet }}</div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-warning"
                                        onclick="goBack()">
                                    <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-body">
                        <div class="pull-right mb-3">
                            <button type="button" class="btn btn-sm btn-primary"
                                    onclick="openModalAddAgent(null)"
                                    data-toggle="modal" data-target="#modalAddAgent">
                                <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouveau agent de suivi
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm dt_example">
                                <thead>
                                    <th>Nom & pr&eacute;nom(s)</th>
                                    <th>Matricule</th>
                                    <th>Email</th>
                                    <th>Téléphone</th>
                                    <th class="no-sort text-center">Actions</th>
                                </thead>
                                <tbody id="contentAgents">
                                <tr>
                                    @foreach($users as $item)
                                    <tr>
                                        <td>{{ $item->nom_prenom }}</td>
                                        <td>{{ $item->matricule }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->telephone }}</td>
                                        <td class="text-center">
                                            <div class='btn-group'>
                                                <button type="button" class="btn btn-sm btn-info" title="D&eacute;tails" onclick="openModalDetailAgent({{$item->str_id}})"
                                                    data-toggle="modal" data-target="#modalDetailAgent">
                                                    <i class="pe-7s-look"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-primary" title="Modifier" onclick="openModalAddAgent({{$item->str_id}})"
                                                    data-toggle="modal" data-target="#modalAddAgent">
                                                    <i class="pe-7s-pen"></i></button>
                                                <button type="button" class="btn btn-sm btn-danger" title="Supprimer ou retirer un agent"
                                                    onclick="deleteAgent({{$item->str_id}}, {{$item->str_nom_complet}})">
                                                    <i class="pe-7s-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script>
    $('.selectSearch').select2({
        placeholder: "Choisir...",
        allowClear: true
    });
        function openModalAddAgent(id) {
            if (id == null) {
                $('.modal-title').text("Ajout d'un nouveau agent de suivi");
                $('#btnAddAgent').empty();
                $('#btnAddAgent').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#id').val(null);
                $('#name').val(null);
                $('#email').val(null);
                $('#structure').val(null);
                $('#matricule').val(null);
                $('#telephone').val(null);
            }
            else {
                $('.modal-title').text('Modification d\'agent de suivi');
                $('#btnAddAgent').empty();
                $('#btnAddAgent').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                $('#id').val(id);
                $('#name').val('Nom1');
                $('#email').val('email@mail.bf');
                $('#structure').val(id);
                $('#matricule').val('MAT001');
                $('#telephone').val('0000000'+id);
            }
        }

        function openModalDetailAgent(id) {
            /*$.ajax({
             url: "/getUser/" + id,
             type: "GET",
             dataType: "JSON",
             success: function (data) {
             console.log(data);*/
            $('.modal-title').text('Détails du compte utilisateur');
            /*$('#lbNom').text(data.user.user_nomprenom);
             $('#lbLogin').text(data.user.user_login);
             $('#lbEmail').text(data.user.user_email);
             $('#lbTel').text(data.user.user_tel);
             $('#lbStruc').text(data.user.user_structure);
             $('#lbProf').text(data.profil.prof_libelle);
             $('#libStatut').text(data.user.user_isactive);
             $('#lbLastconnect').text(data.user.user_lastconnect);
             $('#lbCreate').text(data.user.created_at);*/
            $('#lbNom').text('Nom');
            $('#lbLogin').text('Login');
            $('#lbEmail').text("email1@mail.bf");
            $('#lbTel').text('00000001');
            $('#lbStruc').text('Structure');
            $('#lbProf').text('User');
            $('#libStatut').text('Actif');
            $('#lbLastconnect').text('Il y a 3 jours');
            $('#lbCreate').text('Il y a 7 jours');
            /*},
             error: function () {
             alert("Erreur rencontrer lors de la recuperation des donnees!");
             }
             });*/
        }

        function deleteAgent(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement l'agent : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        /*$.ajax({
                         url: "{{url('/deleteAgent')}}/" + id,
                         type: "get",
                         data: {ins_id: id, '_token': '{{csrf_token()}}'},
                         success: function (data) {
                         if (data == 0) {
                         swal({
                         title: 'Attention',
                         text: "Impossible de supprimer ce agent, elle est rattachée à certaines données !",
                         type: 'info',
                         button: "OK"
                         });
                         } else {
                         swal({
                         title: "Succes!",
                         text: "Suppression de l'agent effectuée avec succès!",
                         type: "success",
                         button: "OK"
                         });
                         location.reload();
                         }
                         },
                         error: function (data) {
                         swal({
                         title: 'Erreur',
                         text: "Une erreure est survenue lors de la suppression de l'agent!\nVeuillez reessayer!",
                         type: 'error',
                         button: "OK"
                         });
                         }
                         });*/
                    });
                },
                allowOutsideClick: false
            });
        }

        //********************** Users managements ********************************//
        function desactiver(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir désactivé le compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Désactiver',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/lockAndUnlockAccount')}}/" + id,
                            type: 'PATCH',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (response) {
                                swal({
                                    title: "Désactivation !",
                                    text: "Compte désactiver avec succès !",
                                    type: "success",
                                    button: "OK"
                                });
                                window.location.href = "users";
                            },
                            error: function () {
                                swal({
                                    title: "Désactivation !",
                                    text: "Une erreur est survenue lors de la désactivation !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function debloquer(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir débloquer le compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Débloquer',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/bloquerDebloquer')}}/" + id,
                            type: 'PATCH',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (response) {
                                swal({
                                    title: "Désactivation !",
                                    text: "Compte débloquer avec succès !",
                                    type: "success",
                                    button: "OK"
                                });
                                window.location.href = "users";
                            },
                            error: function () {
                                swal({
                                    title: "Désactivation !",
                                    text: "Une erreur est survenue lors du déblocage !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function activer(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir activé le compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Activer',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/lockAndUnlockAccount')}}/" + id,
                            type: 'PATCH',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (response) {
                                swal({
                                    title: "Activation !",
                                    text: "Compte activer avec succès !",
                                    type: "success",
                                    button: "OK"
                                })
                                window.location.href = "users";
                            },
                            error: function () {
                                swal({
                                    title: "Activation !",
                                    text: "Une erreur est survenue lors de l'activation !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function reinitialiser(id, login) {
            swal({
                title: 'Êtes-vous sûre ?',
                text: "De vouloir réinitialiser le mot de passe du compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Réinitialiser',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/reinitialiserMotDePasse')}}/" + id,
                            type: 'GET',
                            success: function (data) {
                                swal({
                                    title: "Opération reussie !",
                                    text: "Votre nouveau mot de passe est : " + data,
                                    type: "success",
                                    button: "OK"
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: "Réinitialisation !",
                                    text: "Une erreur est survenue lors de l'envoi du mot de passe par mail. Votre nouveau mot de passe est : " + data,
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script>
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT Agent--}}
    <div class="modal fade my-5" id="modalAddAgent" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            <form class="needs-validation" novalidate method="POST" action="{{route('users.store')}}" id="formAddInstance">
                    <div class="modal-body">
                        <div class="row{{-- justify-content-center--}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="id" name="id" type="hidden"> 
                            <div class="form-group col-md-4">
                                <label for="name">Nom & Pr&eacute;nom <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="nom_prenom" name="nom_prenom"
                                       maxlength="255" required/>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="structure">Structure <span class="requit">*</span></label>
                                {!! Form::select('structure', $structures, null, ['class' => 'form-control form-control-sm selectSearch', 'required'
                                => 'required'])
                                !!}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">Email <span class="requit">*</span></label>
                                <input type="email" class="form-control form-control-sm" id="email" name="email" maxlength="255" required />
                            </div>
                            <div class="form-group col-md-4">
                                <label for="matricule">Matricule </label>
                                <input type="text" class="form-control form-control-sm" id="matricule"
                                       name="matricule" maxlength="255" />
                            </div>
                            <div class="form-group col-md-4">
                                <label for="telephone">T&eacute;l&eacute;phones </label>
                                <input type="text" class="form-control form-control-sm" id="telephone"
                                       name="telephone" maxlength="255" />
                            </div>
                            
                            {{-- <div class="form-group col-md-4">
                                <label for="name">Nom d\'utilisateur<span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="name" name="name" maxlength="255" required />
                            </div> --}}
                            
                            {{-- <div class="form-group col-md-4">
                                <label for="structure">Selectionnez le profil <span class="requit">*</span></label>
                                {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control form-control-sm selectSearch','multiple' => false]) !!}
                            </div> --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>&nbsp;Fermer</button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnAddAgent"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--MODAL DETAIL Agent--}}
    <div class="modal fade my-5" id="modalDetailAgent" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-12">
                            <div class="row justify-content-center">
                                <div class="col-md-11">
                                    <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                                        <table class="table table-sm table-bordered table-striped"
                                               id="users">
                                            <tr>
                                                <th>Nom & Prénom</th>
                                                <td id="lbNom"></td>
                                            </tr>
                                            <tr>
                                                <th>Nom d'utilisateur</th>
                                                <td id="lbLogin"></td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td id="lbEmail"></td>
                                            </tr>
                                            <tr>
                                                <th>Téléphone(s)</th>
                                                <td id="lbTel"></td>
                                            </tr>
                                            <tr>
                                                <th>Structure</th>
                                                <td id="lbStruc"></td>
                                            </tr>
                                            <tr>
                                                <th>Profil</th>
                                                <td id="lbProf"></td>
                                            </tr>
                                            <tr>
                                                <th>Statut</th>
                                                <td id="libStatut">
                                                    {{--@if ($user->user_isactive == false)
                                                        <span class="badge badge-danger">Désactivé</span>
                                                    @else <span class="badge badge-success">Activé</span>
                                                    @endif--}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Dernière connexion il y a</th>
                                                <td id="lbLastconnect"></td>
                                            </tr>
                                            <tr>
                                                <th>Compte crée il y a</th>
                                                <td id="lbCreate"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>&nbsp;Fermer</button>
                </div>

            </div>
        </div>
    </div>

@endsection