<!-- Str Sigle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('str_sigle', 'Str Sigle:') !!}
    {!! Form::text('str_sigle', null, ['class' => 'form-control']) !!}
</div>

<!-- Str Nom Complet Field -->
<div class="form-group col-sm-6">
    {!! Form::label('str_nom_complet', 'Str Nom Complet:') !!}
    {!! Form::text('str_nom_complet', null, ['class' => 'form-control']) !!}
</div>

<!-- Str Categorie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('str_categorie', 'Str Categorie:') !!}
    {!! Form::number('str_categorie', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('structures.index') }}" class="btn btn-secondary">Cancel</a>
</div>
