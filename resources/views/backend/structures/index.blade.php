@extends('layouts.template')

@section('title')
Liste des Structures
@parent
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
@stop

@section('content')
<div class="container-fluid">
    <div class="card mb-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                <li class="breadcrumb-item active" aria-current="page">Gestion des structures</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Liste des structures</div>
                        </div>
                        <div class="widget-content-right">
                            @can('GERER_STRUCTURE')
                        <a type="button" class="btn btn-sm btn-info" href="{{ route('categories.index')}}" style="text-transform:none;">
                                <i class="fa fa-edit fa-lg"></i>&nbsp;&nbsp;Gérer les catégories
                        </a>
                            <button type="button" class="btn btn-sm btn-primary" onclick="openModalAddStructure(null)"
                                data-toggle="modal" data-target="#modalAddStructure">
                                <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvelle structure
                            </button>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="card-body">
                <form class="needs-validation" novalidate method="GET" action="{{ route('structuresSearch')}}"    id="formFilter">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="card-title col-md-1">Filtre</div>
                            <div class="form-group col-md-3" id="div_select">
                                <select class="form-control form-control-sm selectSearch" id="fil_categorie"
                                    name="fil_categorie" required>
                                    <option value=""></option>
                                
                                    @foreach ($categories as $item =>$value)
                                    <option value="{{$item}}" @if (isset($categorie) && $item==$categorie) selected
                                        @endif>
                                        {{$value}}</option>
                                    @endforeach
                               </select>
                            </div>
                            <div class="from-group col-lg-3 col-xl-3">
                                <button type="submit" class="btn btn-sm btn-primary" style="width: 100%" >
                                    <i class="fa fa-search"></i>&nbsp;Rechercher
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-lg-12 col-xl-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 45vh;">
                                @include('backend.structures.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $('.selectSearch').select2({
                width: '100%',
                placeholder: "Choisir...",
                allowClear: true
            });

        $(document).ready(function() {
        $('.dt_example1').DataTable( {
                dom: 'lBfrtip',
                responsive:true,
                processing: true,
                select: true,
                "language": {
                    "url": "{{ asset('lang/datatables.french.json')}}"
                },
                buttons: [
                //'copyHtml5',
                'excelHtml5',
                //'csvHtml5',
                'pdfHtml5'
                ],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false
                }],
                //select input search
            /* initComplete: function () {
                this.api().columns().every( function () {
                    var column = this.column(3);
                   
                    var select = $('<select class="form-control form-control-sm selectSearch" id="fil_categorie" name="fil_categorie" required><option value=""></option></select>')
                    .appendTo( $('#div_select').empty() ).select2({
                            width: '100%',
                            placeholder: "Choisir la catégorie...",
                            allowClear: true
                        })
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );
                } );
            } */
            });
        } );

        function openModalAddStructure(id) {
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAddStructure"),
                placeholder: "Choisir...",
                allowClear: true
            });
            if (id == null) {
                $('.modal-title').text('Ajout d\'une nouvelle structure');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#ins_id').val(null);
                }
            else {
                $.ajax({
                url: APP_URL+"/getStructure/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                $('.modal-title').html("Modification de la structure <span style='color:maroon;'>"+data.str_sigle+ "</span>");
                $('#btnSave').empty();
                $('#btnSave').text('Modifier');
                $('#str_id').val(data.str_id);
                $('#str_sigle').val(data.str_sigle);
                $('#str_nom_complet').val(data.str_nom_complet);
                $('#str_categorie').val(data.str_categorie).trigger('change');
                },
                error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                }
            });
            }
        }
        function deleteStructure(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement la structure : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                        url: "{{url('/deleteStructure')}}/" + id,
                        type: "get",
                        data: {str_id: id, '_token': '{{csrf_token()}}'},
                        success: function (data) {
                        if (data == 0) {
                            swal({
                            title: 'Attention',
                            text: "Impossible de supprimer cette structure, elle est rattachée à certaines données !",
                            type: 'info',
                            button: "OK"
                            });
                        } else {
                            swal({
                            title: "Succes!",
                            text: "Suppression de la structure effectuée avec succès!",
                            type: "success",
                            button: "OK"
                            }).then(response => {
                                location.reload();
                            });
                            
                        }
                        },
                        error: function (data) {
                            swal({
                            title: 'Erreur',
                            text: "Une erreure est survenue lors de la suppression de la structure!\nVeuillez reessayer!",
                            type: 'error',
                            button: "OK"
                            });
                        }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
    </script> 
@endsection

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddStructure" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> 

            <form class="needs-validation" novalidate method="POST" action="{{route('structures.store')}}" id="formAddStructure">
                    <div class="modal-body" style="overflow:hidden;">
                        <div class="row justify-content-center">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="str_id" name="str_id" type="hidden">

                            <div class="form-group col-md-4">
                                <label for="str_sigle">Sigle <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="str_sigle" name="str_sigle"
                                       maxlength="20" placeholder="Ex: SG" required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="str_nom_complet">Nom complet <span class="requit">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="str_nom_complet"
                                       name="str_nom_complet" maxlength="255" placeholder="Ex: Sécretariat Général"required/>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="str_categorie">Cat&eacute;gorie <span class="requit">*</span></label>
                                <select class="form-control form-control-sm selectSearch" id="str_categorie" name="str_categorie" required>
                                    <option value="">Selectionnez une catégorie</option>
                                    @foreach ($categories as $item =>$value)
                                    <option value="{{$item}}" @if (isset($categorie) && $item==$categorie) selected @endif>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i>&nbsp;Fermer</button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection