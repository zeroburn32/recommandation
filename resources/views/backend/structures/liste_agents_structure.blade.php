@extends('layouts.template')
@section('title')
Agents de suivi
@parent
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
    {{-- <style>    
        span.select2-container {
            z-index:10050;
        } 
    </style> --}}

@stop

@section('content')
<div class="container-fluid">
    <div class="card mb-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                <li class="breadcrumb-item"><a href="{{ url('structures') }}">Structures</a></li>
                <li class="breadcrumb-item active" aria-current="page">Gestion des utilisateurs</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Liste des utilisateurs de la structure :
                                <span id="lib_structure" style="color:maroon;">{{$structure->str_nom_complet }}</span></div>
                        </div>
                        <div class="widget-content-right">
                            <button type="button" class="btn btn-sm btn-warning" onclick="goBack()">
                                <i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp;&nbsp;Retour
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="pull-right mb-3">
                        {{-- <button type="button" class="btn btn-sm btn-primary" onclick="openModalAddAgent(null, {{$structure->str_id}})"
                            data-toggle="modal" data-target="#modalAddAgent">
                            <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouveau agent de suivi
                        </button> --}}
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-striped table-hover dt_example">
                            <thead class="thead-dark">
                                <th>#</th>
                                <th>Nom</th>
                                <th>Pr&eacute;nom(s)</th>
                                <th>Sexe</th>
                                <th>Fonction</th>
                                <th>Matricule</th>
                                <th>Téléphone</th>
                               {{--  <th>Structure</th> --}}
                                <th>Email</th>
                                <th>Profil</th>
                                <th>Statut</th>
                                {{-- <th class="no-sort text-center">Action</th> --}}
                            </thead>
                            <tbody id="contentAgents">
                                @foreach($users as $i=>$item)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $item->nom }}</td>
                                    <td>{{ $item->prenom }}</td>
                                    <td>{{ $item->sexe }}</td>
                                    <td>{{ $item->fonction }}</td>
                                    <td>{{ $item->matricule }}</td>
                                    <td>{{ $item->telephone }}</td>
                                    {{-- <td title="{{ $item->str_nom_complet }}">{{ $item->str_sigle }}</td> --}}
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->user_role }}</td>
                                    <td>
                                        @if ($item->is_blocked == true)
                                        <span class="badge badge-danger">Bloqué</span>
                                        @elseif ($item->is_active == false)
                                        <span class="badge badge-danger">Désactivé</span>
                                        @else
                                        <span class="badge badge-success">Actif</span>
                                        @endif
                                    </td>
                                    {{-- <td class="text-center">
                                        <div class='btn-group'>
                                            <button type="" class="btn btn-sm btn-info" title="D&eacute;tails"
                                                onclick="openModalDetailAgent({{$item->age_id}}, '{{$item->str_sigle}}' )" data-toggle="modal"
                                                data-target="#modalDetailAgent"> 
                                                <i class="fa fa-eye"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-primary" title="Modifier"
                                                onclick="openModalAddAgent({{$item->age_id}}, {{$item->str_id}})" data-toggle="modal"
                                                data-target="#modalAddAgent">
                                                <i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger"
                                                title="Supprimer ou retirer un agent"
                                                onclick="deleteAgent({{$item->age_id}}, '{{$item->nom_prenom}}' )">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
<script>
    $('.selectSearch').select2({
    placeholder: "Choisir...",
    allowClear: true
    });
        function openModalAddAgent(id, str_id) {
            console.log(APP_URL);
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAddAgent"),
                placeholder: "Choisir...",
                allowClear: true
            });
            if (id == null) {
                $('.modal-title').html("Ajout d'un nouveau agent de suivi de <span style='color:maroon;'>{{$structure->str_nom_complet }}</span>");
                $('#btnAddAgent').empty();
                $('#btnAddAgent').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                $('#id').val(null);
                $('#nom_prenom').val(null);
                $('#email').val(null);
                /* $("#structure").val(str_id).trigger('change').prop("disabled", null); */
                $('#matricule').val(null);
                $('#telephone').val(null);
            }
            else {
                $.ajax({
                    url: APP_URL+"/getAgent/"+id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $('.modal-title').html("Modification de l'agent de suivi <span style='color:maroon;'> " + data.nom_prenom+"</span> de <span style='color:maroon;'>{{$structure->str_nom_complet }}</span>");
                        $('#btnAddAgent').empty();
                        $('#btnAddAgent').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                        $('#age_id').val(data.age_id);
                        $('#nom_prenom').val(data.nom_prenom);
                        $('#email').val(data.email);
                        $('#matricule').val(data.matricule);
                        $('#telephone').val(data.telephone);
                    },
                    error: function () {
                    alert("Erreur rencontrer lors de la recuperation des donnees!");
                    }
                });
            }
        }

        function openModalDetailAgent(id, structure) {
            $.ajax({
                    url: APP_URL+"/getAgent/"+id,
                    type: "GET",
                    success: function (data) {
                        $('.modal-title').text("Détails du compte utilisateur " + data.nom_prenom);
                        $('#det_structure').text(structure);
                        $('#det_nom_prenom').text(data.nom_prenom);                        
                        $("#det_email").text(data.email);                        
                        $("#det_matricule").text(data.matricule);
                        $("#det_telephone").text(data.telephone);                        
                        $("#det_created_at").text(data.created_at);                        
                        $("#det_created_by").text(data.created_by);                        
                    },
                    error: function () {
                        swal({
                            title: 'Erreur',
                            text: "Une erreure est survenue lors de la récupération des données de l'instance!\nVeuillez reessayer!",
                            type: 'error',
                            button: "OK"
                        });
                    }
                });
        }

        function deleteAgent(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement l'agent " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: APP_URL+"/deleteAgent/" + id,
                            type: "GET",
                            data: {age_id: id, '_token': '{{csrf_token()}}'},
                            success: function (data) {
                                if (data == 0) {
                                    swal({
                                    title: 'Attention',
                                    text: "Impossible de supprimer ce agent, elle est rattachée à certaines données !",
                                    type: 'info',
                                    button: "OK"
                                });
                                } else {
                                    swal({
                                    title: "Succes!",
                                    text: "Suppression de l'agent effectuée avec succès!",
                                    type: "success",
                                    button: "OK"
                                    });
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                swal({
                                title: 'Erreur',
                                text: "Une erreure est survenue lors de la suppression de l'agent!\nVeuillez reessayer!",
                                type: 'error',
                                button: "OK"
                                });
                            }
                         });
                    });
                },
                allowOutsideClick: false
            });
        }

// V   Validate Email
    function surligne(champ, erreur)
        {
            if(erreur)
                 champ.style.backgroundColor = "#fba";
            else
                 champ.style.backgroundColor = "";
        }
        function verifMessage(champ)
        {
            if(champ.value.length < 2 || champ.value.length > 1000)
            {
                surligne(champ, true);
                return false;
            }
            else
            {
                 surligne(champ, false);
                 return true;
             }
        }

        function verifEMail(champ)
        {
            var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
            if(!regex.test(champ.value))
            {
                surligne(champ, true);
                 return false;
             }
            else
            {
                surligne(champ, false);
                return true;
            }
        }

        function verifObjet(champ)
        {
            if(champ.value.length < 3 || champ.value.length > 30)
            {
                surligne(champ, true);
                return false;
            }
            else
            {
                 surligne(champ, false);
                 return true;
             }
        }

        function verifPrenom(champ)
        {
            if(champ.value.length < 3 || champ.value.length > 50)
            {
                surligne(champ, true);
                return false;
            }
            else
            {
                 surligne(champ, false);
                 return true;
             }
        }

        function verifNom(champ)
        {
            if(champ.value.length < 3 || champ.value.length > 25)
            {
                surligne(champ, true);
                return false;
            }
            else
            {
                 surligne(champ, false);
                 return true;
             }
        }

        //verification du formulaire

        function verifForm(f)
        {
            var NomOk = verifNom(f.name);
            var PrenomOk = verifPrenom(f.prenom);
            var ObjetOk = verifObjet(f.objet);
            var EmailOk = verifEMail(f.email);
            var MessageOk = verifMessage(f.message);

            if(NomOk && PrenomOk && ObjetOk && EmailOk && MessageOk)
                return true;
            else
            {
                alert("Veuillez remplir correctement tous les champs");
                return false;
            }
        }


</script>
@endsection

@section('modal')
{{--MODAL AJOUT & EDIT Agent--}}
{{-- <div class="modal fade my-5" id="modalAddAgent" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" action="{{route('agents.store')}}"
                id="formAddInstance">
                <div class="modal-body">
                    <div class="row ">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="age_id" name="age_id" type="hidden">
                        <input id="structure" name="structure" type="hidden" value="{{$structure->str_id }}" >
                        <div class="form-group col-md-6">
                            <label for="nom_prenom">Nom & Pr&eacute;nom <span class="requit">*</span></label>
                            <input type="text" class="form-control form-control-sm" id="nom_prenom" name="nom_prenom"
                                maxlength="255" required />
                        </div>
                       
                        <div class="form-group col-md-6">
                            <label for="email">Email <span class="requit">*</span></label>
                            <input type="email" class="form-control form-control-sm" id="email" name="email" onblur="verifEMail(this)"
                                maxlength="255" required />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="matricule">Matricule </label>
                            <input type="text" class="form-control form-control-sm" id="matricule" name="matricule"
                                maxlength="50" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telephone">T&eacute;l&eacute;phones </label>
                            <input type="text" class="form-control form-control-sm" id="telephone" name="telephone"
                                maxlength="255" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i
                            class="fa fa-ban"></i>&nbsp;Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnAddAgent"></button>
                </div>
            </form>
        </div>
    </div>
</div>
 --}}
{{--MODAL DETAIL Agent--}}
<div class="modal fade my-5" id="modalDetailAgent" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalDetailLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="row justify-content-center">
                            <div class="col-md-11">
                                <div
                                    class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                                    <table class="table table-sm table-bordered table-striped" id="users">
                                        <tr>
                                            <th>Nom & Prénom</th>
                                            <td id="det_nom_prenom"><span id="nom_prenom"></span> </td>
                                        </tr>
                                        <tr>
                                            <th>Matricule</th>
                                            <td id="det_matricule"><span id="det_matricule"></span> </td>
                                        </tr>
                                        <tr>
                                            <th>Structure</th>
                                            <td id="det_structure"><span id="structure"></span></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td id="det_email"><span id="email"></span></td>
                                        </tr>
                                        <tr>
                                            <th>Téléphone(s)</th>
                                            <td id="det_telephone"><span id="telephone"></span></td>
                                        </tr>
                                        <tr>
                                            <th>Crée le </th>
                                            <td id="det_created_at"><span id="created_at"></span></td>
                                        </tr>
                                        <tr>
                                            <th>Crée par </th>
                                            <td id="det_created_by"><span id="created_by"></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i
                        class="fa fa-ban"></i>&nbsp;Fermer</button>
            </div>

        </div>
    </div>
</div>

@endsection