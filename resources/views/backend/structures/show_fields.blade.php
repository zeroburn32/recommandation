<!-- Str Sigle Field -->
<div class="form-group">
    {!! Form::label('str_sigle', 'Str Sigle:') !!}
    <p>{{ $structures->str_sigle }}</p>
</div>

<!-- Str Nom Complet Field -->
<div class="form-group">
    {!! Form::label('str_nom_complet', 'Str Nom Complet:') !!}
    <p>{{ $structures->str_nom_complet }}</p>
</div>

<!-- Str Categorie Field -->
<div class="form-group">
    {!! Form::label('str_categorie', 'Str Categorie:') !!}
    <p>{{ $structures->str_categorie }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $structures->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $structures->updated_by }}</p>
</div>

