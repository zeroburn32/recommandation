    <table class="table table-bordered table-sm table-striped table-hover dt_example1" id="structures-table">
        <thead class="thead-dark">
            <th>#</th>
            <th>Sigle</th>
            <th>Nom Complet</th>
            <th>Categorie</th>
            @can('GERER_STRUCTURE')
            <th class="text-center no-sort">Action</th>
            @endcan
        </thead>
        <tbody>
        @foreach($structures as $number=>$structures)
            <tr>
                <td>{{ $number + 1 }}</td>
                <td>{{ $structures->str_sigle }}</td>
                <td>{{ $structures->str_nom_complet }}</td>
                <td>{{ $structures->cat_libelle }}</td>
                @can('GERER_STRUCTURE')
                <td class="text-center">
                    <div class='btn-group'>
                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu">
                                <a tabindex="0" class="dropdown-item" onclick="openModalAddStructure({{$structures->str_id}})" data-toggle="modal"
                                    data-target="#modalAddStructure">
                                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                                </a>
                                <a tabindex="0" class="dropdown-item"
                                    onclick="deleteStructure({{$structures->str_id}}, '{{addslashes("Sigle Structure " . $structures->str_sigle)}}')">
                                    <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                </a>
                                <hr />
                                
                                <a type="button" class="dropdown-item" style="width: 100%;"
                                    onclick="window.location.href='getAgentsStructure/{{$structures->str_id}}'">
                                    <i class="fa fa-users"></i>&nbsp;&nbsp;Utilisateurs&nbsp;
                                    <span class="badge badge-pill badge-warning">{{ $structures->nb_agents }}</span>
                                </a>


                            {{-- <a type="button" class="dropdown-item" onclick="openModalDetail({{ $instance->ins_id }})" data-toggle="modal"
                                data-target="#modalDetail">
                                <i class="fa fa-eye"></i>&nbsp;&nbsp;D&eacute;tails
                            </a>
                            <a type="button" class="dropdown-item" onclick="openModalAddInstance({{ $instance->ins_id }})"
                                data-toggle="modal" data-target="#modalAddInstance">
                                <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                            </a>
                            <a type="button" class="dropdown-item"
                                onclick="deleteInstance({{ $instance->ins_id }}, '{{ $instance->ins_sigle }}')">
                                <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                            </a>
                            <hr />
                            <a type="button" class="dropdown-item" style="width: 100%;"
                                onclick="window.location.href='getPageActeursInstance/{{ $instance->ins_id }}'">
                                <i class="fa fa-users"></i>&nbsp;&nbsp;Acteurs&nbsp;
                                <span class="badge badge-pill badge-warning">
                                    {{ App\Http\Controllers\MethodesStaticController::nbreActeursByInstance($instance->ins_id) }}
                                </span>
                            </a> --}}
                        </div>
                    </div>
                    
                </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
    </table>
