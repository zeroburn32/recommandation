<!-- Tmsg Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tmsg_type', 'Tmsg Type:') !!}
    {!! Form::text('tmsg_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Tmsg Msg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tmsg_msg', 'Tmsg Msg:') !!}
    {!! Form::text('tmsg_msg', null, ['class' => 'form-control']) !!}
</div>

<!-- Tmsg Periodicite Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tmsg_periodicite', 'Tmsg Periodicite:') !!}
    {!! Form::text('tmsg_periodicite', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('templatemsgs.index') }}" class="btn btn-secondary">Cancel</a>
</div>
