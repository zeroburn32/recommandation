@extends('layouts.template')

@section('title')
    Mod&egrave;les de message
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card mb-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Paramètre</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gestion des mod&egrave;les de message</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Liste des mod&egrave;les de message</div>
                            </div>
                            <div class="widget-content-right">
                                <button type="button" class="btn btn-sm btn-primary"
                                        onclick="openModalAddModele(null)"
                                        data-toggle="modal" data-target="#modalAddModele">
                                    <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Nouvel mod&egrave;le
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="card-body">
                                @include('backend.templatemsgs.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>
    <script>
        function openModalAddModele(id) {
            $('#formAddMessage').attr('action', 'templatemsgs/save');
            if (id == null) {
                $('.modal-title').text('Ajout d\'un nouveau modèle');
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                /*$('#btnSave').on('click',function () {
                 var type=$('#tmsg_type').val();
                 var tsmg=$('#tmsg_msg').val();
                 var periode=$('#tmsg_periodicite').val();
                 var data={
                 tmsg_type:type,
                 tmsg_msg:tsmg,
                 tmsg_periodicite:periode
                 }
                 $.ajax({
                 headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },

                 type:'post',
                 url:'templatemsgs/save',
                 data:data,
                 cache:false,
                 success:function (data) {
                 // window.location.href='templatemsgs/index';

                 }

                 });

                 })*/


            }
            else {
                $('#formAddMessage').attr('action', 'templatemsgs/updating/' + id);
                $.ajax({
                    url: "{{ url("/getTemplate") }}/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $('.modal-title').text('Modification du modèle');
                        //$('#btnSave').empty();
                        $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                        $('#tmsg_type').val(data.tmsg_type);
                        $('#tmsg_msg').val(data.tmsg_msg);
                        $('#tmsg_periodicite').val(data.periodicite);

                        /*},
                         error: function () {
                         alert("Erreur rencontrer lors de la recuperation des donnees!");
                         }
                         });*/
                    }
                });
            }
        }

        function deleteModele(id, libelle) {
            swal({
                title: 'CONFIRMATION',
                text: "Etes vous sûr de vouloir supprimer définitivement le modèle : " + libelle + " ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Oui, supprimer!',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: "{{ url("/templatemsgs/delete") }}/" + id,
                            type: "get",
                            data: {ins_id: id, '_token': '{{csrf_token()}}'},
                            dataType: "JSON",
                            success: function (data) {
                                if (data.message == 'error') {
                                    swal({
                                        title: 'Attention',
                                        text: "Impossible de supprimer ce modèle, il est rattaché à certaines données !",
                                        type: 'info',
                                        button: "OK"
                                    });
                                } else {
                                    swal({
                                        title: "Succes!",
                                        text: "Suppression du modèle effectué avec succès!",
                                        type: "success",
                                        button: "OK"
                                    });
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                swal({
                                    title: 'Erreur',
                                    text: "Une erreure est survenue lors de la suppression du modèle!\nVeuillez reessayer!",
                                    type: 'error',
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
        function openModalDetailTemplate(id) {
            $.ajax({
                url: "{{url('/getTemplate/')}}/" + id,
                type: "get",
                success: function (data) {

                    $('.modal-title').text("Détails du model : " + data.tmsg_type);
                    $('#type').text(data.tmsg_type);
                    $('#periodicite').text(data.periodicite);
                    $('#contenu').text(data.tmsg_msg);


                },
                error: function (data) {
                    swal({
                        title: 'Erreur',
                        text: "Une erreure est survenue lors de la récupération des données du modele!\nVeuillez reessayer!",
                        type: 'error',
                        button: "OK"
                    });
                }
            });
        }


    </script>
@endsection

@section('modal')

@section('modal')
    {{--MODAL AJOUT & EDIT structure--}}
    <div class="modal fade my-5" id="modalAddModele" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="needs-validation" novalidate method="POST" action="#" id="formAddMessage">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <input type="hidden" name="_token" id='token' value="{{ csrf_token() }}">
                            <input id="tmsg_id" name="tmsg_id" type="hidden">


                            <div class="row form-group col-md-12 justify-content-center">
                                <div class="form-group col-md-6">

                                    <label for="tmsg_type">Modèle <span class="requit">*</span></label>
                                    <select class="form-control form-control-sm" id="tmsg_type" name="tmsg_type"
                                            required>
                                        @foreach($type_msg as $type_msg)
                                            <option value="{{$type_msg}}">{{$type_msg}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group col-md-6">

                                    <label for="tmsg_periodicite">P&eacute;riodicit&eacute; <span
                                                class="requit">*</span></label>
                                    <select class="form-control form-control-sm" id="tmsg_periodicite"
                                            name="tmsg_periodicite" required>
                                        @foreach($periodicite_msg as $periodicite_msg)
                                            <option value="{{$periodicite_msg}}">{{$periodicite_msg}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="tmsg_msg">Contenu du message<span class="requit">*</span></label>
                                    <textarea class="form-control" name="tmsg_msg" id="tmsg_msg" rows="4"
                                              required></textarea>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="bud_id">Choisir des paramètres</label>
                                    <select size="6" class="form-control form-control-sm" id="params" name="bud_id">


                                        <option value="[DESCRITION_SESSION]">Description de la session</option>
                                        <option value="[ANNEE_SESSION]">Année de la session</option>
                                        <option value="[DATE_DEBUT_SESSION]">Date debut de la session</option>
                                        <option value="[DATE_FIN_SESSION]">Date fin de la session</option>
                                        <option value="[LIEU_SESSION]">Lieu de la session</option>
                                        <option value="[DATE_FIN_PLANIFICATION]">Date fin de planification de la
                                            session
                                        </option>
                                        <option value="[DATE_FIN_SUIVI]">Date fin de suivi de la session</option>

                                        <option value="[STRUCTURE_SIGLE]">Sigle de la structure</option>
                                        <option value="[STRUCTURE_NOM_COMPLET]">Nom complet de la structure</option>

                                        <option value="[USER_NOM]">Nom destinataire</option>
                                        <option value="[USER_PRENOM]">Prénom destinataire</option>
                                        <option value="[USER_EMAIL]">Email destinataire</option>
                                        <option value="[USER_SEXE]">Sexe destinataire</option>
                                        <option value="[USER_FONCTION]">Fonction destinataire</option>
                                        <option value="[USER_MATRICULE]">Matricule destinataire</option>
                                        <option value="[LISTE_RECO]">Liste des recommandations</option>


                                    </select>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i
                                    class="fa fa-ban"></i>&nbsp;Fermer
                        </button>
                        <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Modele detail-->
    <div class="modal fade my-5" id="modalDetailTemplate" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table table-bordered table-sm table-striped table-hover">
                        <tbody id="contentActeurs">
                        <tr>
                            <th>type du message</th>
                            <td id="type"></td>


                        </tr>
                        <tr>
                            <th>Periodicite</th>
                            <td id="periodicite"></td>

                        </tr>

                        <tr>
                            <th>Contenu du message</th>
                            <td id="contenu"></td>

                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="cursor: pointer;">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger">Fermer</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            /* $('#paramsr').empty();
             $.get('/params/structur/', function (data) {
             console.log(data);
             $('#params').append('<option value="">Veuillez choisir</option>');
             $.each(data, function (index, d) {

             $('#params').append('<option value="' + d.str_sigle + '">' + d.str_sigle + '</option>');

             })
             })*/
            $('#params').on('click', function () {
                if ($('#tmsg_type').val() != 'AUTRE' || ($('#tmsg_type').val() == 'AUTRE' && $(this).val() != '[LISTE_RECO]')) {
                    //$('#tmsg_msg').blur();
                    $('#tmsg_msg').val($('#tmsg_msg').val() + " " + $(this).val())
                    $('#tmsg_msg').focus();
                }

            });

        })


    </script>
@endsection

