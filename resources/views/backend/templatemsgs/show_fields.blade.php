<!-- Tmsg Type Field -->
<div class="form-group">
    {!! Form::label('tmsg_type', 'Tmsg Type:') !!}
    <p>{{ $templatemsgs->tmsg_type }}</p>
</div>

<!-- Tmsg Msg Field -->
<div class="form-group">
    {!! Form::label('tmsg_msg', 'Tmsg Msg:') !!}
    <p>{{ $templatemsgs->tmsg_msg }}</p>
</div>

<!-- Tmsg Periodicite Field -->
<div class="form-group">
    {!! Form::label('tmsg_periodicite', 'Tmsg Periodicite:') !!}
    <p>{{ $templatemsgs->tmsg_periodicite }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $templatemsgs->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $templatemsgs->updated_by }}</p>
</div>

