<div class="table-responsive-sm">
    <table class="table table-striped table-sm dt_example" id="templatemsgs-table">
        <thead class="thead-dark">
        <th>#</th>
        <th>Type</th>
        <th>Contenu</th>
        <th>Periodicite</th>
        <th class="no-sort">Action</th>

        </thead>
        <tbody>
        @foreach($templatemsgs as $number=>$templatemsg)
            <tr>
                <td>{{ $number + 1 }}</td>
                <td>{{ $templatemsg->tmsg_type }}</td>
                <td>{{ $templatemsg->tmsg_msg }}</td>
                <td>{{ $templatemsg->tmsg_periodicite }}</td>

                <td class="text-center no-sort">
                    <div class='btn-group'>
                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>

                        <div class="dropdown-menu">
                        <!--
                        <button class='dropdown-item'
                                data-toggle="modal" data-target="#modalDetailTemplate"  data-placement="top" title="Voir détails du modèle" onclick="openModalDetailTemplate({{ $templatemsg->tmsg_id}})">
                            <i class="fa fa-eye"></i>&nbsp;&nbsp;Voir détails
                        </button> -->
                            <button class='dropdown-item' onclick="openModalAddModele({{$templatemsg->tmsg_id}})"
                                    data-placement="top" title="modifier le modèle" data-toggle="modal"
                                    data-target="#modalAddModele">
                                <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier
                            </button>
                            @if(\App\Http\Controllers\MethodesStaticController::templateHasAlerte($templatemsg->tmsg_id))

                                <button class='dropdown-item'
                                        onclick="deleteModele({{$templatemsg->tmsg_id}},'{{$templatemsg->tmsg_type}}')"
                                        data-toggle="tooltip" data-placement="top" title="Supprimer le modèle">
                                    <i class="fa fa-trash"></i>&nbsp;&nbsp;Supprimer
                                </button>
                            @endif
                        </div>
                    </div>
                <!--

                    <div class='btn-group'>

                        <a href="{{ route('detailTemplate', [$templatemsg->tmsg_id]) }}" class='btn btn-success mr-2'><i class="fa fa-eye"></i></a>

                        <button type="button" class="btn btn-sm btn-primary mr-2"
                                onclick="openModalAddModele({{$templatemsg->tmsg_id}})"
                                data-toggle="modal" data-target="#modalAddModele">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-danger" onclick="deleteModele({{$templatemsg->tmsg_id}},'{{$templatemsg->tmsg_msg}}')"><i class="fa fa-trash"></i> </button>

                    </div>
                    -->

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>