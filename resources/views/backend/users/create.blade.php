@extends('layouts.template')

@section('title')
    Utilisateurs
    @parent
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{asset('backend/css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/bootstrap-datetimepicker.min.css') }}" />
@stop

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
                <li class="breadcrumb-item"><a href="{!! route('users.index') !!}"><i class="cib-co-op"></i>Gestion des
                        utilisateurs</a></li>
                <li class="breadcrumb-item active" aria-current="page">Ajout d'utilisateur</li>
            </ol>
        </nav>
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">Ajouter un nouvel utilisateur</div>
                            </div>
                            <div class="widget-content-right">
                                <button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                    <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['route' => 'users.store']) !!}
                        @include('backend.users.fields', ['formMode' => 'create'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/select2.min.js') }}" type="text/javascript"></script>
<script>
    $('select').select2({
            placeholder: "Choisir...",
            allowClear: true
    });
</script>
@endsection