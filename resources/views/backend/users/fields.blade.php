<div class="row">
    <div class="col-2">
        {!! Form::label('nom_prenom', 'Nom et Prénom(s): ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('nom_prenom', null, ['class' => 'form-control form-control-sm', 'required' => 'required']) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2">{!! Form::label('matricule', 'Matricule: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('matricule', null, ['class' => 'form-control form-control-sm']) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2">
        {!! Form::label('structure', 'Structure: ', ['class' => 'control-label col-form-label  pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::select('structure', $structures, null, ['class' => 'form-control form-control-sm', 'required' => 'required']) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2">
        {!! Form::label('telephone', 'Téléphone: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('telephone', null, ['class' => 'form-control form-control-sm', 'required' => 'required']) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2">
        {!! Form::label('name', 'Nom d\'utilisateur:' , ['class' => 'control-label col-form-label
        pull-right']) !!}</div>
    <div class="form-group  col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ? 'readonly':'']) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2">
        {!! Form::label('email', 'Email: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group  col-sm-8">
        {!! Form::email('email', null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ? 'readonly':'']) !!}
    </div>
    <div class="col-2"></div>
    <!-- Password Field -->
    {{-- <div class="col-2">
        {!! Form::label('password', 'Mot de passe: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::password('password', ['class' => 'form-control form-control-sm', $user->id ? 'readonly':'']) !!}
    </div>
    <div class="col-2"></div> --}}
    <div class="col-2">
        {!! Form::label('role', 'Profil: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">

        {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control form-control-sm selectSearch',
        'multiple' => false]) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2"></div>
    
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-secondary">Annuler</a>
</div>
