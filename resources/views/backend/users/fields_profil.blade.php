<div class="row">
    {{-- <div class="col-2">
        {!! Form::label('nom_prenom', 'Nom et Prénom(s): ', ['class' => 'control-label col-form-label
        pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">

        {!! Form::text('nom_prenom', null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ? 'readonly':'']) !!}
        {{-- {!! $errors->first('nom_prenom', '<p class="help-block">:message</p>') !!} --}}
    {{-- </div>
    <div class="col-2"></div>
    <div class="col-2">{!! Form::label('matricule', 'Matricule: ', ['class' => 'control-label col-form-label
        pull-right']) !!}</div>
    <div class="form-group col-sm-8">

        {!! Form::text('matricule', null, ['class' => 'form-control form-control-sm', $user->id ? 'readonly':'']) !!}
        {{-- {!! $errors->first('matricule', '<p class="help-block">:message</p>') !!} --}}
    {{-- </div>
    <div class="col-2"></div>  --}}
    <div class="col-2">
        {!! Form::label('name', 'Nom d\'utilisateur:' , ['class' => 'control-label col-form-label
        pull-right']) !!}</div>
    <div class="form-group  col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ?
        'readonly':'']) !!}
        {{-- {!! $errors->first('name', '<p class="help-block">:message</p>') !!} --}}
    </div>
    <div class="col-2"></div>
    <div class="col-2">
        {!! Form::label('email', 'Email: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group  col-sm-8">
    
        {!! Form::email('email', null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ?
        'readonly':'']) !!}
        {{-- {!! $errors->first('email', '<p class="help-block">:message</p>') !!} --}}
    </div>
    <div class="col-2"></div>
    <div class="col-2">
        {!! Form::label('structure', 'Selectionnez la Structure: ', ['class' => 'control-label col-form-label
        pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::select('structure', $structures, null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ? 'readonly':'']) !!}
    </div>
    <div class="col-2"></div>
    {{-- <div class="col-2">
        {!! Form::label('telephone', 'Téléphone: ', ['class' => 'control-label col-form-label
        pull-right']) !!}</div>
    <div class="form-group col-sm-8">
        {!! Form::text('telephone', null, ['class' => 'form-control form-control-sm', 'required' => 'required', $user->id ? 'readonly':'']) !!}
    </div>
    <div class="col-2"></div> --}}
    
    <!-- Password Field -->
    {{-- <div class="col-2">
        {!! Form::label('password', 'Mot de passe: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::password('password', ['class' => 'form-control form-control-sm', $user->id ? 'readonly':'']) !!}
    </div>
    <div class="col-2"></div> --}}
    <div class="col-2">
        {!! Form::label('role', 'Selectionnez le Profil: ', ['class' => 'control-label col-form-label pull-right']) !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control form-control-sm',
        'multiple' => false]) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2"></div>
    
</div>

<!-- Submit Field -->
<div class="form-group col-sm-8 offset-sm-2">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-secondary">Annuler</a>
</div>
