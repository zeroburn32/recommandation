@extends('layouts.template')
@section('title')
Utilisateurs
@parent
@stop
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
@stop
@section('content')
<div class="container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
            <li class="breadcrumb-item active" aria-current="page">Gestion des utilisateurs</li>
        </ol>
    </nav>
    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Liste des comptes utilisateurs</div>
                        </div>
                        <div class="widget-content-right">
                            {{-- <a href="{{ route('users.create') }}" class="btn btn-sm btn-primary">
                            <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Ajouter
                            </a> --}}
                            @can('GERER_COMPTE')
                            <button type="button" class="btn btn-sm btn-primary" onclick="openModalAdd(null)"
                                data-toggle="modal" data-target="#modalAdd">
                                <i class="fa fa-plus-square fa-lg"></i>&nbsp;&nbsp;Créer un compte
                            </button>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate method="GET" action="{{ route('usersSearch')}}"
                        id="formFilter">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="card-title col-md-1">Filtre</div>
                            <div class="form-group col-md-3" id="div_select">
                                <select class="form-control form-control-sm " id="fil_structure1" name="fil_structure1">
                                    <option value="">Structure ...</option>
                                    @foreach ($structures as $item =>$value)
                                    <option value="{{$item}}" @if (isset($structure) && $item==$structure) selected
                                        @endif>
                                        {{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="div_select">
                                <select class="form-control form-control-sm " id="fil_profil1" name="fil_profil1">
                                    <option value="">Profil ...</option>
                                    @foreach ($roles as $item =>$value)
                                    <option value="{{$item}}" @if (isset($role) && $item==$role) selected @endif>
                                        {{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2" id="div_select">
                                <select class="form-control form-control-sm " id="fil_statut1" name="fil_statut1">
                                    <option value="">Statut...</option>
                                    <option value="1" @if (isset($statut) && $statut==1) selected @endif>Actif</option>
                                    <option value="0" @if (isset($statut) && $statut==0) selected @endif>Inactif
                                    </option>
                                </select>
                            </div>
                            <div class="from-group col-lg-2 col-xl-2">
                                <button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
                                    <i class="fa fa-search"></i>&nbsp;Rechercher
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-lg-12 col-xl-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive" style="min-height: 45vh;">
                                @include('backend.users.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
{{--MODAL AJOUT & EDIT structure--}}
<div class="modal fade my-5" id="modalAdd" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate method="POST" action="{{route('users.store')}}" id="formAdd">
                <div class="modal-body" style="overflow:hidden;">
                    <div class="row justify-content-center">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="id" name="id" type="hidden">
                        {{-- <input id="age_id" name="age_id" type="hidden"> --}}
                        {{-- <input id="str_id" name="str_id" type="hidden"> --}}

                        <div class="form-group col-md-4">
                            <label for="nom">Nom <span class="requit">*</span></label>
                            {!! Form::text('nom', null, [ 'id' =>'nom','class' => 'form-control form-control-sm',
                            'required' => 'required','placeholder' => 'Nom']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="prenom">Prénom(s) <span class="requit">*</span></label>
                            {!! Form::text('prenom', null, [ 'id' =>'prenom','class' => 'form-control form-control-sm',
                            'required' =>'required','placeholder' => 'Prénom']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="sexe">Sexe <span class="requit">*</span></label>
                            <select class="form-control form-control-sm selectSearch" id="sexe" name="sexe" required>
                                <option value="">Sexe ...</option>
                                <option value="M">Masculin</option>
                                <option value="F">Féminin</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="fonction">Fonction <span class="requit">*</span></label>
                            {!! Form::text('fonction', null, [ 'id' =>'fonction','class' => 'form-control
                            form-control-sm', 'required' =>'required','placeholder' => 'Ex: Directeur']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="matricule">Numéro matricule</label>
                            {!! Form::text('matricule', null, ['id' =>'matricule','class' => 'form-control
                            form-control-sm']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="telephone">Téléphone </label>
                            {!! Form::text('telephone', null, ['id' =>'telephone','class' => 'form-control
                            form-control-sm','placeholder' => 'Ex: +22670001213']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="str_id">Structure <span class="requit">*</span></label>
                            {!! Form::select('str_id', $structures, null, ['id' =>'str_id','class' => 'form-control
                            form-control-sm selectSearch' , 'required' => 'required']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">E-mail <span class="requit">*</span></label>
                            {!! Form::email('email', null, ['id' =>'email','class' => 'form-control form-control-sm',
                            'required' => 'required','placeholder' => 'votremail@gmail.com']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="roles">Profil <span class="requit">*</span></label>
                            {!! Form::select('roles[]', $roles, null, ['id' =>'roles','class' => 'form-control
                            form-control-sm selectSearch','multiple' => false]) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i
                            class="fa fa-ban"></i>&nbsp;Fermer</button>
                    <button type='submit' class="btn btn-sm btn-success" id="btnSave"></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
<script>
    $('#fil_structure1').select2({
            placeholder: "Choisir la structure...",
            allowClear: true
        });
        $('#fil_profil1').select2({
            placeholder: "Choisir le profil...",
            allowClear: true
        });
        $('#fil_statut1').select2({
            placeholder: "Choisir le statut...",
            allowClear: true
        });

        function loadAgents(id) {
            toAppend = '';
            $.get(APP_URL+'/getAgentsByStructure/' + id, function (data) {
                $.each(data, function (i, o) {
                    toAppend += '<option value=' + o.age_id + '>' + o.nom_prenom + '</option>';
                });
                var x = document.getElementById("nom_prenom");
                while (x.options.length) {
                    x.remove(0);
                }
                $('#nom_prenom').append(toAppend).trigger('change');
                //$("#nom_prenom").select2("val","").trigger('change');
            });
        }
        function loadAgent(id) {
            $.get(APP_URL+'/getUser/' + id, function (data) {
                $('#id').val(data.id);
                $('#nom').val(data.nom);
                $('#prenom').val(data.prenom);
                $('#sexe').val(data.sexe);
                $('#fonction').val(data.fonction);
                $('#email').val(data.email);
                $('#matricule').val(data.matricule);
                $('#telephone').val(data.telephone);
                $('#str_id').val(data.str_id).trigger('change');
                $('#roles').val(data.roles[0].name).trigger('change');              
            });
        }

        function openModalAdd(id) {
            $('.selectSearch').select2({
                width: '100%',
                dropdownParent: $("#modalAdd"),
                placeholder: "Choisir...",
                allowClear: true
            });
            if (id == null) {
                $('.modal-title').text("Ajout d'un nouveau compte utilisateur");
                $('#btnSave').empty();
                $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Enregistrer');
                 $('#id').val(null);
                $('#div_npe').addClass('d-none');
                $('#div_np').removeClass('d-none');
                $('#id').val(null);
                $('#nom').val(null);
                $('#prenom').val(null);
                $('#sexe').val(null).trigger('change');
                $('#fonction').val(null);
                $('#email').val(null);
                $("#str_id").val(null).trigger('change');
                $('#matricule').val(null);
                $('#telephone').val(null);
                $('#roles').val(null).trigger('change');
            }
            else {
                var url = '{{ route("users.edit", ":id") }}';
                url = url.replace(':id',id);
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $('.modal-title').html("Modification du compte utilisateur <span style='color:maroon;'> " + data.nom+" "+data.prenom +"</span>");
                        $('#btnSave').empty();
                        $('#btnSave').html('<i class="fa fa-save"></i>&nbsp;Modifier');
                        $('#id').val(data.id);
                        $('#nom').val(data.nom);
                        $('#prenom').val(data.prenom);
                        $('#sexe').val(data.sexe).trigger('change');
                        $('#fonction').val(data.fonction);
                        $('#email').val(data.email);
                        $('#matricule').val(data.matricule);
                        $('#telephone').val(data.telephone);
                        $('#str_id').val(data.str_id).trigger('change');
                        $('#roles').val(data.roles[0].name).trigger('change');
                    },
                    error: function () {
                        alert("Erreur rencontrer lors de la recuperation des donnees!");
                    }
                });
            }
        }

        function supprimerCompte(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir supprimer le compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Supprimer',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        var url = '{{ route("users.destroy", ":id") }}';
                        url = url.replace(':id',id);
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (response) {
                                swal({
                                    title: "Suppression !",
                                    text: "Compte supprimé avec succès !",
                                    type: "success",
                                    button: "OK"
                                }).then(response => {
                                        window.location.href = "users";
                                    });
                                
                            },
                            error: function () {
                                swal({
                                    title: "Suppression !",
                                    text: "Une erreur est survenue lors de la suppression !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
        function desactiver(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir désactivé le compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Désactiver',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/lockAndUnlockAccount')}}/" + id,
                            type: 'PATCH',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (response) {
                                swal({
                                    title: "Désactivation !",
                                    text: "Compte désactiver avec succès !",
                                    type: "success",
                                    button: "OK"
                                }).then(response => {
                                        window.location.href = "users";
                                    });
                            },
                            error: function () {
                                swal({
                                    title: "Désactivation !",
                                    text: "Une erreur est survenue lors de la désactivation !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function activer(id, login) {
            swal({
                title: 'Êtes-vous sûre?',
                text: "De vouloir activé le compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Activer',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/lockAndUnlockAccount')}}/" + id,
                            type: 'PATCH',
                            data: {id: id, '_token': '{{csrf_token()}}'},
                            success: function (response) {
                                swal({
                                    title: "Activation !",
                                    text: "Compte activer avec succès !",
                                    type: "success",
                                    button: "OK"
                                }).then(response => {
                                        window.location.href = "users";
                                    });
                            },
                            error: function () {
                                swal({
                                    title: "Activation !",
                                    text: "Une erreur est survenue lors de l'activation !",
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }

        function reinitialiser(id, login) {
            swal({
                title: 'Êtes-vous sûre ?',
                text: "De vouloir réinitialiser le mot de passe du compte de " + login + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui, Réinitialiser',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non, Annuler',
                cancelButtonColor: '#d33',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "{{url('/reinitialiserMotDePasse')}}/" + id,
                            type: 'GET',
                            success: function (data) {
                                swal({
                                    title: "Opération reussie !",
                                    text: "Votre nouveau mot de passe est : " + data,
                                    type: "success",
                                    button: "OK"
                                });
                            },
                            error: function (data) {
                                swal({
                                    title: "Réinitialisation !",
                                    text: "Une erreur est survenue lors de l'envoi du mot de passe par mail. Votre nouveau mot de passe est : " + data,
                                    type: "error",
                                    button: "OK"
                                });
                            }
                        });
                    });
                },
                allowOutsideClick: false
            });
        }
//  Creation de compte avec ajax
$(document).ready(function() {
$("#formAdd").submit(function (event) {
    event.preventDefault();
    var frm = $('#formAdd'); 
    
    var nom = $('#nom').val() ;
    var prenom =  $('#prenom').val();
    var sexe =  $('#sexe').val();
    var fonction =  $('#fonction').val();
    var structure =  $('#str_id').val();
    var email = $('#email').val() ;

    if(nom && prenom && sexe && fonction && structure && email) {
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
          if(data.statutCode == 0) {
            swal({
                    title: "Compte crée !",
                    text: "Le mot de passe de "+data.user.prenom+" "+data.user.nom+"  est : " + data.password,
                    type: "success",
                    button: "OK"
                }).then((result) => {
                    if (result.value) {
                       goBackReload();
                    }
                });
                //
          } else if (data.statutCode == 1) {
              swal({
                    title: "Succès",
                    text: "Le compte de "+data.user.prenom+" "+data.user.nom+" a été mis à jour avec succès !",
                    type: "success",
                    button: "OK !",
                }).then((result) => {
                    if (result.value) {
                       goBackReload();
                    }
                });
                //goBackReload();
          }else if (data.statutCode == 2) {
              swal({
                title: "Erreur",
                text: "Ce compte existe déjà !",
                type: "error",
                button: "OK !",
            });

          }else if (data.statutCode ==3) {
            swal({
                title: "Erreur",
                text: "Une ereeur est survenue. Veuillez réessayer !",
                type: "error",
                button: "OK !",
            });
          }
        },
        error: function (data) {
            console.log(data);
                swal({
                    title: "Création de compte !",
                    text: "Une erreur est survenue lors de la création du compte. Veuillez réessayer !",
                    type: "error",
                    button: "OK"
                });
            }
    });
    } else {
        swal({
                    title: "Création de compte !",
                    text: "Veuillez renseignez les champs requis !",
                    type: "error",
                    button: "OK"
                });
    }

});
});

</script>
@endsection