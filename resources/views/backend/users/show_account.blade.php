@extends('layouts.template')

@section('title')
    Utilisateurs
    @parent
@stop

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="cib-co-op"></i>Administration</a></li>
                <li class="breadcrumb-item"><a href="{!! route('users.index') !!}"><i class="cib-co-op"></i>Gestion des
                        utilisateurs</a></li>
                <li class="breadcrumb-item active" aria-current="page">D&eacute;tails</li>
            </ol>
        </nav>

        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header widget-content">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="card-title">D&eacute;tails du compte de &nbsp;<strong> {{ $users->nom }} {{ $users->prenom }} </strong></div>
                            </div>
                            <div class="widget-content-right">
                                <button onclick="goBack()" class="float-right btn btn-sm btn-warning">
                                    <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Retour
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="content">
                            <div class="portlet box default" style="margin-top: 10px">
                                <div class="portlet-body flip-scroll">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-sm table-bordered table-striped" id="users">
                                                
                                                    <tr>
                                                        <th>Nom & Prénom(s)</th>
                                                        <td>{{ $users->nom}} {{ $users->prenom }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sexe</th>
                                                        <td>{{ $users->sexe}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fonction</th>
                                                        <td>{{ $users->fonction }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Matricule</th>
                                                        <td>{{ $users->matricule }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Téléphone(s)</th>
                                                        <td>{{ $users->telephone }}</td>
                                                    </tr>
                                                    <tr title="{{ $users->str_nom_complet }}">
                                                        <th>Structure</th>
                                                        <td>{{ $users->str_sigle }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>{{ $users->email }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Profil</th>
                                                        <td>{{ $users->user_role }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Statut</th>
                                                        <td>
                                                            @if ($users->is_active == false)
                                                            <span class="badge badge-danger">Désactivé</span>
                                                            @else <span class="badge badge-success">Activé</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Dernière connexion il y a</th>
                                                        <td>
                                                            {{ \Carbon\Carbon::parse($users->lastconnect)->diffForHumans() }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Compte crée il y a</th>
                                                        <td>
                                                            {{ \Carbon\Carbon::parse($users->created_at)->diffForHumans() }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </br>
                                        </br>
                                            {{-- <div class="flip-scroll table-responsive">
                                                <h5>Liste des droits de l'utilisateur</h5>
                                                <table class="table table-sm table-bordered table-striped table-condensed flip-content example">
                                                    <thead>
                                                        <tr>
                                                            <th>Droit</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($permissions as $item)
                                                        <tr>
                                                            <td>{{ $item->name }}</td>
                                                            <td>{{ $item->label }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div> --}}
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
