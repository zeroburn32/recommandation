<div class="table-responsive-sm ">
    <table class="table table-striped table-sm dt_example" id="users-table">
        <thead class="thead-dark">
            <th>#</th>
            <th>Nom</th>
            <th>Pr&eacute;nom(s)</th>
            <th>Sexe</th>
            <th>Fonction</th>
            <th>Matricule</th>
            <th>Téléphone</th>
            <th>Structure</th>
            <th>Email</th>
            <th>Profil</th>
            <th>Statut</th>
            @can('GERER_COMPTE')
            <th class="no-sort text-center">Actions</th>
            @endcan
        </thead>
        <tbody>
            @foreach($users as $i=>$item)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ $item->nom }}</td>
                <td>{{ $item->prenom }}</td>
                <td>{{ $item->sexe }}</td>
                <td>{{ $item->fonction }}</td>
                <td>{{ $item->matricule }}</td>
                <td>{{ $item->telephone }}</td>
                <td title="{{ $item->str_nom_complet }}">{{ $item->str_sigle }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->user_role }}</td>
                <td>
                    @if ($item->is_blocked == true)
                    <span class="badge badge-danger">Bloqué</span>
                    @elseif ($item->is_active == false)
                    <span class="badge badge-danger">Désactivé</span>
                    @else
                    <span class="badge badge-success">Actif</span>
                    @endif
                </td>
                @can('GERER_COMPTE')
                <td class="text-center">
                    <div class='btn-group'>
                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu">
                            <a href="{!! route('users.show', [$item->id]) !!}" class='dropdown-item'
                                data-toggle="tooltip" data-placement="top" title="Voir détail">
                                <i class="fa fa-eye"></i>&nbsp;&nbsp;Voir détails
                            </a>
                            {{--Comptes utilisateur actif --}}
                            @if ($item->is_active == true)
                            {{-- si le compte appartient a l'utilisateur connecter--}}
                            @if (Auth::user()->id == $item->id)
                            <span data-toggle="popover" data-placement="top"
                                data-content="Vous ne pouvez pas changer votre profil en étant connecté !">
                                <button class='dropdown-item' disabled style="pointer-events: none;"
                                    onclick="openModalAdd({{$item->id}})" data-toggle="modal" data-target="#modalAdd">
                                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier le compte
                                </button>
                            </span>
                            <span data-toggle="popover" data-placement="top"
                                data-content="Vous ne pouvez pas désactiver votre compte en étant connecté !">
                                <button type="submit" class="dropdown-item" disabled style="pointer-events: none;">
                                    <i class="fa fa-lock"></i>&nbsp;&nbsp;Désactiver
                                </button>
                            </span>
                            <span data-toggle="popover" data-placement="top"
                                data-content="Vous ne pouvez pas réinitialiser votre compte en étant connecté !">
                                <button type="submit" class="dropdown-item" disabled style="pointer-events: none;">
                                    <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp;Réinitialiser
                                </button>

                            </span>
                            <span data-toggle="popover" data-placement="top"
                                data-content="Vous ne pouvez pas supprimer votre compte en étant connecté !">
                                {{-- <button onclick="supprimerCompte({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                    data-toggle="tooltip" data-placement="top" disabled style="pointer-events: none;"
                                    title="Supprimer le compte utilisateur">
                                    <i class="fa fa-trash" aria-hidden="true"></i></i>&nbsp;&nbsp;Supprimer le compte
                                </button> --}}
                            </span>
                            @else
                            <button class='dropdown-item' onclick="openModalAdd({{$item->id}})" data-toggle="modal"
                                data-target="#modalAdd" title="Changer de profil">
                                <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier le compte
                            </button>
                            <button onclick="desactiver({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                data-toggle="tooltip" data-placement="top" title="Désactiver le compte">
                                <i class="fa fa-lock"></i>&nbsp;&nbsp;Désactiver
                            </button>
                            <button onclick="reinitialiser({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                data-toggle="tooltip" data-placement="top" title="Réinitialiser le mot de passe">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp;Réinitialiser
                            </button>
                            {{-- <button onclick="supprimerCompte({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                data-toggle="tooltip" data-placement="top" title="Supprimer le compte utilisateur">
                                <i class="fa fa-trash" aria-hidden="true"></i></i>&nbsp;&nbsp;Supprimer le compte
                            </button> --}}
                            @endif

                            {{--Comptes utilisateur inactif --}}
                            @elseif($item->is_active == false)
                            <button class='dropdown-item' onclick="openModalAdd({{$item->id}})" data-toggle="modal"
                                data-target="#modalAdd" title="Changer de profil">
                                <i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier le compte
                            </button>
                            <button onclick="activer({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item" 
                                @if (Auth::user()->id == $item->id) disabled @endif
                                data-toggle="tooltip" data-placement="top" title="Activer le compte">
                                <i class="fa fa-unlock"></i>&nbsp;&nbsp;Activer
                            </button>
                            <button onclick="reinitialiser({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                data-toggle="tooltip" data-placement="top" title="Réinitialiser le mot de passe">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i></i>&nbsp;&nbsp;Réinitialiser
                            </button>
                            {{-- <button onclick="supprimerCompte({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                data-toggle="tooltip" data-placement="top" title="Supprimer le compte utilisateur">
                                <i class="fa fa-trash" aria-hidden="true"></i></i>&nbsp;&nbsp;Supprimer le compte
                            </button> --}}
                            @endif
                            <a href="{!! route('historique.index', [$item->id]) !!}" class='dropdown-item' data-toggle="tooltip" data-placement="top"
                                title="Consulter l'historique des actions">
                                <i class="fa fa-eye"></i>&nbsp;&nbsp;Historique
                            </a>
                            {{-- <button onclick="getHistorique({{$item->id}},'{{$item->nom}} {{$item->prenom}}')" class="dropdown-item"
                                data-toggle="tooltip" data-placement="top" title="Consulter l'historique des actions">
                                <i class="fa fa-eye" aria-hidden="true"></i></i>&nbsp;&nbsp;Historique
                            </button> --}}
                        </div>
                    </div>
                </td>
                @endcan
            </tr>
            @endforeach
        </tbody>
    </table>
</div>