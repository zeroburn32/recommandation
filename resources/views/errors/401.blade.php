@extends('errors::minimal')

@section('title', 'Accès interdit')
@section('code', '401')
@section('message', 'Vous n\'êtes pas connecté, merci de vous connectez')
