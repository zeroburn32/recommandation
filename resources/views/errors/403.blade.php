@extends('errors::minimal')

@section('title', 'Accès interdit')
@section('code', '403')
@section('message', "Vous n'avez pas le droit d'accéder à cette page, merci de contacter l'administrateur")
