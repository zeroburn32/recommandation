@extends('errors::minimal')

@section('title', 'Accès interdit')
@section('code', '404')
@section('message', 'La ressource que vous tentez d\'accéder n\'existe pas dans PSR, merci de contacter l\'administrateur')
