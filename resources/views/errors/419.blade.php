@extends('errors::minimal')

@section('title', 'Accès interdit')
@section('code', '419')
@section('message', 'Votre session est expirée, merci de cliquer le bouton ci-dessous pour vous reconnectez')
