@extends('errors::minimal')

@section('title', 'Accès interdit')
@section('code', '500')
@section('message', 'Une erreur est survenue sur le serveur, merci de contacter l\'administrateur')
