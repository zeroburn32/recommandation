@extends('errors::minimal')

@section('title', 'Accès interdit')
@section('code', '503')
@section('message', 'Le serveur est inaccessible pour l\'instance, merci de contacter l\'administrateur')
