@extends('layouts.template')
@section('title')
    Tableau de bord
    @parent
@stop

@section('header_styles')
    {{--<link rel="stylesheet" type="text/css" href="{{ url('backend/css/highcharts.css') }}">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}"/>--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}"/>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="card-title">Tableau de bord</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="pull-right">Derni&egrave;re connexion : {{ \Carbon\Carbon::parse(Auth::user()->lastconnect)->diffForHumans() }}</div>
                        </div>
                    </div>
                </div>
                @if($nbreAlerteNonLue == 1)
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Vous avez <span style="font-weight: bold; font-size: 18px;">{{ $nbreAlerteNonLue }} notification</span> non lue.&nbsp;
                    @if($nbreAlerteNonLue > 0)<a href="{{route('alertInbox')}}">Cliquez ici</a>@endif
                </div>
                @elseif($nbreAlerteNonLue > 1)
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Vous avez <span style="font-weight: bold; font-size: 18px;">{{ $nbreAlerteNonLue }} notifications</span> non lues.&nbsp;
                        @if($nbreAlerteNonLue > 0)<a href="{{route('alertInbox')}}">Cliquez ici</a>@endif
                    </div>
                @endif
            </div>
        </div>
        {{--GRAPHIQUES--}}
        <div class="row my-2">
            <div class="col-sm-12 col-md-12 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card mb-3 widget-content">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Etat de mise en oeuvre</div>
                                    <div class="widget-subheading">Pour votre structure</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-info">
                                        <span style="cursor: pointer" data-toggle="tooltip" data-placement="top"
                                                title="{{ App\Http\Controllers\MethodesStaticController::structureUserConnect()->str_nom_complet }}">{{ App\Http\Controllers\MethodesStaticController::structureUserConnect()->str_sigle }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                                <div class="card">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Par instance</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div id="chartForUserByInstance" style="width:100%; height: 250px;"></div>
                                    </div>
                                    <div class="card-footer">
                                        <select class="form-control form-control-sm selectSearch" id="instanceUser" onchange="refreshChartForUserByInstance()">
                                            @foreach($userInstances as $userInstance)
                                                <option value="{{ $userInstance->ins_id }}">{{ $userInstance->ins_sigle . ' - ' . $userInstance->ins_nom_complet }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                                <div class="card">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Toutes les instances</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div id="chartForUserAllInstances" style="width:100%; height: 300px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card mb-3 widget-content">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Etat de mise en oeuvre</div>
                                    <div class="widget-subheading">Pour toutes les structures</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-info"><span>Global</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                                <div class="card">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Par instance</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div id="chartByInstance" style="width:100%; height: 250px;"></div>
                                    </div>
                                    <div class="card-footer">
                                        <select class="form-control form-control-sm selectSearch" id="instance" onchange="refreshChartByInstance()">
                                            @foreach($allInstances as $allInstance)
                                                <option value="{{ $allInstance->ins_id }}">{{ $allInstance->ins_sigle . ' - ' . $allInstance->ins_nom_complet }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-12">
                                <div class="card">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Toutes les instances</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div id="chartAllInstances" style="width:100%; height: 300px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--Récommandations non réalisées à écheance de -30 jours--}}
        @if(count($recosNonRealiseAecheances) > 0)
            <div class="row my-2">
                <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                    <div class="card mb-2 widget-content bg-warning">
                        <div class="widget-content-wrapper text-black">
                            <div class="widget-content-left">
                                <div class="widget-heading">Récommandations non réalisées à écheance de moins de</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers">30 jours</div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($recosNonRealiseAecheances as $reco)
                <div class="col-lg-6 col-xl-4">
                    <div class="card mb-2 widget-content" style="cursor: pointer;" onclick="openModalDetailsReco({{ $reco->rec_id }})"
                         data-toggle="modal" data-target="#modalDetails">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <a>
                                    <div class="widget-heading">{{ $reco->rec_intitule }}</div>
                                    <div class="badge badge-warning">
                                        Date &eacute;cheance : {{ \Carbon\Carbon::parse($reco->rec_date_echeance)->format('d/m/Y') }}
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @endif

        {{--Récommandations non réalisées donc l'écheance est dépasssé--}}
        @if(count($recosNonRealiseAecheanceDepasses) > 0)
            <div class="row my-2">
                <div class="col-sm-12 col-md-12 col-lg-12 col-12">
                    <div class="card mb-2 widget-content bg-danger">
                        <div class="widget-content-wrapper text-white">
                            <div class="widget-content-left">
                                <div class="widget-heading">Récommandations non réalisées donc l'écheance est dépasssé</div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($recosNonRealiseAecheanceDepasses as $reco)
                <div class="col-lg-6 col-xl-4">
                    <div class="card mb-2 widget-content" style="cursor: pointer;" onclick="openModalDetailsReco({{ $reco->rec_id }})"
                         data-toggle="modal" data-target="#modalDetails">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <a>
                                    <div class="widget-heading">{{ $reco->rec_intitule }}</div>
                                    <div class="badge badge-danger">
                                        Date &eacute;cheance : {{ \Carbon\Carbon::parse($reco->rec_date_echeance)->format('d/m/Y') }}
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @endif

    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('backend/js/highcharts/highcharts.js')}}"></script>
    <script type="text/javascript" src="{{ url('backend/js/highcharts/exporting.js')}}"></script>
    <script type="text/javascript" src="{{ url('backend/js/highcharts/export-data.js')}}"></script>
    <script type="text/javascript" src="{{ url('backend/js/highcharts/highcharts-nodata.js')}}"></script>
    {{--<script type="text/javascript" src="{{ url('backend/js/date.format.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ url('backend/js/sweetalert2.all.min.js') }}"></script>--}}
    <script src="{{ url('backend/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('backend/js/dashboard.js') }}"></script>
@endsection

@section('modal')
    @include('backend.recommandations.modalDetails')
@endsection
