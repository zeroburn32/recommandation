<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
        @section('title')
        | {{config('app.name')}}
        @show
    </title>
    <title></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://unpkg.com/@coreui/icons/css/coreui-icons.min.css">
    <link rel="stylesheet" href="https://unpkg.com/@coreui/icons@1.0.0/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css')}}" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-hidden">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{url('/home')}}">
        <img src="{{asset('images/dsilogo.png')}}" max-width="60%" height="50px" class="user-image" alt="logo DSI" />
        <b> P S R </b>
        {{-- <img class="navbar-brand-full" src="http://infyom.com/images/logo/blue_logo_150x150.jpg" width="30" height="30"
             alt="MEA Logo">
        <img class="navbar-brand-minimized" src="http://infyom.com/images/logo/blue_logo_150x150.jpg" width="30"
             height="30" alt="Infyom Logo"> --}}
    </a>
    {{-- <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button> --}}
    <ul class="nav nav-tabs">
        <ul class="nav nav-tabs">
            <li class="nav-item">
            <a class="nav-link active" href="{{ url('/home')}}"><i class="fas fa-chart-bar"></i>Tableau de bord</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                    aria-expanded="false"><i class="fas fa-cog"></i>Paramétrage</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ url('/home')}}">Structures</a>
                    <a class="dropdown-item" href="{{ url('/home')}}">Instances</a>
                    <a class="dropdown-item" href="{{ url('/home')}}"></a>
                    <div class="dropdown-divider"></div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                    aria-expanded="false"><i class="fas fa-user"></i>Administration</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ url('/users')}}">Utilisateurs</a>
                    <a class="dropdown-item" href="{{ url('/roles')}}">Profils</a>
                    <a class="dropdown-item" href="{{ url('/permissions')}}">Droits</a>
                    <div class="dropdown-divider"></div>
                </div>
            </li>
        </ul>
        
        <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled link</a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="icon-bell"></i>
                <span class="badge badge-pill badge-danger">5</span>
            </a>
        </li>
  
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                
                <a class="dropdown-item" href="{!! route('users.show', [Auth::user()->id]) !!}" title="Afficher des informations sur mon compte">
                    <i class="fa fa-user"></i> Profile</a>
                
                <a class="dropdown-item" href="{!! route('formEditAccount', [Auth::user()->id]) !!}" title="Modifier les informations de mon compte">
                    <i class="fa fa-users"></i> Modifier mes infos</a>

                <a class="dropdown-item" href="{!! route('formMyPassword') !!}" title="Modifier le mot de passe de mon compte">
                    <i class="fa fa-key"></i> Modifier mon mot de passe</a>

                <a href="{{ url('/logout') }}" class="dropdown-item btn btn-default btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>Me deconnecter
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    @include('layouts.sidebar')
    <main class="main">
        @yield('content')
    </main>
</div>
<footer class="app-footer">
    <div>
        <span>&copy; 2019 .</span><a href="https://www.mea.gov.bf">Ministère de l'Eau et de l'Assainissement </a>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://a2sysconsulting.com/">A2Sys Consulting</a>
    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
@include('flashy::message')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script>
<!-- Datatable -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.dt_example').DataTable();
    } );

    var APP_URL = {!! json_encode(url('/')) !!};

    function goBack() {
    window.history.back();
    }
</script>

@stack('scripts')

</html>
