
<div class="collapse navbar-collapse bg-light text-blue p-4" id="collapsibleNavbar">
    <ul class="navbar-nav" >
        <li class="nav-item">
            <a class="nav-link" href="#">&nbsp;</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">&nbsp;</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">&nbsp;</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">&nbsp;</a>
        </li>
       <li class="nav-item">
        <a href="{{ url('/home')}}" class="nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-home"></i>Accueil
        </a>
    </li>
    
    {{-- Menu for Admin --}}
    @if (Auth::user()->hasRole(['ADMIN']))
    @can(['GERER_STRUCTURE'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ route('structures.index') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-cog"></i>&nbsp;&nbsp;Structures
        </a>
    </li>
    @endcan
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-bar-chart"></i>Etats
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            @can(['CONSULTER_RAPPORT'])
            <a tabindex="0" class="dropdown-item" href="{{ url('rapport') }}">
                <i style="color:blue;" class="fa fa-line-chart"></i>&nbsp;&nbsp;Rapport
            </a>
            @endcan
            @can(['CONSULTER_STATS'])
            <a tabindex="0" class="dropdown-item" href="{{ url('statistiques') }}">
                <i style="color:blue;" class="fa fa-signal"></i>&nbsp;&nbsp;Statistique
            </a>
            @endcan
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-user"></i>Utilisateurs
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            @can(['GERER_COMPTE'])
            <a tabindex="0" class="dropdown-item" href="{{ url('/users')}}">
                <i style="color:blue;" class="fa fa-user"></i>&nbsp;&nbsp;Comptes
            </a>
            @endcan
            @can(['GERER_PROFIL'])
            <a tabindex="0" class="dropdown-item" href="{{ url('/roles')}}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Profils
            </a>
            @endcan
            @can(['GERER_PROFIL'])
            <a tabindex="0" class="dropdown-item" href="{{ url('/permissions')}}">
                <i style="color:blue;" class="fa fa-check"></i>&nbsp;&nbsp;Droits
            </a>
            @endcan
        </div>
    </li>
    @endif
    
    {{-- Menu for CSS --}}
    @if (Auth::user()->hasRole(['CSS']))
    @can(['GERER_INSTANCE'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ route('instances.index') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-bank"></i>Instances
        </a>
    </li>
    @endcan
    
    @can(['GERER_SESSION'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ route('sessions.index') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-calendar-o"></i>Sessions
        </a>
    </li>
    @endcan
    
    @can(['GERER_RECOMMANDATION', 'SAISIE_RECOMMANDATION'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ route('getPageSaisieRecos') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-book"></i>Recommandations
        </a>
    </li>
    @endcan
    
    @can(['GERER_ALERTE'])
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-print"></i>Notification
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('alertes.index') }}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Alertes
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('templatemsgs.index') }}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Modèles de message
            </a>
        </div>
    </li>
    @endcan
    
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-bar-chart"></i>Etats
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            @can(['CONSULTER_RAPPORT'])
            <a tabindex="0" class="dropdown-item" href="{{ url('rapport') }}">
                <i style="color:blue;" class="fa fa-line-chart"></i>&nbsp;&nbsp;Rapport
            </a>
            @endcan
            @can(['CONSULTER_STATS'])
            <a tabindex="0" class="dropdown-item" href="{{ url('statistiques') }}">
                <i style="color:blue;" class="fa fa-signal"></i>&nbsp;&nbsp;Statistique
            </a>
            @endcan
        </div>
    </li>
    @endif
    
    {{-- Menu for CSR --}}
    @if (Auth::user()->hasRole(['CSR']))
    @can(['PLANIFIER_RECOMMANDATION'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ route('getPagePlaningRecos') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-calendar"></i>Planification
        </a>
    </li>
    @endcan
    
    @can(['SUIVI_RECOMMANDATION'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ route('getPageSuiviRecos') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-calendar-check-o"></i>Suivi
        </a>
    </li>
    @endcan
    
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-bar-chart"></i>Etats
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            @can(['CONSULTER_RAPPORT'])
            <a tabindex="0" class="dropdown-item" href="{{ url('rapport') }}">
                <i style="color:blue;" class="fa fa-line-chart"></i>&nbsp;&nbsp;Rapport
            </a>
            @endcan
            @can(['CONSULTER_STATS'])
            <a tabindex="0" class="dropdown-item" href="{{ url('statistiques') }}">
                <i style="color:blue;" class="fa fa-signal"></i>&nbsp;&nbsp;Statistique
            </a>
            @endcan
        </div>
    </li>
    @endif
    
    {{-- Menu for ASR --}}
    @if (Auth::user()->hasRole(['ASR']))
    @can(['CONSULTER_RAPPORT'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ url('rapport') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-line-chart"></i>Rapport
        </a>
    </li>
    @endcan
    @can(['CONSULTER_STATS'])
    <li class="nav-item">
        <a tabindex="0" class="nav-link" href="{{ url('statistiques') }}">
            <i style="color:blue;" class="nav-link-icon fa fa-signal"></i>Statistique
        </a>
    </li>
    @endcan
    @endif
    
    {{-- Menu for SUPER ADMIN --}}
    @if (Auth::user()->hasRole(['SUPERADMIN']))
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-cog"></i>Paramètres
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('structures.index') }}">
                <i style="color:blue;" class="fa fa-cog"></i>&nbsp;&nbsp;Structures
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('instances.index') }}">
                <i style="color:blue;" class="fa fa-bank"></i>&nbsp;&nbsp;Instances
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('sessions.index') }}">
                <i style="color:blue;" class="fa fa-calendar-o"></i>&nbsp;&nbsp;Sessions
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('programmeBudgetaires.index') }}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Programme budg&eacute;taire
            </a>
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-book"></i>Recommandations
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('getPageSaisieRecos') }}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Saisie
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('getPagePlaningRecos') }}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Planification
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('getPageSuiviRecos') }}">
                <i style="color:blue;" class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;Suivi
            </a>
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-history"></i>Notifications
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('alertes.index') }}">
                <i style="color:blue;" class="fa fa-rss"></i>&nbsp;&nbsp;Alertes
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('templatemsgs.index') }}">
                <i style="color:blue;" class="fa fa-fax"></i>&nbsp;&nbsp;Modèles de message
            </a>
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-user"></i>Utilisateurs
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ url('/users')}}">
                <i style="color:blue;" class="fa fa-user"></i>&nbsp;&nbsp;Comptes
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ url('/roles')}}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Profils
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ url('/permissions')}}">
                <i style="color:blue;" class="fa fa-check"></i>&nbsp;&nbsp;Droits
            </a>
    
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-bar-chart"></i>Etats
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ url('rapport') }}">
                <i style="color:blue;" class="fa fa-line-chart"></i>&nbsp;&nbsp;Rapport
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ url('statistiques') }}">
                <i style="color:blue;" class="fa fa-signal"></i>&nbsp;&nbsp;Statistique
            </a>
        </div>
    </li>
    @endif
    
    @if ( ! Auth::user()->hasRole(['SUPERADMIN','ADMIN','CSS','CSR','ASR']))
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-cog"></i>Paramètres
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('structures.index') }}">
                <i style="color:blue;" class="fa fa-cog"></i>&nbsp;&nbsp;Structures
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('instances.index') }}">
                <i style="color:blue;" class="fa fa-bank"></i>&nbsp;&nbsp;Instances
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('sessions.index') }}">
                <i style="color:blue;" class="fa fa-calendar-o"></i>&nbsp;&nbsp;Sessions
            </a>
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-book"></i>Recommandations
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('getPageSaisieRecos') }}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Saisie
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('getPagePlaningRecos') }}">
                <i style="color:blue;" class="fa fa-calendar"></i>&nbsp;&nbsp;Planification
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('getPageSuiviRecos') }}">
                <i style="color:blue;" class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;Suivi
            </a>
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-history"></i>Notification
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ route('alertes.index') }}">
                <i style="color:blue;" class="fa fa-rss"></i>&nbsp;&nbsp;Alertes
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ route('templatemsgs.index') }}">
                <i style="color:blue;" class="fa fa-fax"></i>&nbsp;&nbsp;Modèles de message
            </a>
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-user"></i>Utilisateurs
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ url('/users')}}">
                <i style="color:blue;" class="fa fa-user"></i>&nbsp;&nbsp;Comptes
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ url('/roles')}}">
                <i style="color:blue;" class="fa fa-list"></i>&nbsp;&nbsp;Profils
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ url('/permissions')}}">
                <i style="color:blue;" class="fa fa-check"></i>&nbsp;&nbsp;Droits
            </a>
    
        </div>
    </li>
    <li class="btn-group dropdown nav-item">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn nav-link">
            <i style="color:blue;" class="nav-link-icon fa fa-bar-chart"></i>Etats
            <i style="color:blue;" class="fa fa-angle-down ml-2 opacity-8"></i>
        </a>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <a tabindex="0" class="dropdown-item" href="{{ url('rapport') }}">
                <i style="color:blue;" class="fa fa-line-chart"></i>&nbsp;&nbsp;Rapport
            </a>
            <a tabindex="0" class="dropdown-item" href="{{ url('statistiques') }}">
                <i style="color:blue;" class="fa fa-signal"></i>&nbsp;&nbsp;Statistique
            </a>
        </div>
    </li>
    @endif
    </ul>
</div>