<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('home') }}">
        <img src="{{ asset('theme/img/logo.png') }}" alt="logo" width="100px" class="img-fluid"/>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto margin_right">
            <li class="nav-item {!! (Request::is('/') ? 'active' : '') !!}">
                <a href="{{ route('home') }}" class="nav-link"> Accueil</a>
            </li>
            {{--<li class="nav-item {!! (Request::is('metadonne') ? 'active' : '') !!}">
                <a href="{{ url('metadonne') }}" class="nav-link"> Indicateurs</a>
            </li>
            <li class="nav-item dropdown {!! (Request::is('indicateur/*') ? 'active' : '') !!}">
                <a href="#" class="nav-link scroll hvr-shutter-out-horizontal"> Donn&eacute;es</a>
                <ul class="dropdown-menu" role="menu">
                    @foreach($typeStructures as $typeStructure)
                        <li>
                            <a href="{!! route('indicateur', [$typeStructure->id]) !!}" class="dropdown-item">
                                {{ $typeStructure->type_libelle }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li class="nav-item {!! (Request::is('comparateur') ? 'active' : '') !!}">
                <a href="{{ url('comparateur') }}" class="nav-link">Comparateur</a>
            </li>
            <li class="nav-item {!! (Request::is('api') ? 'active' : '') !!}">
                <a href="{{ url('api') }}" class="nav-link">API</a>
            </li>
            <li class="nav-item {!! (Request::is('contact') ? 'active' : '') !!}">
                <a href="{{ url('contact') }}" class="nav-link">Contact</a>
            </li>--}}
        </ul>
    </div>
</nav>