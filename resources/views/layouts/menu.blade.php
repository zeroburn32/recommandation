<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Users</span>
    </a>
</li>
<li class="nav-item {{ Request::is('roles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('roles.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Profils</span>
    </a>
</li>
<li class="nav-item {{ Request::is('permissions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('permissions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Droits</span>
    </a>
</li>
<li class="nav-item {{ Request::is('acteurs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('acteurs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Acteurs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('instances*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('instances.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Instances</span>
    </a>
</li>
<li class="nav-item {{ Request::is('structures*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('structures.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Structures</span>
    </a>
</li>
<li class="nav-item {{ Request::is('sessions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('sessions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sessions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('templatemsgs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('templatemsgs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Templatemsgs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('alertes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('alertes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Alertes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('participants*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('participants.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Participants</span>
    </a>
</li>
<li class="nav-item {{ Request::is('recommandations*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('recommandations.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Recommandations</span>
    </a>
</li>
<li class="nav-item {{ Request::is('activites*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('activites.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Activites</span>
    </a>
</li>
<li class="nav-item {{ Request::is('partenaires*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('partenaires.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Partenaires</span>
    </a>
</li>
<li class="nav-item {{ Request::is('agents*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('agents.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Agents</span>
    </a>
</li>
<li class="nav-item {{ Request::is('listeAlertes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('listeAlertes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Liste Alertes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('envoiAlertes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('envoiAlertes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Envoi Alertes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('programmeBudgetaires*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('programmeBudgetaires.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Programme Budgetaires</span>
    </a>
</li>
