<!doctype html>
<html>

<head>
    <script type="text/javascript" src="{{asset('backend/js/pace.min.js')}}"></script>
    <link href="{{asset('backend/css/pace.css')}}" rel="stylesheet">
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>
        @section('title')
            | {{config('app.name')}}
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>
    <!-- Main -->
    <link href="{{asset('backend/css/main.css')}}" rel="stylesheet">

    <!-- Datatable -->
    <link href="{{asset('backend/DataTables/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/DataTables/css/buttons.dataTables.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Select2 -->
    <link href="{{asset('backend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/css/select2.min.css')}}" rel="stylesheet" type="text/css">

    @yield('header_styles')
</head>

<body>
{{--@include('layouts.horizontal')--}}
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <div class="app-header header-shadow">
        <div class="app-header__logo" style=" padding-left:0%; width:15%;" >
            <a href="{{ url('/home')}}" class="app-header__logo">
                <img src="{{asset('images/logo-psr.png')}}" max-width="60%" height="50px" class="user-image" alt="P.S.R."/>
            </a>
        </div>

        <div class="app-header__mobile-menu">
            {{-- <div> --}}
                <button type="button" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" aria-haspopup="true" aria-expanded="false" class="navbar-toggler">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
           {{--  </div> --}}
            @include('layouts.dropdown_menu')
        </div>

        <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
               
        </div>

        <div class="app-header__content">
            <div class="app-header-left monmenu" > 
                @include('layouts.horizontal_menu')
            </div>

            <div class="app-header-right">
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="btn-group mr-1">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                   class="p-3 btn">
                                    {{-- <img width="32" class="rounded-circle"
                                        src="{{asset('backend/images/1.jpg')}}" alt=""> --}}
                                    <i class="fa fa-bell" style="font-size: 26px;"></i>
                                    <span class="badge badge-pill badge-danger ml-0">
                                            {{$nbr_alerte->count()}}
                                        </span>
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true"
                                     class="dropdown-menu dropdown-menu-right">
                                    @if($list_alerte->count())
                                        @foreach($list_alerte as $notification)
                                            <a tabindex="0" class="dropdown-item"
                                               href="alerte/boiteReception/detail/{{$notification->ena_id}}"
                                               title="Afficher le contenu">
                                                <i class="fa fa-clock"></i>
                                                Alerte< {{substr($notification->ale_mail_subject,0,20)}} ...>
                                            </a>
                                        @endforeach
                                    @endif

                                    @if(count($list_alerte) > 0)
                                        <div tabindex="-1" class="dropdown-divider"></div>
                                    @endif

                                    <a tabindex="0" class="dropdown-item"
                                       href="{{route('alertInbox')}}"
                                       title="Afficher tous les alertes">
                                        <i class="fa fa-bell"></i>&nbsp;&nbsp;Toutes les alertes
                                    </a>
                                </div>
                            </div>
                            <div class="app-header-right" style="padding-left:0px;">
                                <div class="header-btn-lg pr-0" >
                                    <div class="widget-content p-0" >
                                        <div class="widget-content-wrapper" >
                                            {{-- <div class="widget-content-left">
                                                <div class="btn-group">
                                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                                        
                                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                                    </a>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                                        <a tabindex="0" class="dropdown-item" href="{!! route('users.compte.show', [Auth::user()->id]) !!}"
                                                            title="Afficher des informations sur mon compte">
                                                            <i class="fa fa-user"></i>&nbsp;&nbsp;Mon Profil
                                                        </a>
                                                        
                                                        <a tabindex="0" class="dropdown-item" href="{!! route('formMyPassword') !!}"
                                                            title="Modifier le mot de passe de mon compte">
                                                            <i class="fa fa-key"></i>&nbsp;&nbsp;Modifier mon mot de passe
                                                        </a>
                                                        <div tabindex="-1" class="dropdown-divider"></div>
                                                        <a tabindex="0" class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                                                                   document.getElementById('logout-form').submit();">
                                                            <i class="fa fa-lock"></i>&nbsp;&nbsp;Me d&eacute;connecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="widget-content-left  ml-3 header-user-info btn-group">
                                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                                <div class="widget-heading" style="font-size:14px; white-space:nowrap;">
                                                    {{ Auth::user()->nom }} {{ Auth::user()->prenom }}
                                                </div>
                                                <div class="widget-subheading" style="white-space:nowrap; font-size:10px; color:blue; font-style:italic;">
                                                    {{ App\Methods::getStructure(Auth::user()->str_id)->str_sigle }}
                                                    : {{ Auth::user()->getRoleNames()->first()}}
                                                </div>
                                                </a>
                                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                                    <a tabindex="0" class="dropdown-item" href="{!! route('users.compte.show', [Auth::user()->id]) !!}"
                                                        title="Afficher des informations sur mon compte">
                                                        <i class="fa fa-user"></i>&nbsp;&nbsp;Mon Profil
                                                    </a>
                                                
                                                    <a tabindex="0" class="dropdown-item" href="{!! route('formMyPassword') !!}"
                                                        title="Modifier le mot de passe de mon compte">
                                                        <i class="fa fa-key"></i>&nbsp;&nbsp;Modifier mon mot de passe
                                                    </a>
                                                    <div tabindex="-1" class="dropdown-divider"></div>
                                                    <a tabindex="0" class="dropdown-item" href="{{ url('/logout') }}"
                                                        onclick="event.preventDefault();
                                                                                                                                                   document.getElementById('logout-form').submit();">
                                                        <i class="fa fa-lock"></i>&nbsp;&nbsp;Me d&eacute;connecter
                                                    </a>
                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </div>
                                            {{-- <div class="widget-content-right header-user-info ml-3"> --}}
                                                <div class="widget-content-left">
                                                <div class="btn-group">
                                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                                        {{-- <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt=""> --}}
                                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                                    </a>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                                        <a tabindex="0" class="dropdown-item" href="{!! route('users.compte.show', [Auth::user()->id]) !!}"
                                                            title="Afficher des informations sur mon compte">
                                                            <i class="fa fa-user"></i>&nbsp;&nbsp;Mon Profil
                                                        </a>
                                                
                                                        <a tabindex="0" class="dropdown-item" href="{!! route('formMyPassword') !!}"
                                                            title="Modifier le mot de passe de mon compte">
                                                            <i class="fa fa-key"></i>&nbsp;&nbsp;Modifier mon mot de passe
                                                        </a>
                                                        <div tabindex="-1" class="dropdown-divider"></div>
                                                        <a tabindex="0" class="dropdown-item" href="{{ url('/logout') }}"
                                                            onclick="event.preventDefault();
                                                                                                                                                   document.getElementById('logout-form').submit();">
                                                            <i class="fa fa-lock"></i>&nbsp;&nbsp;Me d&eacute;connecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                                </div>
                                            {{-- </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="app-main">
        <div class="app-main__outer my-3">
            @yield('content')
            <div class="card">
                <div class="card-body">
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                                <div class="app-footer-left">
                                    <div>
                                        <span>&copy; {{--{{ date('Y') }}--}}2020 </span>
                                        <a href="https://www.mea.gov.bf" target="_blank">
                                            Ministère de l'Eau et de l'Assainissement
                                        </a>
                                    </div>
                                </div>
                                <div class="app-footer-right">
                                    <div class="ml-auto">
                                        <span>Réalisé par </span>
                                        <a href="https://a2sysconsulting.com/" target="_blank">
                                            A2SYS Consulting
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div id="preloader"></div> --}}
{!! Notify::render() !!}
<!-- Main -->

<script type="text/javascript" src="{{asset('backend/js/main.js')}}"></script>
<script type="text/javascript" src="{{ asset('backend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('backend/js/global.js') }}"></script>

<!-- Datatable -->
<script type="text/javascript" src="{{ asset('backend/DataTables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/DataTables/js/buttons.print.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ url('backend/js/select2.min.js') }}" type="text/javascript"></script>
<script>
    var APP_URL = {!! json_encode(url('/')) !!};

    function goBack() {
        window.history.back();
    }

    function goBackReload() {
        window.location.reload();
    }

    $(document).ready(function () {
        $('.dt_example').DataTable({
            dom: 'lBfrtip',
            responsive: true,
            processing: true,
            select: true,
            "language": {
                "url": "{{ asset('lang/datatables.french.json')}}"
            },
            buttons: [
                //'copyHtml5',
                'excelHtml5',
                //'csvHtml5',
                'pdfHtml5'
            ],
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
                "searchable": false
            }]
        });
    });
</script>
{{--<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/js/popper.min.js') }}"></script>--}}
@yield('scripts')
</body>
</html>
@yield('modal')