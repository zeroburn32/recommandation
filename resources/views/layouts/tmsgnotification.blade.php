<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
        @section('title')
        | {{config('app.name')}}
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <!-- Main -->
    <link href="{{asset('backend/css/main.css')}}" rel="stylesheet">
    
    <!-- Datatable -->
    <link href="{{asset('backend/DataTables/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/DataTables/css/buttons.dataTables.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Select2 -->
    <link href="{{asset('backend/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    @yield('header_styles')

    <style type="text/css">
        body{
            margin-top:20px;
            background:#FDFDFF
        }
        .badge {
            border-radius: 8px;
            padding: 4px 8px;
            text-transform: uppercase;
            font-size: .7142em;
            line-height: 12px;
            background-color: transparent;
            border: 1px solid;
            margin-bottom: 5px;
            border-radius: .875rem;
        }
        .bg-green {
            background-color: #50d38a !important;
            color: #fff;
        }
        .bg-blush {
            background-color: #ff758e !important;
            color: #fff;
        }
        .bg-amber {
            background-color: #FFC107 !important;
            color: #fff;
        }
        .bg-red {
            background-color: #ec3b57 !important;
            color: #fff;
        }
        .bg-blue {
            background-color: #60bafd !important;
            color: #fff;
        }
        .card {
            background: #fff;
            margin-bottom: 30px;
            transition: .5s;
            border: 0;
            border-radius: .1875rem;
            display: inline-block;
            position: relative;
            width: 100%;
            box-shadow: none;
        }
        .inbox .action_bar .delete_all {
            margin-bottom: 0;
            margin-top: 8px
        }

        .inbox .action_bar .btn,
        .inbox .action_bar .search {
            margin: 0
        }

        .inbox .mail_list .list-group-item {
            border: 0;
            padding: 15px;
            margin-bottom: 1px
        }

        .inbox .mail_list .list-group-item:hover {
            background: #eceeef
        }

        .inbox .mail_list .list-group-item .media {
            margin: 0;
            width: 100%
        }

        .inbox .mail_list .list-group-item .controls {
            display: inline-block;
            margin-right: 10px;
            vertical-align: top;
            text-align: center;
            margin-top: 11px
        }

        .inbox .mail_list .list-group-item .controls .checkbox {
            display: inline-block
        }

        .inbox .mail_list .list-group-item .controls .checkbox label {
            margin: 0;
            padding: 10px
        }

        .inbox .mail_list .list-group-item .controls .favourite {
            margin-left: 10px
        }

        .inbox .mail_list .list-group-item .thumb {
            display: inline-block
        }

        .inbox .mail_list .list-group-item .thumb img {
            width: 40px
        }

        .inbox .mail_list .list-group-item .media-heading a {
            color: #555;
            font-weight: normal
        }

        .inbox .mail_list .list-group-item .media-heading a:hover,
        .inbox .mail_list .list-group-item .media-heading a:focus {
            text-decoration: none
        }

        .inbox .mail_list .list-group-item .media-heading time {
            font-size: 13px;
            margin-right: 10px
        }

        .inbox .mail_list .list-group-item .media-heading .badge {
            margin-bottom: 0;
            border-radius: 50px;
            font-weight: normal
        }

        .inbox .mail_list .list-group-item .msg {
            margin-bottom: 0px
        }

        .inbox .mail_list .unread {
            border-left: 2px solid
        }

        .inbox .mail_list .unread .media-heading a {
            color: #333;
            font-weight: 700
        }

        .inbox .btn-group {
            box-shadow: none
        }

        .inbox .bg-gray {
            background: #e6e6e6
        }

        @media only screen and (max-width: 767px) {
            .inbox .mail_list .list-group-item .controls {
                margin-top: 3px
            }
        }
    </style>
</head>

<body>

    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <a href="{{ url('/home')}}" class="app-header__logo">
                    <img src="{{asset('images/dsilogo.png')}}" max-width="60%" height="50px" class="user-image"
                        alt="logo DSI" />
                    {{--<div class="logo-src"></div>--}}
                </a>
            </div>
            {{--<div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                </button>
            </div>
        </div>--}}
            <div class="app-header__content">
                <div class="app-header-left">
                    @include('layouts.horizontal_menu')
                </div>

                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">


                                <div class="btn-group mr-2">
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                       class="p-0 btn">
                                        {{-- <img width="32" class="rounded-circle"
                                            src="{{asset('backend/images/1.jpg')}}" alt=""> --}}
                                        <i class="fa fa-bell" style="font-size: 26px;"></i>
                                        <span class="badge badge-pill ba">{{auth()->user()->unreadnotifications->count()}}</span>
                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                    </a>
                                    <div tabindex="-1" role="menu" aria-hidden="true"
                                         class="dropdown-menu dropdown-menu-right">
                                        @foreach(auth()->user()->unreadnotifications as $notification)
                                        <a tabindex="0" class="dropdown-item"
                                           href="{{route('alertInfo',['id'=>$notification->id])}}"
                                           title="Afficher des informations sur mon compte">

                                            <i class="fa fa-clock"></i>&nbsp;&nbsp;{{$notification->data['description']}}
                                                @endforeach
                                        </a>


                                        <div tabindex="-1" class="dropdown-divider"></div>
                                        <a tabindex="0" class="dropdown-item"
                                           href="{{route('alertInbox')}}"
                                           title="Modifier les informations de mon compte">
                                            <i class="fa fa-users"></i>&nbsp;&nbsp;Toutes les alertes
                                        </a>


                                    </div>
                                </div>



                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            class="p-0 btn">
                                            {{-- <img width="32" class="rounded-circle"
                                                src="{{asset('backend/images/1.jpg')}}" alt=""> --}}
                                            <span class="widget-heading">{{ \App\Methods::getAgent(Auth::user()->age_id)->nom_prenom }}</span>
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true"
                                            class="dropdown-menu dropdown-menu-right">
                                            <a tabindex="0" class="dropdown-item"
                                                href="{!! route('users.compte.show', [Auth::user()->id]) !!}"
                                                title="Afficher des informations sur mon compte">
                                                <i class="fa fa-user"></i>&nbsp;&nbsp;Mon Profil
                                            </a>
                                            {{-- <a tabindex="0" class="dropdown-item"
                                                href="{!! route('formEditAccount', [Auth::user()->id]) !!}"
                                                title="Modifier les informations de mon compte">
                                                <i class="fa fa-users"></i>&nbsp;&nbsp;Modifier mes infos
                                            </a> --}}
                                            <a tabindex="0" class="dropdown-item" href="{!! route('formMyPassword') !!}"
                                                title="Modifier le mot de passe de mon compte">
                                                <i class="fa fa-key"></i>&nbsp;&nbsp;Modifier mon mot de passe
                                            </a>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <a tabindex="0" class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                                <i class="fa fa-lock"></i>&nbsp;&nbsp;Me d&eacute;connecter
                                            </a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                                style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="app-main">
            <div class="app-main__outer my-3">
                @yield('content')
                <div class="card">
                    <div class="card-body">
                        <div class="app-wrapper-footer">
                            <div class="app-footer">
                                <div class="app-footer__inner">
                                    <div class="app-footer-left">
                                        <div>
                                            <span>&copy; {{ date('Y') }} </span><a
                                                href="https://www.mea.gov.bf">Ministère de
                                                l'Eau
                                                et de l'Assainissement </a>
                                        </div>
                                    </div>
                                    <div class="app-footer-right">
                                        <div class="ml-auto">
                                            <span>Powered by</span>
                                            <a href="https://a2sysconsulting.com/">A2SYS Consulting</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="preloader"></div>
    {!! Notify::render() !!}
    <!-- Main -->
    <script type="text/javascript" src="{{asset('backend/js/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('backend/js/global.js') }}"></script>

    <!-- Datatable -->
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/DataTables/js/buttons.print.min.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ url('backend/js/select2.min.js') }}" type="text/javascript"></script>
    
    <script>
        var APP_URL = {!! json_encode(url('/')) !!};

    function goBack() {
        window.history.back();
    }
    function goBackReload() {
        window.location.reload();
    }

    $(document).ready(function() {
        $('.dt_example').DataTable( {
            dom: 'lBfrtip',
            responsive:true,
            processing: true,
            select: true,
            "language": {
                "url": "{{ asset('lang/datatables.french.json')}}"
            },
            buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5'
            ],
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            
        });
    } );
    </script>
    {{--<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>--}}
    {{--<script src="{{ asset('backend/js/popper.min.js') }}"></script>--}}
    @yield('scripts')
</body>

</html>
@yield('modal')