	document.body.appendChild(fileInput);

			fileInput.onchange = function(e) {
				resolve(Array.prototype.slice.call(e.target.files));
			};

			fileInput.click();
			fileInput.parentNode.removeChild(fileInput);
		});
	};

	return {
		pickFile: pickFile
	};
});



/**
 * Buttons.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2016 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

define("4", [
	"3",
	"i",
	"j",
	"k"
], function (Panel, Conversions, Picker, Actions) {
	var addHeaderButtons = function (editor) {
		var formatBlock = function (name) {
			return function () {
				Actions.formatBlock(editor, name);
			};
		};

		for (var i = 1; i < 6; i++) {
			var name = 'h' + i;

			editor.addButton(name, {
				text: name.toUpperCase(),
				tooltip: 'Heading ' + i,
				stateSelector: name,
				onclick: formatBlock(name),
				onPostRender: function () {
					// TODO: Remove this hack that produces bold H1-H6 when we have proper icons
					var span = this.getEl().firstChild.firstChild;
					span.style.fontWeight = 'bold';
				}
			});
		}
