<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('users', 'UsersAPIController');

Route::resource('roles', 'RolesAPIController');

Route::resource('permissions', 'PermissionsAPIController');

Route::resource('model_has_roles', 'ModelHasRoleAPIController');

Route::resource('model_has_permissions', 'ModelHasPermissionAPIController');

Route::resource('acteurs', 'ActeurAPIController');

Route::resource('instances', 'InstanceAPIController');

Route::resource('structures', 'StructuresAPIController');

Route::resource('sessions', 'SessionAPIController');

Route::resource('templatemsgs', 'TemplatemsgsAPIController');

Route::resource('alertes', 'AlertesAPIController');

Route::resource('participants', 'ParticipantsAPIController');

Route::resource('recommandations', 'RecommandationsAPIController');

Route::resource('activites', 'ActivitesAPIController');

Route::resource('partenaires', 'PartenairesAPIController');

Route::resource('agents', 'AgentsAPIController');

Route::resource('liste_alertes', 'ListeAlertesAPIController');

Route::resource('envoi_alertes', 'EnvoiAlertesAPIController');

Route::resource('categories', 'CategoriesAPIController');

Route::resource('programme_budgetaires', 'ProgrammeBudgetairesAPIController');