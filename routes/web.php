<?php
Route::get('/', function () {
    return redirect(route('login'));
});

Route::patch('firstConnect/{id}', 'Auth\LoginController@firstConnect')->name('firstConnect');
Route::post('authenticate', 'Auth\LoginController@authenticate')->name('authenticate');

Auth::routes(['verify' => true]);
Auth::routes(['register' => false]);
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/admin', 'HomeController@index')->name('admin')->middleware('verified');
Route::middleware('auth')->group(function () {
    Route::group(['middleware' => ['role:ADMIN']], function () {
    });

    Route::group(['middleware' => ['role:CSS']], function () {
    });

    Route::group(['middleware' => ['role:CSR']], function () {
    });

    Route::group(['middleware' => ['role:SUPERADMIN']], function () {
    });

    // Routes for Admin Role
    Route::group(['middleware' => ['permission:GERER_COMPTE']], function () {
        Route::resource('users', 'UsersController');
        Route::patch('/lockAndUnlockAccount/{id}', 'Auth\AccountController@lockAndUnlock')->name('lockAndUnlockAccount');
        Route::get('reinitialiserMotDePasse/{id}', 'Auth\AccountController@reinitialiserMotDePasse')->name('reinitialiserMotDePasse');
        Route::get('users/{user_id}/editCompte', 'UsersController@editUser')->name('users.compte.edit');
        Route::get('historique/{id}', 'ActivityLogController@getActivitiesLog')->name('historique.index');
        Route::post('activityLogSearch', 'ActivityLogController@activityLogSearch')->name('activityLogSearch');
    });
    Route::group(['middleware' => ['permission:GERER_PROFIL']], function () {
        Route::resource('roles', 'RolesController');
        Route::resource('permissions', 'PermissionsController');
    });

    Route::get('/formUserPassword', 'Auth\AccountController@formPassword')->name('formMyPassword');
    Route::patch('/changeUserPassword', 'Auth\AccountController@changePassword')->name('changeMyPassword');
    Route::patch('/update_account', 'UsersController@update_account')->name('update_account');
    Route::get('logout', 'Auth\LoginController@logout');

    Route::group(['middleware' => ['permission:GERER_STRUCTURE']], function () {
        Route::resource('structures', 'StructuresController');
        Route::get('deleteStructure/{id_structure}', 'StructuresController@deleteStructure')->name('deleteStructure');
        Route::get('structuresSearch', 'StructuresController@structuresSearch')->name('structuresSearch');

    });
    Route::group(['middleware' => ['permission:GERER_CATEGORIE_STRUCTURE']], function () {
        Route::resource('categories', 'CategoriesController');
        Route::get('deleteCategorie/{id_categorie}', 'CategoriesController@deleteCategorie')->name('deleteCategorie');
     });

    Route::get('users/{user_id}/showCompte', 'UsersController@showUser')->name('users.compte.show');
    Route::get('getAgentsStructure/{id_structure}', 'UsersController@getAgentsStructure')->name('getAgentsStructure');
    Route::get('getPageAgentSuivi/{id_structure}', 'UsersController@getPageAgentSuivi')->name('getPageAgentSuivi');
    Route::get('getPageParticipants/{id_session}', 'SessionController@getPageParticipants')->name('getPageParticipants');

    Route::resource('acteurs', 'ActeurController');
    Route::get('getPageActeursInstance/{id_instance}', 'ActeurController@getPageActeursInstance')->name('getPageActeursInstance');
    Route::post('saveActeur', 'ActeurController@saveActeur')->name('saveActeur');
    Route::get('getActeur/{id_acteur}', 'ActeurController@getActeur')->name('getActeur');
    Route::get('getAllActeurs', 'ActeurController@getAllActeurs')->name('getAllActeurs');
    Route::get('deleteActeur/{id_acteur}', 'ActeurController@deleteActeur')->name('deleteActeur');
    Route::get('setCss/{id_acteur}', 'ActeurController@setCss')->name('setCss');
    Route::get('setCsr/{id_acteur}', 'ActeurController@setCsr')->name('setCsr');

    Route::resource('instances', 'InstanceController');
    Route::post('saveInstance', 'InstanceController@saveInstance')->name('saveInstance');
    Route::get('getInstance/{id_instance}', 'InstanceController@getInstance')->name('getInstance');
    Route::get('getAllInstances', 'InstanceController@getAllInstances')->name('getAllInstances');
    Route::get('getAllInstancesCSS', 'InstanceController@getAllInstancesCSS')->name('getAllInstancesCSS');
    Route::get('deleteInstance/{id_instance}', 'InstanceController@deleteInstance')->name('deleteInstance');
    Route::get('supprimerProfil/{id_role}', 'RolesController@supprimerProfil')->name('supprimerProfil');

    Route::resource('sessions', 'SessionController');
    Route::post('saveSession', 'SessionController@saveSession')->name('saveSession');
    Route::post('changeStatus/{id_session}/{value}', 'SessionController@changeStatus')->name('changeStatus');
    Route::get('sessionsSearch', 'SessionController@sessionsSearch')->name('sessionsSearch');
    Route::get('structuresWhereRecoNotPlan/{ses_id}', 'SessionController@structuresWhereRecoNotPlan')->name('structuresWhereRecoNotPlan');
    Route::post('joindreListParticipant', 'SessionController@joindreListParticipant')->name('joindreListParticipant');
    Route::get('getSession/{id_session}', 'SessionController@getSession')->name('getSession');
    Route::get('getAllSessions', 'SessionController@getAllSessions')->name('getAllSessions');
    Route::get('getAllSessionsEnFormulation', 'SessionController@getAllSessionsEnFormulation')->name('getAllSessionsEnFormulation');
    Route::get('deleteSession/{id_session}', 'SessionController@deleteSession')->name('deleteSession');
    Route::get('getRecosAreconduire/{ses_id_precedant}', 'SessionController@getRecosAreconduire')->name('getRecosAreconduire');
    Route::post('reconduireRecos', 'SessionController@reconduireRecos')->name('reconduireRecos');
    //    Route::get('reconduireRecos/{ses_id}/{ses_id_precedant}', 'SessionController@reconduireRecos')->name('reconduireRecos');

    
    Route::get('usersSearch', 'UsersController@usersSearch')->name('usersSearch');
    Route::get('deleteAgent/{id_agent}', 'AgentsController@deleteAgent')->name('deleteAgent');
    Route::get('getStructure/{id_structure}', 'StructuresController@getStructure')->name('getStructure');
    Route::get('getCategorie/{id_categorie}', 'CategoriesController@getCategorie')->name('getCategorie');
    Route::get('getAgent/{id_agent}', 'AgentsController@getAgent')->name('getAgent');
    Route::resource('templatemsgs', 'TemplatemsgsController');

    //routes for template
    Route::post('templatemsgs/save', 'TemplatemsgsController@save')->name('saveTemplate');
    Route::post('templatemsgs/updating/{id}', 'TemplatemsgsController@updating')->name('UpdatingTemplate');
    Route::get('templatemsgs/delete/{id}', 'TemplatemsgsController@delete')->name('deleteTemplate');
    Route::get('params/structure', 'TemplatemsgsController@listeStructure')->name('paramsStructure');
    Route::get('templatemsgs/detail/{id}', 'TemplatemsgsController@detailTemplate')->name('detailTemplate');
    Route::get('getTemplate/{id}', 'TemplatemsgsController@getTemplate')->name('getTemplate');
    //fin routes for template

    //debut route for alerts
    Route::get('instance/getSession/{id}', 'AlertesController@getSession')->name('getSession');
    Route::get('getModele', 'AlertesController@getModele')->name('getModele');
    Route::post('alertes/save', 'AlertesController@save')->name('SaveSession');
    Route::get('deleteAlerte/{id}', 'AlertesController@delete')->name('deleteAlerte');
    Route::get('getAlerte/{id}', 'AlertesController@getAlerte')->name('getAlerte');
    Route::post('alerte/updating/{id}', 'AlertesController@updating')->name('UpdateAlerte');
    Route::get('ConsulterMessage/{id}', 'AlertesController@ConsulterMessage')->name('ConsulterMessage');
    Route::get('filtreAlerte', 'AlertesController@filtreAlerteMessage')->name('filtreAlerte');
    Route::get('sendAlerte/{id}', 'EnvoiAlertesController@sendAlerte')->name('sendAlerte');
    Route::get('resendAlerte/{id}', 'EnvoiAlertesController@resendAlerte')->name('resendAlerte');
    Route::get('alertSearch', 'AlertesController@alertSearch')->name('alertSearch');
    Route::get('alerte/boiteReception', 'AlertesController@alertInbox')->name('alertInbox');
    Route::get('alerte/boiteReception/detail/{id}', 'AlertesController@infoAlerte')->name('alertInfo');
    Route::get('getMsg/{id}', 'AlertesController@getMsg')->name('getMsg');
    Route::get('alerte/listMessage/{id}', 'AlertesController@listEnvoi')->name('listMessage');
    Route::get('alertMessageSearch/', 'AlertesController@alertMessageSearch')->name('alertMessageSearch');
    //fin route for alerts

    //routes for envoiealertes
    Route::get('sendAlerte/{id}', 'EnvoiAlertesController@sendAlerte')->name('sendAlerte');
    Route::get('ListeEmailNotSend', 'EnvoiAlertesController@ListeEmailNotSend')->name('ListeEmailNotSend');
    Route::get('alerting', 'EnvoiAlertesController@alerting')->name('notifr'); //route pour tester seulement
    //
    Route::resource('alertes', 'AlertesController');
    Route::get('getPageAlertesSession/{id_session}', 'AlertesController@getPageAlertesSession')->name('getPageAlertesSession');

    Route::resource('participants', 'ParticipantsController');
    Route::get('getPageParticipantsSession/{id_session}', 'ParticipantsController@getPageParticipantsSession')->name('getPageParticipantsSession');

    Route::resource('recommandations', 'RecommandationsController');
    Route::get('getPageRecosBySession/{id_session}', 'RecommandationsController@getPageRecosBySession')->name('getPageRecosBySession');
    Route::get('getPageSaisieRecos', 'RecommandationsController@getPageSaisieRecos')->name('getPageSaisieRecos');
    Route::get('getPagePlaningRecos', 'RecommandationsController@getPagePlaningRecos', 'RecommandationsController@getPagePlaningRecos')->name('getPagePlaningRecos');
    Route::get('getPageSuiviRecos', 'RecommandationsController@getPageSuiviRecos')->name('getPageSuiviRecos');
    Route::get('getPageActiviteReco/{id_reco}', 'RecommandationsController@getPageActiviteReco')->name('getPageActiviteReco');
    Route::post('saveReco', 'RecommandationsController@saveReco')->name('saveReco');
    Route::post('modifierObservationReco', 'RecommandationsController@modifierObservationReco')->name('modifierObservationReco');
    Route::get('recosSearch', 'RecommandationsController@recosSearch')->name('recosSearch');
    Route::get('recosSearchForUserConnect', 'RecommandationsController@recosSearchForUserConnect')->name('recosSearchForUserConnect');
    Route::post('validerPlanningReco/{id_reco}', 'RecommandationsController@validerPlanningReco')->name('validerPlanningReco');
    Route::post('validerSuiviReco/{id_reco}', 'RecommandationsController@validerSuiviReco')->name('validerSuiviReco');
    Route::post('abandonnerReco/{id_reco}/{motif}', 'RecommandationsController@abandonnerReco')->name('abandonnerReco');
    Route::post('changerModeSuivi/{id_reco}/{value}', 'RecommandationsController@changerModeSuivi')->name('changerModeSuivi');
    Route::post('doSuiviReco/{id_reco}', 'RecommandationsController@doSuiviReco')->name('doSuiviReco');
    Route::get('getReco/{id_reco}', 'RecommandationsController@getReco')->name('getReco');
    Route::get('getAllRecos', 'Recommandat²ionsController@getAllRecos')->name('getAllRecos');
    Route::get('deleteReco/{id_reco}', 'RecommandationsController@deleteReco')->name('deleteReco');
    Route::get('deleteReco/{id_reco}', 'RecommandationsController@deleteReco')->name('deleteReco');
    Route::post('importRecos', 'RecommandationsController@importer')->name('importRecos');
    Route::get('downloadTemplateImport', 'RecommandationsController@downloadTemplateImport')->name('downloadTemplateImport');
    Route::get('getDataByInstanceByStructure/{ins_id}', 'RecommandationsController@getChartDataByInstanceByStructure')->name('getDataByInstanceByStructure');
    Route::get('getDataByInstance/{ins_id}', 'RecommandationsController@getChartDataByInstance')->name('getDataByInstance');
    Route::get('getDataAllInstanceByStructure', 'RecommandationsController@getChartDataAllInstanceByStructure')->name('getDataAllInstanceByStructure');
    Route::get('getDataAllInstance', 'RecommandationsController@getChartDataAllInstance')->name('getDataAllInstance');

    Route::resource('activites', 'ActivitesController');
    Route::post('saveActivite', 'ActivitesController@saveActivite')->name('saveActivite');
    Route::get('getActivite/{id_activite}', 'ActivitesController@getActivite')->name('getActivite');
    Route::get('deleteActivite', 'ActivitesController@deleteActivite')->name('deleteActivite');
    Route::get('deleteActivite/{id_partenaire}', 'ActivitesController@deleteActivite')->name('deleteActivite');
    Route::post('doSuiviActivite/{id_activite}', 'ActivitesController@doSuiviActivite')->name('doSuiviActivite');

    Route::resource('partenaires', 'PartenairesController');
    Route::get('getPagePartenaireReco/{id_reco}', 'PartenairesController@getPagePartenaireReco')->name('getPagePartenaireReco');
    Route::get('getListPartenairesByReco/{id_reco}', 'PartenairesController@getListPartenairesByReco')->name('getListPartenairesByReco');
    Route::post('savePartenaire', 'PartenairesController@savePartenaire')->name('savePartenaire');
    Route::get('getPartenaire/{id_partenaire}', 'PartenairesController@getPartenaire')->name('getPartenaire');
    Route::get('getAllPartenaires', 'PartenairesController@getAllPartenaires')->name('getAllPartenaires');
    Route::get('deletePartenaire/{id_partenaire}', 'PartenairesController@deletePartenaire')->name('deletePartenaire');
    Route::get('setResponsable/{id_partenaire}', 'PartenairesController@setResponsable')->name('setResponsable');

    Route::group(['middleware' => ['permission:CONSULTER_RAPPORT']], function () {
        Route::get('rapport', 'RapportController@index')->name('rapport.index');
        Route::post('rapportSearch', 'RapportController@rapportSearch')->name('rapportSearch')->middleware(['permission:CONSULTER_RAPPORT']);
        Route::get('getListPartenaires/{id_reco}', 'RapportController@getListPartenaires')->name('getListPartenaires');
        Route::get('getListActivites/{id_reco}', 'RapportController@getListActivites')->name('getListActivites');
        Route::get('getSessionsByInstance/{id_ins?}/{annee?}', 'RapportController@getSessionsByInstance')->name('getSessionsByInstance');
        Route::get('getActeursByInstance/{id_ins?}', 'RapportController@getActeursByInstance')->name('getActeursByInstance');
    });
    // routes des statistiques
    Route::group(['middleware' => ['permission:CONSULTER_STATS']], function () {
        Route::get('statistiques', 'StatistiquesController@index')->name('statistiques.index');
        Route::post('statistiqueSearch', 'StatistiquesController@statistiqueSearch')->name('statistiqueSearch');
        Route::get('getSessionsByInstance/{id_ins?}/{annee?}', 'RapportController@getSessionsByInstance')->name('getSessionsByInstance');
        Route::get('getActeursByInstance/{id_ins?}', 'RapportController@getActeursByInstance')->name('getActeursByInstance');
    });

    Route::resource('listeAlertes', 'ListeAlertesController');
    Route::resource('envoiAlertes', 'EnvoiAlertesController');

    Route::resource('programmeBudgetaires', 'ProgrammeBudgetairesController');
    Route::post('saveProgBug', 'ProgrammeBudgetairesController@saveProgBug')->name('saveProgBug');
    Route::get('getProgBug/{id_prog}', 'ProgrammeBudgetairesController@getProgBug')->name('getProgBug');
    Route::get('getAllProgBugs', 'ProgrammeBudgetairesController@getAllProgBugs')->name('getAllProgBugs');
    Route::get('deleteProgBug/{id_prog}', 'ProgrammeBudgetairesController@deleteProgBug')->name('deleteProgBug');
    Route::get('progBudgetaires/reload', 'ProgrammeBudgetairesController@reload')->name('progBudgetaires.reload');
    Route::get('getStructuresByInstance/{id}', 'StructuresController@getStructuresByInstance')->name('getStructuresByInstance');


});
