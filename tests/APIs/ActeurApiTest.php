<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Acteur;

class ActeurApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_acteur()
    {
        $acteur = factory(Acteur::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/acteurs', $acteur
        );

        $this->assertApiResponse($acteur);
    }

    /**
     * @test
     */
    public function test_read_acteur()
    {
        $acteur = factory(Acteur::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/acteurs/'.$acteur->act_id
        );

        $this->assertApiResponse($acteur->toArray());
    }

    /**
     * @test
     */
    public function test_update_acteur()
    {
        $acteur = factory(Acteur::class)->create();
        $editedActeur = factory(Acteur::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/acteurs/'.$acteur->act_id,
            $editedActeur
        );

        $this->assertApiResponse($editedActeur);
    }

    /**
     * @test
     */
    public function test_delete_acteur()
    {
        $acteur = factory(Acteur::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/acteurs/'.$acteur->act_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/acteurs/'.$acteur->act_id
        );

        $this->response->assertStatus(404);
    }
}
