<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Activites;

class ActivitesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_activites()
    {
        $activites = factory(Activites::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/activites', $activites
        );

        $this->assertApiResponse($activites);
    }

    /**
     * @test
     */
    public function test_read_activites()
    {
        $activites = factory(Activites::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/activites/'.$activites->act_id
        );

        $this->assertApiResponse($activites->toArray());
    }

    /**
     * @test
     */
    public function test_update_activites()
    {
        $activites = factory(Activites::class)->create();
        $editedActivites = factory(Activites::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/activites/'.$activites->act_id,
            $editedActivites
        );

        $this->assertApiResponse($editedActivites);
    }

    /**
     * @test
     */
    public function test_delete_activites()
    {
        $activites = factory(Activites::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/activites/'.$activites->act_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/activites/'.$activites->act_id
        );

        $this->response->assertStatus(404);
    }
}
