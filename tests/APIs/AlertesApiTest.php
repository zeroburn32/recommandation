<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Alertes;

class AlertesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_alertes()
    {
        $alertes = factory(Alertes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/alertes', $alertes
        );

        $this->assertApiResponse($alertes);
    }

    /**
     * @test
     */
    public function test_read_alertes()
    {
        $alertes = factory(Alertes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/alertes/'.$alertes->ale_id
        );

        $this->assertApiResponse($alertes->toArray());
    }

    /**
     * @test
     */
    public function test_update_alertes()
    {
        $alertes = factory(Alertes::class)->create();
        $editedAlertes = factory(Alertes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/alertes/'.$alertes->ale_id,
            $editedAlertes
        );

        $this->assertApiResponse($editedAlertes);
    }

    /**
     * @test
     */
    public function test_delete_alertes()
    {
        $alertes = factory(Alertes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/alertes/'.$alertes->ale_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/alertes/'.$alertes->ale_id
        );

        $this->response->assertStatus(404);
    }
}
