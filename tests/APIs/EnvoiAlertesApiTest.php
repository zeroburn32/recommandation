<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EnvoiAlertes;

class EnvoiAlertesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/envoi_alertes', $envoiAlertes
        );

        $this->assertApiResponse($envoiAlertes);
    }

    /**
     * @test
     */
    public function test_read_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/envoi_alertes/'.$envoiAlertes->ena_id
        );

        $this->assertApiResponse($envoiAlertes->toArray());
    }

    /**
     * @test
     */
    public function test_update_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->create();
        $editedEnvoiAlertes = factory(EnvoiAlertes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/envoi_alertes/'.$envoiAlertes->ena_id,
            $editedEnvoiAlertes
        );

        $this->assertApiResponse($editedEnvoiAlertes);
    }

    /**
     * @test
     */
    public function test_delete_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/envoi_alertes/'.$envoiAlertes->ena_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/envoi_alertes/'.$envoiAlertes->ena_id
        );

        $this->response->assertStatus(404);
    }
}
