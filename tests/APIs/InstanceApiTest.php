<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Instance;

class InstanceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_instance()
    {
        $instance = factory(Instance::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/instances', $instance
        );

        $this->assertApiResponse($instance);
    }

    /**
     * @test
     */
    public function test_read_instance()
    {
        $instance = factory(Instance::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/instances/'.$instance->ins_id
        );

        $this->assertApiResponse($instance->toArray());
    }

    /**
     * @test
     */
    public function test_update_instance()
    {
        $instance = factory(Instance::class)->create();
        $editedInstance = factory(Instance::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/instances/'.$instance->ins_id,
            $editedInstance
        );

        $this->assertApiResponse($editedInstance);
    }

    /**
     * @test
     */
    public function test_delete_instance()
    {
        $instance = factory(Instance::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/instances/'.$instance->ins_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/instances/'.$instance->ins_id
        );

        $this->response->assertStatus(404);
    }
}
