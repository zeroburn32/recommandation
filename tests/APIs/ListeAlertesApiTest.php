<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ListeAlertes;

class ListeAlertesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/liste_alertes', $listeAlertes
        );

        $this->assertApiResponse($listeAlertes);
    }

    /**
     * @test
     */
    public function test_read_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/liste_alertes/'.$listeAlertes->lia_id
        );

        $this->assertApiResponse($listeAlertes->toArray());
    }

    /**
     * @test
     */
    public function test_update_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->create();
        $editedListeAlertes = factory(ListeAlertes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/liste_alertes/'.$listeAlertes->lia_id,
            $editedListeAlertes
        );

        $this->assertApiResponse($editedListeAlertes);
    }

    /**
     * @test
     */
    public function test_delete_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/liste_alertes/'.$listeAlertes->lia_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/liste_alertes/'.$listeAlertes->lia_id
        );

        $this->response->assertStatus(404);
    }
}
