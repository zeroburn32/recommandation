<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ModelHasPermission;

class ModelHasPermissionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/model_has_permissions', $modelHasPermission
        );

        $this->assertApiResponse($modelHasPermission);
    }

    /**
     * @test
     */
    public function test_read_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/model_has_permissions/'.$modelHasPermission->id
        );

        $this->assertApiResponse($modelHasPermission->toArray());
    }

    /**
     * @test
     */
    public function test_update_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->create();
        $editedModelHasPermission = factory(ModelHasPermission::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/model_has_permissions/'.$modelHasPermission->id,
            $editedModelHasPermission
        );

        $this->assertApiResponse($editedModelHasPermission);
    }

    /**
     * @test
     */
    public function test_delete_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/model_has_permissions/'.$modelHasPermission->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/model_has_permissions/'.$modelHasPermission->id
        );

        $this->response->assertStatus(404);
    }
}
