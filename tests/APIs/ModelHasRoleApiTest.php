<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ModelHasRole;

class ModelHasRoleApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/model_has_roles', $modelHasRole
        );

        $this->assertApiResponse($modelHasRole);
    }

    /**
     * @test
     */
    public function test_read_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/model_has_roles/'.$modelHasRole->id
        );

        $this->assertApiResponse($modelHasRole->toArray());
    }

    /**
     * @test
     */
    public function test_update_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->create();
        $editedModelHasRole = factory(ModelHasRole::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/model_has_roles/'.$modelHasRole->id,
            $editedModelHasRole
        );

        $this->assertApiResponse($editedModelHasRole);
    }

    /**
     * @test
     */
    public function test_delete_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/model_has_roles/'.$modelHasRole->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/model_has_roles/'.$modelHasRole->id
        );

        $this->response->assertStatus(404);
    }
}
