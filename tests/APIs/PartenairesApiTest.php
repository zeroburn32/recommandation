<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Partenaires;

class PartenairesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_partenaires()
    {
        $partenaires = factory(Partenaires::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/partenaires', $partenaires
        );

        $this->assertApiResponse($partenaires);
    }

    /**
     * @test
     */
    public function test_read_partenaires()
    {
        $partenaires = factory(Partenaires::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/partenaires/'.$partenaires->ptn_id
        );

        $this->assertApiResponse($partenaires->toArray());
    }

    /**
     * @test
     */
    public function test_update_partenaires()
    {
        $partenaires = factory(Partenaires::class)->create();
        $editedPartenaires = factory(Partenaires::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/partenaires/'.$partenaires->ptn_id,
            $editedPartenaires
        );

        $this->assertApiResponse($editedPartenaires);
    }

    /**
     * @test
     */
    public function test_delete_partenaires()
    {
        $partenaires = factory(Partenaires::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/partenaires/'.$partenaires->ptn_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/partenaires/'.$partenaires->ptn_id
        );

        $this->response->assertStatus(404);
    }
}
