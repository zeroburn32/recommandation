<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Participants;

class ParticipantsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_participants()
    {
        $participants = factory(Participants::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/participants', $participants
        );

        $this->assertApiResponse($participants);
    }

    /**
     * @test
     */
    public function test_read_participants()
    {
        $participants = factory(Participants::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/participants/'.$participants->par_id
        );

        $this->assertApiResponse($participants->toArray());
    }

    /**
     * @test
     */
    public function test_update_participants()
    {
        $participants = factory(Participants::class)->create();
        $editedParticipants = factory(Participants::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/participants/'.$participants->par_id,
            $editedParticipants
        );

        $this->assertApiResponse($editedParticipants);
    }

    /**
     * @test
     */
    public function test_delete_participants()
    {
        $participants = factory(Participants::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/participants/'.$participants->par_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/participants/'.$participants->par_id
        );

        $this->response->assertStatus(404);
    }
}
