<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProgrammeBudgetaires;

class ProgrammeBudgetairesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/programme_budgetaires', $programmeBudgetaires
        );

        $this->assertApiResponse($programmeBudgetaires);
    }

    /**
     * @test
     */
    public function test_read_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/programme_budgetaires/'.$programmeBudgetaires->bud_id
        );

        $this->assertApiResponse($programmeBudgetaires->toArray());
    }

    /**
     * @test
     */
    public function test_update_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->create();
        $editedProgrammeBudgetaires = factory(ProgrammeBudgetaires::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/programme_budgetaires/'.$programmeBudgetaires->bud_id,
            $editedProgrammeBudgetaires
        );

        $this->assertApiResponse($editedProgrammeBudgetaires);
    }

    /**
     * @test
     */
    public function test_delete_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/programme_budgetaires/'.$programmeBudgetaires->bud_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/programme_budgetaires/'.$programmeBudgetaires->bud_id
        );

        $this->response->assertStatus(404);
    }
}
