<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProgrammePbudgetaires;

class ProgrammePbudgetairesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/programme_pbudgetaires', $programmePbudgetaires
        );

        $this->assertApiResponse($programmePbudgetaires);
    }

    /**
     * @test
     */
    public function test_read_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/programme_pbudgetaires/'.$programmePbudgetaires->bud_id
        );

        $this->assertApiResponse($programmePbudgetaires->toArray());
    }

    /**
     * @test
     */
    public function test_update_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->create();
        $editedProgrammePbudgetaires = factory(ProgrammePbudgetaires::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/programme_pbudgetaires/'.$programmePbudgetaires->bud_id,
            $editedProgrammePbudgetaires
        );

        $this->assertApiResponse($editedProgrammePbudgetaires);
    }

    /**
     * @test
     */
    public function test_delete_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/programme_pbudgetaires/'.$programmePbudgetaires->bud_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/programme_pbudgetaires/'.$programmePbudgetaires->bud_id
        );

        $this->response->assertStatus(404);
    }
}
