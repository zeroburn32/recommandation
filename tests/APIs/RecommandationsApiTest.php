<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Recommandations;

class RecommandationsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_recommandations()
    {
        $recommandations = factory(Recommandations::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/recommandations', $recommandations
        );

        $this->assertApiResponse($recommandations);
    }

    /**
     * @test
     */
    public function test_read_recommandations()
    {
        $recommandations = factory(Recommandations::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/recommandations/'.$recommandations->rec_id
        );

        $this->assertApiResponse($recommandations->toArray());
    }

    /**
     * @test
     */
    public function test_update_recommandations()
    {
        $recommandations = factory(Recommandations::class)->create();
        $editedRecommandations = factory(Recommandations::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/recommandations/'.$recommandations->rec_id,
            $editedRecommandations
        );

        $this->assertApiResponse($editedRecommandations);
    }

    /**
     * @test
     */
    public function test_delete_recommandations()
    {
        $recommandations = factory(Recommandations::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/recommandations/'.$recommandations->rec_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/recommandations/'.$recommandations->rec_id
        );

        $this->response->assertStatus(404);
    }
}
