<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Templatemsgs;

class TemplatemsgsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/templatemsgs', $templatemsgs
        );

        $this->assertApiResponse($templatemsgs);
    }

    /**
     * @test
     */
    public function test_read_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/templatemsgs/'.$templatemsgs->tmsg_id
        );

        $this->assertApiResponse($templatemsgs->toArray());
    }

    /**
     * @test
     */
    public function test_update_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->create();
        $editedTemplatemsgs = factory(Templatemsgs::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/templatemsgs/'.$templatemsgs->tmsg_id,
            $editedTemplatemsgs
        );

        $this->assertApiResponse($editedTemplatemsgs);
    }

    /**
     * @test
     */
    public function test_delete_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/templatemsgs/'.$templatemsgs->tmsg_id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/templatemsgs/'.$templatemsgs->tmsg_id
        );

        $this->response->assertStatus(404);
    }
}
