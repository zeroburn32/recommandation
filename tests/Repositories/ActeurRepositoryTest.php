<?php namespace Tests\Repositories;

use App\Models\Acteur;
use App\Repositories\ActeurRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ActeurRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActeurRepository
     */
    protected $acteurRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->acteurRepo = \App::make(ActeurRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_acteur()
    {
        $acteur = factory(Acteur::class)->make()->toArray();

        $createdActeur = $this->acteurRepo->create($acteur);

        $createdActeur = $createdActeur->toArray();
        $this->assertArrayHasKey('id', $createdActeur);
        $this->assertNotNull($createdActeur['id'], 'Created Acteur must have id specified');
        $this->assertNotNull(Acteur::find($createdActeur['id']), 'Acteur with given id must be in DB');
        $this->assertModelData($acteur, $createdActeur);
    }

    /**
     * @test read
     */
    public function test_read_acteur()
    {
        $acteur = factory(Acteur::class)->create();

        $dbActeur = $this->acteurRepo->find($acteur->act_id);

        $dbActeur = $dbActeur->toArray();
        $this->assertModelData($acteur->toArray(), $dbActeur);
    }

    /**
     * @test update
     */
    public function test_update_acteur()
    {
        $acteur = factory(Acteur::class)->create();
        $fakeActeur = factory(Acteur::class)->make()->toArray();

        $updatedActeur = $this->acteurRepo->update($fakeActeur, $acteur->act_id);

        $this->assertModelData($fakeActeur, $updatedActeur->toArray());
        $dbActeur = $this->acteurRepo->find($acteur->act_id);
        $this->assertModelData($fakeActeur, $dbActeur->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_acteur()
    {
        $acteur = factory(Acteur::class)->create();

        $resp = $this->acteurRepo->delete($acteur->act_id);

        $this->assertTrue($resp);
        $this->assertNull(Acteur::find($acteur->act_id), 'Acteur should not exist in DB');
    }
}
