<?php namespace Tests\Repositories;

use App\Models\Activites;
use App\Repositories\ActivitesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ActivitesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActivitesRepository
     */
    protected $activitesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->activitesRepo = \App::make(ActivitesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_activites()
    {
        $activites = factory(Activites::class)->make()->toArray();

        $createdActivites = $this->activitesRepo->create($activites);

        $createdActivites = $createdActivites->toArray();
        $this->assertArrayHasKey('id', $createdActivites);
        $this->assertNotNull($createdActivites['id'], 'Created Activites must have id specified');
        $this->assertNotNull(Activites::find($createdActivites['id']), 'Activites with given id must be in DB');
        $this->assertModelData($activites, $createdActivites);
    }

    /**
     * @test read
     */
    public function test_read_activites()
    {
        $activites = factory(Activites::class)->create();

        $dbActivites = $this->activitesRepo->find($activites->act_id);

        $dbActivites = $dbActivites->toArray();
        $this->assertModelData($activites->toArray(), $dbActivites);
    }

    /**
     * @test update
     */
    public function test_update_activites()
    {
        $activites = factory(Activites::class)->create();
        $fakeActivites = factory(Activites::class)->make()->toArray();

        $updatedActivites = $this->activitesRepo->update($fakeActivites, $activites->act_id);

        $this->assertModelData($fakeActivites, $updatedActivites->toArray());
        $dbActivites = $this->activitesRepo->find($activites->act_id);
        $this->assertModelData($fakeActivites, $dbActivites->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_activites()
    {
        $activites = factory(Activites::class)->create();

        $resp = $this->activitesRepo->delete($activites->act_id);

        $this->assertTrue($resp);
        $this->assertNull(Activites::find($activites->act_id), 'Activites should not exist in DB');
    }
}
