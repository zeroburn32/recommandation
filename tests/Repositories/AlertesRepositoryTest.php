<?php namespace Tests\Repositories;

use App\Models\Alertes;
use App\Repositories\AlertesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AlertesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AlertesRepository
     */
    protected $alertesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->alertesRepo = \App::make(AlertesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_alertes()
    {
        $alertes = factory(Alertes::class)->make()->toArray();

        $createdAlertes = $this->alertesRepo->create($alertes);

        $createdAlertes = $createdAlertes->toArray();
        $this->assertArrayHasKey('id', $createdAlertes);
        $this->assertNotNull($createdAlertes['id'], 'Created Alertes must have id specified');
        $this->assertNotNull(Alertes::find($createdAlertes['id']), 'Alertes with given id must be in DB');
        $this->assertModelData($alertes, $createdAlertes);
    }

    /**
     * @test read
     */
    public function test_read_alertes()
    {
        $alertes = factory(Alertes::class)->create();

        $dbAlertes = $this->alertesRepo->find($alertes->ale_id);

        $dbAlertes = $dbAlertes->toArray();
        $this->assertModelData($alertes->toArray(), $dbAlertes);
    }

    /**
     * @test update
     */
    public function test_update_alertes()
    {
        $alertes = factory(Alertes::class)->create();
        $fakeAlertes = factory(Alertes::class)->make()->toArray();

        $updatedAlertes = $this->alertesRepo->update($fakeAlertes, $alertes->ale_id);

        $this->assertModelData($fakeAlertes, $updatedAlertes->toArray());
        $dbAlertes = $this->alertesRepo->find($alertes->ale_id);
        $this->assertModelData($fakeAlertes, $dbAlertes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_alertes()
    {
        $alertes = factory(Alertes::class)->create();

        $resp = $this->alertesRepo->delete($alertes->ale_id);

        $this->assertTrue($resp);
        $this->assertNull(Alertes::find($alertes->ale_id), 'Alertes should not exist in DB');
    }
}
