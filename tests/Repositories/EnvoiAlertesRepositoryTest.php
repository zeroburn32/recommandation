<?php namespace Tests\Repositories;

use App\Models\EnvoiAlertes;
use App\Repositories\EnvoiAlertesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EnvoiAlertesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EnvoiAlertesRepository
     */
    protected $envoiAlertesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->envoiAlertesRepo = \App::make(EnvoiAlertesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->make()->toArray();

        $createdEnvoiAlertes = $this->envoiAlertesRepo->create($envoiAlertes);

        $createdEnvoiAlertes = $createdEnvoiAlertes->toArray();
        $this->assertArrayHasKey('id', $createdEnvoiAlertes);
        $this->assertNotNull($createdEnvoiAlertes['id'], 'Created EnvoiAlertes must have id specified');
        $this->assertNotNull(EnvoiAlertes::find($createdEnvoiAlertes['id']), 'EnvoiAlertes with given id must be in DB');
        $this->assertModelData($envoiAlertes, $createdEnvoiAlertes);
    }

    /**
     * @test read
     */
    public function test_read_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->create();

        $dbEnvoiAlertes = $this->envoiAlertesRepo->find($envoiAlertes->ena_id);

        $dbEnvoiAlertes = $dbEnvoiAlertes->toArray();
        $this->assertModelData($envoiAlertes->toArray(), $dbEnvoiAlertes);
    }

    /**
     * @test update
     */
    public function test_update_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->create();
        $fakeEnvoiAlertes = factory(EnvoiAlertes::class)->make()->toArray();

        $updatedEnvoiAlertes = $this->envoiAlertesRepo->update($fakeEnvoiAlertes, $envoiAlertes->ena_id);

        $this->assertModelData($fakeEnvoiAlertes, $updatedEnvoiAlertes->toArray());
        $dbEnvoiAlertes = $this->envoiAlertesRepo->find($envoiAlertes->ena_id);
        $this->assertModelData($fakeEnvoiAlertes, $dbEnvoiAlertes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_envoi_alertes()
    {
        $envoiAlertes = factory(EnvoiAlertes::class)->create();

        $resp = $this->envoiAlertesRepo->delete($envoiAlertes->ena_id);

        $this->assertTrue($resp);
        $this->assertNull(EnvoiAlertes::find($envoiAlertes->ena_id), 'EnvoiAlertes should not exist in DB');
    }
}
