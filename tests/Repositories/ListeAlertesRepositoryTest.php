<?php namespace Tests\Repositories;

use App\Models\ListeAlertes;
use App\Repositories\ListeAlertesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ListeAlertesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ListeAlertesRepository
     */
    protected $listeAlertesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->listeAlertesRepo = \App::make(ListeAlertesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->make()->toArray();

        $createdListeAlertes = $this->listeAlertesRepo->create($listeAlertes);

        $createdListeAlertes = $createdListeAlertes->toArray();
        $this->assertArrayHasKey('id', $createdListeAlertes);
        $this->assertNotNull($createdListeAlertes['id'], 'Created ListeAlertes must have id specified');
        $this->assertNotNull(ListeAlertes::find($createdListeAlertes['id']), 'ListeAlertes with given id must be in DB');
        $this->assertModelData($listeAlertes, $createdListeAlertes);
    }

    /**
     * @test read
     */
    public function test_read_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->create();

        $dbListeAlertes = $this->listeAlertesRepo->find($listeAlertes->lia_id);

        $dbListeAlertes = $dbListeAlertes->toArray();
        $this->assertModelData($listeAlertes->toArray(), $dbListeAlertes);
    }

    /**
     * @test update
     */
    public function test_update_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->create();
        $fakeListeAlertes = factory(ListeAlertes::class)->make()->toArray();

        $updatedListeAlertes = $this->listeAlertesRepo->update($fakeListeAlertes, $listeAlertes->lia_id);

        $this->assertModelData($fakeListeAlertes, $updatedListeAlertes->toArray());
        $dbListeAlertes = $this->listeAlertesRepo->find($listeAlertes->lia_id);
        $this->assertModelData($fakeListeAlertes, $dbListeAlertes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_liste_alertes()
    {
        $listeAlertes = factory(ListeAlertes::class)->create();

        $resp = $this->listeAlertesRepo->delete($listeAlertes->lia_id);

        $this->assertTrue($resp);
        $this->assertNull(ListeAlertes::find($listeAlertes->lia_id), 'ListeAlertes should not exist in DB');
    }
}
