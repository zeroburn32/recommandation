<?php namespace Tests\Repositories;

use App\Models\ModelHasPermission;
use App\Repositories\ModelHasPermissionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ModelHasPermissionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ModelHasPermissionRepository
     */
    protected $modelHasPermissionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->modelHasPermissionRepo = \App::make(ModelHasPermissionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->make()->toArray();

        $createdModelHasPermission = $this->modelHasPermissionRepo->create($modelHasPermission);

        $createdModelHasPermission = $createdModelHasPermission->toArray();
        $this->assertArrayHasKey('id', $createdModelHasPermission);
        $this->assertNotNull($createdModelHasPermission['id'], 'Created ModelHasPermission must have id specified');
        $this->assertNotNull(ModelHasPermission::find($createdModelHasPermission['id']), 'ModelHasPermission with given id must be in DB');
        $this->assertModelData($modelHasPermission, $createdModelHasPermission);
    }

    /**
     * @test read
     */
    public function test_read_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->create();

        $dbModelHasPermission = $this->modelHasPermissionRepo->find($modelHasPermission->id);

        $dbModelHasPermission = $dbModelHasPermission->toArray();
        $this->assertModelData($modelHasPermission->toArray(), $dbModelHasPermission);
    }

    /**
     * @test update
     */
    public function test_update_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->create();
        $fakeModelHasPermission = factory(ModelHasPermission::class)->make()->toArray();

        $updatedModelHasPermission = $this->modelHasPermissionRepo->update($fakeModelHasPermission, $modelHasPermission->id);

        $this->assertModelData($fakeModelHasPermission, $updatedModelHasPermission->toArray());
        $dbModelHasPermission = $this->modelHasPermissionRepo->find($modelHasPermission->id);
        $this->assertModelData($fakeModelHasPermission, $dbModelHasPermission->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_model_has_permission()
    {
        $modelHasPermission = factory(ModelHasPermission::class)->create();

        $resp = $this->modelHasPermissionRepo->delete($modelHasPermission->id);

        $this->assertTrue($resp);
        $this->assertNull(ModelHasPermission::find($modelHasPermission->id), 'ModelHasPermission should not exist in DB');
    }
}
