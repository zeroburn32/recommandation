<?php namespace Tests\Repositories;

use App\Models\ModelHasRole;
use App\Repositories\ModelHasRoleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ModelHasRoleRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ModelHasRoleRepository
     */
    protected $modelHasRoleRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->modelHasRoleRepo = \App::make(ModelHasRoleRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->make()->toArray();

        $createdModelHasRole = $this->modelHasRoleRepo->create($modelHasRole);

        $createdModelHasRole = $createdModelHasRole->toArray();
        $this->assertArrayHasKey('id', $createdModelHasRole);
        $this->assertNotNull($createdModelHasRole['id'], 'Created ModelHasRole must have id specified');
        $this->assertNotNull(ModelHasRole::find($createdModelHasRole['id']), 'ModelHasRole with given id must be in DB');
        $this->assertModelData($modelHasRole, $createdModelHasRole);
    }

    /**
     * @test read
     */
    public function test_read_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->create();

        $dbModelHasRole = $this->modelHasRoleRepo->find($modelHasRole->id);

        $dbModelHasRole = $dbModelHasRole->toArray();
        $this->assertModelData($modelHasRole->toArray(), $dbModelHasRole);
    }

    /**
     * @test update
     */
    public function test_update_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->create();
        $fakeModelHasRole = factory(ModelHasRole::class)->make()->toArray();

        $updatedModelHasRole = $this->modelHasRoleRepo->update($fakeModelHasRole, $modelHasRole->id);

        $this->assertModelData($fakeModelHasRole, $updatedModelHasRole->toArray());
        $dbModelHasRole = $this->modelHasRoleRepo->find($modelHasRole->id);
        $this->assertModelData($fakeModelHasRole, $dbModelHasRole->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_model_has_role()
    {
        $modelHasRole = factory(ModelHasRole::class)->create();

        $resp = $this->modelHasRoleRepo->delete($modelHasRole->id);

        $this->assertTrue($resp);
        $this->assertNull(ModelHasRole::find($modelHasRole->id), 'ModelHasRole should not exist in DB');
    }
}
