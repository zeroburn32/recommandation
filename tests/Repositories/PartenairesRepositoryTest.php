<?php namespace Tests\Repositories;

use App\Models\Partenaires;
use App\Repositories\PartenairesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PartenairesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartenairesRepository
     */
    protected $partenairesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->partenairesRepo = \App::make(PartenairesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_partenaires()
    {
        $partenaires = factory(Partenaires::class)->make()->toArray();

        $createdPartenaires = $this->partenairesRepo->create($partenaires);

        $createdPartenaires = $createdPartenaires->toArray();
        $this->assertArrayHasKey('id', $createdPartenaires);
        $this->assertNotNull($createdPartenaires['id'], 'Created Partenaires must have id specified');
        $this->assertNotNull(Partenaires::find($createdPartenaires['id']), 'Partenaires with given id must be in DB');
        $this->assertModelData($partenaires, $createdPartenaires);
    }

    /**
     * @test read
     */
    public function test_read_partenaires()
    {
        $partenaires = factory(Partenaires::class)->create();

        $dbPartenaires = $this->partenairesRepo->find($partenaires->ptn_id);

        $dbPartenaires = $dbPartenaires->toArray();
        $this->assertModelData($partenaires->toArray(), $dbPartenaires);
    }

    /**
     * @test update
     */
    public function test_update_partenaires()
    {
        $partenaires = factory(Partenaires::class)->create();
        $fakePartenaires = factory(Partenaires::class)->make()->toArray();

        $updatedPartenaires = $this->partenairesRepo->update($fakePartenaires, $partenaires->ptn_id);

        $this->assertModelData($fakePartenaires, $updatedPartenaires->toArray());
        $dbPartenaires = $this->partenairesRepo->find($partenaires->ptn_id);
        $this->assertModelData($fakePartenaires, $dbPartenaires->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_partenaires()
    {
        $partenaires = factory(Partenaires::class)->create();

        $resp = $this->partenairesRepo->delete($partenaires->ptn_id);

        $this->assertTrue($resp);
        $this->assertNull(Partenaires::find($partenaires->ptn_id), 'Partenaires should not exist in DB');
    }
}
