<?php namespace Tests\Repositories;

use App\Models\Participants;
use App\Repositories\ParticipantsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ParticipantsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ParticipantsRepository
     */
    protected $participantsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->participantsRepo = \App::make(ParticipantsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_participants()
    {
        $participants = factory(Participants::class)->make()->toArray();

        $createdParticipants = $this->participantsRepo->create($participants);

        $createdParticipants = $createdParticipants->toArray();
        $this->assertArrayHasKey('id', $createdParticipants);
        $this->assertNotNull($createdParticipants['id'], 'Created Participants must have id specified');
        $this->assertNotNull(Participants::find($createdParticipants['id']), 'Participants with given id must be in DB');
        $this->assertModelData($participants, $createdParticipants);
    }

    /**
     * @test read
     */
    public function test_read_participants()
    {
        $participants = factory(Participants::class)->create();

        $dbParticipants = $this->participantsRepo->find($participants->par_id);

        $dbParticipants = $dbParticipants->toArray();
        $this->assertModelData($participants->toArray(), $dbParticipants);
    }

    /**
     * @test update
     */
    public function test_update_participants()
    {
        $participants = factory(Participants::class)->create();
        $fakeParticipants = factory(Participants::class)->make()->toArray();

        $updatedParticipants = $this->participantsRepo->update($fakeParticipants, $participants->par_id);

        $this->assertModelData($fakeParticipants, $updatedParticipants->toArray());
        $dbParticipants = $this->participantsRepo->find($participants->par_id);
        $this->assertModelData($fakeParticipants, $dbParticipants->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_participants()
    {
        $participants = factory(Participants::class)->create();

        $resp = $this->participantsRepo->delete($participants->par_id);

        $this->assertTrue($resp);
        $this->assertNull(Participants::find($participants->par_id), 'Participants should not exist in DB');
    }
}
