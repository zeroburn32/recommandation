<?php namespace Tests\Repositories;

use App\Models\ProgrammeBudgetaires;
use App\Repositories\ProgrammeBudgetairesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProgrammeBudgetairesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProgrammeBudgetairesRepository
     */
    protected $programmeBudgetairesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->programmeBudgetairesRepo = \App::make(ProgrammeBudgetairesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->make()->toArray();

        $createdProgrammeBudgetaires = $this->programmeBudgetairesRepo->create($programmeBudgetaires);

        $createdProgrammeBudgetaires = $createdProgrammeBudgetaires->toArray();
        $this->assertArrayHasKey('id', $createdProgrammeBudgetaires);
        $this->assertNotNull($createdProgrammeBudgetaires['id'], 'Created ProgrammeBudgetaires must have id specified');
        $this->assertNotNull(ProgrammeBudgetaires::find($createdProgrammeBudgetaires['id']), 'ProgrammeBudgetaires with given id must be in DB');
        $this->assertModelData($programmeBudgetaires, $createdProgrammeBudgetaires);
    }

    /**
     * @test read
     */
    public function test_read_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->create();

        $dbProgrammeBudgetaires = $this->programmeBudgetairesRepo->find($programmeBudgetaires->bud_id);

        $dbProgrammeBudgetaires = $dbProgrammeBudgetaires->toArray();
        $this->assertModelData($programmeBudgetaires->toArray(), $dbProgrammeBudgetaires);
    }

    /**
     * @test update
     */
    public function test_update_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->create();
        $fakeProgrammeBudgetaires = factory(ProgrammeBudgetaires::class)->make()->toArray();

        $updatedProgrammeBudgetaires = $this->programmeBudgetairesRepo->update($fakeProgrammeBudgetaires, $programmeBudgetaires->bud_id);

        $this->assertModelData($fakeProgrammeBudgetaires, $updatedProgrammeBudgetaires->toArray());
        $dbProgrammeBudgetaires = $this->programmeBudgetairesRepo->find($programmeBudgetaires->bud_id);
        $this->assertModelData($fakeProgrammeBudgetaires, $dbProgrammeBudgetaires->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_programme_budgetaires()
    {
        $programmeBudgetaires = factory(ProgrammeBudgetaires::class)->create();

        $resp = $this->programmeBudgetairesRepo->delete($programmeBudgetaires->bud_id);

        $this->assertTrue($resp);
        $this->assertNull(ProgrammeBudgetaires::find($programmeBudgetaires->bud_id), 'ProgrammeBudgetaires should not exist in DB');
    }
}
