<?php namespace Tests\Repositories;

use App\Models\ProgrammePbudgetaires;
use App\Repositories\ProgrammePbudgetairesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProgrammePbudgetairesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProgrammePbudgetairesRepository
     */
    protected $programmePbudgetairesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->programmePbudgetairesRepo = \App::make(ProgrammePbudgetairesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->make()->toArray();

        $createdProgrammePbudgetaires = $this->programmePbudgetairesRepo->create($programmePbudgetaires);

        $createdProgrammePbudgetaires = $createdProgrammePbudgetaires->toArray();
        $this->assertArrayHasKey('id', $createdProgrammePbudgetaires);
        $this->assertNotNull($createdProgrammePbudgetaires['id'], 'Created ProgrammePbudgetaires must have id specified');
        $this->assertNotNull(ProgrammePbudgetaires::find($createdProgrammePbudgetaires['id']), 'ProgrammePbudgetaires with given id must be in DB');
        $this->assertModelData($programmePbudgetaires, $createdProgrammePbudgetaires);
    }

    /**
     * @test read
     */
    public function test_read_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->create();

        $dbProgrammePbudgetaires = $this->programmePbudgetairesRepo->find($programmePbudgetaires->bud_id);

        $dbProgrammePbudgetaires = $dbProgrammePbudgetaires->toArray();
        $this->assertModelData($programmePbudgetaires->toArray(), $dbProgrammePbudgetaires);
    }

    /**
     * @test update
     */
    public function test_update_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->create();
        $fakeProgrammePbudgetaires = factory(ProgrammePbudgetaires::class)->make()->toArray();

        $updatedProgrammePbudgetaires = $this->programmePbudgetairesRepo->update($fakeProgrammePbudgetaires, $programmePbudgetaires->bud_id);

        $this->assertModelData($fakeProgrammePbudgetaires, $updatedProgrammePbudgetaires->toArray());
        $dbProgrammePbudgetaires = $this->programmePbudgetairesRepo->find($programmePbudgetaires->bud_id);
        $this->assertModelData($fakeProgrammePbudgetaires, $dbProgrammePbudgetaires->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_programme_pbudgetaires()
    {
        $programmePbudgetaires = factory(ProgrammePbudgetaires::class)->create();

        $resp = $this->programmePbudgetairesRepo->delete($programmePbudgetaires->bud_id);

        $this->assertTrue($resp);
        $this->assertNull(ProgrammePbudgetaires::find($programmePbudgetaires->bud_id), 'ProgrammePbudgetaires should not exist in DB');
    }
}
