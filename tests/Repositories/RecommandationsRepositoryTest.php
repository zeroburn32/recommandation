<?php namespace Tests\Repositories;

use App\Models\Recommandations;
use App\Repositories\RecommandationsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RecommandationsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RecommandationsRepository
     */
    protected $recommandationsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->recommandationsRepo = \App::make(RecommandationsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_recommandations()
    {
        $recommandations = factory(Recommandations::class)->make()->toArray();

        $createdRecommandations = $this->recommandationsRepo->create($recommandations);

        $createdRecommandations = $createdRecommandations->toArray();
        $this->assertArrayHasKey('id', $createdRecommandations);
        $this->assertNotNull($createdRecommandations['id'], 'Created Recommandations must have id specified');
        $this->assertNotNull(Recommandations::find($createdRecommandations['id']), 'Recommandations with given id must be in DB');
        $this->assertModelData($recommandations, $createdRecommandations);
    }

    /**
     * @test read
     */
    public function test_read_recommandations()
    {
        $recommandations = factory(Recommandations::class)->create();

        $dbRecommandations = $this->recommandationsRepo->find($recommandations->rec_id);

        $dbRecommandations = $dbRecommandations->toArray();
        $this->assertModelData($recommandations->toArray(), $dbRecommandations);
    }

    /**
     * @test update
     */
    public function test_update_recommandations()
    {
        $recommandations = factory(Recommandations::class)->create();
        $fakeRecommandations = factory(Recommandations::class)->make()->toArray();

        $updatedRecommandations = $this->recommandationsRepo->update($fakeRecommandations, $recommandations->rec_id);

        $this->assertModelData($fakeRecommandations, $updatedRecommandations->toArray());
        $dbRecommandations = $this->recommandationsRepo->find($recommandations->rec_id);
        $this->assertModelData($fakeRecommandations, $dbRecommandations->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_recommandations()
    {
        $recommandations = factory(Recommandations::class)->create();

        $resp = $this->recommandationsRepo->delete($recommandations->rec_id);

        $this->assertTrue($resp);
        $this->assertNull(Recommandations::find($recommandations->rec_id), 'Recommandations should not exist in DB');
    }
}
