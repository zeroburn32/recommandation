<?php namespace Tests\Repositories;

use App\Models\Templatemsgs;
use App\Repositories\TemplatemsgsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TemplatemsgsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TemplatemsgsRepository
     */
    protected $templatemsgsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->templatemsgsRepo = \App::make(TemplatemsgsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->make()->toArray();

        $createdTemplatemsgs = $this->templatemsgsRepo->create($templatemsgs);

        $createdTemplatemsgs = $createdTemplatemsgs->toArray();
        $this->assertArrayHasKey('id', $createdTemplatemsgs);
        $this->assertNotNull($createdTemplatemsgs['id'], 'Created Templatemsgs must have id specified');
        $this->assertNotNull(Templatemsgs::find($createdTemplatemsgs['id']), 'Templatemsgs with given id must be in DB');
        $this->assertModelData($templatemsgs, $createdTemplatemsgs);
    }

    /**
     * @test read
     */
    public function test_read_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->create();

        $dbTemplatemsgs = $this->templatemsgsRepo->find($templatemsgs->tmsg_id);

        $dbTemplatemsgs = $dbTemplatemsgs->toArray();
        $this->assertModelData($templatemsgs->toArray(), $dbTemplatemsgs);
    }

    /**
     * @test update
     */
    public function test_update_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->create();
        $fakeTemplatemsgs = factory(Templatemsgs::class)->make()->toArray();

        $updatedTemplatemsgs = $this->templatemsgsRepo->update($fakeTemplatemsgs, $templatemsgs->tmsg_id);

        $this->assertModelData($fakeTemplatemsgs, $updatedTemplatemsgs->toArray());
        $dbTemplatemsgs = $this->templatemsgsRepo->find($templatemsgs->tmsg_id);
        $this->assertModelData($fakeTemplatemsgs, $dbTemplatemsgs->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_templatemsgs()
    {
        $templatemsgs = factory(Templatemsgs::class)->create();

        $resp = $this->templatemsgsRepo->delete($templatemsgs->tmsg_id);

        $this->assertTrue($resp);
        $this->assertNull(Templatemsgs::find($templatemsgs->tmsg_id), 'Templatemsgs should not exist in DB');
    }
}
